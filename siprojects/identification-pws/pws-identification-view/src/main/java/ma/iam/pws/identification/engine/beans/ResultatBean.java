 package ma.iam.pws.identification.engine.beans;
 
 import java.io.Serializable;
 
 
 
 
 
 
 
 
 
 
 
 public class ResultatBean
   implements Serializable
 {
   private static final long serialVersionUID = -2446717644187305012L;
   public static final String ALIAS = "resultat";
/* 20 */   private int codeResultat = 4;
   
 
 
   private String messageResultat;
   
 
 
   public ResultatBean(String messageResultat)
   {
/* 30 */     this.messageResultat = messageResultat;
   }
   
 
 
 
 
 
 
 
 
   public ResultatBean(int codeResultat, String messageResultat)
   {
/* 43 */     this.codeResultat = codeResultat;
/* 44 */     this.messageResultat = messageResultat;
   }
   
 
 
 
 
   public int getCodeResultat()
   {
/* 53 */     return this.codeResultat;
   }
   
 
 
 
 
   public String getMessageResultat()
   {
/* 62 */     return this.messageResultat;
   }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\engine\beans\ResultatBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */