/*     */ package ma.iam.pws.identification.domaine.impl;
/*     */ 
/*     */ import ma.iam.pws.identification.beans.administration.ProfileEligibilityBean;
import ma.iam.pws.identification.beans.contract.ContractBean;
import ma.iam.pws.identification.beans.services.ServiceBean;
import ma.iam.pws.identification.dao.InfoContratsDAO;
import ma.iam.pws.identification.dao.impl.InfoNIGDAOImpl;
import ma.iam.pws.identification.domaine.GetProfileEligibilityDomain;
import ma.iam.pws.identification.exception.FaultWS;

/*     */ import org.apache.commons.logging.Log;
/*     */ import org.apache.commons.logging.LogFactory;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.stereotype.Service;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Service
/*     */ public class GetProfileEligibilityDomainImpl
/*     */   implements GetProfileEligibilityDomain
/*     */ {
/*     */   @Autowired
/*     */   private InfoContratsDAO infoContratsDAO;
/*  34 */   private static Log log = LogFactory.getLog(InfoNIGDAOImpl.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ProfileEligibilityBean getProfileEligibility(String msisdn)
/*     */     throws FaultWS
/*     */   {
/*  43 */     ProfileEligibilityBean profileBean = new ProfileEligibilityBean();
/*     */     try {
/*  45 */       ContractBean contractBean = this.infoContratsDAO.getContractByMsisdn(msisdn);
/*  46 */       if (contractBean != null) {
/*  47 */         if (",1,12,15,21,30,35,44,28,37,20,40,8,16,34,".contains(",".concat(Long.toString(contractBean.getTmCode()).concat(",")))) {
/*  48 */           profileBean.setEligibility(getEligibility(contractBean, "INT3G").longValue());
/*  49 */           profileBean.setProfile(1L);
/*  50 */         } else if (",27,26,".contains(",".concat(Long.toString(contractBean.getTmCode()).concat(",")))) {
/*  51 */           profileBean.setEligibility(getEligibility(contractBean, "IND3G").longValue());
/*  52 */           profileBean.setProfile(0L);
/*     */         } else {
/*  54 */           profileBean.setEligibility(-1L);
/*  55 */           profileBean.setProfile(-1L);
/*     */         }
/*     */       }
/*     */     } catch (FaultWS fw) {
/*  59 */       if (fw.getCodefault().equals("MT0044")) {
/*  60 */         ContractBean contractBean = this.infoContratsDAO.getContractByMsisdnAndStatus(msisdn, "s");
/*  61 */         if (contractBean == null) {
/*  62 */           contractBean = this.infoContratsDAO.getContractByMsisdnAndStatus(msisdn, "d");
/*     */         }
/*  64 */         if (contractBean == null) {
/*  65 */           profileBean.setEligibility(-1L);
/*  66 */           profileBean.setProfile(-1L);
/*  67 */         } else if (",1,12,15,21,30,35,44,28,37,20,40,8,16,34,".contains(",".concat(Long.toString(contractBean.getTmCode()).concat(",")))) {
/*  68 */           profileBean.setEligibility(1L);
/*  69 */           profileBean.setProfile(1L);
/*  70 */         } else if (",27,26,".contains(",".concat(Long.toString(contractBean.getTmCode()).concat(",")))) {
/*  71 */           profileBean.setEligibility(1L);
/*  72 */           profileBean.setProfile(0L);
/*     */         }
/*  74 */       } else if (fw.getCodefault().equals("MT0043")) {
/*  75 */         profileBean.setEligibility(-1L);
/*  76 */         profileBean.setProfile(-1L);
/*     */       } else {
/*  78 */         throw fw;
/*     */       }
/*     */     }
/*  81 */     return profileBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Long getEligibility(ContractBean bcontract, String sncode3G)
/*     */     throws FaultWS
/*     */   {
/*  91 */     if (bcontract.getStatus() != 2L) {
/*  92 */       return new Long(1L);
/*     */     }
/*     */     try
/*     */     {
/*  96 */       ServiceBean svcBean = this.infoContratsDAO.getServiceBean(bcontract.getCo_Id(), sncode3G);
/*  97 */       if ((svcBean.getStatus() != 0L) || (!this.infoContratsDAO.checkInternetDebit(bcontract.getCo_Id(), sncode3G))) {
/*  98 */         return new Long(2L);
/*     */       }
/*     */     } catch (FaultWS fws) {
/* 101 */       if (!fws.getCodefault().equals("MT9999")) {
/* 102 */         return new Long(2L);
/*     */       }
/* 104 */       throw fws;
/*     */     }
/*     */     
/*     */     try
/*     */     {
/* 109 */       ServiceBean svcBeanRFI3G = this.infoContratsDAO.getServiceBean(bcontract.getCo_Id(), "RFI3G");
/* 110 */       if ((svcBeanRFI3G.getStatus() != 0L) && 
/* 111 */         (",9,8,3,12,4,7,6,20,".contains(",".concat(Long.toString(bcontract.getPrgcode()).concat(","))))) {
/* 112 */         return new Long(3L);
/*     */       }
/*     */     }
/*     */     catch (FaultWS fws) {
/* 116 */       log.info("unfound service RFI3G for coId : " + bcontract.getCo_Id());
/* 117 */       if (("MT0047".equals(fws.getCodefault())) && (",9,8,3,12,4,7,6,20,".contains(",".concat(Long.toString(bcontract.getPrgcode()).concat(","))))) {
/* 118 */         return new Long(3L);
/*     */       }
/* 120 */       log.info("unfound service RFI3G for coId : " + bcontract.getCo_Id());
/*     */     }
/*     */     
/* 123 */     ServiceBean svcBeanBBBSE = null;
/* 124 */     ServiceBean svcBeanBBENT = null;
/* 125 */     ServiceBean svcBeanBBWCL = null;
/*     */     try {
/* 127 */       svcBeanBBBSE = this.infoContratsDAO.getServiceBean(bcontract.getCo_Id(), "BBBSE");
/* 128 */       if (svcBeanBBBSE.getStatus() == 0L) {
/* 129 */         return new Long(4L);
/*     */       }
/*     */     } catch (FaultWS fws) {
/* 132 */       log.info("unfound service BBBSE for coId : " + bcontract.getCo_Id());
/*     */     }
/*     */     try {
/* 135 */       svcBeanBBENT = this.infoContratsDAO.getServiceBean(bcontract.getCo_Id(), "BBENT");
/* 136 */       if (svcBeanBBENT.getStatus() == 0L) {
/* 137 */         return new Long(4L);
/*     */       }
/*     */     } catch (FaultWS fws) {
/* 140 */       log.info("unfound service BBENT for coId : " + bcontract.getCo_Id());
/*     */     }
/*     */     try {
/* 143 */       svcBeanBBWCL = this.infoContratsDAO.getServiceBean(bcontract.getCo_Id(), "BBWCL");
/* 144 */       if (svcBeanBBWCL.getStatus() == 0L) {
/* 145 */         return new Long(4L);
/*     */       }
/*     */     } catch (FaultWS fws) {
/* 148 */       log.info("unfound service BBWCL for coId : " + bcontract.getCo_Id());
/*     */     }
/* 150 */     return new Long(0L);
/*     */   }
/*     */ }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\domaine\impl\GetProfileEligibilityDomainImpl.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */