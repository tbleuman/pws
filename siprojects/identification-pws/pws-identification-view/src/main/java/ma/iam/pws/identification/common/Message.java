package ma.iam.pws.identification.common;

import java.util.Properties;

public final class Message {

	// private Message msg= new Message();

	public static String getMessage(String key, Properties msgproperties) {

		return msgproperties.getProperty(key) != null ? " "
				+ (msgproperties.getProperty(key)) : " INVALIDE KEY" + key;

	}

	public static String getMessage(Object obj, String key,
			Properties msgproperties) {

		return msgproperties.getProperty(key) != null ? obj
				+ getMessage(key, msgproperties) : (" INVALIDE KEY" + key);

	}

}
