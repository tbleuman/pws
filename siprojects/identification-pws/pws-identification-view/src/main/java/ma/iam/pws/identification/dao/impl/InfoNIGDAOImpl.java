package ma.iam.pws.identification.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import ma.iam.pws.identification.beans.contract.ContractBean;
import ma.iam.pws.identification.beans.nig.NIG;
import ma.iam.pws.identification.beans.nig.NIG_ELIGIBILITY;
import ma.iam.pws.identification.beans.nig.NIG_STATUS;
import ma.iam.pws.identification.dao.InfoNIGDAO;
import ma.iam.pws.identification.exception.FaultWS;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class InfoNIGDAOImpl implements InfoNIGDAO {
	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("queries")
	Properties queries;
	@Autowired
	@Qualifier("msgpropertiesBean")
	Properties msgProperties;
	/* 50 */private static Log log = LogFactory.getLog(InfoNIGDAOImpl.class);

	public final NIG_ELIGIBILITY getNigEligibility(String msisdn)
			throws FaultWS {
		/* 58 */int check = 0;
		/* 59 */NIG_ELIGIBILITY nigEl = new NIG_ELIGIBILITY();
		try {
			/* 61 */log.info("Operation : - getNigEligibility received : "
					+ msisdn);

			/* 63 */log.info("Operation : - getNigEligibility checking : "
					+ msisdn);
			/* 64 */check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkMsisdn"),
					new Object[] { msisdn });
			/* 65 */if (check > 0) {
				/* 67 */log
						.info("Operation : - getNigEligibility getting contract for : "
								+ msisdn);
				/* 68 */ContractBean contract = (ContractBean) this.jdbcTemplate
						.queryForObject(this.queries
								.getProperty("getActifcontractsByMSISDN"),
								new Object[] { msisdn }, new RowMapper() {
									public ContractBean mapRow(ResultSet rs,
											int rowNum) throws SQLException {
										ContractBean ctrBean = new ContractBean();
										ctrBean.setCo_Id(rs.getLong(1));
										ctrBean.setMsisdn(rs.getString(2));
										ctrBean.setCustCode(rs.getString(3));
										ctrBean.setStatus(rs.getLong(4));
										ctrBean.setCustomerId(rs.getLong(5));
										ctrBean.setCustnum(rs.getString(6));
										ctrBean.setTmCode(rs.getLong(7));
										ctrBean.setCo_Code(rs.getString(8));
										ctrBean.setPrgcode(rs.getLong(9));
										ctrBean.setRpShdes(rs.getString(10));
										return ctrBean;
									}
								});

				/* 87 */log
						.info("Operation : - getNigEligibility checking tmcode Eligibility rule for : "
								+ contract);
				/* 88 */check = this.jdbcTemplate.queryForInt(
						this.queries.getProperty("checkEligibilityRuleTmcode"),
						new Object[] { Long.valueOf(contract.getTmCode()) });
				/* 89 */if (check > 0) {
					/* 91 */log
							.info("Operation : - getNigEligibility checking prgcode Eligibility rule for : "
									+ contract);
					/* 92 */check = this.jdbcTemplate
							.queryForInt(
									this.queries
											.getProperty("checkEligibilityRulePrgcode"),
									new Object[] { Long.valueOf(contract
											.getPrgcode()) });
					/* 93 */if (check > 0)
						if (this.jdbcTemplate
								.queryForInt(this.queries
										.getProperty("checkActif75HService"),
										new Object[] { Long.valueOf(contract
												.getCo_Id()) }) <= 0) {
							/* 94 */if (contract.getTmCode() == 41L) {
								/* 95 */log
										.info("Operation : - getNigEligibility  check forfait value rule for : "
												+ contract);
								/* 96 */check = this.jdbcTemplate
										.queryForInt(
												this.queries
														.getProperty("checkforfaitvalue"),
												new Object[] { Long
														.valueOf(contract
																.getCo_Id()) });
								/* 97 */if (check <= 0) {
									/* 98 */nigEl.setOption(0);
									/* 99 */return nigEl;
								}
							}
							/* 102 */log
									.info("Operation : - getNigEligibility get rule value for : "
											+ contract);
							/* 103 */check = this.jdbcTemplate
									.queryForInt(
											this.queries
													.getProperty("getEligibilityRule"),
											new Object[] {
													Long.valueOf(contract
															.getTmCode()),
													Long.valueOf(contract
															.getPrgcode()) });
							/* 104 */nigEl.setOption(check);

						}
					/* 106 */nigEl.setOption(0);
				} else {
					/* 109 */nigEl.setOption(0);
				}
			} else {
				/* 113 */log
						.info("Operation : - getNigEligibility Exception : MSISDN_NOT_FOUND "
								+ msisdn);
				/* 114 */throw new FaultWS("MT0043", this.msgProperties);
			}
		} catch (EmptyResultDataAccessException dex) {
			/* 119 */log
					.info("Operation : - getNigEligibility Exception : MSISDN_INACTIFCONTRACT "
							+ msisdn);
			/* 120 */throw new FaultWS("MT0044", this.msgProperties);
		} catch (Exception e) {
			/* 123 */if ((e instanceof FaultWS)) {
				/* 124 */throw ((FaultWS) e);
			}
			/* 126 */log.info(e.getLocalizedMessage());
			/* 127 */throw new FaultWS("MT9999", this.msgProperties);
		}

		/* 131 */log
				.info("Operation : - getNigEligibility Setting  : NIG_ELIGIBILITY option to  "
						+ nigEl.getOption());
		/* 132 */return nigEl;
	}

	public NIG_STATUS getNigStatus(String msisdn) throws FaultWS {
		/* 136 */int check = 0;
		/* 137 */NIG_STATUS nigstatus = new NIG_STATUS();
		try {
			/* 140 */log.info("Operation : - getNigStatus Received  msisdn  :"
					+ msisdn);

			/* 144 */log
					.info("Operation : - getNigStatus Getting contract for :  "
							+ msisdn);
			/* 145 */check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkMsisdn"),
					new Object[] { msisdn });
			/* 146 */if (check > 0) {
				/* 148 */ContractBean contract = (ContractBean) this.jdbcTemplate
						.queryForObject(this.queries
								.getProperty("getActifcontractsByMSISDN"),
								new Object[] { msisdn }, new RowMapper() {
									public ContractBean mapRow(ResultSet rs,
											int rowNum) throws SQLException {
										ContractBean ctrBean = new ContractBean();
										ctrBean.setCo_Id(rs.getLong(1));
										ctrBean.setMsisdn(rs.getString(2));
										ctrBean.setCustCode(rs.getString(3));
										ctrBean.setStatus(rs.getLong(4));
										ctrBean.setCustomerId(rs.getLong(5));
										ctrBean.setCustnum(rs.getString(6));
										ctrBean.setTmCode(rs.getLong(7));
										ctrBean.setCo_Code(rs.getString(8));
										ctrBean.setPrgcode(rs.getLong(9));
										return ctrBean;
									}
								});

				try {
					/* 168 */log
							.info("Operation : - getNigStatus check actif contract for :  "
									+ contract);
					/* 169 */check = this.jdbcTemplate.queryForInt(
							this.queries.getProperty("checkActifService"),
							new Object[] { Long.valueOf(contract.getCo_Id()) });
				} catch (EmptyResultDataAccessException dex) {
					/* 171 */check = 1;
				}
				/* 173 */nigstatus.setStatus(check);
			} else {
				/* 175 */log
						.info("Operation : - getNigStatus get Exception MSISDN_NOT_FOUND for msisdn : "
								+ msisdn);
				/* 176 */throw new FaultWS("MT0043", this.msgProperties);
			}
		} catch (EmptyResultDataAccessException dex) {
			/* 181 */log
					.info("Operation : - getNigStatus get Exception MSISDN_INACTIFCONTRACT for msisdn : "
							+ msisdn);
			/* 182 */throw new FaultWS("MT0044", this.msgProperties);
		} catch (Exception e) {
			/* 185 */log
					.info("Operation : - getNigStatus get Exception for msisdn : "
							+ msisdn);
			/* 186 */if ((e instanceof FaultWS)) {
				/* 187 */throw ((FaultWS) e);
			}
			/* 189 */log.info(e.getLocalizedMessage());
			/* 190 */throw new FaultWS("MT9999", this.msgProperties);
		}

		/* 194 */return nigstatus;
	}

	public ArrayList<NIG> getNigMicrocell(String msisdn) throws FaultWS {
		/* 198 */ArrayList<String> nigListtmp = new ArrayList();
		/* 199 */ArrayList<NIG> nigList = new ArrayList();

		/* 201 */int check = 0;
		try {
			/* 204 */log
					.info("Operation : - getNigMicrocell Received  msisdn  :"
							+ msisdn);
			/* 205 */check = this.jdbcTemplate.queryForInt(
					this.queries.getProperty("checkMsisdn"),
					new Object[] { msisdn });
			/* 206 */if (check > 0) {
				/* 208 */log
						.info("Operation : - getNigMicrocell get contract  :"
								+ msisdn);
				/* 209 */ContractBean contract = (ContractBean) this.jdbcTemplate
						.queryForObject(this.queries
								.getProperty("getActifcontractsByMSISDN"),
								new Object[] { msisdn }, new RowMapper() {
									public ContractBean mapRow(ResultSet rs,
											int rowNum) throws SQLException {
										ContractBean ctrBean = new ContractBean();
										ctrBean.setCo_Id(rs.getLong(1));
										ctrBean.setMsisdn(rs.getString(2));
										ctrBean.setCustCode(rs.getString(3));
										ctrBean.setStatus(rs.getLong(4));
										ctrBean.setCustomerId(rs.getLong(5));
										ctrBean.setCustnum(rs.getString(6));
										ctrBean.setTmCode(rs.getLong(7));
										ctrBean.setCo_Code(rs.getString(8));
										ctrBean.setPrgcode(rs.getLong(9));
										return ctrBean;
									}
								});

				try {
					/* 229 */log
							.info("Operation : - getNigMicrocell get MicroCell  :"
									+ contract);
					/* 230 */nigListtmp = (ArrayList) this.jdbcTemplate
							.queryForList(this.queries
									.getProperty("getMicroCell"),
									new Object[] { Long.valueOf(contract
											.getCo_Id()) }, String.class);
				} catch (EmptyResultDataAccessException dex) {
					/* 232 */log.info(dex.getLocalizedMessage());
				}
			} else {
				/* 235 */log
						.info("Operation : - getNigMicrocell get Exception  MSISDN_NOT_FOUND   :"
								+ msisdn);
				/* 236 */throw new FaultWS("MT0043", this.msgProperties);
			}
		} catch (EmptyResultDataAccessException dex) {
			/* 241 */log
					.info("Operation : - getNigMicrocell get Exception  MSISDN_INACTIFCONTRACT   :"
							+ msisdn);
			/* 242 */throw new FaultWS("MT0044", this.msgProperties);
		} catch (Exception e) {
			/* 245 */log.info("Operation : - getNigMicrocell get Exception :"
					+ msisdn);
			/* 246 */if ((e instanceof FaultWS)) {
				/* 247 */throw ((FaultWS) e);
			}
			/* 249 */log.info(e.getLocalizedMessage());
			/* 250 */throw new FaultWS("MT9999", this.msgProperties);
		}

		/* 254 */for (Iterator iterator = nigListtmp.iterator(); iterator
				.hasNext();) {
			/* 255 */String string = (String) iterator.next();
			/* 256 */NIG nig = new NIG();
			/* 257 */nig.setMsisdn(string);
			/* 258 */nigList.add(nig);
		}
		/* 260 */return nigList;
	}
}
