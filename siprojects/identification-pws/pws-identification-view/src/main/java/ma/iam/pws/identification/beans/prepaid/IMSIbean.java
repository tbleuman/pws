 package ma.iam.pws.identification.beans.prepaid;
 
 
 public class IMSIbean
 {
   private String IMSI_ref;
   
   private String IMSI_stat;
   private String co_id;
   private String customerId;
   private String rejectReason;
   
   public String getIMSI_ref()
   {
/* 15 */     return this.IMSI_ref;
   }
   
 
 
 
   public void setIMSI_ref(String iMSI_ref)
   {
/* 23 */     this.IMSI_ref = iMSI_ref;
   }
   
 
 
   public String getIMSI_stat()
   {
/* 30 */     return this.IMSI_stat;
   }
   
 
 
 
   public void setIMSI_stat(String iMSI_stat)
   {
/* 38 */     this.IMSI_stat = iMSI_stat;
   }
   
 
 
   public String getCo_id()
   {
/* 45 */     return this.co_id;
   }
   
 
 
   public void setCo_id(String co_id)
   {
/* 52 */     this.co_id = co_id;
   }
   
 
 
   public String getCustomerId()
   {
/* 59 */     return this.customerId;
   }
   
 
 
   public void setCustomerId(String customerId)
   {
/* 66 */     this.customerId = customerId;
   }
   
 
 
   public String getRejectReason()
   {
/* 73 */     return this.rejectReason;
   }
   
 
 
   public void setRejectReason(String rejectReason)
   {
/* 80 */     this.rejectReason = rejectReason;
   }
 }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\prepaid\IMSIbean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */