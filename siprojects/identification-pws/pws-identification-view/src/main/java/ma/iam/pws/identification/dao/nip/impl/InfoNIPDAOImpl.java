package ma.iam.pws.identification.dao.nip.impl;
//package ma.iam.ws.dao.nip.impl;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Properties;
//import ma.iam.ws.beans.contract.ContractBean;
//import ma.iam.ws.beans.nip.NIPBean;
//import ma.iam.ws.dao.nip.InfoNIPDAO;
//import ma.iam.ws.engine.common.util.Utils;
//import ma.iam.ws.exception.FaultWS;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Service;
//
//@Service
//public class InfoNIPDAOImpl implements InfoNIPDAO {
//	@Autowired
//	@Qualifier("jdbcTemplate")
//	private JdbcTemplate jdbcTemplate;
//	@Autowired
//	@Qualifier("queries")
//	private Properties queries;
//	@Autowired
//	@Qualifier("queriesNIP")
//	private Properties queriesNIP;
//	@Autowired
//	@Qualifier("msgpropertiesBean")
//	private Properties msgProperties;
//	@Autowired
//	@Qualifier("conf")
//	private Properties conf;
//	/* 68 */private static Log log = LogFactory.getLog(InfoNIPDAOImpl.class);
//
//	public NIPBean canNipBeModified(String nd) throws FaultWS
//   {
///*  72 */     NIPBean nb = new NIPBean();
//     
///*  74 */     log.info("function : - canNipBeModified checking : " + nd);
//     
///*  76 */     int check = this.jdbcTemplate.queryForInt(this.queries.getProperty("checkMsisdn"), new Object[] { nd });
//     
///*  78 */     ContractBean contract = null;
///*  79 */     if (check > 0) {
///*  80 */       contract = (ContractBean)this.jdbcTemplate.queryForObject(this.queriesNIP.getProperty("getactifInfoContractByNd"), new Object[] { nd }, new InfoNIPDAOImpl.1(this));
//       
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
///* 101 */       List<String> array = Utils.fetchArrayFromPropFile("prgcode", this.conf, ";");
///* 102 */       log.info("function : - canNipBeModified check Categorie Elgibility " + nd);
//       
///* 104 */       if ((array != null) && (array.size() > 0)) {
///* 105 */         if (array.contains(Long.valueOf(contract.getPrgcode()))) {
///* 106 */           log.info("function : - canNipBeModified check rateplan Elgibility " + nd);
///* 107 */           List<String> arrayRp = Utils.fetchArrayFromPropFile("rpshdes", this.conf, ";");
///* 108 */           if ((array != null) && (array.size() > 0)) {
///* 109 */             if (arrayRp.contains(contract.getRpShdes())) {
///* 110 */               check = this.jdbcTemplate.queryForInt(this.queriesNIP.getProperty("hasActifNipService"), new Object[] { nd, this.conf.getProperty("snshdes") });
//               
///* 112 */               if (check > 0) {
///* 113 */                 check = this.jdbcTemplate.queryForInt(this.queriesNIP.getProperty("hasNipModification"), new Object[] { Long.valueOf(contract.getCo_Id()) });
//                 
///* 115 */                 if (check <= 0) {
///* 116 */                   check = this.jdbcTemplate.queryForInt(this.queriesNIP.getProperty("hasfutureQueuedRequest"), new Object[] { Long.valueOf(contract.getCo_Id()) });
//                   
///* 118 */                   if (check <= 0) {
///* 119 */                     nb.setResult("1");
///* 120 */                     nb.setMessage("Opération OK");
//                   } else {
///* 122 */                     nb.setResult("-6");
///* 123 */                     nb.setMessage("Le contrat a une demande de modification ou de suppression de son NIP dans le futur");
//                   }
//                 } else {
///* 126 */                   nb.setResult("-5");
///* 127 */                   nb.setMessage("Le contrat a modifiÃ© le NIP courant du mois");
//                 }
//               } else {
///* 130 */                 log.info("function : - canNipBeModified Le service NIP n'est pas actif pour ce ND " + nd);
///* 131 */                 nb.setResult("-4");
///* 132 */                 nb.setMessage("Le service NIP n'est pas actif pour ce ND");
//               }
//             } else {
///* 135 */               nb.setResult("-3");
///* 136 */               nb.setMessage("Le plan tarifaire de ce contrat n'est pas éligible");
//             }
//           }
//           else {
///* 140 */             log.info("function : - canNipBeModified Internal Exception : Erreur loading conf file " + nd);
//             
///* 142 */             throw new FaultWS("MT9999", this.msgProperties);
//           }
//         } else {
///* 145 */           nb.setResult("-2");
///* 146 */           nb.setMessage("La catégorie de ce client n'est pas éligible");
//         }
//       } else {
///* 149 */         nb.setResult("-14");
///* 150 */         nb.setMessage("Le nd n'as pas de co_id valide");
//       }
//     } else {
///* 153 */       log.info("function : - canNipBeModified Exception : MSISDN_NOT_FOUND " + nd);
//       
///* 155 */       throw new FaultWS("MT0043", this.msgProperties);
//     }
///* 157 */     return nb;
//   }
//
//	public NIPBean canNipBeDeleted(String nd) throws FaultWS
//   {
///* 162 */     NIPBean nb = new NIPBean();
//     
///* 164 */     log.info("function : - canNipBeModified checking : " + nd);
//     
///* 166 */     int check = this.jdbcTemplate.queryForInt(this.queries.getProperty("checkMsisdn"), new Object[] { nd });
//     
///* 168 */     ContractBean contract = null;
///* 169 */     if (check > 0) {
///* 170 */       contract = (ContractBean)this.jdbcTemplate.queryForObject(this.queriesNIP.getProperty("getactifInfoContractByNd"), new Object[] { nd }, new InfoNIPDAOImpl.2(this));
//       
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
///* 191 */       List<String> array = Utils.fetchArrayFromPropFile("prgcode", this.conf, ";");
///* 192 */       log.info("function : - canNipBeModified check Categorie Elgibility " + nd);
//       
///* 194 */       if ((array != null) && (array.size() > 0)) {
///* 195 */         if (array.contains(Long.valueOf(contract.getPrgcode()))) {
///* 196 */           log.info("function : - canNipBeModified check rateplan Elgibility " + nd);
///* 197 */           List<String> arrayRp = Utils.fetchArrayFromPropFile("rpshdes", this.conf, ";");
///* 198 */           if ((array != null) && (array.size() > 0)) {
///* 199 */             if (arrayRp.contains(contract.getRpShdes())) {
///* 200 */               check = this.jdbcTemplate.queryForInt(this.queriesNIP.getProperty("hasActifNipService"), new Object[] { nd, this.conf.getProperty("snshdes") });
//               
///* 202 */               if (check > 0) {
///* 203 */                 check = this.jdbcTemplate.queryForInt(this.queriesNIP.getProperty("hasfutureQueuedRequest"), new Object[] { Long.valueOf(contract.getCo_Id()) });
//                 
///* 205 */                 if (check <= 0) {
///* 206 */                   nb.setResult("1");
///* 207 */                   nb.setMessage("Opération OK");
//                 } else {
///* 209 */                   nb.setResult("-6");
///* 210 */                   nb.setMessage("Le contrat a une demande de modification ou de suppression de son NIP dans le futur");
//                 }
//               } else {
///* 213 */                 log.info("function : - canNipBeModified Le service NIP n'est pas actif pour ce ND " + nd);
///* 214 */                 nb.setResult("-4");
///* 215 */                 nb.setMessage("Le service NIP n'est pas actif pour ce ND");
//               }
//             } else {
///* 218 */               nb.setResult("-3");
///* 219 */               nb.setMessage("Le plan tarifaire de ce contrat n'est pas éligible");
//             }
//           }
//           else {
///* 223 */             log.info("function : - canNipBeModified Internal Exception : Erreur loading conf file " + nd);
//             
///* 225 */             throw new FaultWS("MT9999", this.msgProperties);
//           }
//         } else {
///* 228 */           nb.setResult("-2");
///* 229 */           nb.setMessage("La catégorie de ce client n'est pas éligible");
//         }
//       } else {
///* 232 */         nb.setResult("-14");
///* 233 */         nb.setMessage("Le nd n'as pas de co_id valide");
//       }
//     } else {
///* 236 */       log.info("function : - canNipBeModified Exception : MSISDN_NOT_FOUND " + nd);
//       
///* 238 */       throw new FaultWS("MT0043", this.msgProperties);
//     }
///* 240 */     return nb;
//   }
//
//	public NIPBean getNipModificationFee(String paramString, int paramInt) {
//		/* 245 */return null;
//	}
//
//	public NIPBean getMaxNipToChange(String paramString) {
//		/* 250 */return null;
//	}
//
//	public NIPBean saveNipChanges(String paramString1, Date paramDate,
//			String paramString2, String paramString3) {
//		/* 256 */return null;
//	}
//}
