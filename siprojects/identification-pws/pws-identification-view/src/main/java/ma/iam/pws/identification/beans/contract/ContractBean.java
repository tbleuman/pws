/*     */ package ma.iam.pws.identification.beans.contract;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ 
/*     */ @javax.xml.bind.annotation.XmlType(name="Contract", propOrder={"CO_ID", "CO_ID_PUB", "RPCODE", "CS_ID", "CS_ID_PUB", "contractedServices", "unContractedServices"})
/*     */ public class ContractBean implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*     */   private long co_Id;
/*     */   private String co_Code;
/*     */   private long tmCode;
/*     */   private long customerId;
/*     */   
/*     */   public String toString()
/*     */   {
/*  16 */     return "ContractBean [co_Id=" + this.co_Id + ", co_Code=" + this.co_Code + ", tmCode=" + this.tmCode + ", customerId=" + this.customerId + ", custCode=" + this.custCode + ", status=" + this.status + ", prgcode=" + this.prgcode + ", msisdn=" + this.msisdn + ", custnum=" + this.custnum + "]";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String custCode;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private long status;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private long prgcode;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String msisdn;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String custnum;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String rpShdes;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getCo_Id()
/*     */   {
/*  74 */     return this.co_Id;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCo_Id(long co_Id)
/*     */   {
/*  80 */     this.co_Id = co_Id;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCo_Code()
/*     */   {
/*  86 */     return this.co_Code;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCo_Code(String co_Code)
/*     */   {
/*  92 */     this.co_Code = co_Code;
/*     */   }
/*     */   
/*     */ 
/*     */   public long getTmCode()
/*     */   {
/*  98 */     return this.tmCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTmCode(long tmCode)
/*     */   {
/* 104 */     this.tmCode = tmCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public long getCustomerId()
/*     */   {
/* 110 */     return this.customerId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCustomerId(long customerId)
/*     */   {
/* 116 */     this.customerId = customerId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCustCode()
/*     */   {
/* 122 */     return this.custCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public final void setCustCode(String custCode)
/*     */   {
/* 128 */     this.custCode = custCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public long getStatus()
/*     */   {
/* 134 */     return this.status;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setStatus(long status)
/*     */   {
/* 140 */     this.status = status;
/*     */   }
/*     */   
/*     */ 
/*     */   public long getPrgcode()
/*     */   {
/* 146 */     return this.prgcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setPrgcode(long prgcode)
/*     */   {
/* 152 */     this.prgcode = prgcode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getMsisdn()
/*     */   {
/* 158 */     return this.msisdn;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMsisdn(String msisdn)
/*     */   {
/* 164 */     this.msisdn = msisdn;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCustnum()
/*     */   {
/* 170 */     return this.custnum;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCustnum(String custnum)
/*     */   {
/* 176 */     this.custnum = custnum;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getRpShdes()
/*     */   {
/* 182 */     return this.rpShdes;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRpShdes(String rpShdes)
/*     */   {
/* 188 */     this.rpShdes = rpShdes;
/*     */   }
/*     */ }


/* Location:              C:\Users\A596968\Desktop\En cours\FC6705\WS-IDENTIFICATION-2.0.0-SNAPSHOT.war!\WEB-INF\classes\ma\iam\ws\beans\contract\ContractBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */