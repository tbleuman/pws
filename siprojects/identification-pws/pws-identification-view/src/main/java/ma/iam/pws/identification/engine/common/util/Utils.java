/*     */ package ma.iam.pws.identification.engine.common.util;
/*     */ 
/*     */ import java.math.BigDecimal;
/*     */ import java.text.DateFormat;
/*     */ import java.text.ParseException;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Date;
/*     */ import java.util.List;
/*     */ import java.util.Properties;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class Utils
/*     */ {
/*     */   public static String nullToBlanc(String str)
/*     */   {
/*  40 */     return str == null ? "" : str.trim();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isEmpty(String s)
/*     */   {
/*  50 */     return (s == null) || (s.equals(""));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isNumeric(String s)
/*     */   {
/*     */     try
/*     */     {
/*  61 */       Long.parseLong(s);
/*  62 */       return true;
/*     */     } catch (Exception e) {}
/*  64 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String dateToString(Date date)
/*     */   {
/*  75 */     return dateToString(date, "dd/MM/yyyy");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String dateToString(Date date, String dateFormat)
/*     */   {
/*     */     try
/*     */     {
/*  88 */       SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
/*  89 */       return simpleDateFormat.format(date);
/*     */     } catch (Exception e) {}
/*  91 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Date stringToDate(String sDate)
/*     */   {
/* 103 */     return stringToDate(sDate, "dd/MM/yyyy");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Date stringToDate(String sDate, String format)
/*     */   {
/* 115 */     Date date = null;
/* 116 */     DateFormat formatter = new SimpleDateFormat(format);
/* 117 */     formatter.setLenient(false);
/*     */     try {
/* 119 */       date = formatter.parse(sDate);
/*     */     } catch (Exception e) {
/* 121 */       return null;
/*     */     }
/* 123 */     return date;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Double round(double d, int n)
/*     */   {
/* 134 */     double result = d;
/* 135 */     int p = (int)Math.pow(10.0D, n);
/* 136 */     result *= p;
/* 137 */     result = Math.round(result);
/* 138 */     result /= p;
/* 139 */     return Double.valueOf(result);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Float round(float d, int n)
/*     */   {
/* 150 */     float result = d;
/* 151 */     int p = (int)Math.pow(10.0D, n);
/* 152 */     result *= p;
/* 153 */     result = Math.round(result);
/* 154 */     result /= p;
/* 155 */     return Float.valueOf(result);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Double roundDown(Double d)
/*     */   {
/* 164 */     Double tmp = Double.valueOf(d.doubleValue() * 100.0D);
/* 165 */     if (tmp.longValue() < tmp.doubleValue()) {
/* 166 */       BigDecimal b = new BigDecimal(d.doubleValue());
/* 167 */       return Double.valueOf(b.setScale(2, 1).doubleValue());
/*     */     }
/* 169 */     return d;
/*     */   }
/*     */   
/*     */   public static Date parseStringToDate(String date, String dateFormat) {
/* 173 */     Date formatedDate = null;
/*     */     try {
/* 175 */       formatedDate = new SimpleDateFormat(dateFormat).parse(date);
/*     */     } catch (ParseException ex) {
/* 177 */       return null;
/*     */     }
/* 179 */     return formatedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<String> fetchArrayFromPropFile(String propertyName, Properties propFile, String separator)
/*     */   {
/* 192 */     String[] a = propFile.getProperty(propertyName).split(separator);
/*     */     
/*     */ 
/* 195 */     ArrayList<String> array = new ArrayList();
/*     */     
/*     */ 
/* 198 */     for (int i = 0; i < a.length; i++) {
/* 199 */       array.add(a[i]);
/*     */     }
/* 201 */     return array;
/*     */   }
/*     */ }

