package ma.iam.pws.identification.exception;

import javax.xml.bind.UnmarshalException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.common.injection.NoJSR250Annotations;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.wss4j.common.ext.WSSecurityException;

import com.ctc.wstx.exc.WstxParsingException;

@NoJSR250Annotations
public class ExceptionInterceptor extends AbstractSoapInterceptor {

	private final static int HTTP_200 = 200;

	public ExceptionInterceptor() {
		super(Phase.PRE_LOGICAL);
	}

	public void handleMessage(SoapMessage message) throws Fault {
		Fault fault = (Fault) message.getContent(Exception.class);
		Throwable ex = fault.getCause();
		if ((ex instanceof FaultWS)) {
			fault.setStatusCode(HTTP_200);
			FaultWS e = (FaultWS) ex;
			generateSoapFault(fault, e);
		} else if (((ex instanceof UnmarshalException))
				|| ((ex instanceof IllegalArgumentException))) {
			FaultWS e = new FaultWS("MT9998",
					"UnmarshalException: For input Arguments Or Incorrect Call");
			generateSoapFault(fault, e);
		} else if (((ex instanceof WSSecurityException))
				|| ((ex instanceof SOAPFaultException))
				|| ((ex instanceof SOAPException))
				|| ((ex instanceof WstxParsingException))) {
			generateSoapFault(fault, ex);
		} else {
			generateSoapFault(fault, null);
		}
	}

	private void generateSoapFault(Fault fault, FaultWS e) {
		fault.setFaultCode(createQName(e.getCodefault() != null ? e
				.getCodefault() : "WS-ERR"));
		fault.setMessage(e.getMessage());
	}

	private static QName createQName(String errorCode) {
		return new QName("ws.iam.ma", String.valueOf(errorCode));
	}

	private void generateSoapFault(Fault fault, Throwable e) {
		fault.setFaultCode(createQName("WS-ERR"));
		fault.setMessage(e.getMessage());
	}
}
