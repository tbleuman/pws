package ma.iam.pws.soi.webservice.services.impl;

import java.util.HashMap;

import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.webservice.services.ActivationNewService;

/**
 * @author mounir_mabchour
 *
 */
@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public class ActivationNewServiceImpl implements ActivationNewService {

	public Bcontract ActivationNewServices(String username, long co_id, long sncode)
			throws FaultWS {
		HashMap<String ,Object> inParams = new HashMap<String ,Object>();
		if ((sncode==0 || co_id ==0) ) {
			throw new FaultWS("RC1500", "WS ActivationNewServices : mandatory SNCODE & CO_ID ");		
		}
		inParams.put("SNCODE", sncode);
		inParams.put("CO_ID", co_id);
		
		
		return null;
	}

}
