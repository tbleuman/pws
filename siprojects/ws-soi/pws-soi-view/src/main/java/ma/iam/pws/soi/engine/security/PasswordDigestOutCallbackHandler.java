package ma.iam.pws.soi.engine.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * PasswordDigestOutCallbackHandler.
 * <br/><br/>
 * Projet : Mutualisation
 * Créé le 3 juin 2011
 * 
 * @author mei
 */
public class PasswordDigestOutCallbackHandler implements CallbackHandler {

	/**
	 * See @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[]).
	 * @param callbacks
	 * @throws IOException
	 * @throws UnsupportedCallbackException
	 */
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		 WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		 
		 pc.setPassword(null);

		 String username = pc.getIdentifier();
		 
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		 if (authentication != null && authentication.isAuthenticated()) {
			 UserDetails user = (UserDetails) authentication.getPrincipal();
			 if (user != null && user.getUsername().equals(username)) {
				 pc.setPassword(user.getPassword());
			 }
		}
	}

}
