package ma.iam.pws.soi.webservice.services;

import ma.iam.pws.soi.beans.contract.MassServicesEx;
import ma.iam.pws.soi.beans.contract.bServicesList;
import ma.iam.pws.soi.beans.errors.FaultWS;



/**
 * @author Alaa ESSA
 *
 */

@javax.jws.WebService(targetNamespace="http://services.webservice.ws.iam.ma/")
public interface MassActivationDeactivationServices {
	
	/**
	 * MassActivationDeactivationServices . : WS user to mass activate/deactivate Services for contract
	 * 
	 * @param coId internal identifier of contract in billing system
	 * @param FirstList public identifier of Service to Activate or Deactivate
	 * @param SecondList public identifier of Service to Activate or Deactivate
	 */
	@javax.jws.WebMethod
	MassServicesEx MassActivationDeactivationServicesImpl( @javax.jws.WebParam(name="CO_ID") long co_id,	@javax.jws.WebParam(name="FirstList") bServicesList   List1, @javax.jws.WebParam(name="SecondList")  bServicesList   List2 ) throws FaultWS  ;

}
