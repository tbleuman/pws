package ma.iam.pws.soi.common;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class Constants {

	/** The Constant PARAM_SESSION_ID. */
	public static final ShaPasswordEncoder ENCODER = new ShaPasswordEncoder();
	public static final String PARAM_SESSION_ID = "idSession";

	/** The Constant PARAM_USER_NAME. */
	public static final String PARAM_USER_NAME = "username";

	public static class SV_TYPES {
		public static final Character COST_CONTROL = new Character('B');
		public static final Character CONTENT = new Character('C');
	}

	public static class ADVINDS {
		public static final Character ADVANCE = new Character('A');
		public static final Character PAST = new Character('P');
	}

	public static class SRVINDS {
		public static final String EVENT_DRIVEN = "E";
		public static final String PERMANENT = "P";
	}

	public static class SC_SNINDS {
		public static final Character USAGE = new Character('U');
		public static final Character EVENT = new Character('E');
		public static final Character VALUE_ADDED = new Character('V');
	}

	public static class ERROR_CODE {
		// General Errors
		public static final String MT_USERUNLOGGED = "MT0001";
		public static final String MT_INTERNALERROR = "MT0001";

		// WS Dependency Error Code
		public static final String MT_WS_DEPNDCY_SNCODE_MISSING = "MT0002";
		public static final String MT_WS_DEPNDCY_NO_DEPENDENCES = "MT0003";

		// WS Authentification Error Code

		public static final String MT_WS_AUTHEN_INPUT_MISSING = "MT0004";
		public static final String MT_WS_AUTHEN_PASSWD_EXPIRE = "MT0005";

		// WS ReadServices
		public static final String MT_WS_READSVC_INPUT_MISSING = "MT0006";

		// WS ChangeStatutContract

		public static final String MT_WS_CHANGESTAT_INPUT_MISSING = "MT0007";
		public static final String MT_WS_CHANGESTAT_INVALID_REASN = "MT0008";
		public static final String MT_WS_CHANGESTAT_INVALID_TRANSI = "MT0009";
		public static final String MT_WS_CHANGESTAT_PENDING_STAT = "MT0010";
		public static final String MT_WS_CHANGESTAT_INVALID_STAT = "MT0011";

		  public static final String MT_WS_CHANGESTAT_NOT_ALLOWED = "MT0051";
		// WS ChangeParamService
		public static final String MT_WS_CHANGESVCPRM_INPUT_MISSING = "MT0012";
		public static final String MT_WS_CHANGESVCPRM_INVALID_CFG = "MT0013";
		public static final String MT_WS_CHANGESVCPRM_UNATTACHED_CTR = "MT0014";
		public static final String MT_WS_CHANGESVCPRM_NOTALLOWED = "MT0015";

		// WS DeactivateService

		public static final String MT_WS_DEACTIVATESVC_INPUT_MISSING = "MT0016";
		public static final String MT_WS_DEACTIVATESVC_UNATTACHED_CTR = "MT0017";
		public static final String MT_WS_DEACTIVATESVC_NOTALLOWED = "MT0018";
		public static final String MT_WS_DEACTIVATESVC_ERROR_NEW_STAT = "MT0019";
		public static final String MT_WS_DEACTIVATESVC_ERROR_STAT = "MT0020";
		public static final String MT_WS_DEACTIVATESVC_PENDING_RQ = "MT0021";

		// WS ActivateNewService

		public static final String MT_WS_ACTIVATESVC_INPUT_MISSING = "MT0022";
		public static final String MT_WS_ACTIVATESVC_PRM_SVC = "MT0023";
		public static final String MT_WS_ACTIVATESVC_PRM_SVC_ERROR = "MT0024";
		public static final String MT_WS_ACTIVATESVC_SVC_NOPRM = "MT0025";
		public static final String MT_WS_ACTIVATESVC_PRM_VAL_ERROR = "MT0026";
		public static final String MT_WS_ACTIVATESVC_PRM_MX_ONEVALUE = "MT0027";
		public static final String MT_WS_ACTIVATESVC_PRM_UN_VALUE = "MT0028";
		public static final String MT_WS_ACTIVATESVC_PRM_MORE_VALUE = "MT0029";
		public static final String MT_WS_ACTIVATESVC_PRM_VALUE_ONE_ATlEAST = "MT0030";
		public static final String MT_WS_ACTIVATESVC_PRM_VAL_TYPE_ERROR = "MT0031";
		public static final String MT_WS_ACTIVATESVC_ACTIVE_SERVICE = "MT0032";
		public static final String MT_CONTRACTNOTFOUND = "MT0033";

		// WS CheckDependency

		public static final String MT_WS_CHCKDPNDNCY_OP_TYPE_ERROR = "MT0034";
		public static final String MT_WS_CHCKDPNDNCY_ACTIVECONTRACT_ERROR = "MT0035";
		public static final String MT_WS_CHCKDPNDNCY_ACT_SUS_SRV_ERROR = "MT0036";
		public static final String MT_WS_CHCKDPNDNCY_ACT_SRV_ERROR = "MT0037";
		public static final String MT_WS_CHCKDPNDNCY_ELG_ERROR = "MT0038";
		public static final String MT_WS_CHCKDPNDNCY_INCONSIS_ERROR = "MT0039";
		public static final String MT_WS_ACTIVATESVC_VAL_TYPE_ERROR = "MT0040";
		public static final String MT_WS_ACTIVATESVC_NO_PRIORITY_ERROR = "MT0041";
		public static final String MT_PASSWORDIO = "MT0042";
		public static final String MT_NOUSER_PASSWORDIO = "MT0043";
		// >******* Updated, A.ESSA FC5937
		public static final String MT_WS_CHCKDPNDNCY_4G_NETWORK_ERROR = "MT0046";
			//>******* Updated, A.ESSA FC7045
		  //public static final String MT_WS_CHCKDPNDNCY_SDM_ACTIVATION_ERROR = "MT0047";
			//<******* Updated, A.ESSA FC7045
		public static final String MT_WS_CHCKDPNDNCY_4G_SLAVES_ERROR = "MT0048";
		public static final String MT_WS_CHCKDPNDNCY_4G_MASTER_ERROR = "MT0049";
		public static final String MT_WS_CHCKDPNDNCY_D4G_NETWORK_ERROR = "MT0050";
		// <******* Updated, A.ESSA FC5937

		// WS RecharcheInternetService

		public static final String MT_WS_MSISDN_ERROR = "MT0044";
		public static final String MT_DATE_FORMAT = "MT0045";

		public static final String MT_WS_CONTRACT_NOT_FOUND = "MT0091";
		public static final String MT_WS_SERVICE_NOT_FOUND = "MT0092";
		public static final String MT_WS_SERVICE_NO_PARAMETER = "MT0093";
		public static final String MT_WS_SERVICE_NOT_ATHORIZED = "MT0094";
		public static final String MT_WS_CONTRACT_HAS_PENDINGREQUEST = "MT0095";
		public static final String MT_WS_SERVICE_NOT_ATTACHED = "MT0096";
		public static final String MT_WS_SERVICE_NOT_SUPPORTED = "MT0097";

		public static final String MT_WS_CMS_ERROR = "MTXXXX";

	}

}
