package ma.iam.pws.soi.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ma.iam.pws.soi.beans.contract.BBsg_configuration;
import ma.iam.pws.soi.beans.contract.BContractedServices;
import ma.iam.pws.soi.beans.contract.BCug_memberships;
import ma.iam.pws.soi.beans.contract.BDirectoryNumber;
import ma.iam.pws.soi.beans.contract.BPort;
import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.services.ClassServiceBean;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.engine.common.util.Utils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.SoiUtils;

import com.lhs.ccb.soi.types.DateI;
import com.lhs.ccb.soi.types.DateTimeI;
import com.lhs.ccb.soi.types.MoneyI;

public class BContractedServicesAdapter {

	
	private static List<BPort> buildPortsBean(HashMap<String , Object>[] paramsIn ) {
		List<BPort> lPorts = new ArrayList<BPort>() ;
		
		for (int i = 0; i <= paramsIn.length; i++) {
			BPort bPort = new BPort();
			bPort.setCOSP_PORTNUM(paramsIn[i].get("COSP_PORTNUM")!=null? paramsIn[i].get("COSP_PORTNUM").toString(): null );
			bPort.setCOSP_STATUS(paramsIn[i].get("COSP_STATUS")!=null? new Integer(paramsIn[i].get("COSP_STATUS").toString()) : 0 ) ; 	
			lPorts.add(bPort);
		}
		return lPorts;
	}
	
	private static List<BDirectoryNumber> buildDIRECTORY_NUMBERS(HashMap<String , Object>[] paramsIn ) {
		List<BDirectoryNumber> lDirecoryNumber = new ArrayList<BDirectoryNumber>() ;
		SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		
		for (int i = 0; i <= paramsIn.length; i++) {
			BDirectoryNumber bDn = new BDirectoryNumber();
			bDn.setDN_ID(paramsIn[i].get("DN_ID")!=null? new Long(paramsIn[i].get("DN_ID").toString()).longValue() : 0 );
			bDn.setDIRNUM(paramsIn[i].get("DIRNUM")!=null? paramsIn[i].get("DIRNUM").toString(): null );
			if (paramsIn[i].get("DN_ACTIVATION_DATE")!=null) {
				bDn.setDN_ACTIVATION_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)paramsIn[i].get("DN_ACTIVATION_DATE"), format));
			}
			if (paramsIn[i].get("DN_DEACTIVATION_DATE")!=null) {
				bDn.setDN_DEACTIVATION_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)paramsIn[i].get("DN_DEACTIVATION_DATE"), format));
			}
			lDirecoryNumber.add(bDn);
		}
		return lDirecoryNumber;
	}
	
	private static List<BCug_memberships> buildCUG_MEMBERSHIPS(HashMap<String , Object>[] paramsIn ) {
		List<BCug_memberships> lcug_memberships = new ArrayList<BCug_memberships>() ;
		
		for (int i = 0; i <= paramsIn.length; i++) {
			BCug_memberships bCug_memberships = new BCug_memberships();
			bCug_memberships.setCUG_ID(paramsIn[i].get("CUG_ID")!=null? new Long(paramsIn[i].get("CUG_ID").toString()).longValue() : 0 );
			bCug_memberships.setCUG_INDEX(paramsIn[i].get("CUG_INDEX")!=null? new Long(paramsIn[i].get("CUG_INDEX").toString()).longValue() : 0 );
			bCug_memberships.setCUG_INTERLOCK_CODE(paramsIn[i].get("CUG_INTERLOCK_CODE")!=null? paramsIn[i].get("CUG_INTERLOCK_CODE").toString(): null );
			bCug_memberships.setCUG_NAME(paramsIn[i].get("CUG_NAME")!=null? paramsIn[i].get("CUG_NAME").toString(): null );
			bCug_memberships.setCUG_PENDING_STATUS(paramsIn[i].get("CUG_PENDING_STATUS")!=null? new Integer(paramsIn[i].get("CUG_PENDING_STATUS").toString()) : 0 );
			lcug_memberships.add(bCug_memberships);
		}
		return lcug_memberships;
	}
	
	private static List<BBsg_configuration> buildBSG_CONFIGURATION(HashMap<String , Object>[] paramsIn ) {
		List<BBsg_configuration> lbsg_configuration = new ArrayList<BBsg_configuration>() ;
		
		for (int i = 0; i <= paramsIn.length; i++) {
			BBsg_configuration bsg_configuration = new BBsg_configuration();
			bsg_configuration.setBSG_DES(paramsIn[i].get("BSG_DES")!=null? paramsIn[i].get("BSG_DES").toString(): null );
			bsg_configuration.setBSG_ID(paramsIn[i].get("BSG_ID")!=null? new Long(paramsIn[i].get("BSG_ID").toString()).longValue() : 0 );
			bsg_configuration.setPREFERED_CUG_ID(paramsIn[i].get("PREFERED_CUG_ID")!=null? new Long(paramsIn[i].get("PREFERED_CUG_ID").toString()).longValue() : 0 );
			bsg_configuration.setPREFERED_CUG_NAME(paramsIn[i].get("PREFERED_CUG_NAME")!=null? paramsIn[i].get("PREFERED_CUG_NAME").toString(): null );
			lbsg_configuration.add(bsg_configuration);
		}
		return lbsg_configuration;
	}
	
	/* Pr�paration de la liste des Services UnContract� */
	public static void buildUnContractedServicesBean(
			HashMap<String, Object> outParams, Bcontract bcontract , List <Long>  allowedSncode  ) {
			List<BContractedServices> listUnContractedservices = new ArrayList<BContractedServices>();
			List<BContractedServices> listContractedservices = bcontract.getContractedServices();
			Set spcodeKey = outParams.keySet();
			Iterator itspcode = spcodeKey.iterator();
			while (itspcode.hasNext()){
					String spcode = itspcode.next().toString();
					HashMap<String, Object>[] typeByList = (HashMap<String, Object>[]) outParams.get(spcode); 
					for (int i = 0; i < typeByList.length; i++) {
						HashMap<String, Object> type = typeByList[i];
						BContractedServices bServices = new BContractedServices();
						//System.out.println( "spcode : "+ spcode + "type" + type);
						//System.out.println("type : UnContractedServices Package  "+ SoiUtils.CACHE_SERVICES_PACKAGES.get(String.valueOf(spcode)) + type );
						if (isContracted(type,listContractedservices) && allowedSncode.contains(new Long(type.get("SNCODE").toString()))){
							bServices.setSNCODE(type.get("SNCODE")!=null? new Long(type.get("SNCODE").toString()).longValue() : 0 );
							// MMA : fix ano Tig 9858 
							//System.out.println("TEST user_has_auth_class" + type.get("SNCODE") " SRVIND " +  type.get("SRVIND") +  " PDE_IMPLICIT_IND " + type.get("PDE_IMPLICIT_IND") );
							if (!SoiUtils.user_has_auth_class(bcontract.getRPCODE(),bServices.getSNCODE(),new Long(spcode).longValue()) ||
									//>******* Update, A.ESSA DI6717
									SoiUtils.is_service_clcode(bcontract.getRPCODE(), bServices.getSNCODE(), new Long(spcode).longValue(), 7))
									//<******* Update, A.ESSA DI6717
								continue;
							//System.out.println("TEST user_has_auth_class" + type.get("SNCODE"));
							if (((Boolean.TRUE.equals(type.get("PDE_IMPLICIT_IND")!=null? new Boolean(type.get("PDE_IMPLICIT_IND").toString()): null))))
						            continue;
						 //  System.out.println("SRVIND : " + type.get("SRVIND"));
							
							if (Constants.SRVINDS.EVENT_DRIVEN.equals(type.get("SRVIND")!=null?  type.get("SRVIND").toString(): null))
						            continue;

							bServices.setSNCODE_PUB(type.get("SNCODE_PUB")!=null? type.get("SNCODE_PUB").toString(): null );
							bServices.setSPCODE((new Long(spcode).longValue()));
							bServices.setSPCODE_PUB(type.get("SPCODE_PUB")!=null? type.get("SPCODE_PUB").toString(): null );
							bServices.setCS_ADVIND(type.get("ADVIND")!=null? type.get("ADVIND").toString(): null );
							bServices.setCS_PROIND(type.get("PROIND")!=null? type.get("PROIND").toString(): null );
							bServices.setSERVICE_DES(SoiUtils.CACHE_SERVICES.get(String.valueOf(bServices.getSNCODE())) );
							bServices.setSERVICE_PKG_DES(SoiUtils.CACHE_SERVICES_PACKAGES.get(String.valueOf(bServices.getSPCODE())));
							HashMap<String, Object>[] charges = (HashMap<String, Object> [])type.get("CHARGES");
							for(int j=0 ; j <charges.length ; j++ ){
								HashMap<String, Object> tmp_charges = (HashMap<String, Object> )charges[j];
							//	System.out.println("tmp_charges" + tmp_charges);
								if ( "3".equals(tmp_charges.get("CHARGE_TYPE_ID").toString())) {
									bServices.setCS_ACCESS(tmp_charges.get("CHARGE_AMOUNT")!=null? ((MoneyI)tmp_charges.get("CHARGE_AMOUNT")).amount:0);
								}else if ( "1".equals(tmp_charges.get("CHARGE_TYPE_ID").toString())) {
									bServices.setCS_SUBSCRIPTION(tmp_charges.get("CHARGE_AMOUNT")!=null? ((MoneyI)tmp_charges.get("CHARGE_AMOUNT")).amount:0);
								}
							}
							//System.out.println(SoiUtils.CACHE_SERVICES.get(String.valueOf(type.get("SNCODE"))));
							System.out.println(type.get("SNCODE"));
							listUnContractedservices.add(bServices);
						}
					}
						//listContractedservices
			}
			bcontract.setUnContractedServices(listUnContractedservices);
			bcontract.setContractedServices(listContractedservices);		
	}
	
	
	
	
	
	/* Pr�paration de la liste des Services Contract� */
	public static void buildContractedServicesBean(
			HashMap<String, Object> outParams, Bcontract bcontract) {

		// _logger.debug("Getting creator name ...");
		bcontract.setCO_ID(new Long(outParams.get("CO_ID").toString())
				.longValue());
		bcontract.setCO_ID_PUB(outParams.get("CO_ID_PUB").toString());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		
		if (outParams.containsKey("services")) {
			List<BContractedServices> listservices = new ArrayList<BContractedServices>();
			
			try {
				HashMap <String, Object> [] mapInfosServices= (HashMap <String, Object> []) outParams.get("services");
				// attach the bean services to bcontract				
				for (int i = 0; i < mapInfosServices.length; i++) {
						BContractedServices bServices = new BContractedServices();
                                                //>******* Update, A.ESSA DI6717
						String spcode = mapInfosServices[i].get("SPCODE").toString();
						if (SoiUtils.is_service_clcode(bcontract.getRPCODE(),(Long)mapInfosServices[i].get("SNCODE"),new Long(spcode).longValue(), 7))  continue;
                                                //<******* Update, A.ESSA DI6717
						//bServices.setADVANCE_ACCESS_OVW(mapInfosServices[i].get("ADVANCE_ACCESS_OVW")!=null? new Float(mapInfosServices[i].get("ADVANCE_ACCESS_OVW").toString()).floatValue() : 0 );
						//bServices.setADVANCE_ACCESS_OVW_PRD(mapInfosServices[i].get("ADVANCE_ACCESS_OVW_PRD")!=null? new Integer(mapInfosServices[i].get("ADVANCE_ACCESS_OVW_PRD").toString()) : 0 );
						bServices.setCS_ADVIND(mapInfosServices[i].get("ADVANCE_ACCESS_OVW_TYPE")!=null? mapInfosServices[i].get("ADVANCE_ACCESS_OVW_TYPE").toString(): null );
						if (mapInfosServices[i].containsKey("BSG_CONFIGURATION ")) {
							//TODO We should to build this bean if is not empty
							//bServices.setBSG_CONFIGURATION(buildBSG_CONFIGURATION((HashMap<String , Object>[]) mapInfosServices[i].get("BSG_CONFIGURATION")));
						}
						bServices.setCS_ACCESS(mapInfosServices[i].get("CALC_ACC")!=null? new Float(mapInfosServices[i].get("CALC_ACC").toString()).floatValue() : 0);
						//bServices.setCALC_ADVANCE_ACC(mapInfosServices[i].get("CALC_ADVANCE_ACC")!=null? new Float(mapInfosServices[i].get("CALC_ADVANCE_ACC").toString()).floatValue() : 0 );
						bServices.setCS_SUBSCRIPTION(mapInfosServices[i].get("CALC_SUB")!=null? new Float(mapInfosServices[i].get("CALC_SUB").toString()).floatValue() : 0 );
						//bServices.setCOS_ACCFEE_OVW(mapInfosServices[i].get("COS_ACCFEE_OVW")!=null? new Float(mapInfosServices[i].get("COS_ACCFEE_OVW").toString()).floatValue() : 0 );
						//bServices.setCOS_ACCFEE_OVW_PRD(mapInfosServices[i].get("COS_ACCFEE_OVW_PRD")!=null? new Integer(mapInfosServices[i].get("COS_ACCFEE_OVW_PRD").toString()) : 0 ) ; 		
						bServices.setCS_PROIND(mapInfosServices[i].get("COS_ACCFEE_OVW_TYPE")!=null? mapInfosServices[i].get("COS_ACCFEE_OVW_TYPE").toString(): null );
						//bServices.setCOS_CURRENCY(mapInfosServices[i].get("COS_CURRENCY")!=null? new Long(mapInfosServices[i].get("COS_CURRENCY").toString()).longValue() : 0 );
						//bServices.setADVANCE_ACCESS_OVW_TYPE(mapInfosServices[i].get("COS_CURRENCY_PUB")!=null? mapInfosServices[i].get("COS_CURRENCY_PUB").toString(): null );
						
						if (mapInfosServices[i].get("COS_DATE_BILLED")!=null) {
						//	bServices.setCOS_DATE_BILLED(Utils.converttoDatefromDateTimeI((DateTimeI)mapInfosServices[i].get("COS_DATE_BILLED"), format));
						}
						
						if (mapInfosServices[i].get("COS_LAST_ACT_DATE")!=null) {
							bServices.setCOS_LAST_ACT_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)mapInfosServices[i].get("COS_LAST_ACT_DATE"), format));
						}						

						bServices.setCOS_PENDING_STATUS(mapInfosServices[i].get("COS_PENDING_STATUS")!=null? new Integer(mapInfosServices[i].get("COS_PENDING_STATUS").toString()) : 0 ) ; 
						if (mapInfosServices[i].get("COS_PENDING_STATUS_DATE")!=null) {
							bServices.setCOS_PENDING_STATUS_DATE(Utils.converttoDatefromDateI((DateI)mapInfosServices[i].get("COS_PENDING_STATUS_DATE"), format));
						}
						//bServices.setCOS_QUANTITY(mapInfosServices[i].get("COS_QUANTITY")!=null? new Integer(mapInfosServices[i].get("COS_QUANTITY").toString()) : 0 ) ; 	
						bServices.setCOS_STATUS(mapInfosServices[i].get("COS_STATUS")!=null? new Integer(mapInfosServices[i].get("COS_STATUS").toString()) : 0 ) ; 	
						if (mapInfosServices[i].get("COS_STATUS_DATE")!=null) {
							bServices.setCOS_STATUS_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)mapInfosServices[i].get("COS_STATUS_DATE"), format));
						}
						//bServices.setCOS_SUBFEE_OVW(mapInfosServices[i].get("COS_SUBFEE_OVW")!=null? new Float(mapInfosServices[i].get("COS_SUBFEE_OVW").toString()).floatValue() : 0 );
						//bServices.setCOS_SUBFEE_OVW_TYPE(mapInfosServices[i].get("COS_SUBFEE_OVW_TYPE")!=null? mapInfosServices[i].get("COS_SUBFEE_OVW_TYPE").toString(): null );
						if (mapInfosServices[i].get("COS_TRIAL_END_DATE")!=null) {
						//	bServices.setCOS_TRIAL_END_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)mapInfosServices[i].get("COS_TRIAL_END_DATE"), format));
						}
						bServices.setCO_REQ_PENDING(mapInfosServices[i].get("CO_REQ_PENDING")!=null? new Long(mapInfosServices[i].get("CO_REQ_PENDING").toString()).longValue() : 0 );
						//bServices.setCS_PAYMENT_CONDITION_USG_IND(mapInfosServices[i].get("CS_PAYMENT_CONDITION_USG_IND")!=null? new Integer(mapInfosServices[i].get("CS_PAYMENT_CONDITION_USG_IND").toString()) : 0 ) ; 	
						//bServices.setCS_PREPAID_TP_ID(mapInfosServices[i].get("CS_PREPAID_TP_ID")!=null? new Long(mapInfosServices[i].get("CS_PREPAID_TP_ID").toString()).longValue() : 0 );
						//bServices.setCS_PREPAID_TP_ID_PUB(mapInfosServices[i].get("CS_PREPAID_TP_ID_PUB")!=null? mapInfosServices[i].get("CS_PREPAID_TP_ID_PUB").toString(): null );
						if (mapInfosServices[i].containsKey("CUG_MEMBERSHIPS")){
							// bServices.setCUG_MEMBERSHIPS
							//bServices.setCUG_MEMBERSHIPS(buildCUG_MEMBERSHIPS((HashMap<String , Object>[])  mapInfosServices[i].get("CUG_MEMBERSHIPS")));
						}							
						//bServices.setPARAMS_IND(mapInfosServices[i].get("PARAMS_IND")!=null? new Boolean(mapInfosServices[i].get("PARAMS_IND").toString()).booleanValue(): null );
						//bServices.setPROFILE_ID(mapInfosServices[i].get("PROFILE_ID")!=null? new Long(mapInfosServices[i].get("PROFILE_ID").toString()).longValue() : 0 );
						//bServices.setREASON(mapInfosServices[i].get("REASON")!=null? new Integer(mapInfosServices[i].get("REASON").toString()) : 0 ) ; 
						bServices.setSNCODE(mapInfosServices[i].get("SNCODE")!=null? new Long(mapInfosServices[i].get("SNCODE").toString()).longValue() : 0 );
						//bServices.setORIG_ACC(mapInfosServices[i].get("ORIG_ACC")!=null? new Float(mapInfosServices[i].get("ORIG_ACC").toString()).floatValue() : 0 );
						bServices.setSNCODE_PUB(mapInfosServices[i].get("SNCODE_PUB")!=null? mapInfosServices[i].get("SNCODE_PUB").toString(): null );
						bServices.setSPCODE(mapInfosServices[i].get("SPCODE")!=null? new Long(mapInfosServices[i].get("SPCODE").toString()).longValue() : 0 );
						bServices.setSPCODE_PUB(mapInfosServices[i].get("SPCODE_PUB")!=null? mapInfosServices[i].get("SPCODE_PUB").toString(): null );
						//bServices.setUSER_REASON(mapInfosServices[i].get("SPCODE")!=null? new Long(mapInfosServices[i].get("SPCODE").toString()).longValue() : 0 );
						if (mapInfosServices[i].containsKey("DIRECTORY_NUMBERS")){
							// bServices.setDIRECTORY_NUMBERS
							//bServices.setDIRECTORY_NUMBERS(buildDIRECTORY_NUMBERS((HashMap<String , Object>[])  mapInfosServices[i].get("DIRECTORY_NUMBERS")));
						}
						if (mapInfosServices[i].containsKey("PORTS"))	{
							//bServices.setPORTS(buildPortsBean((HashMap<String , Object>[])  mapInfosServices[i].get("PORTS")));
						}
									
						bServices.setSERVICE_DES(SoiUtils.CACHE_SERVICES.get(String.valueOf(bServices.getSNCODE())) );
						bServices.setSERVICE_PKG_DES(SoiUtils.CACHE_SERVICES_PACKAGES.get(String.valueOf(bServices.getSPCODE())));
			
						listservices.add(bServices);
				}
				bcontract.setContractedServices(listservices);
				
			} catch (Exception ex) {
				// TODO
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
			// _logger.debug("last modified user is " + _strUserModified);
		}
	}
	
	
	/* Si le service est contract� Ajout de param�tre  (ADVIND, PROIND)*/
	
	private static boolean isContracted(HashMap<String, Object> type , List<BContractedServices> listContractedservices) {
		for (int i = 0; i < listContractedservices.size(); i++) {
			BContractedServices btemp = listContractedservices.get(i);
			if(btemp.getSNCODE()== new Long(type.get("SNCODE").toString()).longValue()){
				listContractedservices.get(i).setCS_ADVIND(type.get("ADVIND")!=null? type.get("ADVIND").toString(): null );
				listContractedservices.get(i).setCS_PROIND(type.get("PROIND")!=null? type.get("PROIND").toString(): null );
				return false ;
			}
		}
		return true;
	}
	

	//>******* Update, A.ESSA DI6717
	/*private static boolean user_has_auth_class(long tmcode , long sncode , long spcode) {
		//Iterator< String  > iterUserClass = SoiUtils.CACHE_USER_CLASSES.iterator();
	//	System.out.println("SoiUtils.CACHE_SERVICES_CLASSES" + SoiUtils.CACHE_SERVICES_CLASSES);
		List internalCACHE_SERVICES_CLASSES = SoiUtils.CACHE_SERVICES_CLASSES;
		//while (iterUserClass.hasNext()) {
		     ClassServiceBean clSvc7 = new ClassServiceBean(sncode, spcode, tmcode, 7); 
		     ClassServiceBean clSvc8 = new ClassServiceBean(sncode, spcode, tmcode, 8); 
		     ClassServiceBean clSvc5 = new ClassServiceBean(sncode, spcode, tmcode, 5);  
		     ClassServiceBean clSvc6 = new ClassServiceBean(sncode, spcode, tmcode, 6);  
		     if (internalCACHE_SERVICES_CLASSES.contains(clSvc7) || internalCACHE_SERVICES_CLASSES.contains(clSvc8) || internalCACHE_SERVICES_CLASSES.contains(clSvc5) || internalCACHE_SERVICES_CLASSES.contains(clSvc6) ) {
		    	 	//if (clSvc.getClcode()== 5 || clSvc.getClcode()== 7 || clSvc.getClcode()== 8 ) {
		    	 //		System.out.println("sncode : " + clSvc.getSncode() + "clSvc..getClcode() : " + clSvc.getClcode());
		    	 		return false;		
		     }else
		    	 		return true; 
	}*/
	//<******* Update, A.ESSA DI6717

}