package ma.iam.pws.soi.engine.common.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;

import com.lhs.ccb.soi.types.DateI;
import com.lhs.ccb.soi.types.DateIHelper;
import com.lhs.ccb.soi.types.DateTimeI;

import ma.iam.pws.soi.engine.common.Constants;

/**
* The Class Utils : Classe Utilitaire.
*
* @author Atos Origin
*/
public final class Utils {

	/**
	 * Instantiates a new utils.
	 */
	private Utils() {

	}
	
	/**
	 * Null to blanc.
	 * 
	 * @param str
	 *            the str
	 * @return the string
	 */
	public static String nullToBlanc(String str) {
		return str == null ? "" : str.trim();
	}

	/**
	 * check if a string is empty.
	 * 
	 * @param s the s
	 * @return true, if checks if is empty
	 */
	public static boolean isEmpty(String s) {
		return s == null || s.equals("");
	} 

	/**
	 * Checks if is numeric.
	 * 
	 * @param s the s
	 * @return true, if is numeric
	 */
	public static boolean isNumeric(String s) {
		try {
			Long.parseLong(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Date to string.
	 * 
	 * @param date the date
	 * @return the string
	 */
	public static String dateToString(Date date) {
		return dateToString(date, Constants.FORMAT_DATE);
	}

	/**
	 * Date to string.
	 * 
	 * @param date the date
	 * @param dateFormat the date format
	 * @return the string
	 */
	public static String dateToString(Date date, String dateFormat) {

		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
			return simpleDateFormat.format(date);
		} catch (Exception e) {
			return null;
		}

	}
	
	/**
	 * Method that convert a String to Date Object according to default format.
	 * 
	 * @param sDate the s date
	 * @return Date
	 */
	public static Date stringToDate(String sDate) {
		return stringToDate(sDate, Constants.FORMAT_DATE);
	}

	/**
	 * Method that convert a String to Date Object according to default format.
	 * 
	 * @param sDate the s date
	 * @param format the format
	 * @return Date
	 */
	public static Date stringToDate(String sDate, String format) {

		Date date = null;
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		try {
			date = formatter.parse(sDate);
		} catch (Exception e) {
			return null;
		}
		return date;
	}
	
	/**
	 * Arrondir un nombre.
	 * 
	 * @param d : nombre à arrondir
	 * @param n : nombre de digits après la virgule
	 * @return the double
	 */
	public static Double round(double d, int n) {
		double result = d;
		int p = (int) Math.pow(10, n);
		result *= p;
		result = Math.round(result);
		result /= p;
		return result;
	}

	/**
	 * Round.
	 * 
	 * @param d the d
	 * @param n the n
	 * @return the float
	 */
	public static Float round(float d, int n) {
		float result = d;
		int p = (int) Math.pow(10, n);
		result *= p;
		result = Math.round(result);
		result /= p;
		return result;
	}
	
	/**
	 * Cette méthode permet d'arrondir un double.</br></br>
	 * @param d
	 * @return
	 */
	public static Double roundDown(Double d) {
		Double tmp = d*100;
		if (tmp.longValue() < tmp) {
			BigDecimal b = new BigDecimal(d);
			return b.setScale(2,BigDecimal.ROUND_DOWN).doubleValue();
		}
		return d;
	}
	
	//TODO
	
	public static Date converttoDatefromDateI (DateI orbDate,SimpleDateFormat frm) {
		try{
		orbDate.month=orbDate.month<12?++orbDate.month:1;	
		String resDate = orbDate.day + "/" + (orbDate.month<10? "0" + orbDate.month : orbDate.month)  + "/" + orbDate.year ; 
		frm.setLenient(false);
		   return frm.parse(resDate);
		}catch (Exception e) {
			// TODO: handle exception
		}	
		return null;
	}
	//TODO
	
	public static Date converttoDatefromDateTimeI (DateTimeI orbDate,SimpleDateFormat frm) {
		try{
	//	String _resDate = orbDate.day + "/" + (orbDate.month<10? "0" + orbDate.month : orbDate.month)  + "/" + orbDate.year ; 
		
			    Date date = new Date(orbDate.time);
			    frm.setLenient(false);
			    return date;		
		}catch (Exception e) {
			// TODO: handle exception
		}	
		return null;
	}
	
				 //Date converttoDatefromDateI (DateI orbDate,SimpleDateFormat frm) {
	public static DateI convertFromDateToDateI (Date date) {
		try{
		
			DateI orbDate = new DateI();
			orbDate.day = date.getDay();
			orbDate.year = date.getYear();		
			orbDate.month = date.getMonth();
			
		return 	orbDate;
			
		}catch (Exception e) {
			// TODO: handle exception
		}	
		return null;		
	}
	
	public static Date parseStringToDate(String date, String dateFormat) {// dateFormat="dd/MM/yyyy"
		Date formatedDate = null;
		try {
			formatedDate = new SimpleDateFormat(dateFormat).parse(date);
		} catch (ParseException ex) {
			return null;
		}
		return formatedDate;
	}
}
