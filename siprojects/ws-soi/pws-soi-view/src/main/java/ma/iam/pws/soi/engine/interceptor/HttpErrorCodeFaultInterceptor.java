package ma.iam.pws.soi.engine.interceptor;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.wss4j.common.ext.WSSecurityException;

/**
 * TODO comment.
 * <br/><br/>
 * Projet : Mutuamisation
 * Créé le 8 juil. 2011
 * 
 * @author mei
 */
public class HttpErrorCodeFaultInterceptor extends AbstractSoapInterceptor {
	
	public HttpErrorCodeFaultInterceptor() {
        super(Phase.MARSHAL);
        getAfter().add(HttpErrorCodeFaultInterceptor.class.getName());
    }

	public void handleMessage(SoapMessage message) throws Fault {
		Fault fault = (Fault) message.getContent(Exception.class);
		if (fault != null && fault.getCause() != null
				&& fault.getCause() instanceof WSSecurityException) {
			message.put(org.apache.cxf.message.Message.RESPONSE_CODE, new Integer(200));
		}

		
	}



}
