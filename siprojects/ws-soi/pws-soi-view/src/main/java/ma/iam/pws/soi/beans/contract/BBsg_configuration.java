package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;

public class BBsg_configuration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String BSG_DES;

	private long BSG_ID;

	private long PREFERED_CUG_ID;

	private String PREFERED_CUG_NAME;
	
	public static final String ALIAS = "BSG_CONFIGURATION";

	/**
	 * GET : Configured BSG description
	 * 
	 * @return
	 */
	public String getBSG_DES() {
		return BSG_DES;
	}

	/**
	 * SET : Configured BSG description
	 * 
	 * @param bSG_DES
	 */
	public void setBSG_DES(String bSG_DES) {
		BSG_DES = bSG_DES;
	}

	/**
	 * GET : Configured BSG id
	 * 
	 * @return
	 */
	public long getBSG_ID() {
		return BSG_ID;
	}

	/**
	 * SET : Configured BSG id
	 * 
	 * @param bSG_ID
	 */
	public void setBSG_ID(long bSG_ID) {
		BSG_ID = bSG_ID;
	}

	/**
	 * GET : Prefered CUG for this configred BSG, -1 for NONE
	 * 
	 * @return
	 */
	public long getPREFERED_CUG_ID() {
		return PREFERED_CUG_ID;
	}

	/**
	 * SET : Prefered CUG for this configred BSG, -1 for NONE
	 * 
	 * @param pREFERED_CUG_ID
	 */
	public void setPREFERED_CUG_ID(long pREFERED_CUG_ID) {
		PREFERED_CUG_ID = pREFERED_CUG_ID;
	}

	/**
	 * SET : Prefered CUG name
	 * 
	 * @return
	 */
	public String getPREFERED_CUG_NAME() {
		return PREFERED_CUG_NAME;
	}

	/**
	 * GET : Prefered CUG name
	 * 
	 * @param pREFERED_CUG_NAME
	 */
	public void setPREFERED_CUG_NAME(String pREFERED_CUG_NAME) {
		PREFERED_CUG_NAME = pREFERED_CUG_NAME;
	}

}
