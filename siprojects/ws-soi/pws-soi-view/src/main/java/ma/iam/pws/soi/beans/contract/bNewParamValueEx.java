package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
//@XmlType(name="Param" , propOrder = { "" })
public class bNewParamValueEx extends bNewParamValue {

	
	long PRM_NO;
	boolean MULT_VALUE_IND;
	String PRM_DES;
	String PRM_ID_PUB;
	private List<bNewValuesEx> BNewValueEx= new ArrayList<bNewValuesEx>();

	public List<bNewValuesEx> getBNewValueEx() {
		return BNewValueEx;
	}
	public void setBNewValueEx(List<bNewValuesEx> bNewValueEx) {
		BNewValueEx = bNewValueEx;
	}
	public boolean isMULT_VALUE_IND() {
		return MULT_VALUE_IND;
	}
	public void setMULT_VALUE_IND(boolean mULT_VALUE_IND) {
		MULT_VALUE_IND = mULT_VALUE_IND;
	}
	public String getPRM_DES() {
		return PRM_DES;
	}
	public void setPRM_DES(String pRM_DES) {
		PRM_DES = pRM_DES;
	}
	public String getPRM_ID_PUB() {
		return PRM_ID_PUB;
	}
	public void setPRM_ID_PUB(String pRM_ID_PUB) {
		PRM_ID_PUB = pRM_ID_PUB;
	}
	public long getPRM_NO() {
		return PRM_NO;
	}
	public void setPRM_NO(long pRM_NO) {
		PRM_NO = pRM_NO;
	}
	
	
}
