package ma.iam.pws.soi.engine.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * PasswordDigestInCallbackHandler.
 * <br/><br/>
 * Projet : Mutualisation
 * Créé le 5 juil. 2011
 * 
 * @author mei
 */
public class PasswordDigestInCallbackHandler implements CallbackHandler {


	/**
	 * (non javadoc)
	 * See @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[]).
	 * @param callbacks
	 * @throws IOException
	 * @throws UnsupportedCallbackException
	 */
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		 if (authentication != null && authentication.isAuthenticated()) {
			 UserDetails user = (UserDetails) authentication.getPrincipal();
			 if (user != null) {
				 pc.setPassword(user.getPassword());
				 pc.setIdentifier(user.getUsername());
			 }
		}
    }

}
