package ma.iam.pws.soi.adapter;


import java.text.SimpleDateFormat;
import java.util.HashMap;

import com.lhs.ccb.soi.types.DateTimeI;

import ma.iam.pws.soi.beans.contract.Bcontract;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.contract.ContractStatBean;
import ma.iam.pws.soi.engine.common.util.Utils;

public class BContractAdapterAll {

	public static BcontractAll getMainContractInfo(HashMap<String, Object> paramsOut) {
		BcontractAll reqContract = new BcontractAll();

		reqContract.setRPCODE(paramsOut.get("RPCODE") != null ? new Long(paramsOut.get("RPCODE").toString()).longValue() : 0);
		reqContract.setCS_ID(paramsOut.get("CS_ID") != null ? new Long(paramsOut.get("CS_ID").toString()).longValue() : 0);
		reqContract.setCS_ID_PUB(paramsOut.get("CS_ID_PUB") != null ? paramsOut.get("CS_ID_PUB").toString() : null);
		reqContract.setRPCODE_PUB(paramsOut.get("RPCODE_PUB") != null ? paramsOut.get("RPCODE_PUB").toString() : null);
		reqContract.setCO_ID(paramsOut.get("CO_ID") != null ? new Long(paramsOut.get("CO_ID").toString()).longValue() : null);
		return reqContract;
	}
	
	public static ContractStatBean getContractSatutInfo(HashMap<String, Object> paramsOut) {
		ContractStatBean reqstatContract = new ContractStatBean();
		SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		reqstatContract.setCO_ID(paramsOut.get("CO_ID")!=null? new Integer(paramsOut.get("CO_ID").toString()) : 0);
		reqstatContract.setCO_PENDING_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)paramsOut.get("CO_PENDING_DATE"), format));
		reqstatContract.setCO_PENDING_STATUS(paramsOut.get("CO_PENDING_STATUS")!=null? new Integer(paramsOut.get("CO_PENDING_STATUS").toString()) : 0);
		reqstatContract.setCO_STATUS(paramsOut.get("CO_STATUS")!=null? new Integer(paramsOut.get("CO_STATUS").toString()) : 0);
		reqstatContract.setCO_STATUS_DATE(Utils.converttoDatefromDateTimeI((DateTimeI)paramsOut.get("CO_STATUS_DATE"), format));
		return reqstatContract;
	}

}
