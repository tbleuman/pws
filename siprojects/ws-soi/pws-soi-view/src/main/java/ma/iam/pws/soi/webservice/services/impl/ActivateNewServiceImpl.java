package ma.iam.pws.soi.webservice.services.impl;

/**
 * @author Mo.Aissa
 *
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.xml.ws.WebServiceContext;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.pws.soi.adapter.ServiceParamAdapter;
import ma.iam.pws.soi.beans.contract.BcontractAll;
import ma.iam.pws.soi.beans.contract.bNewParamValueEx;
import ma.iam.pws.soi.beans.contract.bNewServices;
import ma.iam.pws.soi.beans.contract.bNewServicesEx;
import ma.iam.pws.soi.beans.contract.bNewValuesEx;
import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.common.Constants;
import ma.iam.pws.soi.common.Message;
import ma.iam.pws.soi.context.SoiContext;
import ma.iam.pws.soi.engine.security.PasswordUtils;
import ma.iam.pws.soi.service.SOIService;
import ma.iam.pws.soi.service.util.ServiceUtils;
import ma.iam.pws.soi.service.util.SoiUtils;
import ma.iam.pws.soi.webservice.services.ActivateNewService;
import ma.iam.security.SecuredObject;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import com.alcatel.ccl.client.CMSException;
import com.alcatel.ccl.common.Contract;

//@javax.jws.WebService
@SecuredObject
public class ActivateNewServiceImpl implements ActivateNewService {

	private static final Logger logger = LogManager.getLogger(ServiceOperaterImpl.class.getName());
	Contract contract = new Contract();
	ServiceUtils service = new ServiceUtils();
	@Resource
	WebServiceContext wsContext;

	private SoiContext soiContext;

	public final SoiContext getSoiContext() {
		return soiContext;
	}

	public final void setSoiContext(SoiContext soiContext) {
		this.soiContext = soiContext;
	}

	public void setWsContext(WebServiceContext wsContext) {
		this.wsContext = wsContext;
	}

	private Properties msgProperties;

	public final Properties getMsgProperties() {
		return msgProperties;
	}

	public final void setMsgProperties(Properties msgProperties) {
		this.msgProperties = msgProperties;
	}

	private String securityName;
	private String securityVersion;

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSecurityVersion() {
		return securityVersion;
	}

	public void setSecurityVersion(String securityVersion) {
		this.securityVersion = securityVersion;
	}

	private UserCache userCache = new NullUserCache();

	private String cilName;
	private String cilVersion;

	public String getCilName() {
		return cilName;
	}

	public void setCilName(String cilName) {
		this.cilName = cilName;
	}

	public String getCilVersion() {
		return cilVersion;
	}

	public void setCilVersion(String cilVersion) {
		this.cilVersion = cilVersion;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	@RolesAllowed("ROLE_WRITE")
	public List<bNewServicesEx> ActivateNewServiceImpl(String username, long co_id, List<bNewServices> Services)
			throws FaultWS {
		logger.info(getClass().toString() + " Activate Service : " + Services + " Contract : " + co_id);
		try {

			UserDetails user = userCache.getUserFromCache(wsContext.getUserPrincipal().getName());
			// wsContext.getMessageContext().

			// SOIService soiComm =
			// SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

			SOIService soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);

			if ((soiComm == null) || (soiComm != null && !soiComm.isConnectionOk())) {
				String password = null;
				try {
					String encryptedPass = PasswordUtils.getPassword(username);
					if (encryptedPass != null)
						password = PasswordUtils.decryptPassword(username, encryptedPass);
					else {
						throw new FaultWS(Constants.ERROR_CODE.MT_NOUSER_PASSWORDIO, msgProperties);
					}
				} catch (Exception e) {
					throw new FaultWS(Constants.ERROR_CODE.MT_PASSWORDIO, msgProperties);
				}

				if (SoiUtils.Authentify(wsContext, username, password, msgProperties, securityName, securityVersion,
						cilName, cilVersion, soiContext, userCache) == null)
					throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED, msgProperties);
			}

			// soiComm = SoiUtils.getCILSOIServiceByUser(wsContext,soiContext );

			soiComm = SoiUtils.getCILSOIServiceByUser2(username, soiContext);

			if (soiComm == null)
				throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED, msgProperties);

			List<bNewServicesEx> ServicesEx = new ArrayList<bNewServicesEx>();

			boolean result = false;
			if (co_id == 0 || Services == null || Services.size() == 0) {
				logger.error(
						getClass().toString() + "WS ActivateService : Erreur Input value Parameters CO_ID | Services");
				// throw new
				// FaultWS("RC1500","WS Activate Service : Erreur Input value
				// Parameters CO_ID | Services");
				throw new FaultWS(Constants.ERROR_CODE.MT_WS_ACTIVATESVC_INPUT_MISSING, msgProperties);
			}

			HashMap<String, Object> inParams = new HashMap<String, Object>();
			HashMap<String, Object> contractReadOutParams = new HashMap<String, Object>();
			HashMap<String, Object> outParamsCtrSrv = new HashMap<String, Object>();

			inParams.put("CO_ID", new Long(co_id).longValue());
			inParams.put("SYNC_WITH_DB", true);
			contractReadOutParams = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_READ, inParams);
			inParams.remove("SYNC_WITH_DB");
			contract.setCoId(new Long(co_id).longValue());
			Long rpCode = (Long) contractReadOutParams.get("RPCODE");
			contract.setRpCode(rpCode);

			// >******* Updated, A.ESSA ReactivateService
			inParams = new HashMap<String, Object>();
			inParams.put("CO_ID", new Long(co_id).longValue());
			outParamsCtrSrv = soiComm.executeCommand(SoiUtils.CMD_CONTRACT_SERVICES_READ, inParams);
			bNewServicesEx SrvEx = null;

			// >******* Updated, A.ESSA FC5937
			// >******* Updated, A.ESSA FC7045
			// check4GServices(Services,contract, soiComm);
			// <******* Updated, A.ESSA FC7045
			// <******* Updated, A.ESSA FC5937

			for (Iterator it = Services.iterator(); it.hasNext();) {
				try {
					bNewServices Srv = new bNewServices();
					SrvEx = new bNewServicesEx();
					Srv = (bNewServices) it.next();
					Long spCode = Srv.getSPCODE();
					Long snCode = Srv.getSNCODE();

					// >******* Updated, A.ESSA FC5937
					if ("IND4G".equals(ServiceUtils.getShdesFromSncode(Srv.getSNCODE(), soiComm))) {
						BcontractAll bAllContract = new BcontractAll();
						bAllContract.setCO_ID(contract.getCoId());
						// bAllContract.setCO_ID_PUB(bcontract.getCO_ID_PUB());
						String value_des = ServiceParamAdapter.GetValueDesServicefromParameters("ODDS3", bAllContract,
								soiComm, "NDMASTER");
						if (!ServiceUtils.searchContractMaster4G(value_des, soiComm)) {
							ArrayList params = new ArrayList();
							params.add(value_des);
							throw new FaultWS(Constants.ERROR_CODE.MT_WS_CHCKDPNDNCY_4G_MASTER_ERROR, msgProperties,
									params);
						}

					}
					// <******* Updated, A.ESSA FC5937

					// >******* Update, A.ESSA DI6717
					// Checking if the user has access to this service

					if (SoiUtils.is_service_clcode(rpCode, snCode, spCode.longValue(), 7)
							|| SoiUtils.is_service_clcode(rpCode, snCode, spCode.longValue(), 8)
							|| SoiUtils.is_service_clcode(rpCode, snCode, spCode.longValue(), 9)) {
						throw new FaultWS(Constants.ERROR_CODE.MT_WS_SERVICE_NOT_ATHORIZED, msgProperties);
					}
					// <******* Update, A.ESSA DI6717

					if (ServiceUtils.isAllowedToBeDeactivated(rpCode, snCode, soiComm)) {// A.ESSA:If
																							// allowed
																							// to
																							// be
																							// deactivated
																							// then
																							// allowed
																							// to
																							// be
																							// activated,
																							// so
																							// we
																							// use
																							// same
																							// method
						// Extend All Input Param
						SrvEx = ServiceUtils.getServiceParamValueExtends(SrvEx, Srv);
						SrvEx.setCO_ID(co_id);
						SrvEx.setRPCODE(rpCode);
						service.setSnCode(snCode);
						service.setSpCode(spCode);
						String srvStatus = ServiceUtils.getServiceStatus(service, contract, outParamsCtrSrv);

						// >******* Update, A.ESSA DI6717
						HashMap<String, List<String>> eligSrvPrm = SoiUtils.getFilteredPrmValues(soiComm, co_id, snCode,
								msgProperties, null, null, Services);

						for (Iterator ItPrm = SrvEx.getBNewParamValueEx().iterator(); ItPrm.hasNext();) {
							bNewParamValueEx pRmVal = (bNewParamValueEx) ItPrm.next();

							for (int nVal = 0; nVal < pRmVal.getBNewValueEx().size(); nVal++) {
								bNewValuesEx vaLue = pRmVal.getBNewValueEx().get(nVal);
								if (vaLue.getVALUE_DES() != null) {
									if (eligSrvPrm.get((new Long(pRmVal.getPRM_ID())).toString()) != null
											&& !eligSrvPrm.get((new Long(pRmVal.getPRM_ID())).toString())
													.contains(vaLue.getVALUE_DES())) {
										throw new FaultWS(Constants.ERROR_CODE.MT_WS_ACTIVATESVC_PRM_VAL_ERROR,
												msgProperties);
									}

								}
							}
						}

						// <******* Update, A.ESSA DI6717
						if (SoiUtils.SERVICE_STATUT_UNDEFINED.equals(srvStatus)) {
							// Add Service
							result = ServiceUtils.addServiceInContract(service, contract, false, soiComm, SrvEx,
									msgProperties, Services,null);
						} else if (SoiUtils.SERVICE_STATUT_DEACTIVE.equals(srvStatus)
								|| SoiUtils.SERVICE_STATUT_ON_HOLD.equals(srvStatus)) {
							// Activate Service
							result = ServiceUtils.activateServiceInContract(service, contract, false, soiComm, SrvEx,
									SoiUtils.SERVICE_STATUT_DEACTIVE, msgProperties, Services);
						} else if (SoiUtils.SERVICE_STATUT_ACTIVE.equals(srvStatus)) {
							// Service already activate
							SrvEx.setMsgError(Constants.ERROR_CODE.MT_WS_ACTIVATESVC_ACTIVE_SERVICE.concat(":")
									.concat(Message.getMessage(Constants.ERROR_CODE.MT_WS_ACTIVATESVC_ACTIVE_SERVICE,
											msgProperties)));
							ServicesEx.add(SrvEx);
						}

					} else {
						throw new FaultWS(Constants.ERROR_CODE.MT_WS_DEACTIVATESVC_NOTALLOWED, msgProperties);
					}
					if (result) {
						// soiComm.cmsClient.commit();
						ServicesEx.add(SrvEx);
					}
				} catch (CMSException cex) {
					// >******* Updated, A.ESSA QC12126
					if (soiComm != null) {
						soiComm.cmsClient.rollback();
					}
					// <******* Updated, A.ESSA QC12126
					if (cex.getErrors() != null && cex.getErrors().length > 0) {
						logger.error(
								getClass().toString() + " : CMSException " + cex.getErrors()[0].getAdditionalInfo());
						SrvEx.setMsgError(cex.getErrors()[0].getErrorCode().concat(": ")
								.concat(cex.getErrors()[0].getAdditionalInfo()));
						ServicesEx.add(SrvEx);
						// throw new
						// FaultWS(cex.getErrors()[0].getErrorCode(),cex.getErrors()[0].getAdditionalInfo());
					} else {
						if (cex.getMessage().contains("CMS client not connected")) {
							if (soiComm == null) {
								// >******* Updated, A.ESSA QC12126
								// soiComm.cmsClient.rollback();
								// <******* Updated, A.ESSA QC12126
								throw new FaultWS(Constants.ERROR_CODE.MT_USERUNLOGGED, msgProperties);
							}
						} else {
							logger.error(getClass().toString() + " : CMSException " + cex.getMessage());

							SrvEx.setMsgError("RC1000: ".concat(cex.getMessage()));
							ServicesEx.add(SrvEx);
							// throw new FaultWS("RC1000", cex.getMessage());
						}
					}
				} catch (Exception e) {
					soiComm.cmsClient.rollback();
					if (e instanceof FaultWS)
						throw (FaultWS) e;
					logger.error(getClass().toString() + " : Exception " + e.getMessage());
					e.printStackTrace();
					throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR, msgProperties);
				} finally {
					result = false;
				}
			}
			result = true;
			soiComm.cmsClient.commit();

			ServiceUtils.remove_Auto_params(soiComm, ServicesEx);

			return ServicesEx;
			// <******* Updated, A.ESSA ReactivateService

		} catch (CMSException cex) {

			logger.error(getClass().toString() + " : CMSException " + cex.getMessage());
			if (cex.getErrors() != null && cex.getErrors().length > 0) {
				logger.error(getClass().toString() + " : CMSException " + cex.getErrors()[0].getAdditionalInfo());
				throw new FaultWS(cex.getErrors()[0].getErrorCode(), cex.getErrors()[0].getAdditionalInfo());
			} else {
				if (cex.getMessage().contains("CMS client not connected")) {
					throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR, msgProperties);
				} else {
					logger.error(getClass().toString() + " : CMSException " + cex.getMessage());
					throw new FaultWS("RC1000", cex.getMessage());
				}
			}
		} catch (Exception e) {
			if (e instanceof FaultWS)
				throw (FaultWS) e;
			logger.error(getClass().toString() + " : Exception " + e.getMessage());
			e.printStackTrace();
			throw new FaultWS(Constants.ERROR_CODE.MT_INTERNALERROR, msgProperties);
		}
	}

	// >******* Updated, A.ESSA FC5937
	// >******* Updated, A.ESSA FC7045
	/*
	 * public void check4GServices(List <bNewServices> Services, Contract
	 * contract, SOIService soiComm) throws CMSException, FaultWS {
	 * 
	 * boolean serv4GExists = false; boolean servSDMExists = false; Long
	 * spCodeSDM = null; Long snCodeSDM = null;
	 * 
	 * for (Iterator it = Services.iterator(); it.hasNext();) { bNewServices Srv
	 * = new bNewServices(); Srv = (bNewServices) it.next(); Long snCode =
	 * Srv.getSNCODE();
	 * 
	 * if(",IN4FA,INT4G,INP4G,IND4G,".contains(",".concat(ServiceUtils.
	 * getShdesFromSncode(snCode,soiComm)).concat(","))){ serv4GExists = true;
	 * spCodeSDM = Srv.getSPCODE(); } else
	 * if("SDM4G".equals(ServiceUtils.getShdesFromSncode(snCode,soiComm))){
	 * servSDMExists = true; snCodeSDM = snCode; }
	 * 
	 * 
	 * } if(servSDMExists && !serv4GExists){ throw new
	 * FaultWS(ma.iam.ws.common.Constants.ERROR_CODE.
	 * MT_WS_CHCKDPNDNCY_SDM_ACTIVATION_ERROR,msgProperties); } else
	 * if(!servSDMExists && serv4GExists){ HashMap<String, Object> inParams =
	 * new HashMap<String, Object>(); ServiceUtils serviceSDM = new
	 * ServiceUtils(); if(snCodeSDM==null){ snCodeSDM =
	 * Long.parseLong(ServiceUtils.getSncodeFromShdes("SDM4G", soiComm)); }
	 * boolean isActive = false; try{ serviceSDM.setSnCode(snCodeSDM);
	 * inParams.put("CO_ID", contract.getCoId()); inParams.put("SNCODE",
	 * snCodeSDM); HashMap<String, Object > outParams =
	 * soiComm.executeCommand(SoiUtils.CMD_CONTRACT_SERVICES_READ,inParams);
	 * HashMap<String, Object>[] inDeactivateService = (HashMap<String,
	 * Object>[]) outParams.get("services"); isActive =
	 * ServiceUtils.isServiceWithStatus(serviceSDM,
	 * contract,inDeactivateService[0], SoiUtils.SERVICE_STATUT_ACTIVE); }
	 * catch(Exception e){
	 * 
	 * } if (!isActive){ bNewServices SrvSDM = new bNewServices();
	 * SrvSDM.setSNCODE(snCodeSDM); SrvSDM.setSPCODE(spCodeSDM);
	 * Services.add(SrvSDM); } }
	 * 
	 * }
	 */
	// >******* Updated, A.ESSA FC7045

	@Override
	public Integer modeByPass() {

		Integer rb = -1;
		try {
			SOIService soiComm = new SOIService(securityName, securityVersion);
			soiComm.login("TTSUSER", "TSTPWD");
			HashMap<String, Object> outParams = soiComm.executeCommand(SoiUtils.CMD_CHECk_SOI_RESPONSE, null);
			return 1;
		} catch (CMSException cex) {
			if (cex.getMessage().contains("CMS client not connected")
					|| cex.getMessage().contains("CMS connection error : Utilisateur ou mot de passe incorrect")) {
				return 1;
			}

			return rb;
		} catch (Exception e) {
			logger.error(getClass().toString() + " : Exception " + e.getMessage());

			return rb;
		}

	}
}
