package ma.iam.pws.soi.webservice.plaforme;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ma.iam.pws.soi.beans.errors.FaultWS;
import ma.iam.pws.soi.beans.plateforme.ResultBean;

@WebService(targetNamespace="http://plaforme.webservice.ws.iam.ma/")
public interface CheckSoiComm {

	@WebMethod
	ResultBean CheckSoiComm() throws FaultWS;

}
