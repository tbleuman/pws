package ma.iam.pws.soi.service.util;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
@XmlType(name="Services" , propOrder = { "sncode", "dependency_ids"})
public class Dependency  implements Serializable {

	/**
	 * 
	 */
	private Long sncode;
	private List<Long> dependency_ids;
	
	public Dependency(){
		super();
	}
	
	public Dependency(Long sncode, List dependency_ids) {
		super();
		this.sncode = sncode;
		this.dependency_ids = dependency_ids;
	}
	
	public Long getSncode() {
		return sncode;
	}
	
	public void setSncode(Long sncode) {
		this.sncode = sncode;
	}

	public List<Long> getDependency_ids() {
		return dependency_ids;
	}

	public void setDependency_ids(List<Long> dependency_ids) {
		this.dependency_ids = dependency_ids;
	}
	
}
