package ma.iam.pws.soi.exception;



/**
 * The Class FunctionalException : Gestion des exceptions fonctionnelles.
 *
 * @author Atos Origin
 * @version 1.0
 */
public class FunctionalException extends BaseException {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7981637821409421377L;
	
	/** The params. */
	private Object[] params = new Object[0];

	/**
	 * Instantiates a new functional exception.
	 * @param messageKey the message key
	 */
	public FunctionalException(String messageKey) {
		super(messageKey);
	}
	
	/**
	 * Instantiates a new functional exception.
	 *
	 * @param messageKey the message key
	 * @param params the params
	 */
	public FunctionalException(String messageKey, Object[] params) {
		super(messageKey);
		this.params = new Object[params.length];
		System.arraycopy(params, 0, this.params, 0, params.length);
	}

	/**
	 * Gets the params.
	 *
	 * @return the params
	 */
	public Object[] getParams() {
		return params;
	}
	
	
}
