package ma.iam.pws.soi.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import ma.iam.pws.soi.service.util.SoiUtils;

/**
 * TServlet lancer au démarrage pour charger en cache les données de référence.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class LoadReferenceDataServlet extends HttpServlet {
	
	
	/** serial Version UID */
	private static final long serialVersionUID = 3529113281163331876L;
	
	
	/** Instance du LOGGER */
	//private static final GEVELogger LOGGER = LogManager.getInstance().getLogger(Constantes.TECHNICAL_LOGGER_NAME);

	/**
	 * Instantiates a new export servlet.
	 */
	public LoadReferenceDataServlet() {
		super();
	}

	
//#################################################################################	
	/**
	 * lance la recherche des données de référence.
	 * See @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig).
	 * @param config
	 * @throws ServletException
	 */
	public void init(ServletConfig config) throws ServletException {
		//try {
		//	WSContextManager contextManager = WSContextManager.getInstance();
			
			/*ParametrageMngt parametrageMngt = (ParametrageMngt) contextManager.getBean(Constantes.PARAMETRAGE_MNGT_BEAN);
			parametrageMngt.getListPayModeCategory();
			parametrageMngt.getListOpTrxType();
			parametrageMngt.getListTypeCarte();
			parametrageMngt.getListOpCodes();*/
			
			/*ConsultationPaiementMngt consultationPaiementMngt = (ConsultationPaiementMngt) contextManager.getBean(Constantes.CONSULTAION_PAIEMENT_MNGT_BEAN);
			consultationPaiementMngt.getAllRaisonAnnulation();
			consultationPaiementMngt.getAllRaisonejet();*/
			
			/*PermissionMngt permissionMngt = (PermissionMngt) contextManager.getBean(Constantes.PERMISSION_MNT_BEAN);
			permissionMngt.getListPermission();*/
			
			/*ConsultationBankMngt consultationBankMngt = (ConsultationBankMngt) contextManager.getBean(Constantes.CONSULTAION_BANK_MNGT_BEAN);
			consultationBankMngt.getListBankGe();*/
			
			/**
			 * initialize CMS properties
			 */
	/*		try {
				//SoiUtils. = getProperties();
			} catch (IOException e1) {
				throw new ServletException(
						"Error when loading build.properties : ", e1);
			}

		} catch (Exception e) {
			//LOGGER.error(this.getClass(), "service", e.getMessage(), e);
		}*/
	}
	
//#################################################################################		
	/**
	 * get properties
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Properties getProperties() throws IOException {

		Class thisClass = getClass();
		ClassLoader theClassLoader = thisClass.getClassLoader();
		InputStream inputStream = theClassLoader
				.getResourceAsStream("ressource/build.properties");

		Properties properties = new Properties();
		properties.load(inputStream);
		return properties;
	}
//#################################################################################	
}
