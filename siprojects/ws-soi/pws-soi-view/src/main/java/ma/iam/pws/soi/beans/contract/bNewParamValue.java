package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

//TODO
//@XmlType(name="Param" , propOrder = { "" })
public class bNewParamValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private long PRM_ID;
	//private long SCCODE;

	private List<bNewValues> bNewValues= new ArrayList<bNewValues>();
	
	
	
	public List<bNewValues> getbNewValues() {
		return bNewValues;
	}
	public void setbNewValues(List<bNewValues> bNewValues) {
		this.bNewValues = bNewValues;
	}
	public long getPRM_ID() {
		return PRM_ID;
	}
	public void setPRM_ID(long pRM_ID) {
		PRM_ID = pRM_ID;
	}

}
