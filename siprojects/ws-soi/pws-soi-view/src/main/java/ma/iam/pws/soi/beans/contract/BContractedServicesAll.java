package ma.iam.pws.soi.beans.contract;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Aissa
 *
 */

public class BContractedServicesAll implements Serializable {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ALIAS = "SERVICES";
	
	private double CS_SUBSCRIPTION ;
	private double CS_ACCESS ;
	private String CS_PROIND;
	@Override
	public String toString() {
		return "BContractedServices [SPCODE=" + SPCODE + ", SNCODE=" + SNCODE
				+ ", SNCODE_PUB=" + SNCODE_PUB + ", SERVICE_DES=" + SERVICE_DES
				+ ", SERVICE_PKG_DES=" + SERVICE_PKG_DES + "]";
	}
	private String CS_ADVIND;
	private long SPCODE;
	private String SPCODE_PUB;
	private long SNCODE;
	private Integer COS_STATUS;
	private Date COS_STATUS_DATE;
	private Integer COS_PENDING_STATUS;
	private Date COS_PENDING_STATUS_DATE;
	private Date COS_LAST_ACT_DATE;
	private long CO_REQ_PENDING;
	private String SNCODE_PUB;
	private String SERVICE_DES ;
	private String SERVICE_PKG_DES;
	private Integer CO_SN_STATUS;
	
	
//	private float ADVANCE_ACCESS_OVW;
//	private Integer ADVANCE_ACCESS_OVW_PRD;
//	private String ADVANCE_ACCESS_OVW_TYPE;;
//	private List<BBsg_configuration> BSG_CONFIGURATION;
//	private float CALC_ACC;
//	private float CALC_ADVANCE_ACC;
//	private float CALC_SUB;
//	private float COS_ACCFEE_OVW;
//	private Integer COS_ACCFEE_OVW_PRD;
//	private String COS_ACCFEE_OVW_TYPE;
//	private long COS_CURRENCY;
//	private String COS_CURRENCY_PUB;
//	private Date COS_DATE_BILLED;
//
//	private Integer COS_QUANTITY;
//	private float COS_SUBFEE_OVW;
//	private String COS_SUBFEE_OVW_TYPE;
//	private Date COS_TRIAL_END_DATE;
//	private Integer CS_PAYMENT_CONDITION_USG_IND;
//	private long CS_PREPAID_TP_ID;
//	private String CS_PREPAID_TP_ID_PUB;
//	private List<BCug_memberships> CUG_MEMBERSHIPS;
//	private boolean PARAMS_IND;
//	private long PROFILE_ID;
//	private Integer REASON;
//	private float ORIG_ACC;

//	private long USER_REASON;
//	private List<BDirectoryNumber> DIRECTORY_NUMBERS;
//	private List<BPort> PORTS;



	public String getSERVICE_DES() {
		return SERVICE_DES;
	}

	public void setSERVICE_DES(String sERVICE_DES) {
		SERVICE_DES = sERVICE_DES;
	}

	public String getSERVICE_PKG_DES() {
		return SERVICE_PKG_DES;
	}

	public void setSERVICE_PKG_DES(String sERVICE_PKG_DES) {
		SERVICE_PKG_DES = sERVICE_PKG_DES;
	}

	public String getCS_ADVIND() {
		return CS_ADVIND;
	}

	public void setCS_ADVIND(String cS_ADVIND) {
		CS_ADVIND = cS_ADVIND;
	}

	public double getCS_ACCESS() {
		return CS_ACCESS;
	}

	public double getCS_SUBSCRIPTION() {
		return CS_SUBSCRIPTION;
	}

	public void setCS_SUBSCRIPTION(double cS_SUBSCRIPTION) {
		CS_SUBSCRIPTION = cS_SUBSCRIPTION;
	}

	public void setCS_ACCESS(double cS_ACCESS) {
		CS_ACCESS = cS_ACCESS;
	}

	public String getCS_PROIND() {
		return CS_PROIND;
	}

	public void setCS_PROIND(String cS_PROIND) {
		CS_PROIND = cS_PROIND;
	}
	//	/**
//	 * Get Advance recurring charge (formerly: access fee)
//	 * @return
//	 */
//	public float getADVANCE_ACCESS_OVW() {
//		return ADVANCE_ACCESS_OVW;
//	}
//
//	/**
//	 * Set Advance recurring charge (formerly: access fee)
//	 * 
//	 * @param aDVANCE_ACCESS_OVW
//	 * 
//	 */
//	public void setADVANCE_ACCESS_OVW(float aDVANCE_ACCESS_OVW) {
//		ADVANCE_ACCESS_OVW = aDVANCE_ACCESS_OVW;
//	}
//	
//	/**
//	 * Get Number of billing cycles to bill the modified advance recurring charge (formerly: 
//	 * @return
//	 */
//
//	public Integer getADVANCE_ACCESS_OVW_PRD() {
//		return ADVANCE_ACCESS_OVW_PRD;
//	}
//
//	/**
//	 * Set Number of billing cycles to bill the modified advance recurring charge
//	 * (formerly: access fee)
//	 * 
//	 * @param aDVANCE_ACCESS_OVW_PRD
//	 */
//	public void setADVANCE_ACCESS_OVW_PRD(Integer aDVANCE_ACCESS_OVW_PRD) {
//		ADVANCE_ACCESS_OVW_PRD = aDVANCE_ACCESS_OVW_PRD;
//	}
//	/**
//	 * Get
//		Type of the advanced access fee amount:
//		
//		    "N" -> advance recurring charge has to be taken from the rate plan price. No change relatives to rate plan price has to be made
//		    "A" -> advance recurring charge (ADVANCE_ACCESS_OVW) is an absolute (lump sum) amount
//		    "R" -> advance recurring charge (ADVANCE_ACCESS_OVW) is a relative (percentage) amount
//
//	 * @return
//	 */
//	public String getADVANCE_ACCESS_OVW_TYPE() {
//		return ADVANCE_ACCESS_OVW_TYPE;
//	}
//
//	/**
//	 * Set
//		Type of the advanced access fee amount:
//		
//		    "N" -> advance recurring charge has to be taken from the rate plan price. No change relatives to rate plan price has to be made
//		    "A" -> advance recurring charge (ADVANCE_ACCESS_OVW) is an absolute (lump sum) amount
//		    "R" -> advance recurring charge (ADVANCE_ACCESS_OVW) is a relative (percentage) amount
//
//	 * @return
//	 */
//	public void setADVANCE_ACCESS_OVW_TYPE(String aDVANCE_ACCESS_OVW_TYPE) {
//		ADVANCE_ACCESS_OVW_TYPE = aDVANCE_ACCESS_OVW_TYPE;
//	}
//
//	public List<BBsg_configuration> getBSG_CONFIGURATION() {
//		return BSG_CONFIGURATION;
//	}
//
//	public void setBSG_CONFIGURATION(List<BBsg_configuration> bSG_CONFIGURATION) {
//		BSG_CONFIGURATION = bSG_CONFIGURATION;
//	}
//
//	public float getCALC_ACC() {
//		return CALC_ACC;
//	}
//
//	/**
//	 * Set 
//	 * Calculated Recurring charge (formerly: access fee) considering the service charge period and the billing cycle. The value equals to ORIG_ACC if no overwritten recurring charge is defined.
//	 * @param cALC_ACC
//	 */
//	public void setCALC_ACC(float cALC_ACC) {
//		CALC_ACC = cALC_ACC;
//	}
//	/**
//	 * Get 
//	 * Calculated Recurring charge (formerly: access fee) considering the service charge period and the billing cycle. The value equals to ORIG_ACC if no overwritten recurring charge is defined.
//	 * @return
//	 */
//	public float getCALC_ADVANCE_ACC() {
//		return CALC_ADVANCE_ACC;
//	}
//	/**
//	 * SET
//	 * Calculated advance recurring charge (formerly: access fee) considering the service charge period and the billing cycle.
//	 * @param cALC_ADVANCE_ACC
//	 */
//	public void setCALC_ADVANCE_ACC(float cALC_ADVANCE_ACC) {
//		CALC_ADVANCE_ACC = cALC_ADVANCE_ACC;
//	}
//	/**
//	 * GET
//	 * Calculated one-time charge (formerly: subscription fee).
//	 * @return
//	 */
//	public float getCALC_SUB() {
//		return CALC_SUB;
//	}
//	/**
//	 * SET
//	 * Calculated one-time charge (formerly: subscription fee).
//	 * @param cALC_SUB
//	 */
//	public void setCALC_SUB(float cALC_SUB) {
//		CALC_SUB = cALC_SUB;
//	}
//	/**
//	 * GET
//	 * Recurring charge (formerly: access fee). If the value of the column PROFILE_SERVICE.ACCESSFEE is null in the database, the default value for the pricing element will be used
//	 * @return
//	 */
//	public float getCOS_ACCFEE_OVW() {
//		return COS_ACCFEE_OVW;
//	}
//	/**
//	 * SET
//	 * Recurring charge (formerly: access fee). If the value of the column PROFILE_SERVICE.ACCESSFEE is null in the database, the default value for the pricing element will be used
//	 * @param cOS_ACCFEE_OVW
//	 */
//	public void setCOS_ACCFEE_OVW(float cOS_ACCFEE_OVW) {
//		COS_ACCFEE_OVW = cOS_ACCFEE_OVW;
//	}
//	/**
//	 * GET
//	 * Number od billing cycles to bill the modified recurring charge (formerly: access fee)
//	 *	-1 means indefinitly
//	 * @return
//	 */
//	public Integer getCOS_ACCFEE_OVW_PRD() {
//		return COS_ACCFEE_OVW_PRD;
//	}
//	/**
//	 * SET
//	 * 	 Number od billing cycles to bill the modified recurring charge (formerly: access fee)
//	 *	-1 means indefinitly
//	 * @param cOS_ACCFEE_OVW_PRD
//	 */
//	public void setCOS_ACCFEE_OVW_PRD(Integer cOS_ACCFEE_OVW_PRD) {
//		COS_ACCFEE_OVW_PRD = cOS_ACCFEE_OVW_PRD;
//	}
//	/**
//	 * GET
//	 * 
//		Type of the recurring charge (formerly: access fee):
//    		"A" -> Recurring charge (COS_ACCFEE_OVW) is an absolute (lump sum) amount
//    		"R" -> Recurring charge (COS_ACCFEE_OVW) is a relative (percentage) amount
//		Note: If there is no value set in the database, the type should be "A"
//	 * @return
//	 */
//	
//	public String getCOS_ACCFEE_OVW_TYPE() {
//		return COS_ACCFEE_OVW_TYPE;
//	}
//	/**
//	 * SET
//	 * 
//		Type of the recurring charge (formerly: access fee):
//    		"A" -> Recurring charge (COS_ACCFEE_OVW) is an absolute (lump sum) amount
//    		"R" -> Recurring charge (COS_ACCFEE_OVW) is a relative (percentage) amount
//		Note: If there is no value set in the database, the type should be "A"
//	 * @param cOS_ACCFEE_OVW_TYPE
//	 */
//	public void setCOS_ACCFEE_OVW_TYPE(String cOS_ACCFEE_OVW_TYPE) {
//		COS_ACCFEE_OVW_TYPE = cOS_ACCFEE_OVW_TYPE;
//	}
//	/**
//	 * GET
//	 * Currency of the service
//	 * @return
//	 */
//	public long getCOS_CURRENCY() {
//		return COS_CURRENCY;
//	}
//	/**
//	 * SET
//	 * Currency of the service
//	 * @param cOS_CURRENCY
//	 */
//	public void setCOS_CURRENCY(long cOS_CURRENCY) {
//		COS_CURRENCY = cOS_CURRENCY;
//	}
//	/**
//	 * GET 
//	 * Public key of the currency
//	 * @return
//	 */
//	public String getCOS_CURRENCY_PUB() {
//		return COS_CURRENCY_PUB;
//	}
//	/**
//	 * SET
//	 * Public key of the currency
//	 * @param cOS_CURRENCY_PUB
//	 */
//	public void setCOS_CURRENCY_PUB(String cOS_CURRENCY_PUB) {
//		COS_CURRENCY_PUB = cOS_CURRENCY_PUB;
//	}
//	/**
//	 * GET
//	 * Last bill date
//	 * @return
//	 */
//	public Date getCOS_DATE_BILLED() {
//		return COS_DATE_BILLED;
//	}
//	/**
//	 * SET
//	 * Last bill date
//	 * @param cOS_DATE_BILLED
//	 */
//	public void setCOS_DATE_BILLED(Date cOS_DATE_BILLED) {
//		COS_DATE_BILLED = cOS_DATE_BILLED;
//	}
	/**
	 * GET
	 * Last activation date
	 * @return
	 */
	public Date getCOS_LAST_ACT_DATE() {
		return COS_LAST_ACT_DATE;
	}
	/**
	 * SET
	 * Last activation date
	 * @param cOS_LAST_ACT_DATE
	 */
	public void setCOS_LAST_ACT_DATE(Date cOS_LAST_ACT_DATE) {
		COS_LAST_ACT_DATE = cOS_LAST_ACT_DATE;
	}

	/**
	 * GET
	 * 
		Pending status of the service
		
		    0 = Undefined
		    1 = OnHold
		    2 = Active
		    3 = Suspended
		    4 = Deactive
		    5 = Invisible
	 * @return
	 */
	public Integer getCOS_PENDING_STATUS() {
		return COS_PENDING_STATUS;
	}
	/**
	 * SET
	 * 

		Pending status of the service
		
		    0 = Undefined
		    1 = OnHold
		    2 = Active
		    3 = Suspended
		    4 = Deactive
		    5 = Invisible
		

	 * @param cOS_PENDING_STATUS
	 */
	public void setCOS_PENDING_STATUS(Integer cOS_PENDING_STATUS) {
		COS_PENDING_STATUS = cOS_PENDING_STATUS;
	}
	/**
	 * GET
	 *  Pending status date of the service
	 * @return
	 */
	public Date getCOS_PENDING_STATUS_DATE() {
		return COS_PENDING_STATUS_DATE;
	}
	/**
	 * SET
	 *  Pending status date of the service
	 * @param cOS_PENDING_STATUS_DATE
	 */
	public void setCOS_PENDING_STATUS_DATE(Date cOS_PENDING_STATUS_DATE) {
		COS_PENDING_STATUS_DATE = cOS_PENDING_STATUS_DATE;
	}
//	/**
//	 * GET
//	 *  Quantity for selling goods VAS
//	 * @return
//	 */
//	public Integer getCOS_QUANTITY() {
//		return COS_QUANTITY;
//	}
//	/**
//	 * SET
//	 *  Quantity for selling goods VAS
//	 * @param cOS_QUANTITY
//	 */
//	public void setCOS_QUANTITY(Integer cOS_QUANTITY) {
//		COS_QUANTITY = cOS_QUANTITY;
//	}
	/**
	 * GET
			
			Status of the service.
			
			    0 = Undefined
			    1 = OnHold
			    2 = Active
			    3 = Suspended
			    4 = Deactive
			    5 = Invisible
			

	 * @return
	 */
	public Integer getCOS_STATUS() {
		return COS_STATUS;
	}
	/**
	 * SET
	 * 
		Status of the service.
		
		    0 = Undefined
		    1 = OnHold
		    2 = Active
		    3 = Suspended
		    4 = Deactive
		    5 = Invisible

	 * @param cOS_STATUS
	 */
	public void setCOS_STATUS(Integer cOS_STATUS) {
		COS_STATUS = cOS_STATUS;
	}
	/**
	 * GET
	 * Valid from date of the current status
	 * @return
	 */
	public Date getCOS_STATUS_DATE() {
		return COS_STATUS_DATE;
	}
	/**
	 * SET
	 * Valid from date of the current status
	 * @param cOS_STATUS_DATE
	 */
	public void setCOS_STATUS_DATE(Date cOS_STATUS_DATE) {
		COS_STATUS_DATE = cOS_STATUS_DATE;
	}
//	/**
//	 * get
//	 * One-time charge (formerly: subscription fee)
//	 * @return
//	 */
//	public float getCOS_SUBFEE_OVW() {
//		return COS_SUBFEE_OVW;
//	}
//	/**
//	 * set
//	 * One-time charge (formerly: subscription fee)
//	 * @param cOS_SUBFEE_OVW
//	 */
//	public void setCOS_SUBFEE_OVW(float cOS_SUBFEE_OVW) {
//		COS_SUBFEE_OVW = cOS_SUBFEE_OVW;
//	}
	/**
	 * GET
			Type of the one-time charge (formerly: subscription fee):
			
			    "A" -> One-time charge (COS_SUBFEE_OVW) is an absolute (lump sum) amount
			    "R" -> One-time charge (COS_SUBFEE_OVW) is a relative (percentage) amount
			
			Note: If there is no value set in the database, the type should be "A"

	 * @return
	 */
//	public String getCOS_SUBFEE_OVW_TYPE() {
//		return COS_SUBFEE_OVW_TYPE;
//	}
//	/**
//	 * SET
//		Type of the one-time charge (formerly: subscription fee):
//		
//		    "A" -> One-time charge (COS_SUBFEE_OVW) is an absolute (lump sum) amount
//		    "R" -> One-time charge (COS_SUBFEE_OVW) is a relative (percentage) amount
//		
//		Note: If there is no value set in the database, the type should be "A"
//
//	 * @param cOS_SUBFEE_OVW_TYPE
//	 */
//	public void setCOS_SUBFEE_OVW_TYPE(String cOS_SUBFEE_OVW_TYPE) {
//		COS_SUBFEE_OVW_TYPE = cOS_SUBFEE_OVW_TYPE;
//	}
//	/**
//	 * GET
//	 * Service trial end date
//	 * @return
//	 */
//	public Date getCOS_TRIAL_END_DATE() {
//		return COS_TRIAL_END_DATE;
//	}
//	/**
//	 * SET
//	 * Service trial end date
//	 * @param cOS_TRIAL_END_DATE
//	 */
//	public void setCOS_TRIAL_END_DATE(Date cOS_TRIAL_END_DATE) {
//		COS_TRIAL_END_DATE = cOS_TRIAL_END_DATE;
//	}
	/**
	 * GET : Request id of service pending request
	 * @return
	 */

	public long getCO_REQ_PENDING() {
		return CO_REQ_PENDING;
	}
	/**
	 * SET : Request id of service pending request
	 * @param cO_REQ_PENDING
	 */
	public void setCO_REQ_PENDING(long cO_REQ_PENDING) {
		CO_REQ_PENDING = cO_REQ_PENDING;
	}
//	/**
//	 * GET :   	Prepaid indicator for the contracted service.
//		
//		    1 - postpaid
//		    2 - prepaid
//
//	 * @return
//	 */
//	public Integer getCS_PAYMENT_CONDITION_USG_IND() {
//		return CS_PAYMENT_CONDITION_USG_IND;
//	}
//	/**
//	 * SET 	Prepaid indicator for the contracted service.
//		
//		    1 - postpaid
//		    2 - prepaid
//		    
//	 * @param cS_PAYMENT_CONDITION_USG_IND
//	 */
//	public void setCS_PAYMENT_CONDITION_USG_IND(
//			Integer cS_PAYMENT_CONDITION_USG_IND) {
//		CS_PAYMENT_CONDITION_USG_IND = cS_PAYMENT_CONDITION_USG_IND;
//	}
//	/**
//	 * GET : Prepaid time package identifier. Returned only in case the service is prepaid.
//	 * @return
//	 */
//	public long getCS_PREPAID_TP_ID() {
//		return CS_PREPAID_TP_ID;
//	}
//	/**
//	 * SET : Prepaid time package identifier. Returned only in case the service is prepaid.
//	 * @param cS_PREPAID_TP_ID
//	 */
//	public void setCS_PREPAID_TP_ID(long cS_PREPAID_TP_ID) {
//		CS_PREPAID_TP_ID = cS_PREPAID_TP_ID;
//	}
//	/**
//	 * GET : Public key of the time package
//	 * @return
//	 */
//	public String getCS_PREPAID_TP_ID_PUB() {
//		return CS_PREPAID_TP_ID_PUB;
//	}
//	/**
//	 * SET : Public key of the time package
//	 * @param cS_PREPAID_TP_ID_PUB
//	 */
//	public void setCS_PREPAID_TP_ID_PUB(String cS_PREPAID_TP_ID_PUB) {
//		CS_PREPAID_TP_ID_PUB = cS_PREPAID_TP_ID_PUB;
//	}
//	/**
//	 * GET : List of closed user groups the contract is member of in case of a CUG service
//	 * @return
//	 */
//	public List<BCug_memberships> getCUG_MEMBERSHIPS() {
//		return CUG_MEMBERSHIPS;
//	}
//	/**
//	 * SET : List of closed user groups the contract is member of in case of a CUG service
//	 * @param cUG_MEMBERSHIPS
//	 */
//	public void setCUG_MEMBERSHIPS(List<BCug_memberships> cUG_MEMBERSHIPS) {
//		CUG_MEMBERSHIPS = cUG_MEMBERSHIPS;
//	}
//	/**
//	 * GET : Service parameter indicator
//	 * @return
//	 */
//	public boolean isPARAMS_IND() {
//		return PARAMS_IND;
//	}
//	/**
//	 * SET :Service parameter indicator
//	 * @param pARAMS_IND
//	 */
//	public void setPARAMS_IND(boolean pARAMS_IND) {
//		PARAMS_IND = pARAMS_IND;
//	}
//	/**
//	 * GET : Profile id
//	 * @return
//	 */
//	public long getPROFILE_ID() {
//		return PROFILE_ID;
//	}
//	/**
//	 * SET : Profile id
//	 * @param pROFILE_ID
//	 */
//	public void setPROFILE_ID(long pROFILE_ID) {
//		PROFILE_ID = pROFILE_ID;
//	}
//	/**
//	 * GET : 
//		Code of the last reason for service status change The reason code is assigned internally. The reason returned , 
//		in case this contract has a pending status, will be the one of the pending status otherwise will be the reason of the 
//		latest completed status change. 
//		In case the field is not set , nothing will be returned. Possible values for reason:
//		
//		    0 -> Initial activation
//		    1 -> Following state value of the service during its life cycle without any external reason
//		    2 -> Service package change, external reason
//		    3 -> Rate plan change, external reason
//		    4 -> Profile change, external reason
//		    5 -> Contract suspension, external reason
//		    9 -> Service is invisible
//
//	 * @return
//	 */
//	public Integer getREASON() {
//		return REASON;
//	}
//	/**
//	 * SET : 
//	 *  Code of the last reason for service status change The reason code is assigned internally. The reason returned , 
//		in case this contract has a pending status, will be the one of the pending status otherwise will be the reason of the 
//		latest completed status change. 
//		In case the field is not set , nothing will be returned. Possible values for reason:
//		
//		    0 -> Initial activation
//		    1 -> Following state value of the service during its life cycle without any external reason
//		    2 -> Service package change, external reason
//		    3 -> Rate plan change, external reason
//		    4 -> Profile change, external reason
//		    5 -> Contract suspension, external reason
//		    9 -> Service is invisible
//	 * @param rEASON
//	 */
//	public void setREASON(Integer rEASON) {
//		REASON = rEASON;
//	}
	/**
	 * GET : Identifies a service by the public or private key.
	 * @return
	 */
	public long getSNCODE() {
		return SNCODE;
	}
	/**
	 * SET : Identifies a service by the public or private key.
	 * @param sNCODE
	 */
	public void setSNCODE(long sNCODE) {
		SNCODE = sNCODE;
	}
//	/**
//	 * GET : Recurring charge (formerly: access fee) defined by the pricing element considering the service charge period and the billing cycle.
//	 * @return
//	 */
//	public float getORIG_ACC() {
//		return ORIG_ACC;
//	}
//	/**
//	 * SET : Recurring charge (formerly: access fee) defined by the pricing element considering the service charge period and the billing cycle.
//	 * @param oRIG_ACC
//	 */
//	public void setORIG_ACC(float oRIG_ACC) {
//		ORIG_ACC = oRIG_ACC;
//	}
	/**
	 * GET : Public key of the service
	 * @return
	 */
	public String getSNCODE_PUB() {
		return SNCODE_PUB;
	}
	/**
	 * SET : Public key of the service
	 * @param sNCODE_PUB
	 */
	public void setSNCODE_PUB(String sNCODE_PUB) {
		SNCODE_PUB = sNCODE_PUB;
	}
	/**
	 * GET : Service package code (SimpleProduct)
	 * @return
	 */
	public long getSPCODE() {
		return SPCODE;
	}
	/**
	 * SET : Service package code (SimpleProduct)
	 * @param sPCODE
	 */
	public void setSPCODE(long sPCODE) {
		SPCODE = sPCODE;
	}
	/**
	 * GET : Public key of the service package
	 * @return
	 */
	public String getSPCODE_PUB() {
		return SPCODE_PUB;
	}
	/**
	 * SET : Public key of the service package
	 * @param sPCODE_PUB
	 */
	public void setSPCODE_PUB(String sPCODE_PUB) {
		SPCODE_PUB = sPCODE_PUB;
	}
	/**
	 * GET : Code of the last user defined reason for service status change. The value of this parameter is a reason code from REASONSTATUS_ALL table 
	 * @return
	 */
//	public long getUSER_REASON() {
//		return USER_REASON;
//	}
//	/**
//	 * SET : Code of the last user defined reason for service status change. The value of this parameter is a reason code from REASONSTATUS_ALL table 
//	 * @param uSER_REASON
//	 */
//	public void setUSER_REASON(long uSER_REASON) {
//		USER_REASON = uSER_REASON;
//	}
//
//	public List<BDirectoryNumber> getDIRECTORY_NUMBERS() {
//		return DIRECTORY_NUMBERS;
//	}
//
//	public void setDIRECTORY_NUMBERS(List<BDirectoryNumber> dIRECTORY_NUMBERS) {
//		DIRECTORY_NUMBERS = dIRECTORY_NUMBERS;
//	}
//
//	public List<BPort> getPORTS() {
//		return PORTS;
//	}
//
//	public void setPORTS(List<BPort> pORTS) {
//		PORTS = pORTS;
//	}

	/**
	 * @return the cO_SN_STATUS
	 */
	public Integer getCO_SN_STATUS() {
		return CO_SN_STATUS;
	}

	/**
	 * @param cO_SN_STATUS the cO_SN_STATUS to set
	 */
	public void setCO_SN_STATUS(Integer cO_SN_STATUS) {
		CO_SN_STATUS = cO_SN_STATUS;
	}

}
