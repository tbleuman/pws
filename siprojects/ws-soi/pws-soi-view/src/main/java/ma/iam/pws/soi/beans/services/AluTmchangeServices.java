package ma.iam.pws.soi.beans.services;
//>******* Created, A.ESSA DI6717
public class AluTmchangeServices {
	
	private Long id; 
	private Long tmcode; 
	private Long spcode; 
	private Long sncode; 
	private Long prmId; 
	private Long value; 
	private String valueDes; 
	private String fuFlag;

	private String cmdFlag; 
	private Long depSncode;
	
	public AluTmchangeServices() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTmcode() {
		return tmcode;
	}
	public void setTmcode(Long tmcode) {
		this.tmcode = tmcode;
	}
	public Long getSpcode() {
		return spcode;
	}
	public void setSpcode(Long spcode) {
		this.spcode = spcode;
	}
	public Long getSncode() {
		return sncode;
	}
	public void setSncode(Long sncode) {
		this.sncode = sncode;
	}
	public Long getPrmId() {
		return prmId;
	}
	public void setPrmId(Long prmId) {
		this.prmId = prmId;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public String getValueDes() {
		return valueDes;
	}
	public void setValueDes(String valueDes) {
		this.valueDes = valueDes;
	}
	public String getFuFlag() {
		return fuFlag;
	}
	public void setFuFlag(String fuFlag) {
		this.fuFlag = fuFlag;
	}
	public String getCmdFlag() {
		return cmdFlag;
	}
	public void setCmdFlag(String cmdFlag) {
		this.cmdFlag = cmdFlag;
	}
	public Long getDepSncode() {
		return depSncode;
	}
	public void setDepSncode(Long depSncode) {
		this.depSncode = depSncode;
	}
	public AluTmchangeServices(Long id, Long tmcode, Long spcode, Long sncode,
			Long prmId, Long value, String valueDes, String fuFlag,
			String cmdFlag, Long depSncode) {
		super();
		this.id = id;
		this.tmcode = tmcode;
		this.spcode = spcode;
		this.sncode = sncode;
		this.prmId = prmId;
		this.value = value;
		this.valueDes = valueDes;
		this.fuFlag = fuFlag;
		this.cmdFlag = cmdFlag;
		this.depSncode = depSncode;
	}
	
}