package ma.iam.pws.soi.beans.contract;

import java.util.Date;

/**
 * @author mabchour
 * 
 */

public class BDirectoryNumber {

	private long BCCODE;
	private long COSC_DN_NP;
	private String COSC_DN_NP_PUB;
	private String COSC_DN_STATUS;
	private String COSC_LINKED_PUBLIC_DIRNUM;
	private long COSC_LINKED_PUBLIC_DIRNUM_NP;
	private String COSC_LINKED_PUBLIC_DIRNUM_NP_PUB;
	private long COSC_LINKED_PUBLIC_DN_ID;
	private String COSC_PENDING_DN_STATUS;
	private String DIRNUM;
	private boolean DIRNUM_ON_BILL;
	private String DNB_STATUS;
	private Date DN_ACTIVATION_DATE;
	private Date DN_DEACTIVATION_DATE;
	private long DN_ID;
	private boolean EXTERNAL_FLAG;
	private String LOWER_EXT;
	private boolean MAIN_DIRNUM;
	private long PENDING_BCCODE;
	private String PENDING_DNB_STATUS;
	private String PRE_DIRNUM;
	private long PRE_DN_ID;
	private String UPPER_EXT;
	private long VPN_ID;
	private String VPN_ID_PUB;

	public long getBCCODE() {
		return BCCODE;
	}

	/**
	 * @param bCCODE
	 */
	public void setBCCODE(long bCCODE) {
		BCCODE = bCCODE;
	}

	public long getCOSC_DN_NP() {
		return COSC_DN_NP;
	}

	public void setCOSC_DN_NP(long cOSC_DN_NP) {
		COSC_DN_NP = cOSC_DN_NP;
	}

	public String getCOSC_DN_NP_PUB() {
		return COSC_DN_NP_PUB;
	}

	public void setCOSC_DN_NP_PUB(String cOSC_DN_NP_PUB) {
		COSC_DN_NP_PUB = cOSC_DN_NP_PUB;
	}

	public String getCOSC_DN_STATUS() {
		return COSC_DN_STATUS;
	}

	public void setCOSC_DN_STATUS(String cOSC_DN_STATUS) {
		COSC_DN_STATUS = cOSC_DN_STATUS;
	}

	public String getCOSC_LINKED_PUBLIC_DIRNUM() {
		return COSC_LINKED_PUBLIC_DIRNUM;
	}

	public void setCOSC_LINKED_PUBLIC_DIRNUM(String cOSC_LINKED_PUBLIC_DIRNUM) {
		COSC_LINKED_PUBLIC_DIRNUM = cOSC_LINKED_PUBLIC_DIRNUM;
	}

	public long getCOSC_LINKED_PUBLIC_DIRNUM_NP() {
		return COSC_LINKED_PUBLIC_DIRNUM_NP;
	}

	public void setCOSC_LINKED_PUBLIC_DIRNUM_NP(
			long cOSC_LINKED_PUBLIC_DIRNUM_NP) {
		COSC_LINKED_PUBLIC_DIRNUM_NP = cOSC_LINKED_PUBLIC_DIRNUM_NP;
	}

	public String getCOSC_LINKED_PUBLIC_DIRNUM_NP_PUB() {
		return COSC_LINKED_PUBLIC_DIRNUM_NP_PUB;
	}

	public void setCOSC_LINKED_PUBLIC_DIRNUM_NP_PUB(
			String cOSC_LINKED_PUBLIC_DIRNUM_NP_PUB) {
		COSC_LINKED_PUBLIC_DIRNUM_NP_PUB = cOSC_LINKED_PUBLIC_DIRNUM_NP_PUB;
	}

	public long getCOSC_LINKED_PUBLIC_DN_ID() {
		return COSC_LINKED_PUBLIC_DN_ID;
	}

	public void setCOSC_LINKED_PUBLIC_DN_ID(long cOSC_LINKED_PUBLIC_DN_ID) {
		COSC_LINKED_PUBLIC_DN_ID = cOSC_LINKED_PUBLIC_DN_ID;
	}

	public String getCOSC_PENDING_DN_STATUS() {
		return COSC_PENDING_DN_STATUS;
	}

	public void setCOSC_PENDING_DN_STATUS(String cOSC_PENDING_DN_STATUS) {
		COSC_PENDING_DN_STATUS = cOSC_PENDING_DN_STATUS;
	}

	public String getDIRNUM() {
		return DIRNUM;
	}

	public void setDIRNUM(String dIRNUM) {
		DIRNUM = dIRNUM;
	}

	public boolean isDIRNUM_ON_BILL() {
		return DIRNUM_ON_BILL;
	}

	public void setDIRNUM_ON_BILL(boolean dIRNUM_ON_BILL) {
		DIRNUM_ON_BILL = dIRNUM_ON_BILL;
	}

	public String getDNB_STATUS() {
		return DNB_STATUS;
	}

	public void setDNB_STATUS(String dNB_STATUS) {
		DNB_STATUS = dNB_STATUS;
	}

	public Date getDN_ACTIVATION_DATE() {
		return DN_ACTIVATION_DATE;
	}

	public void setDN_ACTIVATION_DATE(Date dN_ACTIVATION_DATE) {
		DN_ACTIVATION_DATE = dN_ACTIVATION_DATE;
	}

	public Date getDN_DEACTIVATION_DATE() {
		return DN_DEACTIVATION_DATE;
	}

	public void setDN_DEACTIVATION_DATE(Date dN_DEACTIVATION_DATE) {
		DN_DEACTIVATION_DATE = dN_DEACTIVATION_DATE;
	}

	public long getDN_ID() {
		return DN_ID;
	}

	public void setDN_ID(long dN_ID) {
		DN_ID = dN_ID;
	}

	public boolean isEXTERNAL_FLAG() {
		return EXTERNAL_FLAG;
	}

	public void setEXTERNAL_FLAG(boolean eXTERNAL_FLAG) {
		EXTERNAL_FLAG = eXTERNAL_FLAG;
	}

	public String getLOWER_EXT() {
		return LOWER_EXT;
	}

	public void setLOWER_EXT(String lOWER_EXT) {
		LOWER_EXT = lOWER_EXT;
	}

	public boolean isMAIN_DIRNUM() {
		return MAIN_DIRNUM;
	}

	public void setMAIN_DIRNUM(boolean mAIN_DIRNUM) {
		MAIN_DIRNUM = mAIN_DIRNUM;
	}

	public long getPENDING_BCCODE() {
		return PENDING_BCCODE;
	}

	public void setPENDING_BCCODE(long pENDING_BCCODE) {
		PENDING_BCCODE = pENDING_BCCODE;
	}

	public String getPENDING_DNB_STATUS() {
		return PENDING_DNB_STATUS;
	}

	public void setPENDING_DNB_STATUS(String pENDING_DNB_STATUS) {
		PENDING_DNB_STATUS = pENDING_DNB_STATUS;
	}

	public String getPRE_DIRNUM() {
		return PRE_DIRNUM;
	}

	public void setPRE_DIRNUM(String pRE_DIRNUM) {
		PRE_DIRNUM = pRE_DIRNUM;
	}

	public long getPRE_DN_ID() {
		return PRE_DN_ID;
	}

	public void setPRE_DN_ID(long pRE_DN_ID) {
		PRE_DN_ID = pRE_DN_ID;
	}

	public String getUPPER_EXT() {
		return UPPER_EXT;
	}

	public void setUPPER_EXT(String uPPER_EXT) {
		UPPER_EXT = uPPER_EXT;
	}

	public long getVPN_ID() {
		return VPN_ID;
	}

	public void setVPN_ID(long vPN_ID) {
		VPN_ID = vPN_ID;
	}

	public String getVPN_ID_PUB() {
		return VPN_ID_PUB;
	}

	public void setVPN_ID_PUB(String vPN_ID_PUB) {
		VPN_ID_PUB = vPN_ID_PUB;
	}

}
