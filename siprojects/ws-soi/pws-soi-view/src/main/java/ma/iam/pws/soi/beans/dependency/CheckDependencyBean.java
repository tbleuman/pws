package ma.iam.pws.soi.beans.dependency;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import ma.iam.pws.soi.service.util.Dependency;


@XmlAccessorType(XmlAccessType.NONE)
//@XmlRootElement
//@XmlType(name="CheckDependencyBean" , propOrder = { "costcenter_id", "description" })
public class CheckDependencyBean {
	
	/** The Constant ALIAS. */
	public static final String ALIAS = "CheckDependency";
	@XmlElement
	private List<Dependency> servicesToDel;	
	@XmlElement
	private List<Dependency> servicesToAdd ; 
	

	public List<Dependency> getServicesToAdd() {
		return servicesToAdd;
	}


	public void setServicesToAdd(List<Dependency> servicesToAdd) {
		this.servicesToAdd = servicesToAdd;
	}


	public List<Dependency> getServicesToDel() {
		return servicesToDel;
	}


	public void setServicesToDel(List<Dependency> servicesToDel) {
		this.servicesToDel = servicesToDel;
	}


	public static String getAlias() {
		return ALIAS;
	} 
	
}
