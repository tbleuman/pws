/*
 * @author Atos
 */
package ma.iam.mobile.customer.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The Class WSConstants.
 */
public final class WSConstants {

	public static final String EXEC_QUERY = "Begin executing the query";
	public static final String EXEC_QUERY_END = "End executing the query";
	
	public static final int PRODUCT_CODE_MAX_LENGTH = 100;
	public static final String FID_REJ_MAX_LENGTH = "Le libell� fourni d�passe la taille autoris�e";

	public static final List<String> PRODUCT_LIST = new ArrayList<String>(
			Arrays.asList("M", "I", "F"));
	public static final String ERRO_DATA_ACCESS = "Erreur d'insertion au niveau MOP";
	public static final String ERRO_REQUEST_ID = "Erreur du request ID MOP";
	public static final String ERRO_ND_STAT = "Le statut du ND saisi n’est pas autorisé pour le portage IN";
	public static final String ERRO_ONGOING_PORTIN = "Verification demande Portage IN en cours";
	public static final String ERRO_OP_DONNEUR = "Opérateur donneur doit être MDTL ou WNA";
}
