package ma.iam.mobile.customer.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import ma.iam.mobile.customer.bean.MopOptions;
import ma.iam.mobile.customer.bean.Operateur;
import ma.iam.mobile.customer.bean.PortabiliteRequest;
import ma.iam.mobile.customer.bean.Rateplan;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.constants.WSConstants;
import ma.iam.mobile.customer.dao.PortabilityDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

@Repository
public class PortabilityDaoImpl extends BaseDAOMobileImpl implements PortabilityDao {
	private static final Logger LOG = LoggerFactory.getLogger(PortabilityDaoImpl.class);

	private static final String LIST_OPERATOR = PortabilityDaoImpl.class.getName() + ".LIST_OPERATOR";
	private static final String OWNER_OPERATOR = PortabilityDaoImpl.class.getName() + ".OWNER_OPERATOR";
	private static final String ONGOING_POIN = PortabilityDaoImpl.class.getName() + ".ONGOING_POIN";
	private static final String ADDREQ_POIN = PortabilityDaoImpl.class.getName() + ".ADDREQ_POIN";
	private static final String LIST_RATEPLAN = PortabilityDaoImpl.class.getName() + ".LIST_RATEPLAN";
	private static final String CHECKSTAT_POIN = PortabilityDaoImpl.class.getName() + ".CHECKSTAT_POIN";
	private static final String LISTOPTIONS_POIN = PortabilityDaoImpl.class.getName() + ".LISTOPTIONS_POIN";
	private static final String GETREQID_POIN = PortabilityDaoImpl.class.getName() + ".GETREQID_POIN";

	@Override
	public String createPortIN(PortabiliteRequest request)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		LOG.debug("--> createPortIN");
		long iRet;
		String trxID;
		if(!verifiedemandePortageIN(request.getMsisdn())) {
			if(!checkNdStatus(request.getMsisdn())) {
				iRet = getReqID();
				if(iRet > 0) {
					try {
						if ("MARM1".equals(request.getOpAttributaire()))
							trxID = "7";
						else
							trxID = "1";
						if (!"MDTL".equals(request.getOpDonneur()) && !"WNA".equals(request.getOpDonneur())) {
							LOG.debug("L’opérateur donneur est incorrect");
							throw new FunctionnalException(
									ExceptionCodeTypeEnum.ERRO_OP_DONNEUR,
									WSConstants.ERRO_OP_DONNEUR);
						}
			
						String sql = CustomSQLUtil.get(ADDREQ_POIN);
						Map<String, Object> namedParameters = new HashMap();
						namedParameters.put("IREQ",  Long.toString(iRet));
						namedParameters.put("MSISDN", request.getMsisdn());
						namedParameters.put("TRXID", trxID);
						namedParameters.put("PRENOM", request.getPrenom());
						namedParameters.put("NOM", request.getNom());
						namedParameters.put("DOB", request.getDateNaissance());
						namedParameters.put("CIN", request.getIdentifiant());
						namedParameters.put("ADRESSE", request.getAdresse());
						namedParameters.put("CODEPOSTALE", request.getCodePostale());
						namedParameters.put("VILLE", request.getVille());
						namedParameters.put("TMCODE", request.getTmcode());
						namedParameters.put("SNCODE", request.getSncode());
						namedParameters.put("FUPACKID", request.getFuPackId());
						namedParameters.put("CONTACTNUMBER", request.getContactNumber());
						namedParameters.put("OPA", request.getOpAttributaire());
						namedParameters.put("OPR", "MARM1");
						namedParameters.put("OPD", request.getOpDonneur());
						namedParameters.put("IDTYPE", request.getIdType());
						namedParameters.put("POCHETTE", request.getNdPochette());
						super.getNamedParameterJdbcTemplate().update(sql, namedParameters);
						LOG.debug("<-- createPortIN");
					} catch (EmptyResultDataAccessException e) {
						LOG.debug("Validation demande Portage IN  échouée ");
						throw new FunctionnalException(
								ExceptionCodeTypeEnum.ERRO_DATA_ACCESS,
								WSConstants.ERRO_DATA_ACCESS);
					}
					return "PO" + iRet;
				}else {
					LOG.debug("Récupération du request ID échouée ");
					throw new FunctionnalException(
							ExceptionCodeTypeEnum.ERRO_REQUEST_ID,
							WSConstants.ERRO_REQUEST_ID);
				}
			}else {
				LOG.debug("Le statut du ND saisi n’est pas autorisé pour le portage IN");
				throw new FunctionnalException(
						ExceptionCodeTypeEnum.ERRO_ND_STAT,
						WSConstants.ERRO_ND_STAT);
			}
		}else {
			LOG.debug("Verification demande Portage IN en cours");
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.ERRO_ONGOING_PORTIN,
					WSConstants.ERRO_ONGOING_PORTIN);
		}
	}

	@Override
	public Operateur getOperateurAttributaire(String msisdn)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		LOG.debug("--> getOperateurAttributaire");
		List<Operateur> list;
		try {
			String sql = CustomSQLUtil.get(OWNER_OPERATOR);
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("msisdn", msisdn);
			CustomBeanPropertyRowMapper<Operateur> custRowMapper = new CustomBeanPropertyRowMapper<Operateur>(
					Operateur.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
			LOG.debug("<-- getOperateurAttributaire");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("Owner Operator is  null ");
			return null;
		}
		return (list != null && !list.isEmpty()) ? list.get(0) : null;
	}

	@Override
	public List<Operateur> getOperateur() throws FunctionnalException, SyntaxiqueException, TechnicalException {
		LOG.debug("--> getOperateur");
		List<Operateur> list;
		try {
			String sql = CustomSQLUtil.get(LIST_OPERATOR);
			Map<String, String> namedParameters = new HashMap();

			final CustomBeanPropertyRowMapper<Operateur> custRowMapper = new CustomBeanPropertyRowMapper<Operateur>(
					Operateur.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);

			LOG.debug("<-- getOperateur");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("List operator is  null ");
			return null;
		}
		return list;
	}

	@Override
	public boolean verifiedemandePortageIN(String msisdn)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		LOG.debug("--> verifiedemandePortageIN");
		int iRet;
		try {
			String sql = CustomSQLUtil.get(ONGOING_POIN);
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("msisdn", msisdn);
			iRet = super.getNamedParameterJdbcTemplate().queryForInt(sql, namedParameters);
			if (iRet > 0)
				return true;
			LOG.debug("<-- verifiedemandePortageIN");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("Verification demande Portage IN en cours ");
			return false;
		}
		return false;
	}
	
	public boolean checkNdStatus(String msisdn)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		LOG.debug("--> checkNdStatus");
		int iRet=0;
		try {
			String sql = CustomSQLUtil.get(CHECKSTAT_POIN);
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("MSISDN", msisdn);
			iRet = super.getNamedParameterJdbcTemplate().queryForInt(sql, namedParameters);
			if (iRet > 0)
				return true;
			LOG.debug("<-- checkNdStatus");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("Verification demande Portage IN en cours ");
			return false;
		}
		return false;
	}	

	@Override
	public List<Rateplan> getRateplanByCategory(String prgcode) {
		LOG.debug("--> getRateplanByCategory");
		List<Rateplan> list;
		try {
			String sql = CustomSQLUtil.get(LIST_RATEPLAN);
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("PRGCODE", prgcode);
			final CustomBeanPropertyRowMapper<Rateplan> custRowMapper = new CustomBeanPropertyRowMapper<Rateplan>(
					Rateplan.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);

			LOG.debug("<-- getRateplanByCategory");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("Category is  null ");
			return null;
		}
		return list;
	}

	@Override
	public List<MopOptions> getMopOptions(String tmcode) {
		LOG.debug("--> getMopOptions");
		List<MopOptions> list;
		try {
			String sql = CustomSQLUtil.get(LISTOPTIONS_POIN);
			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("TMCODE", tmcode);
			final CustomBeanPropertyRowMapper<MopOptions> custRowMapper = new CustomBeanPropertyRowMapper<MopOptions>(
					MopOptions.class);
			list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);

			LOG.debug("<-- getMopOptions");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("Tmcode is null ");
			return null;
		}
		return list;
	}
	
	public long getReqID()
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		LOG.debug("--> getReqID");
		long iRet;
		try {
			String sql = CustomSQLUtil.get(GETREQID_POIN);
			Map<String, String> namedParameters = new HashMap();
			iRet = super.getNamedParameterJdbcTemplate().queryForLong(sql, namedParameters);
			LOG.debug("<-- getReqID");
		} catch (EmptyResultDataAccessException e) {
			LOG.debug("Récupération du request ID ");
			return -1;
		}
		return iRet;
	}	
}
