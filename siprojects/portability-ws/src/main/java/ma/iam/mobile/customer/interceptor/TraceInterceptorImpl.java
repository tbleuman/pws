package ma.iam.mobile.customer.interceptor;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.SecurityContext;
import org.apache.cxf.service.model.MessageInfo;
import org.apache.cxf.service.model.OperationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TraceInterceptorImpl extends AbstractSoapInterceptor implements TraceInterceptor {

	private BaseDAO baseDAO;
	private static final Logger LOG = LoggerFactory.getLogger(TraceInterceptorImpl.class);
	final static SimpleDateFormat FORMATER = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

	private String webServiceName;
	private String product;
	private String caller;
	private String pwsTraceQuery = "INSERT INTO PWS_TRACES (CALL_ID,USERNAME,CALLER,LOGIN,REQUEST_EXTERNAL_REFERENCE,WEBSERVICE_NAME,WEBMETHOD,PRODUCT,ENTRYDATE) VALUES (:call_id,:username,:caller,:login,:request_external_reference,:webservice_name,:webmethod,:product,:entrydate)";
	private String cmsTraceQuery = "INSERT INTO PWS_REQUESTS (CALL_ID,REQUEST,ENTRYDATE) VALUES (:call_id,:request,sysdate)";
	private String sequenceId = "SELECT PWS_TRACES_SEQ.nextval FROM dual";
	private String login;
	private Map<String, String> operations;

	private Map<String, String> operationsCMS;

	public TraceInterceptorImpl() {
		super(Phase.PREPARE_SEND);
	}

	public void handleMessage(SoapMessage message) throws Fault {
		SecurityContext context = message.getExchange().getInMessage().get(SecurityContext.class);
		String userName = context.getUserPrincipal().getName();
		Object ticket = message.getExchange().get("ma.iam.ticket");
		Object userNameId = message.getExchange().get("ma.iam.userName");
		LOG.info("++++ **** //// "+ ticket);
		final List contents = message.getContent(List.class);
		if (contents != null && contents.size() < 2) {
			Object object = contents.get(0);
			Message inMessage = message.getExchange().getInMessage();
			MessageInfo mi = (MessageInfo) inMessage.get(MessageInfo.class);
			OperationInfo operationInfo = mi.getOperation();
			try {
				performTrace(object, operationInfo.getInputName(), userName,ticket==null?"":ticket.toString(),userNameId==null?"":userNameId.toString());
			} catch (Throwable e) {
				LOG.debug("Throwable : "+e.getMessage());
				e.printStackTrace();
			}
		} else {
			LOG.info("Multiple or empty response " + (contents == null ? "<empty>" : contents.size()));
		}

	}

	private void performTrace(Object respObject, String operation, String userName,String ticket,String userNameId) throws TraceException {
		String expression = operations.get(operation);
		if (expression != null && !"".equals(expression.trim())) {
			String requestExternalReference = null;

			if (!"~".equals(expression)) {
				if ("primitive".equals(expression.toLowerCase())) {
					requestExternalReference = respObject.toString();

				} else {
					try {
						requestExternalReference = BeanUtils.getProperty(respObject, expression);
					} catch (Throwable e) {
						LOG.debug("Throwable : "+e.getMessage());
						throw new TraceException(e.getMessage());
					}
				}
			}
			Map<String, String> namedParameters = new HashMap<String, String>();
			if (ticket!=null && !"".equals(ticket.trim())) {
				String call_id = baseDAO.getNamedParameterJdbcTemplate().queryForObject(sequenceId, namedParameters, String.class);
				namedParameters.put("call_id", call_id);
				namedParameters.put("username", userName);
				namedParameters.put("caller", caller);
				namedParameters.put("login", userNameId);
				namedParameters.put("request_external_reference", ticket);
				namedParameters.put("webservice_name", webServiceName);
				namedParameters.put("webmethod", operation);
				namedParameters.put("product", product);
				namedParameters.put("entrydate", FORMATER.format(new Date()));
				baseDAO.getNamedParameterJdbcTemplate().update(pwsTraceQuery, namedParameters);
				if (requestExternalReference != null && '-' != requestExternalReference.trim().charAt(0)) {
					namedParameters.clear();
					namedParameters.put("call_id", call_id);
					namedParameters.put("request", requestExternalReference);
					baseDAO.getNamedParameterJdbcTemplate().update(cmsTraceQuery, namedParameters);
				}
			}
		}
	}


	public BaseDAO getBaseDAO() {
		return baseDAO;
	}

	public void setBaseDAO(BaseDAO baseDAO) {
		this.baseDAO = baseDAO;
	}

	public String getWebServiceName() {
		return webServiceName;
	}

	public void setWebServiceName(String webServiceName) {
		this.webServiceName = webServiceName;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getPwsTraceQuery() {
		return pwsTraceQuery;
	}

	public void setPwsTraceQuery(String pwsTraceQuery) {
		this.pwsTraceQuery = pwsTraceQuery;
	}

	public String getCmsTraceQuery() {
		return cmsTraceQuery;
	}

	public void setCmsTraceQuery(String cmsTraceQuery) {
		this.cmsTraceQuery = cmsTraceQuery;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Map<String, String> getOperations() {
		return operations;
	}

	public void setOperations(Map<String, String> operations) {
		this.operations = operations;
	}

}
