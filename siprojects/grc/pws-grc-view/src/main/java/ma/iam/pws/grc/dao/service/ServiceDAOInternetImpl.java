/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CreditMobileWS;
import ma.iam.pws.grc.bean.CugWS;
import ma.iam.pws.grc.bean.InfoDataWS;
import ma.iam.pws.grc.bean.ServiceCreditWS;
import ma.iam.pws.grc.bean.ServiceDnsWS;
import ma.iam.pws.grc.bean.ServiceHistoryWS;
import ma.iam.pws.grc.bean.ServiceParamWS;
import ma.iam.pws.grc.bean.ServiceWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.CommonDAOFixe;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceDAOInternetImpl.
 */
@Repository
public class ServiceDAOInternetImpl extends CommonDAOFixe implements
		IServiceDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ServiceDAOInternetImpl.class);

	/** La constante GETLISTESERVICES. */
	public static final String GETLISTESERVICES = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTESERVICES";

	/** The Constant GETLISTESERVICESDNS. */
	public static final String GETLISTESERVICESDNS = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTESERVICESDNS";

	/** La constante GETLISTESERVICEHIST. */
	public static final String GETLISTESERVICEHIST = ServiceDAOFixeImpl.class
			.getName() + ".GETLISTESERVICEHIST";

	public static final String GETLISTESERVICEPARAM_WITH_SHDES = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTESERVICEPARAM_WITH_SHDES";
	public static final String GETLISTESERVICEPARAM_WITHOUT_SHDES = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTESERVICEPARAM_WITHOUT_SHDES";

	/** La constante GETLISTESERVICEPARAM. */
	public static final String GETLISTESERVICEPARAM = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTESERVICEPARAM";
	public static final String GETLISTPARAM = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTPARAM";
	public static final String GETSTRPARAM = ServiceDAOInternetImpl.class
			.getName() + ".GETSTRPARAM";
	public static final String GETCHOICEPARAM = ServiceDAOInternetImpl.class
			.getName() + ".GETCHOICEPARAM";

	/** La constante GETLISTERDBPARAM. */
	public static final String GETLISTERDBPARAM = ServiceDAOInternetImpl.class
			.getName() + ".GETLISTERDBPARAM";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServices(java.lang.String
	 * )
	 */

	public List<ServiceWS> findServices(final String contractIdNeto) {
		LOG.debug("--> findServices contractIdNeto {}", contractIdNeto);
		List<ServiceWS> listServices = null;
		final String sql = CustomSQLUtil.get(GETLISTESERVICES);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdNeto", contractIdNeto);

		final CustomBeanPropertyRowMapper<ServiceWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceWS>(
				ServiceWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		listServices = super.getNamedParameterJdbcTemplateNeto().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findServices");
		return listServices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServicesDns(java.lang
	 * .String)
	 */

	public List<ServiceDnsWS> findServicesDns(final String contractIdBscs)
			throws GrcWsException {
		LOG.debug("--> findServicesDns contractIdBsc {}", contractIdBscs);
		List<ServiceDnsWS> listServices = null;
		String sql = CustomSQLUtil.get(GETLISTESERVICESDNS);

		final List<String> tags = new ArrayList<String>();

		tags.add("SERVICES_DNS;" + GRCWSConstantes.DNS_CODES);
		try {
			sql = GRCStringUtil.formatQuery(sql, tags);
		} catch (final Exception e) {
			throw new GrcWsException(ExceptionCodeTypeEnum.GRC_SYNTAX_ERROR,
					e.getMessage(), e);
		}

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);

		final CustomBeanPropertyRowMapper<ServiceDnsWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceDnsWS>(
				ServiceDnsWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		listServices = super.getNamedParameterJdbcTemplateNeto().query(sql,
				namedParameters, invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findServicesDns");
		return listServices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServiceHistory(java.
	 * lang.String, java.lang.String)
	 */

	public List<ServiceHistoryWS> findServiceHistory(
			final String contractIdBscs, final String shdes) {
		LOG.debug("--> findServiceHistory contractIdBsc {} shdes {}",
				contractIdBscs, shdes);
		List<ServiceHistoryWS> listServicesHist = null;
		final StringBuffer sql = new StringBuffer(
				CustomSQLUtil.get(GETLISTESERVICEHIST));

		// final Map<String, String> namedParameters = new HashMap<String,
		// String>();
		// namedParameters.put("contractIdBscs", contractIdBscs);
		// namedParameters.put("sncode", sncode);

		sql.append(" AND CO_ID = " + contractIdBscs);
		if (shdes != null && !"".equals(shdes)) {
			sql.append("   AND sn.SHDES= '" + shdes + "'");
		}

		sql.append(" ORDER BY HISTNO");
		final CustomBeanPropertyRowMapper<ServiceHistoryWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceHistoryWS>(
				ServiceHistoryWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listServicesHist = super.getNamedParameterJdbcTemplate()
				.getJdbcOperations().query(sql.toString(), invoiceRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- findServiceHistory");
		return listServicesHist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServiceParams(java.lang
	 * .String)
	 */

//	public List<ServiceParamWS> findServiceParams_old(final String paramId,
//			final String contractIdNeto) {
//		LOG.debug("--> findServiceParams paramId {}", paramId);
//		final String sql = CustomSQLUtil.get(GETLISTESERVICEPARAM);
//
//		final Map<String, String> namedParameters = new HashMap<String, String>();
//		namedParameters.put("paramId", paramId);
//		namedParameters.put("contractIdNeto", contractIdNeto);
//		final CustomBeanPropertyRowMapper<ServiceParamWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceParamWS>(
//				ServiceParamWS.class);
//
//		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
//		final List<ServiceParamWS> listServicesParamsTmp = super
//				.getNamedParameterJdbcTemplateNeto().query(sql,
//						namedParameters, invoiceRowMapper);
//		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
//
//		final List<ServiceParamWS> listResult = new ArrayList<ServiceParamWS>();
//
//		for (final ServiceParamWS serviceParamTmpWs : listServicesParamsTmp) {
//			final String paramType = serviceParamTmpWs.getDesc();
//			final String paramSubType = serviceParamTmpWs.getDeletedFlag();
//			final String valueId = serviceParamTmpWs.getRequestId();
//			if ("S".equals(paramType)) {
//				if ("S".equals(paramSubType)) {
//					clearTmp(serviceParamTmpWs);
//					serviceParamTmpWs.setDesc(getValue(valueId));
//					listResult.add(serviceParamTmpWs);
//				} else if ("L".equals(paramSubType)) {
//					// listResult.addAll(getListValues(serviceParamTmpWs,
//					// false));
//					// Do nothing because c'est déjé dans le union all
//				} else {
//					clearTmp(serviceParamTmpWs);
//					listResult.add(serviceParamTmpWs);
//				}
//			} else if ("B".equals(paramType) || "I".equals(paramType)
//					|| "d".equals(paramType) || "D".equals(paramType)
//					|| "DT".equals(paramType)) {
//				clearTmp(serviceParamTmpWs);
//				serviceParamTmpWs.setDesc(getValue(valueId));
//				listResult.add(serviceParamTmpWs);
//			} else if ("C".equals(paramType)) {
//				if ("H".equals(paramSubType)) {
//					listResult.addAll(getListValues(serviceParamTmpWs, true));
//				} else if ("L".equals(paramSubType)) {
//					listResult.addAll(getListValues(serviceParamTmpWs, false));
//				} else {
//					clearTmp(serviceParamTmpWs);
//					listResult.add(serviceParamTmpWs);
//				}
//			} else {
//				serviceParamTmpWs.setRequestId("");
//				serviceParamTmpWs.setDeletedFlag("");
//				listResult.add(serviceParamTmpWs);
//			}
//		}
//
//		LOG.debug("<-- findServiceParams");
//		return listResult;
//	}

//	private List<ServiceParamWS> getListValues(
//			final ServiceParamWS serviceParamTmpWs, final boolean choice) {
//		final List<ServiceParamWS> listResult = new ArrayList<ServiceParamWS>();
//		final String sql;
//		if (choice) {
//			sql = CustomSQLUtil.get(GETCHOICEPARAM);
//		} else {
//			sql = CustomSQLUtil.get(GETLISTPARAM);
//		}
//		final Map<String, String> namedParameters = new HashMap<String, String>();
//		namedParameters.put("valueId", serviceParamTmpWs.getRequestId());
//		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
//		final List<String> values = super.getNamedParameterJdbcTemplateNeto()
//				.queryForList(sql, namedParameters, String.class);
//		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
//		for (final String value : values) {
//			final ServiceParamWS serviceParamWs = new ServiceParamWS();
//			serviceParamWs.setName(serviceParamTmpWs.getName());
//			serviceParamWs.setDesc(value);
//			serviceParamWs.setValidationDate(serviceParamTmpWs
//					.getValidationDate());
//			serviceParamWs.setParamId(serviceParamTmpWs.getParamId());
//			listResult.add(serviceParamWs);
//		}
//
//		return listResult;
//	}
//
//	private void clearTmp(final ServiceParamWS serviceParamTmpWs) {
//		serviceParamTmpWs.setDesc("");
//		serviceParamTmpWs.setRequestId("");
//		serviceParamTmpWs.setDeletedFlag("");
//	}

	private String getValue(final String valueId) {
		final String sql = CustomSQLUtil.get(GETSTRPARAM);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_NETO, sql);
		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("valueId", valueId);
		final String value = super.getNamedParameterJdbcTemplateNeto()
				.queryForObject(sql, namedParameters, String.class);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findRdbParams(java.lang.
	 * String)
	 */
	// hors perimetre AtoS
	//
	// public RdbWS findRdbParams(final String nd) {
	// LOG.debug("--> findRdbParams nd {}", nd);
	// throw new
	// NotImplementedException("Service RDB non disponible pour le produit INTERNET");
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findInfoData(java.lang.String
	 * , java.lang.String)
	 */

	public InfoDataWS findInfoData(final String contractIdBscs,
			final String contractIdNeto) {
		LOG.debug("--> findInfoData contractIdBsc {} contractIdNeto {}",
				contractIdBscs, contractIdNeto);

		final InfoDataWS info = new InfoDataWS();

		// Getting the SLA
		String gtrValue = null;
		String delaiValue = null;
		String gtdValue = null;

		final String slaClass = getSlaClass(contractIdBscs);

		if (GRCWSConstantes.C1_CLASS.equals(slaClass)) {
			gtrValue = GRCWSConstantes.C1_GTR;
			delaiValue = GRCWSConstantes.C1_DELAI;
			gtdValue = GRCWSConstantes.C1_GTD;
		} else if (GRCWSConstantes.C2_CLASS.equals(slaClass)) {
			gtrValue = GRCWSConstantes.C2_GTR;
			delaiValue = GRCWSConstantes.C2_DELAI;
			gtdValue = GRCWSConstantes.C2_GTD;

		} else if (GRCWSConstantes.C3_CLASS.equals(slaClass)) {
			gtrValue = GRCWSConstantes.C3_GTR;
			delaiValue = GRCWSConstantes.C3_DELAI;
			gtdValue = GRCWSConstantes.C3_GTD;

		} else {
			gtrValue = GRCWSConstantes.C0_GTR;
			delaiValue = GRCWSConstantes.C0_DELAI;
			gtdValue = GRCWSConstantes.C0_GTD;
		}

		info.setClasse1(slaClass);
		info.setGtrValue(gtrValue);
		info.setDelaiValue(delaiValue);
		info.setGtdValue(gtdValue);

		// Getting the Adress1
		info.setAdressL1(getAdress(GRCWSConstantes.TYPE_L1_ADRESS,
				contractIdNeto));

		// Getting the Adress2
		info.setAdressL2(getAdress(GRCWSConstantes.TYPE_L2_ADRESS,
				contractIdNeto));

		LOG.debug("<-- findInfoData info {}", info);
		return info;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.capgemini.iam.grc.dao.service.IServiceDAO#findAllHlr()
	 */

	public List<String> findAllHlr() {
		throw new NotImplementedException(
				"Service HLR non disponible pour le produit INTERNET");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCustomerHlr(java.lang
	 * .String)
	 */

	public String findCustomerHlr(final String nd) {
		throw new NotImplementedException(
				"Service HLR non disponible pour le produit INTERNET");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findServicesCredit(java.
	 * lang.String)
	 */

	public List<ServiceCreditWS> findServicesCredit(final String contractIdBscs) {
		throw new NotImplementedException(
				"Service CREDIT non disponible pour le produit INTERNET");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCreditMobileParams(java
	 * .lang.String)
	 */

	public CreditMobileWS findCreditMobileParams(final String contractId) {
		throw new NotImplementedException(
				"Service CREDIT non disponible pour le produit INTERNET");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.service.IServiceDAO#findCug(java.lang.String)
	 */

	public List<CugWS> findCug(final String contractIdBscs) {
		throw new NotImplementedException(
				"Service CUG non disponible pour le produit INTERNET");
	}

	public String getSlaClass(final String contractIdBscs) {
		return super.getSlaClass(contractIdBscs);
	}

	public List<ServiceParamWS> findServiceParams(final String shdes,
			final String contractIdBscs) {
		LOG.debug("--> findServiceParams shdes {}", shdes);
		String sql = CustomSQLUtil.get(GETLISTESERVICEPARAM_WITHOUT_SHDES);

		final Map<String, String> namedParameters = new HashMap<String, String>();

		namedParameters.put("contractIdBscs", contractIdBscs);
		if (shdes != null && !"".equals(shdes)) {
			namedParameters.put("shdes", shdes);
			sql = CustomSQLUtil.get(GETLISTESERVICEPARAM_WITH_SHDES);

		}

		final CustomBeanPropertyRowMapper<ServiceParamWS> invoiceRowMapper = new CustomBeanPropertyRowMapper<ServiceParamWS>(
				ServiceParamWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY_END, sql);
		final List<ServiceParamWS> listServicesParams = super
				.getNamedParameterJdbcTemplate().query(sql, namedParameters,
						invoiceRowMapper);

		LOG.debug("<-- findServiceParams");
		return listServicesParams;
	}

}
