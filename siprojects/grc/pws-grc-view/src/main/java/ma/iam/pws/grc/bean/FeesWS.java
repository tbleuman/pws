/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class FeesWS.
 */
public class FeesWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 1422961456095860730L;

    /** The cust acct. */
    private CustAcctWS custAcct = new CustAcctWS();

    /** The contract. */
    private ContractWS contract = new ContractWS();

    /** The fee type. */
    private String feeType;

    /** The amount. */
    private double amount;

    /** The remark. */
    private String remark;

    /** The gl code. */
    private String glCode;

    /** The gl code disc. */
    private String glCodeDisc;

    /** The gl code mincom. */
    private String glCodeMincom;

    /** The valid from. */
    private String validFrom;

    /** The user name. */
    private String username;

    /** The fee class. */
    private Integer feeClass;

    /**
     * Gets the cust acct.
     * 
     * @return the cust acct
     */
    public CustAcctWS getCustAcct() {
        return custAcct;
    }

    /**
     * Sets the cust acct.
     * 
     * @param custAcct
     *            the new cust acct
     */
    public void setCustAcct(final CustAcctWS custAcct) {
        this.custAcct = custAcct;
    }

    /**
     * Gets the contract.
     * 
     * @return the contract
     */
    public ContractWS getContract() {
        return contract;
    }

    /**
     * Sets the contract.
     * 
     * @param contract
     *            the new contract
     */
    public void setContract(final ContractWS contract) {
        this.contract = contract;
    }

    /**
     * Gets the fee type.
     * 
     * @return the fee type
     */
    public String getFeeType() {
        return feeType;
    }

    /**
     * Sets the fee type.
     * 
     * @param feeType
     *            the new fee type
     */
    public void setFeeType(final String feeType) {
        this.feeType = feeType;
    }

    /**
     * Gets the amount.
     * 
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Gets the remark.
     * 
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the remark.
     * 
     * @param remark
     *            the new remark
     */
    public void setRemark(final String remark) {
        this.remark = remark;
    }

    /**
     * Gets the gl code.
     * 
     * @return the gl code
     */
    public String getGlCode() {
        return glCode;
    }

    /**
     * Sets the gl code.
     * 
     * @param glCode
     *            the new gl code
     */
    public void setGlCode(final String glCode) {
        this.glCode = glCode;
    }

    /**
     * Gets the gl code disc.
     * 
     * @return the gl code disc
     */
    public String getGlCodeDisc() {
        return glCodeDisc;
    }

    /**
     * Sets the gl code disc.
     * 
     * @param glCodeDisc
     *            the new gl code disc
     */
    public void setGlCodeDisc(final String glCodeDisc) {
        this.glCodeDisc = glCodeDisc;
    }

    /**
     * Gets the gl code mincom.
     * 
     * @return the gl code mincom
     */
    public String getGlCodeMincom() {
        return glCodeMincom;
    }

    /**
     * Sets the gl code mincom.
     * 
     * @param glCodeMincom
     *            the new gl code mincom
     */
    public void setGlCodeMincom(final String glCodeMincom) {
        this.glCodeMincom = glCodeMincom;
    }

    /**
     * Sets the amount.
     * 
     * @param amount
     *            the new amount
     */
    public void setAmount(final double amount) {
        this.amount = amount;
    }

    /**
     * Gets the valid from.
     * 
     * @return the valid from
     */
    public String getValidFrom() {
        return validFrom;
    }

    /**
     * Sets the valid from.
     * 
     * @param validFrom
     *            the new valid from
     */
    public void setValidFrom(final String validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Gets the user name.
     * 
     * @return the user name
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the user name.
     * 
     * @param userName
     *            the new user name
     */
    public void setUsername(final String userName) {
        this.username = userName;
    }

    /**
     * Gets the fee class.
     * 
     * @return the fee class
     */
    public Integer getFeeClass() {
        return feeClass;
    }

    /**
     * Sets the fee class.
     * 
     * @param feeClass
     *            the new fee class
     */
    public void setFeeClass(final Integer feeClass) {
        this.feeClass = feeClass;
    }

}
