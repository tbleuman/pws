/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.List;

import ma.iam.pws.grc.bean.ContractHistoryWS;
import ma.iam.pws.grc.bean.PeripheriqueHistoryWS;
import ma.iam.pws.grc.bean.PlanTarifaireHistoryWS;
import ma.iam.pws.grc.bean.PreidentificationWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.constants.ProduitTypeEnum;
import ma.iam.pws.grc.dao.contrat.IContractDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ContractHistoryManagerBiz.
 */
@Component(value = "contractHistoryManagerBiz")
public class ContractHistoryManagerBiz extends CommonManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ContractHistoryManagerBiz.class);

	/**
	 * Cherche l'istorique des contrats.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<ContractHistoryWS> findContractHistory(
			final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findContractHistory: contractIdBscs {}", contractIdBscs);

		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}

		final IContractDAO iContractDAO = this
				.getContractDAOForProduct(product);

		final List<ContractHistoryWS> listContractWS = iContractDAO
				.findContractHistory(contractIdBscs);

		if (product == ProduitTypeEnum.FIXE.getIdTypeProduit()) {
			ContractHistoryWS contractHistoryWS = iContractDAO
					.getCreatContract(contractIdBscs);
			if (contractHistoryWS != null
					&& contractHistoryWS.getCreePar() != null) {
				for (ContractHistoryWS contract : listContractWS) {

					if ("1".equals(contract.getNumber())) {
						contract.setCreePar(contractHistoryWS.getCreePar());
						break;
					}
				}
			}

		}
		LOG.info("<- findContractHistory");
		return listContractWS;
	}

	/**
	 * Cherche l'historique des peripherique.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PeripheriqueHistoryWS> findPeripheriqueHistory(
			final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findPeripheriqueHistory: contractIdBscs {}",
				contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}

		final IContractDAO iContractDAO = this
				.getContractDAOForProduct(product);

		final List<PeripheriqueHistoryWS> listContractWS = iContractDAO
				.findPeripheriqueHistory(contractIdBscs);
		LOG.info("<- findPeripheriqueHistory");
		return listContractWS;
	}

	/**
	 * Cherche l'historique des plans tarifaires.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PlanTarifaireHistoryWS> findPlanTarifaireHistory(
			final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findPlanTarifaireHistory: contractIdBscs {}",
				contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IContractDAO iContractDAO = this
				.getContractDAOForProduct(product);

		final List<PlanTarifaireHistoryWS> listContractWS = iContractDAO
				.findPlanTarifaireHistory(contractIdBscs);
		LOG.info("<- findPlanTarifaireHistory");
		return listContractWS;
	}

	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PreidentificationWS> findDonneesPreidentification(
			final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findDonneesPreidentification: contractIdBscs {}",
				contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IContractDAO iContractDAO = this
				.getContractDAOForProduct(product);

		final List<PreidentificationWS> listPreidentifWS = iContractDAO
				.findDonneesPreidentification(contractIdBscs);
		LOG.info("<- findDonneesPreidentification");
		return listPreidentifWS;
	}

	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<PreidentificationWS> findDonneesPreidentificationOM(
			final String contractIdBscs, final int product)
			throws GrcWsException {
		LOG.info("-> findDonneesPreidentificationOM: contractIdBscs {}",
				contractIdBscs);
		if (!WSValidator.validateNumber(contractIdBscs)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					GRCWSConstantes.CONTRACTIDBSCS_INCORRECT + contractIdBscs);
		}
		final IContractDAO iContractDAO = this
				.getContractDAOForProduct(product);

		final List<PreidentificationWS> listPreidentifWS = iContractDAO
				.findDonneesPreidentificationOM(contractIdBscs);
		LOG.info("<- findDonneesPreidentificationOM");
		return listPreidentifWS;
	}

}
