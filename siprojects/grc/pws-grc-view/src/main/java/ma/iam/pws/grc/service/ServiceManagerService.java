/*
 * @author Capgemini
 */
package ma.iam.pws.grc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ma.iam.pws.grc.bean.CreditMobileWS;
import ma.iam.pws.grc.bean.CugWS;
import ma.iam.pws.grc.bean.InfoDataWS;
import ma.iam.pws.grc.bean.RdbWS;
import ma.iam.pws.grc.bean.ServiceCreditWS;
import ma.iam.pws.grc.bean.ServiceDnsWS;
import ma.iam.pws.grc.bean.ServiceHistoryWS;
import ma.iam.pws.grc.bean.ServiceParamWS;
import ma.iam.pws.grc.bean.ServiceWS;
import ma.iam.pws.grc.business.ServiceManagerBiz;
import ma.iam.pws.grc.exceptions.GrcWsException;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * The Class ServiceManagerService.
 */
@WebService(serviceName = "ServiceManagerService",targetNamespace="http://service.grc.iam.capgemini.fr/")
@Component(value = "serviceManagerService")
public class ServiceManagerService extends SpringBeanAutowiringSupport {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ServiceManagerService.class);

	/** The customer infos manager biz. */
	@Autowired
	private ServiceManagerBiz serviceManagerBiz;

	/**
	 * Cherche les services.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	/*
	 * f.tatbi FC 5864 update operation name
	 */
	@WebMethod(operationName = "findServiceForContract")
	@WebResult(name = "ListServiceWS")
	public List<ServiceWS> findServiceForContract(
			@WebParam(name = "contractIdBscs") final String contractIdBscs,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: serviceManagerBiz.findServices contractIdBscs={}",
				contractIdBscs);
		return serviceManagerBiz.findServiceForContract(contractIdBscs, product);
	}

	/**
	 * Find services dns.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	/*
	 * f.tatbi FC 5864 delete operation
	 */
	// @WebMethod(operationName = "findServicesDns")
	// @WebResult(name = "ListServiceDnsWS")
	// public List<ServiceDnsWS> findServicesDns(@WebParam(name =
	// "contractIdBscs") final String contractIdBscs, @WebParam(name =
	// "produit") final int product)
	// throws GrcWsException {
	// LOG.trace("@WebMethod: serviceManagerBiz.findServicesDns contractIdBscs={}",
	// contractIdBscs);
	// return serviceManagerBiz.findServicesDns(contractIdBscs, product);
	// }

	/**
	 * Find services credit.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	/*
	 * f.tatbi FC 5864 delete parameter produit
	 */
	@WebMethod(operationName = "findServicesCredit")
	@WebResult(name = "ListServiceCreditWS")
	public List<ServiceCreditWS> findServicesCredit(
			@WebParam(name = "contractIdBscs") final String contractIdBscs)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: serviceManagerBiz.findServicesCredit contractIdBscs={}",
				contractIdBscs);
		return serviceManagerBiz.findServicesCredit(contractIdBscs);
	}

	/**
	 * Find cug.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */
	/*
	 * f.tatbi FC 5864 update opration s name 
	 * 
	 */
	@WebMethod(operationName = "findCugForContract")
	@WebResult(name = "ListCugWS")
	public List<CugWS> findCugForContract(
			@WebParam(name = "contractIdBscs") final String contractIdBscs,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace("@WebMethod: serviceManagerBiz.findCug contractIdBscs={}",
				contractIdBscs);
		return serviceManagerBiz.findCug(contractIdBscs, product);
	}

	/**
	 * Find credit mobile params.
	 * 
	 * @param contractIdBscs
	 *            the contract id bscs
	 * @param product
	 *            the product
	 * @return the list
	 * @throws GrcWsException
	 *             the grc ws exception
	 */

	/*
	 * f.tatbi FC 5864 delete operation findCreditMobileParams
	 */
	// @WebMethod(operationName = "findCreditMobileParams")
	// @WebResult(name = "CreditMobileWS")
	// public CreditMobileWS findCreditMobileParams(@WebParam(name =
	// "contractIdBscs") final String contractIdBscs, @WebParam(name =
	// "produit") final int product)
	// throws GrcWsException {
	// LOG.trace("@WebMethod: serviceManagerBiz.findCreditMobileParams contractIdBscs={}",
	// contractIdBscs);
	// return serviceManagerBiz.findCreditMobileParams(contractIdBscs, product);
	// }

	/**
	 * Cherche les service history.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param sncode
	 *            the sncode
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@WebMethod(operationName = "findServiceHistory")
	@WebResult(name = "ListServiceHistoryWS")
	public List<ServiceHistoryWS> findServiceHistory(
			@WebParam(name = "contractIdBscs") final String contractIdBscs,
			@WebParam(name = "shdes") final String shdes,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: serviceManagerBiz.findServiceHistory contractIdBscs={} shdes={}",
				contractIdBscs, shdes);
		return serviceManagerBiz.findServiceHistory(contractIdBscs, shdes,
				product);
	}

	/**
	 * Cherche les service params.
	 * 
	 * @param paramId
	 *            l'identifiant du paramétre
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	/*
	 * f.tatbi FC 5864 update operation s name
	 */
	@WebMethod(operationName = "findServiceParamsForContract")
	@WebResult(name = "ListServiceParamWS")
	public List<ServiceParamWS> findServiceParamsForContract(
			@WebParam(name = "shdes") final String shdes,
			@WebParam(name = "contractIdBscs") final String contractIdBscs,
			@WebParam(name = "produit") final int product)
			throws GrcWsException {
		LOG.trace(
				"@WebMethod: serviceManagerBiz.findServiceParamsForContract shdes={} contractIdBscs={}",
				shdes, contractIdBscs);
		return serviceManagerBiz.findServiceParams(shdes, contractIdBscs,
				product);
	}

	/**
	 * Cherche les rdb params.
	 * 
	 * @param numeroNd
	 *            the numero nd
	 * @param product
	 *            le produit
	 * @return the rdb ws
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	// hors perimetre AtoS
	// @WebMethod(operationName = "findRdbParams")
	// @WebResult(name = "RdbWS")
	// public RdbWS findRdbParams(@WebParam(name = "numeroNd") final String
	// numeroNd, @WebParam(name = "produit") final int product) throws
	// GrcWsException {
	// LOG.trace("@WebMethod: serviceManagerBiz.findRdbParams nd={}", numeroNd);
	// return serviceManagerBiz.findRdbParams(numeroNd, product);
	// }

	/**
	 * Cherche les info data.
	 * 
	 * @param contractIdBscs
	 *            le contrat bscs du client (champ CO_ID)
	 * @param contractIdNeto
	 *            l'identifiant du contrat netonomy correspondant au client
	 * @param product
	 *            le produit
	 * @return the info data ws
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	/*
	 * f.tatbi FC 5864 delete operation findInfoData
	 */
	// @WebMethod(operationName = "findInfoData")
	// @WebResult(name = "InfoDataWS")
	// public InfoDataWS findInfoData(@WebParam(name = "contractIdBscs") final
	// String contractIdBscs,
	// @WebParam(name = "contractIdNeto") final String contractIdNeto,
	// @WebParam(name = "produit") final int product) throws GrcWsException {
	// LOG.trace("@WebMethod: serviceManagerBiz.findInfoData contractIdBscs={} contractIdNeto={}",
	// contractIdBscs, contractIdNeto);
	// return serviceManagerBiz.findInfoData(contractIdBscs, contractIdNeto,
	// product);
	// }

	// /**
	// * Find sla class.
	// *
	// * @param contractIdBscs
	// * the contract id bscs
	// * @param product
	// * the product
	// * @return the string
	// * @throws GrcWsException
	// * the grc ws exception
	// */
	// @WebMethod(operationName = "findSLAClass")
	// @WebResult(name = "slaClass")
	// public String findSLAClass(@WebParam(name = "contractIdBscs") final
	// String contractIdBscs, @WebParam(name = "produit") final int product)
	// throws GrcWsException {
	// LOG.trace("@WebMethod: serviceManagerBiz.findInfoData contractIdBscs={} contractIdNeto={}",
	// contractIdBscs);
	// return serviceManagerBiz.findSLAClass(contractIdBscs, product);
	// }
	@Autowired
	private JDBCPing pingMobileBSCS;
	@Autowired
	private JDBCPing pingFixeBSCS;
	@Autowired
	private JDBCPing pingFixeNETO;
	@Autowired
	private JDBCPing pingMobileFIDELIO;

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass() {
		boolean ret1 = pingMobileBSCS.check();
		boolean ret2 = pingFixeBSCS.check();
		boolean ret3 = pingFixeNETO.check();
		boolean ret4 = pingMobileFIDELIO.check();
		if (ret1 && ret2 && ret3 && ret4)
			return "1";
		return "-1";
	}
}
