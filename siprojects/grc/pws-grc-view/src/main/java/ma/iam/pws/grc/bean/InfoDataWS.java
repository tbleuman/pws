/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class InfoDataWS.
 */
public class InfoDataWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 2237286422339345603L;

    /** The adress l1. */
    private String adressL1;

    /** The adress l2. */
    private String adressL2;

    /** The classe1. */
    private String classe1;

    /** The gtr value. */
    private String gtrValue;

    /** The delai value. */
    private String delaiValue;

    /** The gtd value. */
    private String gtdValue;

    /**
     * Gets the adress l1.
     * 
     * @return the adress l1
     */
    public String getAdressL1() {
        return adressL1;
    }

    /**
     * Sets the adress l1.
     * 
     * @param adressL1
     *            the new adress l1
     */
    public void setAdressL1(final String adressL1) {
        this.adressL1 = adressL1;
    }

    /**
     * Gets the adress l2.
     * 
     * @return the adress l2
     */
    public String getAdressL2() {
        return adressL2;
    }

    /**
     * Sets the adress l2.
     * 
     * @param adressL2
     *            the new adress l2
     */
    public void setAdressL2(final String adressL2) {
        this.adressL2 = adressL2;
    }

    /**
     * Gets the classe1.
     * 
     * @return the classe1
     */
    public String getClasse1() {
        return classe1;
    }

    /**
     * Sets the classe1.
     * 
     * @param classe1
     *            the new classe1
     */
    public void setClasse1(final String classe1) {
        this.classe1 = classe1;
    }

    /**
     * Gets the gtr value.
     * 
     * @return the gtr value
     */
    public String getGtrValue() {
        return gtrValue;
    }

    /**
     * Sets the gtr value.
     * 
     * @param gtrValue
     *            the new gtr value
     */
    public void setGtrValue(final String gtrValue) {
        this.gtrValue = gtrValue;
    }

    /**
     * Gets the delai value.
     * 
     * @return the delai value
     */
    public String getDelaiValue() {
        return delaiValue;
    }

    /**
     * Sets the delai value.
     * 
     * @param delaiValue
     *            the new delai value
     */
    public void setDelaiValue(final String delaiValue) {
        this.delaiValue = delaiValue;
    }

    /**
     * Gets the gtd value.
     * 
     * @return the gtd value
     */
    public String getGtdValue() {
        return gtdValue;
    }

    /**
     * Sets the gtd value.
     * 
     * @param gtdValue
     *            the new gtd value
     */
    public void setGtdValue(final String gtdValue) {
        this.gtdValue = gtdValue;
    }

}
