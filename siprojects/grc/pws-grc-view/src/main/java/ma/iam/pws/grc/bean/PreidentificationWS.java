/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class PeripheriqueHistoryWS.
 */
public class PreidentificationWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 4386015476722702121L;

    private String origin;
    private String entdate;
    private String id;
    private String originDate;
    private String msisdn;
    private String imsi;
    private String passportNo;
    private String lastName;
    private String firstName;
    private String status;
    private String rejectReason;
    private String preidentifDate;
    private String barringDate;
    private String suspendDate;
    private String identifDate;
    private String prepaidPreident;
    private String notifiedRejection;
    private String carteSim;
    private String contractId;
    
    private String actionType;
    private String actionDetails;
    private String luDate;
    private String response;
    
    
    
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getEntdate() {
		return entdate;
	}
	public void setEntdate(String entdate) {
		this.entdate = entdate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOriginDate() {
		return originDate;
	}
	public void setOriginDate(String originDate) {
		this.originDate = originDate;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getPreidentifDate() {
		return preidentifDate;
	}
	public void setPreidentifDate(String preidentifDate) {
		this.preidentifDate = preidentifDate;
	}
	public String getBarringDate() {
		return barringDate;
	}
	public void setBarringDate(String barringDate) {
		this.barringDate = barringDate;
	}
	public String getSuspendDate() {
		return suspendDate;
	}
	public void setSuspendDate(String suspendDate) {
		this.suspendDate = suspendDate;
	}
	public String getIdentifDate() {
		return identifDate;
	}
	public void setIdentifDate(String identifDate) {
		this.identifDate = identifDate;
	}
	public String getPrepaidPreident() {
		return prepaidPreident;
	}
	public void setPrepaidPreident(String prepaidPreident) {
		this.prepaidPreident = prepaidPreident;
	}
	public String getNotifiedRejection() {
		return notifiedRejection;
	}
	public void setNotifiedRejection(String notifiedRejection) {
		this.notifiedRejection = notifiedRejection;
	}
	public String getCarteSim() {
		return carteSim;
	}
	public void setCarteSim(String carteSim) {
		this.carteSim = carteSim;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getActionDetails() {
		return actionDetails;
	}
	public void setActionDetails(String actionDetails) {
		this.actionDetails = actionDetails;
	}
	public String getLuDate() {
		return luDate;
	}
	public void setLuDate(String luDate) {
		this.luDate = luDate;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
}
