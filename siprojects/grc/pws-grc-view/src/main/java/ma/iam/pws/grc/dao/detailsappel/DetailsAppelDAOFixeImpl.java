/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.detailsappel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.CustomDetailWS;
import ma.iam.pws.grc.bean.DetailsAppelWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

/**
 * The Class DetailsAppelFixeImpl.
 */
@Repository
public class DetailsAppelDAOFixeImpl extends BaseDAOFixe implements
		IDetailsAppelDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(DetailsAppelDAOFixeImpl.class);

	/** La constante GETLISTEDETAILS. */
	public static final String GETLISTEDETAILS = DetailsAppelDAOFixeImpl.class
			.getName() + ".GETLISTEDETAILS";
	
	/*
	 * f.tatbi FC 5864 ajout de requete sql pour verifier l'existence de
	 * contractid
	 */
	public static final String GETDETAILSCONTRACTID = DetailsAppelDAOFixeImpl.class
			.getName() + ".GETDETAILSCONTRACTID";



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.detailsappel.IDetailsAppelDAO#getDetailsAppels
	 * (java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */

	public List<DetailsAppelWS> getDetailsAppels(final String customerId,
			final String contractIdBscs, final String dateDebut,
			final String dateFin) {
		LOG.debug(
				"--> getDetailsAppels contractIdBsc {} customerId {} dateDebut "
						+ dateDebut + " dateFin " + dateFin, contractIdBscs,
				customerId);
		List<DetailsAppelWS> listDetailsAppel = null;
		final String sql = CustomSQLUtil.get(GETLISTEDETAILS);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put(GRCWSConstantes.CUSTOMER_ID, customerId);
		namedParameters.put("contractIdBscs", contractIdBscs);
		namedParameters.put("dateDebut", dateDebut);
		namedParameters.put("dateFin", dateFin);

		final CustomBeanPropertyRowMapper<DetailsAppelWS> detailsAppelsRowMapper = new CustomBeanPropertyRowMapper<DetailsAppelWS>(
				DetailsAppelWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		listDetailsAppel = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, detailsAppelsRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- getDetailsAppels");
		return listDetailsAppel;
	}

	public CustomDetailWS getContract(final String contractIdBscs) {
		LOG.debug("--> isContractExist  contractID {}  ", contractIdBscs);
		List<CustomDetailWS> list = null;
		final String sql = CustomSQLUtil.get(GETDETAILSCONTRACTID);

		final Map<String, String> namedParameters = new HashMap<String, String>();
		namedParameters.put("contractIdBscs", contractIdBscs);
		final CustomBeanPropertyRowMapper<CustomDetailWS> detailsAppelsRowMapper = new CustomBeanPropertyRowMapper<CustomDetailWS>(
				CustomDetailWS.class);

		LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
		list = super.getNamedParameterJdbcTemplate().query(sql,
				namedParameters, detailsAppelsRowMapper);
		LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

		LOG.debug("<-- isContractExist");
		return (list != null && !list.isEmpty()) ? list.get(0) : null;
	}

}
