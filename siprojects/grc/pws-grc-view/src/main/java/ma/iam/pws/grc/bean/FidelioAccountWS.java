/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class FidelioAccountWS.
 */
public class FidelioAccountWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 3774341367560289706L;

    /** The account id. */
    private String accountId;

    /** The entry date. */
    private String entryDate;

    /** The update date. */
    private String updateDate;

    /** The credit. */
    private String credit;

    /** The credit en cours. */
    private String creditEnCours;

    /** The description. */
    private String description;

    /** The code fidelio. */
    private String codeFidelio;

    /** La qualite fidelio. */
    private String qualiteFidelio;

    /**
     * Gets the account id.
     * 
     * @return the account id
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the account id.
     * 
     * @param accountId
     *            the new account id
     */
    public void setAccountId(final String accountId) {
        this.accountId = accountId;
    }

    /**
     * Gets the entry date.
     * 
     * @return the entry date
     */
    public String getEntryDate() {
        return entryDate;
    }

    /**
     * Sets the entry date.
     * 
     * @param entryDate
     *            the new entry date
     */
    public void setEntryDate(final String entryDate) {
        this.entryDate = entryDate;
    }

    /**
     * Gets the credit.
     * 
     * @return the credit
     */
    public String getCredit() {
        return credit;
    }

    /**
     * Sets the credit.
     * 
     * @param credit
     *            the new credit
     */
    public void setCredit(final String credit) {
        this.credit = credit;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the credit en cours.
     * 
     * @return the credit en cours
     */
    public String getCreditEnCours() {
        return creditEnCours;
    }

    /**
     * Sets the credit en cours.
     * 
     * @param creditEnCours
     *            the new credit en cours
     */
    public void setCreditEnCours(final String creditEnCours) {
        this.creditEnCours = creditEnCours;
    }

    /**
     * Gets the code fidelio.
     * 
     * @return the code fidelio
     */
    public String getCodeFidelio() {
        return codeFidelio;
    }

    /**
     * Sets the code fidelio.
     * 
     * @param codeFidelio
     *            the new code fidelio
     */
    public void setCodeFidelio(final String codeFidelio) {
        this.codeFidelio = codeFidelio;
    }

    /**
     * Gets the update date.
     * 
     * @return the update date
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the update date.
     * 
     * @param updateDate
     *            the new update date
     */
    public void setUpdateDate(final String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Gets the qualite fidelio.
     * 
     * @return the qualite fidelio
     */
    public String getQualiteFidelio() {
        return qualiteFidelio;
    }

    /**
     * Sets the qualite fidelio.
     * 
     * @param qualiteFidelio
     *            the new qualite fidelio
     */
    public void setQualiteFidelio(final String qualiteFidelio) {
        this.qualiteFidelio = qualiteFidelio;
    }

}
