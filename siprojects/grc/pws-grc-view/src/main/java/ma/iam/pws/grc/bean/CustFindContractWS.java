package ma.iam.pws.grc.bean;

public class CustFindContractWS {
	private String customerId;
	private String coId;
	private String nd;
	private String statut_contrat;
	private String libellePT;
	private String codePT;
	private String dateActivation;
	private String date_engagement;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCoId() {
		return coId;
	}
	public void setCoId(String coId) {
		this.coId = coId;
	}
	public String getNd() {
		return nd;
	}
	public void setNd(String nd) {
		this.nd = nd;
	}
	public String getStatut_contrat() {
		return statut_contrat;
	}
	public void setStatut_contrat(String statut_contrat) {
		this.statut_contrat = statut_contrat;
	}
	
	public String getCodePT() {
		return codePT;
	}
	public void setCodePT(String codePT) {
		this.codePT = codePT;
	}
	public String getDateActivation() {
		return dateActivation;
	}
	public void setDateActivation(String dateActivation) {
		this.dateActivation = dateActivation;
	}
	public String getDate_engagement() {
		return date_engagement;
	}
	public void setDate_engagement(String date_engagement) {
		this.date_engagement = date_engagement;
	}
	public String getLibellePT() {
		return libellePT;
	}
	public void setLibellePT(String libellePT) {
		this.libellePT = libellePT;
	}
	public CustFindContractWS(String customerId) {
		super();
		this.customerId = customerId;
	}
	public CustFindContractWS() {
		super();
	}

}
