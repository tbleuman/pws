/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class FeesWS.
 */
public class MemoAddInfoWS extends GrcWS {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8810682240063475685L;

	/** The memo id. */
	private Integer memoId;

	/** The param id. */
	private Integer paramId;

	/** The value id. */
	private Integer valueId;

	/** The value type. */
	private String valueType;

	/** The value string. */
	private String valueString;

	/**
	 * Gets the memo id.
	 * 
	 * @return the memo id
	 */
	public Integer getMemoId() {
		return memoId;
	}

	/**
	 * Sets the memo id.
	 * 
	 * @param memoId
	 *            the new memo id
	 */
	public void setMemoId(final Integer memoId) {
		this.memoId = memoId;
	}

	/**
	 * Gets the param id.
	 * 
	 * @return the param id
	 */
	public Integer getParamId() {
		return paramId;
	}

	/**
	 * Sets the param id.
	 * 
	 * @param paramId
	 *            the new param id
	 */
	public void setParamId(final Integer paramId) {
		this.paramId = paramId;
	}

	/**
	 * Gets the value id.
	 * 
	 * @return the value id
	 */
	public Integer getValueId() {
		return valueId;
	}

	/**
	 * Sets the value id.
	 * 
	 * @param valueId
	 *            the new value id
	 */
	public void setValueId(final Integer valueId) {
		this.valueId = valueId;
	}

	/**
	 * Gets the value type.
	 * 
	 * @return the value type
	 */
	public String getValueType() {
		return valueType;
	}

	/**
	 * Sets the value type.
	 * 
	 * @param valueType
	 *            the new value type
	 */
	public void setValueType(final String valueType) {
		this.valueType = valueType;
	}

	/**
	 * Gets the value string.
	 * 
	 * @return the value string
	 */
	public String getValueString() {
		return valueString;
	}

	/**
	 * Sets the value string.
	 * 
	 * @param valueString
	 *            the new value string
	 */
	public void setValueString(final String valueString) {
		this.valueString = valueString;
	}

}
