/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import java.io.Serializable;

import fr.capgemini.iam.grc.core.utils.GRCReflectUtils;

/**
 * The Class GrcWS.
 */
public class GrcWS implements Serializable {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -8616113284323002751L;

    protected StringBuilder sb;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    
    public String toString() {
        return GRCReflectUtils.getFormattedStringValue(this);

    }

}
