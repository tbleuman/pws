/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class DemandeWS.
 */
public class DemandeWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -1065189238787002072L;

    /** The commande id. */
    private String commandeId;

    /** The order type produit id. */
    private String orderTypeProduitId;

    /** The order type code. */
    private String orderTypeCode;

    /** The order status code. */
    private String orderStatusCode;

    /** The com order id. */
    private String comOrderId;

    /** The linenumber. */
    private String linenumber;

    /** The customer ref. */
    private String customerRef;

    /** The order batch id. */
    private String orderBatchId;

    /** The order agence. */
    private String orderAgence;

    /** The order status date. */
    private String orderStatusDate;

    /** The order context. */
    private String orderContext;

    /** The order creation date. */
    private String orderCreationDate;

    /** The order rp id. */
    private String orderRpId;

    /**
     * Gets the order type produit id.
     * 
     * @return the order type produit id
     */
    public String getOrderTypeProduitId() {
        return orderTypeProduitId;
    }

    /**
     * Sets the order type produit id.
     * 
     * @param orderTypeProduitId
     *            the new order type produit id
     */
    public void setOrderTypeProduitId(final String orderTypeProduitId) {
        this.orderTypeProduitId = orderTypeProduitId;
    }

    /**
     * Gets the order type code.
     * 
     * @return the order type code
     */
    public String getOrderTypeCode() {
        return orderTypeCode;
    }

    /**
     * Sets the order type code.
     * 
     * @param orderTypeCode
     *            the new order type code
     */
    public void setOrderTypeCode(final String orderTypeCode) {
        this.orderTypeCode = orderTypeCode;
    }

    /**
     * Gets the order status code.
     * 
     * @return the order status code
     */
    public String getOrderStatusCode() {
        return orderStatusCode;
    }

    /**
     * Sets the order status code.
     * 
     * @param orderStatusCode
     *            the new order status code
     */
    public void setOrderStatusCode(final String orderStatusCode) {
        this.orderStatusCode = orderStatusCode;
    }

    /**
     * Gets the com order id.
     * 
     * @return the com order id
     */
    public String getComOrderId() {
        return comOrderId;
    }

    /**
     * Sets the com order id.
     * 
     * @param comOrderId
     *            the new com order id
     */
    public void setComOrderId(final String comOrderId) {
        this.comOrderId = comOrderId;
    }

    /**
     * Gets the linenumber.
     * 
     * @return the linenumber
     */
    public String getLinenumber() {
        return linenumber;
    }

    /**
     * Sets the linenumber.
     * 
     * @param linenumber
     *            the new linenumber
     */
    public void setLinenumber(final String linenumber) {
        this.linenumber = linenumber;
    }

    /**
     * Gets the customer ref.
     * 
     * @return the customer ref
     */
    public String getCustomerRef() {
        return customerRef;
    }

    /**
     * Sets the customer ref.
     * 
     * @param customerRef
     *            the new customer ref
     */
    public void setCustomerRef(final String customerRef) {
        this.customerRef = customerRef;
    }

    /**
     * Gets the order batch id.
     * 
     * @return the order batch id
     */
    public String getOrderBatchId() {
        return orderBatchId;
    }

    /**
     * Sets the order batch id.
     * 
     * @param orderBatchId
     *            the new order batch id
     */
    public void setOrderBatchId(final String orderBatchId) {
        this.orderBatchId = orderBatchId;
    }

    /**
     * Gets the order agence.
     * 
     * @return the order agence
     */
    public String getOrderAgence() {
        return orderAgence;
    }

    /**
     * Sets the order agence.
     * 
     * @param orderAgence
     *            the new order agence
     */
    public void setOrderAgence(final String orderAgence) {
        this.orderAgence = orderAgence;
    }

    /**
     * Gets the order status date.
     * 
     * @return the order status date
     */
    public String getOrderStatusDate() {
        return orderStatusDate;
    }

    /**
     * Sets the order status date.
     * 
     * @param orderStatusDate
     *            the new order status date
     */
    public void setOrderStatusDate(final String orderStatusDate) {
        this.orderStatusDate = orderStatusDate;
    }

    /**
     * Gets the order context.
     * 
     * @return the order context
     */
    public String getOrderContext() {
        return orderContext;
    }

    /**
     * Sets the order context.
     * 
     * @param orderContext
     *            the new order context
     */
    public void setOrderContext(final String orderContext) {
        this.orderContext = orderContext;
    }

    /**
     * Gets the order creation date.
     * 
     * @return the order creation date
     */
    public String getOrderCreationDate() {
        return orderCreationDate;
    }

    /**
     * Sets the order creation date.
     * 
     * @param orderCreationDate
     *            the new order creation date
     */
    public void setOrderCreationDate(final String orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    /**
     * Gets the order rp id.
     * 
     * @return the order rp id
     */
    public String getOrderRpId() {
        return orderRpId;
    }

    /**
     * Sets the order rp id.
     * 
     * @param orderRpId
     *            the new order rp id
     */
    public void setOrderRpId(final String orderRpId) {
        this.orderRpId = orderRpId;
    }

    /**
     * Gets the commande id.
     * 
     * @return the commande id
     */
    public String getCommandeId() {
        return commandeId;
    }

    /**
     * Sets the commande id.
     * 
     * @param commandeId
     *            the new commande id
     */
    public void setCommandeId(final String commandeId) {
        this.commandeId = commandeId;
    }

}
