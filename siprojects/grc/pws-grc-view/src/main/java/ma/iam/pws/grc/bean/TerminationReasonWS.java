package ma.iam.pws.grc.bean;

/**
 * The Class TerminationReasonWS.
 */
public class TerminationReasonWS extends GrcWS {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2530942786742515942L;

    /** The reason id. */
    private String reasonId;

    /** The reason cd. */
    private String reasonCd;

    /** The reason desc. */
    private String reasonDesc;

    /**
     * Gets the reason id.
     * 
     * @return the reason id
     */
    public String getReasonId() {
        return reasonId;
    }

    /**
     * Sets the reason id.
     * 
     * @param reasonId
     *            the new reason id
     */
    public void setReasonId(final String reasonId) {
        this.reasonId = reasonId;
    }

    /**
     * Gets the reason cd.
     * 
     * @return the reason cd
     */
    public String getReasonCd() {
        return reasonCd;
    }

    /**
     * Sets the reason cd.
     * 
     * @param reasonCd
     *            the new reason cd
     */
    public void setReasonCd(final String reasonCd) {
        this.reasonCd = reasonCd;
    }

    /**
     * Gets the reason desc.
     * 
     * @return the reason desc
     */
    public String getReasonDesc() {
        return reasonDesc;
    }

    /**
     * Sets the reason desc.
     * 
     * @param reasonDesc
     *            the new reason desc
     */
    public void setReasonDesc(final String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }
}
