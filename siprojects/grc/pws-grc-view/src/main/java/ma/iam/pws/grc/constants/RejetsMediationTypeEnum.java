/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

/**
 * Enumération contenant pour chaque paramétre et action_id, le type de ticket é
 * créer ainsi que le commentaire é rajouter é l'action<br>
 * Attention: l'ordre des elements est important:
 */
public enum RejetsMediationTypeEnum {

    /** The RESIDENTIEL. */
    NO_ERROR("NO ERROR", null, RejetsMediationConstantes.NO_TYPE_ERROR, "SSV:Pour investigation, rejet sans motif "),

    VMS("VMS", null, RejetsMediationConstantes.VMS_TYPE_ERROR, "SSV:Pour investigation, rejet VMS "),

    KPSAIN_00("KPSAIN2-ERROR(00) Fichier OUT de la commande CIB est mal formate", null, RejetsMediationConstantes.KPSA_TYPE_ERROR,
            "Message d'erreur é analyser"),

    KPSAIN_55("KPSAIN2-ERROR(55) MSISDN est introuvable", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    KPSAIN_56("KPSAIN2-ERROR(56) Erreur retournee par l IN", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    KPSAIN("HUAWEI-OCS", null, RejetsMediationConstantes.IN_TYPE_ERROR, "SSV:Pour investigation, rejet Platform IN "),

    MMS("MMS", null, RejetsMediationConstantes.MMS_TYPE_ERROR, "SSV:Pour investigation, rejet Platform MMS"),

    DASP("DASP", null, RejetsMediationConstantes.DASP_TYPE_ERROR, "SSV:Pour investigation, rejet Platform DASP "),

    IMPS("IMPS", null, RejetsMediationConstantes.IMPS_TYPE_ERROR, "SSV:Pour investigation, rejet Platform IMPS "),

    ERREUR_IMPS("Erreur de connexion PF imps", null, RejetsMediationConstantes.IMPS_TYPE_ERROR, "SSV:Pour investigation, rejet Platform IMPS "),

    PCRF("PCRF", null, RejetsMediationConstantes.PCRF_TYPE_ERROR, "CL:Pour action manuelle sur HLR et PCRF, rejet Platform PCRF "),

    MSISDN_ALREADY_EXISTS("MSISDN already exists in the BDD", null, RejetsMediationConstantes.MTV_TYPE_ERROR, "SSV:Pour investigation, rejet Platform MTV "),

    MSISDN_DOES_NOT_EXIST("MSISDN does not exist on the BDD", null, RejetsMediationConstantes.MTV_TYPE_ERROR, "SSV:Pour investigation, rejet Platform MTV "),

    MSISDN_IS_NOT_VALID("MSISDN is not valid in the BDD", null, RejetsMediationConstantes.MTV_TYPE_ERROR, "SSV:Pour investigation, rejet Platform MTV "),

    KPSAPOC("KPSAPOC-ERROR(020) MDN ou MIN non existant dans la BDD PoC", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse"),

    HLR_CART("HLR Cart connection failed", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse"),

    HLR_NOKIA("hlr nokia 4-Cart session is unavailable", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse"),

    MMSI_CART("mmsi-Cart session is unavailable", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse"),

    MMSI_PROBLEME("mmsi-Probleme de connexion Maximum tentatives de connexion atteint", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse"),

    CART("Cart ", null, RejetsMediationConstantes.PLATFORM_TYPE_ERROR, "SEM:Pour investigation, Probléme de connexion Platform "),

    CONNEXION_PF("connexion PF", null, RejetsMediationConstantes.PLATFORM_TYPE_ERROR, "SEM:Pour investigation, Probléme de connexion Platform "),

    BLACKBERRY_RIM("BlackBerry RIM - Code Erreur (61040) inconnue", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    JETMULTIMEDIA_406("JetMultiMedia - (406) - Type d operation invalide", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    JETMULTIMEDIA_407("JetMultiMedia - (407) - Service inconnu", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    KPSA_UNKNOWN_RROR("KPSA UNKNOWN ERROR", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "SSV:Pour investigation, Bug KPSA"),

    KPSA_ERROR("KPSA-ERROR(1) Operation inconnue sur XEV 25", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "SSV:Pour investigation, Bug KPSA"),

    KPSA_3003("KPSA (3003)  HLR Huawei - Template non definie", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSA_ERROR_GPRS("KPSA-ERROR(1) GPRS Data service ne peut pas etre supprime pour ce contrat", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSA_ERROR_SPEAT("KPSA-ERROR(1) GPRS-Data services ou SPEAT, SPEDT ne sont pas trouves ", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSA_ERROR_MSISDN("KPSA-ERROR(1) MSISDN introuvable", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSA_ERROR_FOM("KPSA-ERROR(1) Plusieurs FoM services trouves sur le contract", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSA_ERROR_AHAH("KPSA-ERROR(1) Service inconnue AH//AH", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSAIN2_ERROR("KPSAIN2-ERROR(1) IN destination address not found", null, RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    OK_IN("OK-IN INDEX not defined in SOD- ", null, RejetsMediationConstantes.USER_TYPE_ERROR, "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    NPTE("NPTE Parsing error ...", null, RejetsMediationConstantes.USER_TYPE_ERROR, "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    FOP("FOP Parsing error", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    ABONNE_NON_RECONNU("Abonne non reconnu dans l HLR", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    ABONNE_EXISTE_DEJA("Abonne existe deja sur l HLR", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    CLIENT_DEJA_EXISTANT("Client deja existant", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    MSISDN_DEJA_ATTRIBUE("MSISDN deja attribue", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_006("KPSA (006)  HLR Nokia - Service basique existe deja", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    CLIENT_INEXISTANT("Client inexistant", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    CARTE_SIM_INCONNUE("Carte SIM inconnue au HLR", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    TYPE_DE_CARTE_INVALIDE("Type de carte invalide", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    CARTE_NON_AUTHENTIFIEE("Carte non authentifiee", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    AUC("AUC", null, RejetsMediationConstantes.HLR_TYPE_ERROR, "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_001("KPSA (001)  HLR Nokia - Abonne non reconnu dans l HLR", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_022("KPSA (022)  HLR Nokia - Erreur DX inconnue", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_3001("KPSA (3001)  HLR Huawei - Client inexistant", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_3007("KPSA (3007)  HLR Huawei - Enregistrement non defini", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_3016("KPSA (3016)  HLR Huawei - Service basique non active", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_3007_A8("KPSA (3007)  HLR Huawei - Enregistrement non defini", "8", RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    KPSA_3007_A9("KPSA (3007)  HLR Huawei - Enregistrement non defini", "9", RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    KPSA_3007_X("KPSA (3007)  HLR Huawei - Enregistrement non defini", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "SSV: Nouveau Message é analyser "),

    ERREUR_INTERNE_A1("Erreur interne dans la base des donnees", "1", RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    ERREUR_INTERNE_X("Erreur interne dans la base des donnees", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    INCOMPATIBILITE_SERVICES_A1("Incompatibilite entre services supplementaires", "1", RejetsMediationConstantes.USER_TYPE_ERROR,
            "HOTLINE:Pour coordination avec agences : Erreur Utilisateur :"),

    INCOMPATIBILITE_SERVICES_X("Incompatibilite entre services supplementaires", null, RejetsMediationConstantes.HLR_TYPE_ERROR,
            "CL:Consultation puis envoyer au Sce Fact pour analyse "),

    IN_NON_DEFINI("numero plateforme IN non defini", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    PROCESS_HLR("processHLR Parsing error", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    PROCESS_NIFI("processNIFI Parsing error", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    BSG_PARAMETER("OK-BSG PARAMETER not defined in SOD", null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "Message d'erreur é analyser"),

    AUTRE(null, null, RejetsMediationConstantes.KPSA_TYPE_ERROR, "SSV: Nouveau Message é analyser "), ;

    /** The code category type. */
    private final String prmValue;

    /** The desc category type. */
    private final String typeId;

    /** The action id. */
    private final String actionId;

    private final String commentaire;

    /**
     * Instantiates a new cust acct category type enum.
     * 
     * @param codeCategoryType
     *            the code category type
     * @param descCategoryType
     *            the desc category type
     */
    RejetsMediationTypeEnum(final String prmValue, final String actionId, final String typeId, final String commentaire) {
        this.prmValue = prmValue;
        this.actionId = actionId;
        this.typeId = typeId;
        this.commentaire = commentaire;
    }

    /**
     * @return the prmValue
     */
    public String getPrmValue() {
        return prmValue;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @return the entity
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @return the actionId
     */
    public String getActionId() {
        return actionId;
    }

}
