/*
 * @author Capgemini
 */
package ma.iam.pws.grc.business;

import java.util.List;

import ma.iam.pws.grc.bean.FidelioAccountWS;
import ma.iam.pws.grc.bean.FidelioActionHistWS;
import ma.iam.pws.grc.bean.FidelioOrderDetailWS;
import ma.iam.pws.grc.bean.FidelioOrderHistWS;
import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCProperties;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import ma.iam.pws.grc.dao.fidelio.IFidelioDAO;
import ma.iam.pws.grc.exceptions.GrcWsException;
import ma.iam.pws.grc.util.WSValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class FidelioManagerBiz.
 */
@Component(value = "fidelioManagerBiz")
public class FidelioManagerBiz extends CommonManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(FidelioManagerBiz.class);

	/**
	 * Cherche les comptes fidelio.
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param customer_id
	 * 
	 * @param product
	 *            le produit
	 * @return the fidelio account ws
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	/*
	 * f.tatbi FC 5864 add parameter customer_id
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public FidelioAccountWS findFidelioAccount(final String customer_id,
			final String custCode, final int product) throws GrcWsException {
		LOG.info("-> findFidelioAccount: custCode {}", custCode);
		if (GRCStringUtil.isNullOrEmpty(custCode)
				&& GRCStringUtil.isNullOrEmpty(customer_id)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"custCode ou customer_id doit étre rensignée");
		}
		final IFidelioDAO iFidelioDAO = this.getFidelioDAOForProduct(product);

		final FidelioAccountWS fidelioAccountWS = iFidelioDAO
				.findFidelioAccount(customer_id, custCode);
		LOG.info("<- findFidelioAccount fidelioAccountWS {}", fidelioAccountWS);
		return fidelioAccountWS;
	}

	/**
	 * Cherche l'historique des actions fidelio.
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<FidelioActionHistWS> findFidelioActionHist(
			final String customer_id, final String custCode, final int product)
			throws GrcWsException {
		LOG.info("-> findFidelioActionHist: custCode{}", custCode);
		if (GRCStringUtil.isNullOrEmpty(custCode)
				&& GRCStringUtil.isNullOrEmpty(customer_id)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"custCode ou customer_id doit étre rensignée");
		}

		final IFidelioDAO iFidelioDAO = this.getFidelioDAOForProduct(product);

		final List<FidelioActionHistWS> listFidelioActionHistWS = iFidelioDAO
				.findFidelioActionHist(customer_id, custCode);
		LOG.info("<- findFidelioActionHist");
		return listFidelioActionHistWS;
	}

	/*
	 * f.tatbi FC 5864 add parameter customer_id
	 */
	/**
	 * Cherche l'historique des ordres fidélio.
	 * 
	 * @param custCode
	 *            le code client au niveau de bscs (CUSTCODE)
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<FidelioOrderHistWS> findFidelioOrderHist(
			final String customer_id, final String custCode, final int product)
			throws GrcWsException {
		LOG.info("-> findFidelioOrderHist: custCode {}", custCode);
		if (GRCStringUtil.isNullOrEmpty(custCode)
				&& GRCStringUtil.isNullOrEmpty(customer_id)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"custCode ou customer_id doit étre rensignée");
		}

		final IFidelioDAO iFidelioDAO = this.getFidelioDAOForProduct(product);

		final List<FidelioOrderHistWS> listFidelioOrderHistWS = iFidelioDAO
				.findFidelioOrderHist(customer_id, custCode);
		LOG.info("<- findFidelioOrderHist");
		return listFidelioOrderHistWS;
	}

	/**
	 * Cherche les détails des ordres fidelio.
	 * 
	 * @param orderId
	 *            l'identifiant de l'ordre de conversion des points fidélio
	 * @param product
	 *            le produit
	 * @return la liste des résultats
	 * @throws GrcWsException
	 *             l'exception GrcWsException
	 */
	@Transactional(readOnly = true, timeout = GRCProperties.TIMEOUT)
	public List<FidelioOrderDetailWS> findFidelioOrderDetail(
			final String orderId, final int product) throws GrcWsException {
		LOG.info("-> findFidelioOrderDetail: orderId  {}", orderId);
		if (!WSValidator.validateNumber(orderId)) {
			throw new GrcWsException(ExceptionCodeTypeEnum.PROBLEM_CRITERIA_WS,
					"orderId incorrect : " + orderId);
		}

		final IFidelioDAO iFidelioDAO = this.getFidelioDAOForProduct(product);

		final List<FidelioOrderDetailWS> listFidelioOrderDetailWS = iFidelioDAO
				.findFidelioOrderDetail(orderId);
		LOG.info("<- findFidelioOrderDetail");
		return listFidelioOrderDetailWS;
	}

}
