/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.memo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.pws.grc.bean.MemoAddInfoWS;
import ma.iam.pws.grc.bean.MemoWS;
import ma.iam.pws.grc.bean.MemoWiamWS;
import ma.iam.pws.grc.bean.TerminationReasonWS;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import ma.iam.pws.grc.dao.BaseDAOFixe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class MemoDAOFixeImpl.
 */
@Repository
public class MemoDAOFixeImpl extends BaseDAOFixe implements IMemoDAO {

    /** Le logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(MemoDAOFixeImpl.class);

    /** La constante GETLISTEMEMO. */
    public static final String GETLISTEMEMO = MemoDAOFixeImpl.class.getName() + ".GETLISTEMEMO";

    /** The Constant INSERTMEMOWIAM. */
    public static final String INSERTMEMOWIAM = MemoDAOFixeImpl.class.getName() + ".INSERTMEMOWIAM";

    /** The Constant INSERTMEMOADDINFOVALUE. */
    public static final String INSERTMEMOADDINFOVALUE = MemoDAOFixeImpl.class.getName() + ".INSERTMEMOADDINFOVALUE";

    /** The Constant INSERTMEMOADDINFO. */
    public static final String INSERTMEMOADDINFO = MemoDAOFixeImpl.class.getName() + ".INSERTMEMOADDINFO";

    /** The Constant GETTERMINATIONREASON. */
    public static final String GETTERMINATIONREASON = MemoDAOFixeImpl.class.getName() + ".GETTERMINATIONREASON";

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.memo.IMemoDAO#findMemos(java.lang.String,
     * java.lang.String)
     */
    
    public List<MemoWS> findMemos(final String customerId, final String contractIdBscs) {
        LOG.debug("--> findMemos contractIdBsc {} customerId {}", contractIdBscs, customerId);
        List<MemoWS> listMemos = null;
        final String sql = CustomSQLUtil.get(GETLISTEMEMO);

        final Map<String, String> namedParameters = new HashMap<String, String>();
        namedParameters.put("customerId", customerId);
        namedParameters.put("contractIdBscs", contractIdBscs);

        final CustomBeanPropertyRowMapper<MemoWS> memoRowMapper = new CustomBeanPropertyRowMapper<MemoWS>(MemoWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        listMemos = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, memoRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- findMemos");
        return listMemos;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.capgemini.iam.grc.dao.memo.IMemoDAO#insertMemoWiam(ma.iam
     * .grc.bean.MemoWiamWS)
     */
    
    public Boolean insertMemoWiam(final MemoWiamWS memoWiamWS, final List<MemoAddInfoWS> listMemoAddInfos) {
        LOG.debug("--> insertMemoWiam memoWiamWS {}", memoWiamWS);
        final String sql = CustomSQLUtil.get(INSERTMEMOWIAM);
        final String sqlMemoAddInfoValue = CustomSQLUtil.get(INSERTMEMOADDINFOVALUE);
        final String sqlMemoAddInfo = CustomSQLUtil.get(INSERTMEMOADDINFO);

        final SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(memoWiamWS);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        super.getNamedParameterJdbcTemplateNeto().update(sql, namedParameters);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        SqlParameterSource namedParametersMemoAddInfoWS = null;

        if (null != listMemoAddInfos) {
            for (final MemoAddInfoWS memoAddInfoWS : listMemoAddInfos) {
                namedParametersMemoAddInfoWS = new BeanPropertySqlParameterSource(memoAddInfoWS);
                // Insertion dans MEMO_ADD_INFO_VALUE
                LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlMemoAddInfoValue);
                super.getNamedParameterJdbcTemplateNeto().update(sqlMemoAddInfoValue, namedParametersMemoAddInfoWS);
                LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

                // Insertion dans MEMO_ADD_INFO
                LOG.debug(GRCWSConstantes.EXEC_QUERY, sqlMemoAddInfo);
                super.getNamedParameterJdbcTemplateNeto().update(sqlMemoAddInfo, namedParametersMemoAddInfoWS);
                LOG.debug(GRCWSConstantes.EXEC_QUERY_END);
            }

        }
        LOG.debug("<-- insertMemoWiam");
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.dao.memo.IMemoDAO#getTerminationReasons()
     */
    
    public List<TerminationReasonWS> getTerminationReasons() {
        LOG.debug("--> getTerminationReasons");

        final String sql = CustomSQLUtil.get(GETTERMINATIONREASON);

        final CustomBeanPropertyRowMapper<TerminationReasonWS> reasonRowMapper = new CustomBeanPropertyRowMapper<TerminationReasonWS>(TerminationReasonWS.class);

        LOG.debug(GRCWSConstantes.EXEC_QUERY, sql);
        final List<TerminationReasonWS> listReasons = super.getNamedParameterJdbcTemplateNeto().query(sql, new HashMap<String, String>(), reasonRowMapper);
        LOG.debug(GRCWSConstantes.EXEC_QUERY_END);

        LOG.debug("<-- getTerminationReasons");
        return listReasons;
    }

}
