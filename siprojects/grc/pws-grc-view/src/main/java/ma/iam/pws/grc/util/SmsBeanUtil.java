package ma.iam.pws.grc.util;

import ma.iam.pws.grc.bean.SmsDaoWS;
import ma.iam.pws.grc.bean.SmsWS;

public class SmsBeanUtil {

	public static final SmsDaoWS Sms2SmsDao(SmsWS smsWS) {
		SmsDaoWS daoWS = null;
		if (smsWS != null) {
			daoWS = new SmsDaoWS();
			daoWS.setApplication(smsWS.getApplication());
			daoWS.setNd(smsWS.getNd());
//			daoWS.setNpCode(smsWS.getNpCode());
//			daoWS.setNpShdes(smsWS.getNpShdes());
			daoWS.setSmsText(smsWS.getSmsText());
		}
		return daoWS;
	}
}
