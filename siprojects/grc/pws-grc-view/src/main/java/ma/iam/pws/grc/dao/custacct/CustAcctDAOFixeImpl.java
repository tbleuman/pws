/*
 * @author Capgemini
 */
package ma.iam.pws.grc.dao.custacct;

import java.util.List;

import ma.iam.pws.grc.bean.CustAcctWS;
import ma.iam.pws.grc.dao.CommonDAOFixe;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

// TODO: Auto-generated Javadoc
/**
 * The Class CustAcctDAOFixeImpl.
 */
@Repository
@Deprecated
public class CustAcctDAOFixeImpl extends CommonDAOFixe implements ICustAcctDAO {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CustAcctDAOFixeImpl.class);

	private String getAppendedSQL(final CustAcctWS searchCustAcctBean,
			final int product) throws GrcWsException {
		String appendSql = getAppendedSQLByCCU(searchCustAcctBean.getCcu(),
				product);
		appendSql = appendSql.concat(getAppendedSQLByCommande(
				searchCustAcctBean.getCommande(), product));
		return appendSql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.capgemini.iam.grc.dao.custacct.ICustAcctDAO#findContractsForCustomer
	 * (fr.capgemini.iam.grc.bean.CustAcctWS)
	 */

	public List<CustAcctWS> findContractsForCustomer(
			final CustAcctWS searchCustAcctBean, final Boolean allowVIP,
			final int product) throws GrcWsException {
		LOG.debug(
				"--> findContractsForCustomer allowVIP {} searchCustAcctBean {}",
				allowVIP, searchCustAcctBean);

		normaliserND(searchCustAcctBean);

		final String appendSql = getAppendedSQL(searchCustAcctBean, product);

		final List<CustAcctWS> listCustAcct = getListCustAcctWS(appendSql,
				searchCustAcctBean, allowVIP, product);

		addCustAcctByCommande(listCustAcct, searchCustAcctBean, product);

		LOG.debug("<-- findContractsForCustomer");
		return listCustAcct;
	}

}
