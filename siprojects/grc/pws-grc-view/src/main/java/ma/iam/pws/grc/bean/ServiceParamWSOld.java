/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;


/**
 * The Class ServiceParamWS.
 */
public class ServiceParamWSOld extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -5251838196144505681L;

    /** The param id. */
    private Integer paramId;

    /** The number. */
    private String number;

    /** The order. */
    private String order;

    /** The parent seq no. */
    private String parentSeqNo;

    /** The sibling seq no. */
    private String siblingSeqNo;

    /** The complex seq no. */
    private String complexSeqNo;

    /** The value seq no. */
    private String valueSeqNo;

    /** The sc code. */
    private String scCode;

    /** The deleted flag. */
    private String deletedFlag;

    /** The name. */
    private String name;

    /** The desc. */
    private String desc;

    /** The request id. */
    private String requestId;

    /** The validation date. */
    private String validationDate;

    /**
     * Gets l'identifiant du paramétre.
     * 
     * @return l'identifiant du paramétre
     */
    public Integer getParamId() {
        return paramId;
    }

    /**
     * Sets l'identifiant du paramétre.
     * 
     * @param paramId
     *            the new param id
     */
    public void setParamId(final Integer paramId) {
        this.paramId = paramId;
    }

    /**
     * Gets the number.
     * 
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number.
     * 
     * @param number
     *            the new number
     */
    public void setNumber(final String number) {
        this.number = number;
    }

    /**
     * Gets the order.
     * 
     * @return the order
     */
    public String getOrder() {
        return order;
    }

    /**
     * Sets the order.
     * 
     * @param order
     *            the new order
     */
    public void setOrder(final String order) {
        this.order = order;
    }

    /**
     * Gets the parent seq no.
     * 
     * @return the parent seq no
     */
    public String getParentSeqNo() {
        return parentSeqNo;
    }

    /**
     * Sets the parent seq no.
     * 
     * @param parentSeqNo
     *            the new parent seq no
     */
    public void setParentSeqNo(final String parentSeqNo) {
        this.parentSeqNo = parentSeqNo;
    }

    /**
     * Gets the sibling seq no.
     * 
     * @return the sibling seq no
     */
    public String getSiblingSeqNo() {
        return siblingSeqNo;
    }

    /**
     * Sets the sibling seq no.
     * 
     * @param siblingSeqNo
     *            the new sibling seq no
     */
    public void setSiblingSeqNo(final String siblingSeqNo) {
        this.siblingSeqNo = siblingSeqNo;
    }

    /**
     * Gets the complex seq no.
     * 
     * @return the complex seq no
     */
    public String getComplexSeqNo() {
        return complexSeqNo;
    }

    /**
     * Sets the complex seq no.
     * 
     * @param complexSeqNo
     *            the new complex seq no
     */
    public void setComplexSeqNo(final String complexSeqNo) {
        this.complexSeqNo = complexSeqNo;
    }

    /**
     * Gets the value seq no.
     * 
     * @return the value seq no
     */
    public String getValueSeqNo() {
        return valueSeqNo;
    }

    /**
     * Sets the value seq no.
     * 
     * @param valueSeqNo
     *            the new value seq no
     */
    public void setValueSeqNo(final String valueSeqNo) {
        this.valueSeqNo = valueSeqNo;
    }

    /**
     * Gets the sc code.
     * 
     * @return the sc code
     */
    public String getScCode() {
        return scCode;
    }

    /**
     * Sets the sc code.
     * 
     * @param scCode
     *            the new sc code
     */
    public void setScCode(final String scCode) {
        this.scCode = scCode;
    }

    /**
     * Gets the deleted flag.
     * 
     * @return the deleted flag
     */
    public String getDeletedFlag() {
        return deletedFlag;
    }

    /**
     * Sets the deleted flag.
     * 
     * @param deletedFlag
     *            the new deleted flag
     */
    public void setDeletedFlag(final String deletedFlag) {
        this.deletedFlag = deletedFlag;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the desc.
     * 
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Sets the desc.
     * 
     * @param desc
     *            the new desc
     */
    public void setDesc(final String desc) {
        this.desc = desc;
    }

    /**
     * Gets the request id.
     * 
     * @return the request id
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the request id.
     * 
     * @param requestId
     *            the new request id
     */
    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    /**
     * Gets the validation date.
     * 
     * @return the validation date
     */
    public String getValidationDate() {
        return validationDate;
    }

    /**
     * Sets the validation date.
     * 
     * @param validationDate
     *            the new validation date
     */
    public void setValidationDate(final String validationDate) {
        this.validationDate = validationDate;
    }

}
