/*
 * @author Capgemini
 */
package ma.iam.pws.grc.soap.handler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import ma.iam.pws.grc.constants.ExceptionCodeTypeEnum;
import ma.iam.pws.grc.constants.GRCWSConstantes;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import ma.iam.pws.grc.exceptions.GrcWsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GrcWsInvoiceSOAPHandler.
 */
public class GrcWsInvoiceSOAPHandler implements SOAPHandler<SOAPMessageContext> {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(GrcWsInvoiceSOAPHandler.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
     */
    
    public void close(final MessageContext context) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext
     * )
     */
    
    public boolean handleFault(final SOAPMessageContext context) {
        final boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outbound) {
            LOG.trace("Direction=outbound (handleFault)");
        } else {
            LOG.trace("Direction=inbound (handleFault)");
        }
        if (!outbound) {
            final SOAPMessage msg = (context).getMessage();
            dumpSOAPMessage(msg);
        }
        return true;
    }

    /**
     * Gets the message encoding.
     * 
     * @param msg
     *            the msg
     * @return the message encoding
     * @throws SOAPException
     *             the sOAP exception
     */
    private String getMessageEncoding(final SOAPMessage msg) throws SOAPException {
        String encoding = "utf-8";
        if (msg.getProperty(SOAPMessage.CHARACTER_SET_ENCODING) != null) {
            encoding = msg.getProperty(SOAPMessage.CHARACTER_SET_ENCODING).toString();
        }
        return encoding;
    }

    /**
     * Dump soap message.
     * 
     * @param msg
     *            the msg
     */
    private void dumpSOAPMessage(final SOAPMessage msg) {
        if (msg == null) {
            LOG.trace("SOAP Message is null");
            return;
        }
        LOG.trace("");
        LOG.trace("--------------------");
        LOG.trace("DUMP OF SOAP MESSAGE");
        LOG.trace("--------------------");
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            msg.writeTo(baos);
            LOG.trace(baos.toString(getMessageEncoding(msg)));
        } catch (final SOAPException e) {
            LOG.error("SOAPException : ", e);
        } catch (final IOException e) {
            LOG.error("IOException : ", e);
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.
     * MessageContext)
     */
    
    public boolean handleMessage(final SOAPMessageContext context) {

        boolean exit = false;
        // Inquire incoming or outgoing message.
        final boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        SOAPMessage msg = null;
        try {
            if (outbound) {
                LOG.trace("Direction=outbound (handleMessage)");
                msg = context.getMessage();
                final SOAPPart sp = msg.getSOAPPart();
                final SOAPEnvelope env = sp.getEnvelope();
                env.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
                env.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                env.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope");
                dumpSOAPMessage(msg);

            } else {
                LOG.trace("Direction=inbound (handleMessage)");
                msg = (context).getMessage();

                dumpSOAPMessage(msg);
            }
            // get SOAP envelope from SOAP message
            final SOAPEnvelope se = msg.getSOAPPart().getEnvelope();

            // get the headers from envelope
            final SOAPHeader sh = se.getHeader();
            if (sh == null) {
                LOG.debug("--- No headers found in the input SOAP request");
                exit = false;
            } else {
                // call method to process header
                try {
                    exit = processSOAPHeader(sh);
                } catch (final GrcWsException e) {
                    // FIXME : throw access denied exception
                    LOG.error("Exception : ", e);
                }
            }

        } catch (final SOAPException e) {
            LOG.error("Exception : ", e);
            exit = false;
        }

        return exit;
    }

    /**
     * Process soap header.
     * 
     * @param sh
     *            the sh
     * @return true, if successful
     * @throws GrcWsException
     *             the grc ws exception
     */
    private boolean processSOAPHeader(final SOAPHeader sh) throws GrcWsException {
        Boolean retour = Boolean.FALSE;
        if ((null == sh.getFirstChild()) || GRCStringUtil.isNullOrEmpty(sh.getFirstChild().getNamespaceURI())) {
            // Throw Exception : Header Not Found
            throw new GrcWsException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
        } else if (GRCWSConstantes.URI_INVOICE_SERVICE.equals(sh.getFirstChild().getNamespaceURI())) {
            retour = Boolean.TRUE;
        } else {
            // Throw Exception : URI Not Defined
            throw new GrcWsException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
        }
        return retour;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
     */
    
    public Set<QName> getHeaders() {
        return null;
    }

}