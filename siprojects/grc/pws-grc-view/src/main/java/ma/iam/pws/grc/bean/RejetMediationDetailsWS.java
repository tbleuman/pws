/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

/**
 * The Class ContractHistoryWS.
 */
public class RejetMediationDetailsWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    private String prmValues;

    private String actionDes;

    private String actionId;

    private String request;

    private String coId;

    private String customerId;

    private String data1;

    private String data2;

    private String data3;

    private String userId;

    private String dateDerniereModification;

    private String imsi;

    private String msisdn;

    private String portOld;

    private String portNew;

    private String services;

    private String commentaire;

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        sb = new StringBuilder();
        sb.append("[RejetMediationDetailsWS:");
        if (customerId != null) {
            sb.append(",customerId=").append(customerId);
        }
        if (coId != null) {
            sb.append(",coId=").append(coId);
        }
        if (commentaire != null) {
            sb.append(",commentaire=").append(commentaire);
        }
        if (prmValues != null) {
            sb.append(",prmValues=").append(prmValues);
        }
        if (actionDes != null) {
            sb.append(",actionDes=").append(actionDes);
        }
        if (actionId != null) {
            sb.append(",actionId=").append(actionId);
        }
        if (request != null) {
            sb.append(",request=").append(request);
        }
        if (data1 != null) {
            sb.append(",data2=").append(data2);
        }
        if (data2 != null) {
            sb.append(",data2=").append(data2);
        }
        if (data3 != null) {
            sb.append(",data3=").append(data3);
        }
        if (userId != null) {
            sb.append(",userId=").append(userId);
        }
        if (dateDerniereModification != null) {
            sb.append(",dateDerniereModification=").append(dateDerniereModification);
        }
        if (imsi != null) {
            sb.append(",imsi=").append(imsi);
        }
        if (msisdn != null) {
            sb.append(",msisdn=").append(msisdn);
        }
        if (portOld != null) {
            sb.append(",portOld=").append(portOld);
        }
        if (portNew != null) {
            sb.append(",portNew=").append(portNew);
        }
        if (services != null) {
            sb.append(",services=").append(services);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * @return the prmValues
     */
    public String getPrmValues() {
        return prmValues;
    }

    /**
     * @param prmValues
     *            the prmValues to set
     */
    public void setPrmValues(final String prmValues) {
        this.prmValues = prmValues;
    }

    /**
     * @return the actionDes
     */
    public String getActionDes() {
        return actionDes;
    }

    /**
     * @param actionDes
     *            the actionDes to set
     */
    public void setActionDes(final String actionDes) {
        this.actionDes = actionDes;
    }

    /**
     * @return the actionId
     */
    public String getActionId() {
        return actionId;
    }

    /**
     * @param actionId
     *            the actionId to set
     */
    public void setActionId(final String actionId) {
        this.actionId = actionId;
    }

    /**
     * @return the request
     */
    public String getRequest() {
        return request;
    }

    /**
     * @param request
     *            the request to set
     */
    public void setRequest(final String request) {
        this.request = request;
    }

    /**
     * @return the coId
     */
    public String getCoId() {
        return coId;
    }

    /**
     * @param coId
     *            the coId to set
     */
    public void setCoId(final String coId) {
        this.coId = coId;
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the data1
     */
    public String getData1() {
        return data1;
    }

    /**
     * @param data1
     *            the data1 to set
     */
    public void setData1(final String data1) {
        this.data1 = data1;
    }

    /**
     * @return the data2
     */
    public String getData2() {
        return data2;
    }

    /**
     * @param data2
     *            the data2 to set
     */
    public void setData2(final String data2) {
        this.data2 = data2;
    }

    /**
     * @return the data3
     */
    public String getData3() {
        return data3;
    }

    /**
     * @param data3
     *            the data3 to set
     */
    public void setData3(final String data3) {
        this.data3 = data3;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(final String userId) {
        this.userId = userId;
    }

    /**
     * @return the dateDerniereModification
     */
    public String getDateDerniereModification() {
        return dateDerniereModification;
    }

    /**
     * @param dateDerniereModification
     *            the dateDerniereModification to set
     */
    public void setDateDerniereModification(final String dateDerniereModification) {
        this.dateDerniereModification = dateDerniereModification;
    }

    /**
     * @return the imsi
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * @param imsi
     *            the imsi to set
     */
    public void setImsi(final String imsi) {
        this.imsi = imsi;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn
     *            the msisdn to set
     */
    public void setMsisdn(final String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the portOld
     */
    public String getPortOld() {
        return portOld;
    }

    /**
     * @param portOld
     *            the portOld to set
     */
    public void setPortOld(final String portOld) {
        this.portOld = portOld;
    }

    /**
     * @return the portNew
     */
    public String getPortNew() {
        return portNew;
    }

    /**
     * @param portNew
     *            the portNew to set
     */
    public void setPortNew(final String portNew) {
        this.portNew = portNew;
    }

    /**
     * @return the services
     */
    public String getServices() {
        return services;
    }

    /**
     * @param services
     *            the services to set
     */
    public void setServices(final String services) {
        this.services = services;
    }

    /**
     * @return the entity
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @param entity
     *            the entity to set
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

}
