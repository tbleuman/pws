/*
 * @author Capgemini
 */
package ma.iam.pws.grc.constants;

// TODO: Auto-generated Javadoc
/**
 * Constantes correspondant aux rejets de mediation
 */
public final class RejetsMediationConstantes {

    private RejetsMediationConstantes() {
    }

    /**
     * Correspondance entre le code SR 126 et le code GRC pour le type: KPSA
     * Parser errors
     */
    public static final String KPSA_TYPE_ERROR = "KPSA_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 127 et le code GRC pour le type: HLR
     * Related errors
     */
    public static final String HLR_TYPE_ERROR = "HLR_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 128 et le code GRC pour le type: VMS
     * related Errors
     */
    public static final String VMS_TYPE_ERROR = "VMS_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 129 et le code GRC pour le type: MMS
     * Related Errors
     */
    public static final String MMS_TYPE_ERROR = "MMS_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 130 et le code GRC pour le type: IN
     * Related errors
     */
    public static final String IN_TYPE_ERROR = "IN_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 131 et le code GRC pour le type: DASP
     * Related Errors
     */
    public static final String DASP_TYPE_ERROR = "DASP_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 132 et le code GRC pour le type: NO Error
     * Cause
     */
    public static final String NO_TYPE_ERROR = "NO_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 1433 et le code GRC pour le type: Erreur
     * PF IMPS
     */
    public static final String IMPS_TYPE_ERROR = "IMPS_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 1434 et le code GRC pour le type: Erreur
     * PF MTV
     */
    public static final String MTV_TYPE_ERROR = "MTV_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 1435 et le code GRC pour le type:
     * Probléme connexion Plateformes
     */
    public static final String PLATFORM_TYPE_ERROR = "PLATFORM_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 1436 et le code GRC pour le type: Erreur
     * Utilisateur
     */
    public static final String USER_TYPE_ERROR = "USER_TYPE_ERROR";

    /**
     * Correspondance entre le code SR 200380 et le code GRC pour le type:
     * Erreur PF PCRF
     */
    public static final String PCRF_TYPE_ERROR = "PCRF_TYPE_ERROR";

}
