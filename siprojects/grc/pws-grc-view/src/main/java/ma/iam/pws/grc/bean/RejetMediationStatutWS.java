/*
 * @author Capgemini
 */
package ma.iam.pws.grc.bean;

import ma.iam.pws.grc.constants.RejetMediationStatutEnum;

/**
 * The Class ContractHistoryWS.
 */
public class RejetMediationStatutWS extends GrcWS {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = 7318181874005994924L;

    private String requestId;

    private String ticketId;

    /** The status. */
    private String statusCode;

    /** The status desc. */
    private String statusDesc;

    public void setStatus(final RejetMediationStatutEnum rejetMediationStatutEnum) {
        this.statusCode = rejetMediationStatutEnum.getStatusCode();
        this.statusDesc = rejetMediationStatutEnum.getStatusDesc();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.capgemini.iam.grc.core.bean.GrcBean#toString()
     */
    
    public String toString() {
        sb = new StringBuilder();
        sb.append("[RejetMediationStatutWS:");
        if (requestId != null) {
            sb.append(",requestId=").append(requestId);
        }
        if (ticketId != null) {
            sb.append(",ticketId=").append(ticketId);
        }
        if (statusCode != null) {
            sb.append(",statusCode=" + statusCode);
        }
        if (statusDesc != null) {
            sb.append(",statusDesc=" + statusDesc);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId
     *            the requestId to set
     */
    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the ticketId
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * @param ticketId
     *            the ticketId to set
     */
    public void setTicketId(final String ticketId) {
        this.ticketId = ticketId;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *            the statusCode to set
     */
    public void setStatusCode(final String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the statusDesc
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * @param statusDesc
     *            the statusDesc to set
     */
    public void setStatusDesc(final String statusDesc) {
        this.statusDesc = statusDesc;
    }

}
