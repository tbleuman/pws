package ma.iam.mobile.mobicash.business.impl;

import ma.iam.mobile.mobicash.business.ByPassManagerBiz;
import ma.iam.mobile.mobicash.dao.ByPassDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc

@Component(value = "byPassManagerBiz")
public class ByPassManagerBizImpl implements ByPassManagerBiz {

	/** Le logger de la classe. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ByPassManagerBizImpl.class);
	@Autowired
	private ByPassDao byPassDao;

	public Integer modeByPass() {

		LOG.info("acces to fct");
		return (byPassDao.modeByPass()) ? 1 : -1;

	}

}
