package ma.iam.mobile.mobicash.business;


import java.util.ArrayList;
import java.util.Date;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.bean.MobicashOpenInvoice;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;
import ma.iam.mobile.mobicash.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface MobicashManagerBiz {

	public CustomerPosition getOpenInvoiceListMobile (String domain, String uniqueId, String pin) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public String payInvoiceListMobile ( String domain, int customerId, String uniqueId, String pin, String authNum,
			ArrayList<MobicashOpenInvoice> invoiceList ) throws FunctionnalException, SyntaxiqueException, TechnicalException;

	public String reconcileMobile ( String domain, Date day, int operationCount, float totalPaidAmount ) throws FunctionnalException, SyntaxiqueException, TechnicalException;

}
