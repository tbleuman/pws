package ma.iam.ws.tree;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



public class CheckDependencyBean {
	

	private boolean status;
	private Set<Long> dependingServices;
	private Set<Long> notDependingServices;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Set<Long> getDependingServices() {
		return dependingServices;
	}

	public void setDependingServices(Set<Long> dependingServices) {
		this.dependingServices = dependingServices;
	}

	public Set<Long> getNotDependingServices() {
		return notDependingServices;
	}

	public void setNotDependingServices(Set<Long> notDependingServices) {
		this.notDependingServices = notDependingServices;
	}

	@Override
	public String toString() {
		return "{" +
				"status=" + status +
				", dependingServices=" + dependingServices +
				", notDependingServices=" + notDependingServices +
				'}';
	}
}
