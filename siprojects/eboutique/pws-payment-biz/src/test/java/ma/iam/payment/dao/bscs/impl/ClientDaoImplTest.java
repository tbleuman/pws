package ma.iam.payment.dao.bscs.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.bscs.ClientDao;
import ma.iam.payment.domaine.bscs.Client;

public class ClientDaoImplTest extends DbUnitTestCase {
	
	ClientDao clientDao = (ClientDao) contextManager.getBean("clientDao");
	
	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}
	
	public void testGetClientById() throws TechnicalException {
		Client client = clientDao.getClientById(1L);
		assertNotNull("L'objet est null", client);
	}
	
	public void testGetClient() throws TechnicalException {
		Client client  = clientDao.getClient("2");
		assertNotNull("L'objet est null", client);
	}
	
	public void testUpdateBalanceClient() throws TechnicalException {
		clientDao.updateBalanceClient(1L, 1D);
	}
	
	public void testGetResponsablePaiement() throws TechnicalException {
		Client client = clientDao.getResponsablePaiement("");
		assertNotNull("L'objet est null", client);
	}
	
	public void testGetClientByDN() throws TechnicalException {
		Client client = clientDao.getClientByDN("");
		assertNotNull("L'objet est null", client);
	}
	
}
