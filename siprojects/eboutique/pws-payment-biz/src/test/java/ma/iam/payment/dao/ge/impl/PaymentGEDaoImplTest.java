package ma.iam.payment.dao.ge.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.DbUnitTestCase;
import ma.iam.payment.dao.ge.PaymentGEDao;
import ma.iam.payment.domaine.ge.GlCode;
import ma.iam.payment.domaine.ge.PaymentCommandeEB;
import ma.iam.payment.domaine.ge.PaymentGE;
import ma.iam.payment.domaine.ge.PaymentGEDetail;

public class PaymentGEDaoImplTest extends DbUnitTestCase {
	
	private PaymentGEDao paymentGEDao = (PaymentGEDao) contextManager.getBean("paymentGEDao");
	
	public void setPaymentGEDao(PaymentGEDao paymentGEDao) {
		this.paymentGEDao = paymentGEDao;
	}
	
	public void testSavePaymentGE() throws TechnicalException {
		PaymentGE paymentGE = new PaymentGE();
		paymentGEDao.savePaymentGE(paymentGE);
	}
	
	public void testSavePaiementDetailGE() throws TechnicalException {
		PaymentGEDetail paymentGEDetail = new PaymentGEDetail();
		paymentGEDao.savePaiementDetailGE(paymentGEDetail );
	}
	
	public void testGetNextNumBordereau() throws TechnicalException {
		Long resultat = paymentGEDao.getNextNumBordereau();
		assertNotNull("Le resultat est null", resultat);
	}
	
	public void testUpdatePaymentStatus() throws TechnicalException {
		PaymentGE pGE = new PaymentGE();
		String status = "";
		String userName = "";
		int version = 0;
		paymentGEDao.updatePaymentStatus(pGE, status, userName, version);
	}
	
	public void testGetGlcodeByTrxAndLocation() throws TechnicalException {
		Integer trxId = 0;
		Integer locId = 0;
		GlCode glCode = paymentGEDao.getGlcodeByTrxAndLocation(trxId, locId);
		assertNotNull("L'objet est null", glCode);
	}
	
	public void testSavePaymentCommandeEB() throws TechnicalException {
		PaymentCommandeEB paymentCommandeEB = new PaymentCommandeEB(1L);
		paymentGEDao.savePaymentCommandeEB(paymentCommandeEB);
	}
	
	public void testGetPaymentByNumCommande() throws TechnicalException {
		PaymentGE paymentGE = paymentGEDao.getPaymentByNumCommande("1");
		assertNotNull("L'objet est null", paymentGE);
	}
}