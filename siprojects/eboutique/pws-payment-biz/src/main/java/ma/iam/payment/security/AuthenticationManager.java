package ma.iam.payment.security;

import ma.iam.common.Constantes;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.dto.UserDto;
import ma.iam.payment.service.UserService;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * This class represent . <br/>
 * <br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class AuthenticationManager extends
		ma.iam.services.authentication.AuthenticationManager {

	private UserService userService = null;

	/**
	 * The setter method for the field userService.
	 * 
	 * @param userService
	 *            the userService to set.
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * This method allows you to .</br></br> Created on Sprint #
	 * 
	 * @param username
	 * @return
	 * @throws TechnicalException
	 * @throws DataAccessException
	 */
	public UserDetails authenticate(String username, String password)
			throws FunctionalException, TechnicalException {
		super.authenticate(username, password);
		UserDto userDto = userService.getUserByLogin(username);
		if (userDto.getLocationId() == null) {
			throw new FunctionalException(
					Constantes.ERR_AUCUNE_REGIE_UTILISATEUR);
		}
		/*
		 * ftat add authorities Userdetails n'accepte pas null
		 */

		return new ma.iam.services.authentication.UserDetails(username,
				password, userDto.getCostId(), userDto.getLocationId());
	}
}
