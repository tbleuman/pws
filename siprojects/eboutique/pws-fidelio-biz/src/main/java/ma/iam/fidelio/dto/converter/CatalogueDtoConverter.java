package ma.iam.fidelio.dto.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ma.iam.fidelio.dto.ArticleDto;
import ma.iam.fidelio.dto.PrixEngagementDto;
import ma.iam.services.catalogue.rs.ArticleRS;
import ma.iam.services.catalogue.rs.LignePointRS;
import ma.iam.services.catalogue.rs.LignePrixRS;

/**
 * Classe de conversion des DTO catalogue
 * @author mei
 *
 */
public class CatalogueDtoConverter {
	
	/**
	 * Construit un tableau des article de type ArticleRS à partir d'une liste de DTO
	 * @param articles
	 * @return ArticleRS[]
	 */
	public static ArticleRS[] creeListArticleRS(Collection<ArticleDto> articles) {
		
		List<ArticleRS> listArticleRS = new ArrayList<ArticleRS>(articles.size());
		ArticleRS articleRS = null;
		LignePointRS lignePoints = null;
		LignePrixRS lignePrix = null;
		PrixEngagementDto prixEngagementDto = null;
		
		for (ArticleDto articleDto : articles) {
			articleRS = new ArticleRS();
			articleRS.setCodeArticle(articleDto.getCodeArticle());
			articleRS.setIdPoste(articleDto.getIdPoste());
			articleRS.setLibelle(articleDto.getLibelle());
			if (articleDto.getEnService()) {
				articleRS.setSuspendu(0);
			} else {
				articleRS.setSuspendu(1);
			}
			articleRS.setCodeArticleSAP(articleDto.getCodeArticleSAP());
			
			lignePoints = new LignePointRS();
			lignePrix = new LignePrixRS();
			
			for (int i = 0; i < articleDto.getListPrixEngagementDto().size(); i++) {
				prixEngagementDto = articleDto.getListPrixEngagementDto().get(i);
				
				if(prixEngagementDto.getDureeEnMois() == 12) {
					lignePoints.setPoints12Mois(prixEngagementDto.getPrixPointEngagement());
					lignePrix.setPrix12Mois(prixEngagementDto.getPrixEngagement());
				} 
				
				if(prixEngagementDto.getDureeEnMois() == 24) {
					lignePoints.setPoints24Mois(prixEngagementDto.getPrixPointEngagement());
					lignePrix.setPrix24Mois(prixEngagementDto.getPrixEngagement());
				}
				
			}
			
			lignePrix.setPrixNu(articleDto.getPrixStandardDto().getPrixNu());
			lignePoints.setPointsNu(articleDto.getPrixStandardDto().getPointsNu());
			articleRS.setPrix(lignePrix);
			articleRS.setPoints(lignePoints);
			listArticleRS.add(articleRS);
		}
		return listArticleRS.toArray(new ArticleRS[listArticleRS.size()]);
	}

}
