package ma.iam.fidelio.services.engagement;

import java.util.Date;

import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.fidelio.domaine.data.fidelio.CompteClient;
import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

/**
 * <br/><br/>
 * 
 * @author mei
 *
 */
public interface EngagementService {
	
	/**
	 * Vérifier si le contrat a un engagement échu
	 * @param contratId
	 * @throws FunctionalException
	 * @throws TechnicalException
	 */
	void verifierEngagementEchu(Long contratId) throws FunctionalException, TechnicalException;
	
	/**
	 * 
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @param articleId
	 * @param qualiteId
	 * @param sousCategorieId
	 * @param planTarifaireId
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	void verifierDisponiblite(Integer articleId, Integer qualiteId,
				Integer sousCategorieId, Integer planTarifaireId)
				throws TechnicalException, FunctionalException;	

	/**
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @param numAppel
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	FidelioContrat verifierContrat(String numAppel) throws TechnicalException, FunctionalException;
	
	/**
	 * 
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @param modeEngagementId
	 * @param articleId
	 * @param categorieClientId
	 * @param numAppel
	 * @param contrat
	 * @param compteClient
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	ComplementDto calculPoints(Integer modeEngagementId,
			Integer articleId, Integer categorieClientId, String numAppel,
			FidelioContrat fidelioContrat, CompteClient compteClient) throws TechnicalException,
			FunctionalException;
	
	/**
	 * Vérifier l'eligibilité du client :
	 * 	- Vérifier si le contrat à des commandes en instance
	 * 	- Verifier le nombre de conversion autorisé par mois
	 * 	- Verifier le nombre de commandes livrees dans le mois en cours
	 * 	- Vérifier le délai de la premiere conversion
	 * 	- Vérifier si le Seuil Maximal Impaye est atteint
	 * @param clientId
	 * @param contratId
	 * @param dateCretaionCompte
	 * @param priceGroupId
	 * @param categorieClientId
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public void verifierCompteClient(Integer clientId, Long contratId,
			Date dateCretaionCompte, Integer priceGroupId,
			Integer categorieClientId) throws TechnicalException,
			FunctionalException;
	
	/**
	 * Vérifier l'eligibilité du ND
	 * @param numAppel
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public void verifierEligibiliteClient(String numAppel) throws TechnicalException, FunctionalException,Exception;
	
	
}