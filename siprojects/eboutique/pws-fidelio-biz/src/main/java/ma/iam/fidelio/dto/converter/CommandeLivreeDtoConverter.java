package ma.iam.fidelio.dto.converter;

import java.util.ArrayList;
import java.util.List;

import ma.iam.fidelio.dto.CommandeLivreeDto;
import ma.iam.services.commande.rs.InfoCommandeRS;

/**
 * @author A175029
 *
 */
public abstract class CommandeLivreeDtoConverter {
	
	/**
	 * Creer VO-WS : Détails des commandes E-Boutique livrées
	 * 
	 * @param pDto
	 * @return
	 */
	public static InfoCommandeRS[] creerListeCommandesEBLivrees(List<CommandeLivreeDto> pDto) {
		List<InfoCommandeRS> lRetour = new ArrayList<InfoCommandeRS>();
		InfoCommandeRS lInfoCommande = null;
		
		for (CommandeLivreeDto commandeLivreeDto : pDto) {
			lInfoCommande = new InfoCommandeRS(); 
			lInfoCommande.setNumCommandeEB(commandeLivreeDto.getNumCommandeEB());
			lInfoCommande.setDateLivraison(commandeLivreeDto.getDateLivraison());
			
			lRetour.add(lInfoCommande);
		}
		return lRetour.toArray(new InfoCommandeRS[0]);
	}
	
}
