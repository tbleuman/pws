package ma.iam.fidelio.dto.converter;

import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.services.catalogue.rs.DetailComplementRS;

/**
 * Classe de conversion des DTO Complement
 * @author mei
 *
 */
public class ComplementDtoConverter {
	
	/**
	 * Construit un objet DetailComplementRS à partir d'un DTO
	 * @param complementDto
	 * @return DetailComplementRS
	 */
	public static DetailComplementRS creerDetailComplementRS(ComplementDto complementDto) {
		DetailComplementRS detailComplementRS = new DetailComplementRS();

		detailComplementRS.setComplement(complementDto.getComplement().floatValue());
		detailComplementRS.setPointsAcquis(complementDto.getPointsAcquis().intValue());
		detailComplementRS.setPointsEncours(complementDto.getPointsEncours().intValue());

		return detailComplementRS;
	}

}
