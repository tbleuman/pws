package ma.iam.fidelio.services.commande.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.client.ClientDao;
import ma.iam.fidelio.dao.commande.CommandeDao;
import ma.iam.fidelio.dao.compte.CompteClientDao;
import ma.iam.fidelio.dao.parametrage.RechercheParametreDao;
import ma.iam.fidelio.domaine.data.bscs.FidelioClient;
import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.fidelio.domaine.data.fidelio.Affaire;
import ma.iam.fidelio.domaine.data.fidelio.AffectationCommande;
import ma.iam.fidelio.domaine.data.fidelio.Commande;
import ma.iam.fidelio.domaine.data.fidelio.CommandeHestory;
import ma.iam.fidelio.domaine.data.fidelio.Compensation;
import ma.iam.fidelio.domaine.data.fidelio.CompteClient;
import ma.iam.fidelio.domaine.data.fidelio.DetailCompensation;
import ma.iam.fidelio.domaine.data.fidelio.HistStatutAffaire;
import ma.iam.fidelio.domaine.data.fidelio.StatutAffaire;
import ma.iam.fidelio.domaine.data.parametrage.Agence;
import ma.iam.fidelio.domaine.data.parametrage.Article;
import ma.iam.fidelio.domaine.data.parametrage.ConversionMonsuelle;
import ma.iam.fidelio.domaine.data.parametrage.DelaiPremiereConversion;
import ma.iam.fidelio.domaine.data.parametrage.ModeEngagement;
import ma.iam.fidelio.domaine.data.parametrage.SeuilImpaye;
import ma.iam.fidelio.domaine.data.parametrage.SousCategorie;
import ma.iam.fidelio.domaine.data.parametrage.TypeAffaire;
import ma.iam.fidelio.dto.CommandeDto;
import ma.iam.fidelio.dto.CommandeLivreeDto;
import ma.iam.fidelio.dto.ComplementDto;
import ma.iam.fidelio.services.commande.CommandeService;
import ma.iam.fidelio.services.engagement.EngagementService;
import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;
import ma.iam.mutualisation.fwk.common.util.Utils;

/**
 * 
 * @author mei
 *
 */
public class CommandeServiceImpl implements CommandeService {
	
	/**
	 * Fonction passerCommande : permet de passer une commande et de l'enregistrer
	 * dans la base fidelio.
	 * @param : commandeDto
	 * @return : Date 
	 */
	public Date passerCommande(CommandeDto commandeDto) throws FunctionalException, TechnicalException {
		
		if (Utils.isEmpty(commandeDto.getNumCommande())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_33, Constantes.NUMERO_COMMANDE_OBLIGATOIRE);
		}
		if (commandeDto.getCodeEngagement() == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_19, Constantes.ERREUR_CODE_ENGAGEMENT_OBLIGATOIRE);
		}
		if (Utils.isEmpty(commandeDto.getNumeroAppel())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_21, Constantes.ERREUR_NUM_APPEL_OBLIGATOIRE);
		}
		if (commandeDto.getIdArticle() == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_26, Constantes.ERREUR_ID_POSTE_OBLIGATOIRE);
		}
		/* Vérifier le Complement */
		if (commandeDto.getComplement() == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_15, Constantes.ERREUR_COMPLEMENT_OBLIGATOIRE);
		}
		if (commandeDto.getSoldeAcquis() == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_24, Constantes.NOMBRE_POINTS_ACQUIS_OBLIGATOIRE);
		}
		if (commandeDto.getSoldeEncours() == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_23, Constantes.NOMBRE_POINTS_ENCOURS_OBLIGATOIRE);
		}
		/* Vérifier l'agence de livraison */
		if(commandeDto.getCodeAgence() == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_18, Constantes.CODE_AGENCE_OBLIGATOIRE);
		}
		
		TypeAffaire typeAffaireEB = commandeDao.getTypeAffaireByCode(Constantes.TYPE_AFFAIRE_EB_AVEC_ENGAGEMENT_CODE);
		if (typeAffaireEB == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_29, Constantes.ERREUR_TYPE_AFFAIRE_EB_NON_PARAMETRE);
		}
		
		Integer engagementBscsId = null;
		if(Constantes.CODE_ENGAGEMENT_12_MOIS.equals(commandeDto.getCodeEngagement())) {
			engagementBscsId = Constantes.ID_ENGAGEMENT_BSCS_12_MOIS;
		} else if(Constantes.CODE_ENGAGEMENT_24_MOIS.equals(commandeDto.getCodeEngagement())) {
			engagementBscsId = Constantes.ID_ENGAGEMENT_BSCS_24_MOIS;
		} else {
			throw new FunctionalException(Constantes.ERREUR_CODE_28, Constantes.ERREUR_TYPE_ENGAGEMENT_INVALIDE);
		}
		
		/* Vérifier l'existance de la commande */
		Commande commande = commandeDao.getCommandeByNumCommandeEB(commandeDto.getNumCommande());
		if(commande != null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_30, Constantes.ERREUR_NUM_COMMANDE_EXISTANT);
		}		
		
		/* Récupération du mode d'engagement */
		ModeEngagement modeEngagement = rechercheParametreDao.getModeEngagementByBscsId(engagementBscsId);
		
		if(modeEngagement == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_20, Constantes.ERREUR_MODE_ENGAGEMENT_INEXISTANT);
		}
		
		/* Récupération d'article */
		Article article = rechercheParametreDao.getArticleById(commandeDto.getIdArticle());
		if (article == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_03, Constantes.ERREUR_ID_ARTICLE_INEXISTANT);
		}
		
		/* Vérification de l'état de suspension de l'article commandé */
		if(!article.isEnService()) {
			throw new FunctionalException(Constantes.ERREUR_CODE_11, Constantes.ERREUR_ARTICLE_SUSPENDU);
		}
		
		// L’offre sans terminale ne doit pas être traitée dans EB
		if (Constantes.CODE_POSTE_SANS_TERMINAL.equals(article.getCodeProduit())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_31, Constantes.ERREUR_OFFRE_SANS_TERMINAL);
		}
		
		//CostCenter costCenter = costCenterDao.getCostCenterByCode(commandeDto.getCodeAgence());
		
		Agence agence = commandeDao.getActiveAgenceByCostId(commandeDto.getCodeAgence());
		if (agence == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_17, Constantes.ERREUR_CODE_AGENCE_INEXISTANT);
		}
		
		/* Récuperation du contratId */
		FidelioContrat contrat = engagementService.verifierContrat(commandeDto.getNumeroAppel());
		
		Long contratId = contrat.getId().longValue();
		
		/* Récuperation du client */
		FidelioClient client = contrat.getRefClient();
		
		Integer priceGroupId = client.getPriceGroup().getId();
		
		/* Recuperer : categorieClientId */
		SousCategorie sousCategorie = compteClientDao.getSousCategorieByPriceGroupe(priceGroupId);
		Integer categorieClientId = sousCategorie.getRefCategorieClient().getId();
		
		/* Récupération du compte client */
		CompteClient compteClient = compteClientDao.getCompteClientByCustomerId(client.getId());
		if (compteClient == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_34, Constantes.ERREUR_COMPTE_FIDELIO_INEXISTANT);
		}
		
		engagementService.verifierCompteClient(client.getId(), contratId,
						compteClient.getDateCreation(), priceGroupId, categorieClientId);
		
		// Vérifier si l'article est disponible pour le client
		engagementService.verifierDisponiblite(article.getId(), compteClient.getRefQualite().getId(),
				sousCategorie.getId(), contrat.getRefPlanTarifaire().getId().intValue());
		
		/* Création de la date */
		Date dateCommande = Calendar.getInstance().getTime();
		
		ComplementDto complementDto = engagementService.calculPoints(
				modeEngagement.getId(), article.getId(), categorieClientId,
				commandeDto.getNumeroAppel(), contrat, compteClient);
		if (complementDto == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_25,
					Constantes.ERREUR_ARTICLE_INDISPONIBLE_MODE_ENGAGEMENT,
					new Object[] {modeEngagement.getDureeEnMois()});
		}
		
		if (!commandeDto.getSoldeAcquis().equals(complementDto.getPointsAcquis())
				|| !commandeDto.getSoldeEncours().equals(complementDto.getPointsEncours())
						|| !commandeDto.getComplement().equals(complementDto.getComplement())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_32, Constantes.ERREUR_COMPLEMENT_POINTS_UTILSE_INCORRECT);
		}
		
		/* Récupérer Le Prix GSM */
		float prixGSM = rechercheParametreDao.getPrixPackXMois(contrat.getRefPlanTarifaire().getId().intValue(), 
				article.getCodeProduit(), modeEngagement.getDureeEnMois());
		
		/**********************************************************************************************************/
		
		/* Création de la commande */
		Commande cmd = new Commande();
		cmd.setCoId(contratId.longValue());
		cmd.setDateCommande(dateCommande);
		cmd.setComplementArgent(commandeDto.getComplement());
		cmd.setSoldeAcquisUtilise(commandeDto.getSoldeAcquis()); 
		cmd.setSoldeEnCoursUtilise(commandeDto.getSoldeEncours());		
		
		cmd.setMontantRachat(0D);
		cmd.setPointRachat(0D);
		
		cmd.setRefArticle(article);
		
		// Affectation de la commande
		AffectationCommande affectation = new AffectationCommande();
		affectation.setDateAffectation(dateCommande);
		affectation.setRefCommande(cmd);
		affectation.setRefAgence(agence);
		
		Collection<AffectationCommande> affectations = new ArrayList<AffectationCommande>();
		affectations.add(affectation);
		cmd.setRefAffectationCommandes(affectations);
		cmd.setRefModeEngagement(modeEngagement);

		// Affaire
		Affaire affaire = new Affaire();
		affaire.setDateCreation(Calendar.getInstance().getTime());
		affaire.setUtilisateur(commandeDto.getUtilisateur());
		affaire.setRefCompteClient(compteClient);
		
		affaire.setRefTypeAffaire(typeAffaireEB);
		
		// ************* Statut affaire ********************
		HistStatutAffaire statut = new HistStatutAffaire();
		statut.setDateSaisie(dateCommande);
		statut.setDateStatut(dateCommande);
		statut.setUtilisateur(commandeDto.getUtilisateur());
		statut.setRefAffaire(affaire);
		statut.setRefStatutAffaire(rechercheParametreDao.getStatusAffaireByCode(Constantes.STATUS_AFFAIRE_CREE));
		List<HistStatutAffaire> statuts = new ArrayList<HistStatutAffaire>();
		statuts.add(statut);
		affaire.setRefHistStatutAffaires(statuts);
		cmd.setRefAffaire(affaire);
		
		// ************* Compensation ********************
		Compensation compensation = new Compensation();
		compensation.setDateApplication(dateCommande);
		compensation.setDateTransaction(dateCommande);
		compensation.setRefAffaire(affaire);
		compensation.setRefArticle(cmd.getRefArticle());
		compensation.setRefCompteClient(affaire.getRefCompteClient());
		compensation.setSoldeAcquis(-commandeDto.getSoldeAcquis());
		compensation.setSoldeEnCours(-commandeDto.getSoldeEncours());
		
		DetailCompensation detail = new DetailCompensation();
		
		detail.setDateTransaction(dateCommande);
		detail.setSoldeConverti(compensation.getSoldeAcquis() + compensation.getSoldeEnCours());
		detail.setRefCompensation(compensation);
		detail.setSoldeAcquis(compensation.getSoldeAcquis());
		detail.setSoldeEnCours(compensation.getSoldeEnCours());
		detail.setUtilisateur(commandeDto.getUtilisateur());
		Collection<DetailCompensation> details = new ArrayList<DetailCompensation>();
		details.add(detail);
		compensation.setRefDetailCompensations(details);
		cmd.setRefCompensation(compensation);
		
		/* Ajout du champ numéro E-Boutique */
		cmd.setNumCommandeEB(commandeDto.getNumCommande());
		
		// ************* Compte client ********************
		Double cmP = cmd.getPointRachat();
		Long soldeAcquisClient = compteClient.getSoldeAcquis();
		Long soldeEnCours = compteClient.getSoldeEnCours();
		compteClient.setSoldeAcquis(compteClient.getSoldeAcquis() - (commandeDto.getSoldeAcquis() + cmP.longValue()));
		compteClient.setSoldeEnCours(compteClient.getSoldeEnCours()	- commandeDto.getSoldeEncours());
		compteClient.setDateMAJSolde(dateCommande);
		
		
		commandeDao.saveCommande(cmd);
		
		updateCompensations(cmd.getRefCompensation().getRefCompteClient().getId(),
				cmd.getSoldeAcquisUtilise(),
				cmd.getSoldeEnCoursUtilise());
		
		
		CommandeHestory commandeHestory = new CommandeHestory();
		commandeHestory.setId(cmd.getRefAffaire().getId());
		commandeHestory.setIdCommande(cmd.getId());
		commandeHestory.setDateCreation(cmd.getDateCommande());
		commandeHestory.setDateLivraison(cmd.getDateLivraison());
		commandeHestory.setIdArticle(commandeDto.getIdArticle());
		commandeHestory.setCoId(contratId.longValue());
		commandeHestory.setPointsAcquis(commandeDto.getSoldeAcquis().intValue());
		commandeHestory.setPointsEnCours(commandeDto.getSoldeEncours().intValue());
		commandeHestory.setComplementArgent(commandeDto.getComplement());
		commandeHestory.setMontantRachat(cmd.getMontantRachat());
		commandeHestory.setPrixEffectif(complementDto.getPrixPtEffectif());
		commandeHestory.setBonus(complementDto.getBonus());
		commandeHestory.setPrixEnPoints(complementDto.getPrixEnPoints().intValue());
		commandeHestory.setPrixGsm(prixGSM);
		commandeHestory.setEntreprise(null);
		commandeHestory.setPrivilege(null);
		commandeHestory.setIdClient(cmd.getRefAffaire().getRefCompteClient().getIdClient());
		commandeHestory.setSoldeAcquisClient(soldeAcquisClient.intValue());
		commandeHestory.setSoldeEncoursClient(soldeEnCours.intValue());
		commandeHestory.setTypeCatalogue("E");
		commandeHestory.setNd(commandeDto.getNumeroAppel());
		commandeDao.saveCommandeHistory(commandeHestory);
		
		return dateCommande;
	}
	
	/**
	 * Fonction updateCompensations : paremet de mettre à jour le compte client.
	 * @param : compteClientId
	 * @param : soldAcquisArg
	 * @param : soldEnCoursArg
	 * @throws TechnicalException
	 */
	private void updateCompensations(Integer compteClientId, Long soldAcquisArg, Long soldEnCoursArg) throws TechnicalException {
		long totalAcquis = soldAcquisArg;
		long totalEncours = soldEnCoursArg;
		List<Compensation> listComp = compteClientDao.getCreditCompensationsByClient(compteClientId.intValue());
		
		// parcours de l'ensemble des lignes de compensation du client
		for (Compensation cmpsClient : listComp) {
			long fromAcquis = 0L;
			long fromEncous = 0L;
			long soldeAcquisNonConvertis = cmpsClient.getAcquisNonConvertis();
			long soldeEnCoursNonConvertis = cmpsClient.getEnCoursNonConvertis();
			long soldeAcquisConvertis = (cmpsClient.getSoldeConvertis() != null) ? cmpsClient.getSoldeConvertis() : new Long("0");
			long soldeEnCoursConvertis = (cmpsClient.getSoldeEnCoursConvertis() != null) ? cmpsClient.getSoldeEnCoursConvertis() : new Long("0");

			// si le solde acquis de cette ligne est inferieur au solde total acquis utilisé
			// pour la compensation alors le solde acquis de cette ligne sera convertis en entier
			// sinon sera convertis du solde acquis uniquement la valeur du total
			if (soldeAcquisNonConvertis < totalAcquis) {
				fromAcquis = soldeAcquisNonConvertis;
			} else {
				fromAcquis = totalAcquis;
			}
			
			// si le solde en cours de cette ligne est inférieur au total en cours utilisé pour la conversion alors le solde
			// en cours de cette ligne sera utilisé en entier
			if (soldeEnCoursNonConvertis < totalEncours) {
				fromEncous = soldeEnCoursNonConvertis;
			} else { 
				fromEncous = totalEncours;
			}

			// Cette ligne n'est plus valide depuis la sépartion des champs solde_acquis_cenvertis et solde_en_cours_convertis
			// currentSoldeConvertis += fromAcquis + fromEncous;
			soldeAcquisConvertis += fromAcquis;
			cmpsClient.setSoldeConvertis(soldeAcquisConvertis);
			soldeEnCoursConvertis += fromEncous;
			cmpsClient.setSoldeEnCoursConvertis(soldeEnCoursConvertis);

			commandeDao.saveCompensation(cmpsClient);
			if (totalAcquis > 0L) {
				totalAcquis -= fromAcquis;
			} else {
				totalAcquis = 0L;
			}

			if (totalEncours > 0L) {
				totalEncours -= fromEncous;
			} else {
				totalEncours = 0L;
			}

			if (totalAcquis <= 0L && totalEncours <= 0L) {
					break;
			}
		}
	}	
	
	/**
	 * (non javadoc)
	 * See @see ma.iam.fidelio.dao.service.commande.CommandeService#annulerCommande(java.lang.String, java.lang.String, java.lang.String, java.lang.String).
	 * @param numCommandeEB
	 * @param codeClient
	 * @param utilisateur
	 * @param motif
	 * @return
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	public String annulerCommande(String numCommandeEB, String codeClient, String utilisateur, String motif) throws TechnicalException, FunctionalException {
		
		if (Utils.isEmpty(numCommandeEB)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_07, Constantes.NUMERO_COMMANDE_OBLIGATOIRE);
		}
		/* Récupération de la commande */
		Commande commande = commandeDao.getCommandeByNumCommandeEB(numCommandeEB);
		
		/* Vérifier l'existance de la commande */
		if(commande == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_03, Constantes.ERREUR_COMMANDE_INEXISTANTE);
		}
		
		/* Récupération de l'affaire */
		Affaire affaire = commande.getRefAffaire();
		
		/* Récupération de la liste des historique status Affaire*/
		Collection<HistStatutAffaire> listHistStatusAffaire = affaire.getRefHistStatutAffaires();
		Iterator<HistStatutAffaire> iteratorListHistStatusAffaire = listHistStatusAffaire.iterator();
		Date dateStatus = new Date();
		StatutAffaire statutAffaire = null;
		
		for (int i=0 ; i < listHistStatusAffaire.size() ; i++){
			HistStatutAffaire histStatusAffaire = iteratorListHistStatusAffaire.next();
			if(i == 0) {
				/* Traitement cas 1 : status = créer : C */
				dateStatus = histStatusAffaire.getDateStatut();
				statutAffaire = histStatusAffaire.getRefStatutAffaire();
			}
			else
				/* Traitement cas 2 : status = Livré ou Annulé : L ou A */
				if(dateStatus.compareTo(histStatusAffaire.getDateStatut()) < 0){
					dateStatus = histStatusAffaire.getDateStatut();
					statutAffaire = histStatusAffaire.getRefStatutAffaire();
				}
		}
		
		
		/* Tester si l'affaire est en status livrée */
		if (Constantes.STATUS_AFFAIRE_LIVRE.equals(statutAffaire.getCode())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_04, Constantes.ERREUR_COMMANDE_LIVRE);
		}
		
		/* Tester si l'affaire est en status annulée */
		if (Constantes.STATUS_AFFAIRE_ANNULE.equals(statutAffaire.getCode())) {
			throw new FunctionalException(Constantes.ERREUR_CODE_05, Constantes.ERREUR_COMMANDE_ANNULE);
		}
		
		/* Ajouter une nouvel status dans l'historique status affaire */
		HistStatutAffaire newHistStatusAffaire = new HistStatutAffaire();
		
		newHistStatusAffaire.setDateSaisie(new Date());
		newHistStatusAffaire.setDateStatut(new Date());
		newHistStatusAffaire.setRefAffaire(affaire);
		newHistStatusAffaire.setRefStatutAffaire(rechercheParametreDao.getStatusAffaireByCode(Constantes.STATUS_AFFAIRE_ANNULE));
		newHistStatusAffaire.setUtilisateur(utilisateur);//Ajout utilisateur
		newHistStatusAffaire.setCommentaire(motif);
		
		listHistStatusAffaire.add(newHistStatusAffaire);
		
		affaire.setRefHistStatutAffaires(listHistStatusAffaire);
		affaire.setCommentaire(motif);//Ajout motif
		
		/* Vérification du solde ainsi que de l'article */
		long acquisCommande = 0;
		long enCoursCommande = 0;
		if (commande.getSoldeAcquisUtilise() != null) {
			acquisCommande = commande.getSoldeAcquisUtilise();
		}
			
		if (commande.getSoldeEnCoursUtilise() != null) {
			enCoursCommande = commande.getSoldeEnCoursUtilise();
		}
		
		/* Récupération du compte Client*/
		CompteClient compteClient = affaire.getRefCompteClient();
		
		/* Vérification du code client */
		if(!Utils.isEmpty(codeClient)) {
			FidelioClient client = clientDao.getClientByCode(codeClient);
			if (client == null) {
				throw new FunctionalException(Constantes.ERREUR_CODE_02, Constantes.ERREUR_CLIENT_INCONNU);
			}
			if (!client.getId().equals(compteClient.getId())) {
				throw new FunctionalException(Constantes.ERREUR_CODE_06, Constantes.ERREUR_COMMANDE_NA_APPARTIENT_PAS_CLIENT);
			}
		}
		
		// modification des compensations et la mise à jour du compte client
		Collection<Compensation> listCompensation = compteClientDao.getListCompensationAannulerByClient(compteClient.getId());
		List<Compensation> compensationsToSave = new ArrayList<Compensation>();
		
		long tmpAcquisCommande = acquisCommande;
		long tmpenCoursCommande = enCoursCommande;
	
		boolean soustractAcquis = false;
		boolean soustractEnCours = false;
	
		if (tmpAcquisCommande > 0)
			soustractAcquis = true;
		if (tmpenCoursCommande > 0)
			soustractEnCours = true;
		
		for (Compensation compensation : listCompensation) {
			long soldeAcquisToSet = new Long(0);
			long tmpAnCoursCommandeToSet = new Long(0);
	
			// Si la valeur de solde convertis est null en affacte la valeur
			// 0
			long compensationSoldeConvertis = new Long("0");
			if (compensation.getSoldeConvertis() != null)
				compensationSoldeConvertis = compensation.getSoldeConvertis();
	
			// Si la valeur de solde en cours convertis est null en affacte
			// la valeur 0
			long compensationSoldeEnCoursConvertis = new Long("0");
			if (compensation.getSoldeEnCoursConvertis() != null)
				compensationSoldeEnCoursConvertis = compensation.getSoldeEnCoursConvertis();
	
			if (compensation.getSoldeAcquis() <= tmpAcquisCommande) {
				soldeAcquisToSet = new Long(0);
				tmpAcquisCommande -= compensationSoldeConvertis;
			} else {
				soldeAcquisToSet = compensationSoldeConvertis - tmpAcquisCommande;
				tmpAcquisCommande = new Long(0);
			}
	
			long compensationSoldeEnCours = (compensation.getSoldeEnCours() != null) ? compensation.getSoldeEnCours() : new Long("0");
			if (compensationSoldeEnCours <= tmpenCoursCommande) {
				tmpAnCoursCommandeToSet = new Long(0);
				tmpenCoursCommande -= compensationSoldeEnCoursConvertis;
			} else {
				tmpAnCoursCommandeToSet = compensationSoldeEnCoursConvertis - tmpenCoursCommande;
				tmpenCoursCommande = new Long(0);
			}
			if (soustractAcquis)
				compensation.setSoldeConvertis(soldeAcquisToSet);
			if (soustractEnCours)
				compensation.setSoldeEnCoursConvertis(tmpAnCoursCommandeToSet);
	
			if (tmpAcquisCommande <= 0)
				soustractAcquis = false;
			if (tmpenCoursCommande <= 0)
				soustractEnCours = false;
	
			compensationsToSave.add(compensation);
	
			if (tmpAcquisCommande == new Long(0) && tmpenCoursCommande == new Long(0)) {
				break;
			}
		}
		
		Collection<Compensation> listComp = affaire.getRefCompensations();
		
		for (Compensation compensation : listComp) {
			// Mise à jour du solde client
			DetailCompensation dCompensation = new DetailCompensation();
			Collection<DetailCompensation> listDetailCompensation = new ArrayList<DetailCompensation>();
			
			dCompensation.setDateTransaction(new Date());
			dCompensation.setRefCompensation(compensation);
			dCompensation.setSoldeAcquis(acquisCommande);
			dCompensation.setSoldeEnCours(enCoursCommande);
			dCompensation.setUtilisateur(utilisateur);
			
			listDetailCompensation.add(dCompensation);
			compensation.setRefDetailCompensations(listDetailCompensation);
			compensation.setSoldeAcquis(new Long(0));
			compensation.setSoldeEnCours(new Long(0));
		}
		
		compteClient.setSoldeAcquis(compteClient.getSoldeAcquis() + acquisCommande);
		compteClient.setSoldeEnCours(compteClient.getSoldeEnCours() + enCoursCommande);
		compteClient.setDateMAJSolde(new Date());
		
		commandeDao.saveAffaire(affaire);
		commandeDao.saveCompensations(compensationsToSave);
		
		return MessageUtils.getMessage(Constantes.OPERATION_OK);
	}
	
	/** (non-Javadoc)
	 * @see ma.iam.fidelio.dao.service.commande.CommandeService#rechercheCommandesLivrees(java.util.Date, java.util.Date)
	 */
	public List<CommandeLivreeDto> rechercheCommandesLivrees(Date dateDebut, Date dateFin)
			throws TechnicalException, FunctionalException {
		
		// Controle des paramètres en entrée
		if (dateDebut == null) {
			throw new FunctionalException(Constantes.ERREUR_CODE_01, Constantes.DATE_DEBUT_OBLIGATOIRE);
		}
		
		if (dateFin != null && dateDebut.after(dateFin)) {
			throw new FunctionalException(Constantes.ERREUR_CODE_02, Constantes.INTERVAL_DATE_ERRONE);
		}
		
		/* Récupération des commandes */
		List<Commande> lCommandes = commandeDao.getCommandesEBLivreesParPeriode(dateDebut, dateFin);
		
		if(lCommandes.isEmpty()) {
			throw new FunctionalException(Constantes.ERREUR_CODE_03, Constantes.AUCUNE_COMMANDE_LIVREE);
		}
		
		List<CommandeLivreeDto> lRetour = new ArrayList<CommandeLivreeDto>();
		for (Commande commande : lCommandes) {
			lRetour.add(new CommandeLivreeDto(commande.getNumCommandeEB(), commande.getDateLivraison()));
		}
		
		return lRetour;
		
	}
	
private CommandeDao commandeDao;
	
	private ClientDao clientDao;
	
	private CompteClientDao compteClientDao;
	
	private RechercheParametreDao rechercheParametreDao;
	
	private EngagementService engagementService;
	
	/**
	 * @param engagementService
	 */
	public void setEngagementService(EngagementService engagementService) {
		this.engagementService = engagementService;
	}
	

	/**
	 * @param commandeDao
	 */
	public void setCommandeDao(CommandeDao commandeDao) {
		this.commandeDao = commandeDao;
	}

	/**
	 * @return
	 */
	public ClientDao getClientDao() {
		return clientDao;
	}

	/**
	 * @param clientDao
	 */
	public void setClientDao(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

	/**
	 * @return
	 */
	public CompteClientDao getCompteClientDao() {
		return compteClientDao;
	}

	/**
	 * @param compteClientDao
	 */
	public void setCompteClientDao(CompteClientDao compteClientDao) {
		this.compteClientDao = compteClientDao;
	}

	/**
	 * @return
	 */
	public RechercheParametreDao getRechercheParametreDao() {
		return rechercheParametreDao;
	}

	/**
	 * @param rechercheParametreDao
	 */
	public void setRechercheParametreDao(RechercheParametreDao rechercheParametreDao) {
		this.rechercheParametreDao = rechercheParametreDao;
	}
}