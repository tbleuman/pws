package ma.iam.payment.domaine.bscs;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * <p>
 * Table : customer_all
 * </p>
 * <p>
 * Indication :Réccupération des informations client partir du customer_id
 * </p> 
 * <p>
 * SELECT CC.CCLINE1, CC.CCLINE2 ||' '|| CC.CCLINE3, CC.CCLINE4 ||' '||&nbsp; CC.CCLINE5, BC.DESCRIPTION,&nbsp; CU.CSACTIVATED,
 * DECODE(CU.CSTYPE,'a','Actif','d','Inactif','s','Suspendu','i','Int&eacute;ress&eacute;',CU.CSTYPE), RS.RS_DESC, PR.PRGNAME FROM CUSTOMER_ALL CU,
 * CCONTACT_ALL CC, REASONSTATUS_ALL RS, PRICEGROUP_ALL PR, BILLCYCLES BC WHERE CU.CUSTOMER_ID = 15491392 AND CU.CUSTOMER_ID = CC.CUSTOMER_ID AND
 * RS.RS_ID = CU.CSREASON AND PR.PRGCODE = CU.PRGCODE AND BC.BILLCYCLE = CU.BILLCYCLE AND CCSEQ = (select max(ccseq) from ccontact_all where
 * customer_id = CC.customer_id and ccbill='X' ) CCBILL='X'
 * </p>
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class Client implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5898185061680370020L;

	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_CODE. */
	public static final String LABEL_CODE = "code";

	/** The Constant LABEL_COST_CENTER. */
	public static final String LABEL_COST_CENTER = "costCenter";

	/** The Constant LABEL_DATE_ACTIVATION. */
	public static final String LABEL_DATE_ACTIVATION = "dateActivation";

	/** The Constant LABEL_STATUT. */
	public static final String LABEL_STATUT = "statut";

	/** The Constant LABEL_GOLD. */
	public static final String LABEL_GOLD = "gold";

	/** The Constant LABEL_REF_PARENT. */
	public static final String LABEL_REF_PARENT = "refParent";

	/** The Constant LABEL_REF_CATEGORIE_CLIENT. */
	public static final String LABEL_REF_CATEGORIE_CLIENT = "refCategorieClient";

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CUSTOMER_ID</li>
	 * <li>Description : Identitifant du client</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long id;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : le code du client</li>
	 * <li>Champs : CUSTCODE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String code;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : Le code de l'actel</li>
	 * <li>Champs : COSTCENTER_ID</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long costCenter;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : La date d'activation du compte client</li>
	 * <li>Champs : CSACTIVATED</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date dateActivation;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : Le statut du compte client</li>
	 * <li>Champs : CSTYPE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String statut;

	/**
	 * <p>
	 * Description : Si le client est VIP(GOLD) Champs : GOLD
	 * </p>
	 * .
	 */
	private Long gold;

	/** The balance. */
	private Double balance;

	/** The version. */
	private Long version;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : Le compte parent</li>
	 * <li>Champs : CUSTOMER_ID_HIGH</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Client refParent;

	/**
	 * La liste des contacts
	 */
	private Collection<Contact> contacts;

	/**
	 * The getter method for the field contacts.
	 * @return the contacts.
	 */
	public Collection<Contact> getContacts() {
		return contacts;
	}

	/**
	 * The setter method for the field contacts.
	 * @param contacts the contacts to set.
	 */
	public void setContacts(Collection<Contact> contacts) {
		this.contacts = contacts;
	}

	/** The cin. */
	private String cin;

	/** The resp paiement. */
	private String respPaiement;

	/**
	 * Gets the resp paiement.
	 * 
	 * @return the resp paiement
	 */
	public String getRespPaiement() {
		return respPaiement;
	}

	/**
	 * Sets the resp paiement.
	 * 
	 * @param respPaiement the new resp paiement
	 */
	public void setRespPaiement(String respPaiement) {
		this.respPaiement = respPaiement;
	}

	/**
	 * Equals.
	 * 
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Client)) {
			return false;
		}
		final Client obj = (Client) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the date activation.
	 * 
	 * @return the date activation
	 */
	public Date getDateActivation() {
		return dateActivation != null ? (Date) dateActivation.clone() : null;
	}

	/**
	 * Sets the date activation.
	 * 
	 * @param dateActivation the new date activation
	 */
	public void setDateActivation(Date dateActivation) {
		this.dateActivation = dateActivation != null ? (Date) dateActivation.clone() : null;
	}

	/**
	 * Gets the gold.
	 * 
	 * @return the gold
	 */
	public Long getGold() {
		return gold;
	}

	/**
	 * Sets the gold.
	 * 
	 * @param gold the new gold
	 */
	public void setGold(Long gold) {
		this.gold = gold;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the ref contact.
	 * 
	 * @return the ref contact
	 */
	public Contact getRefContact() {
		Contact refContact = null;
		long numOrder = 0;
		if (this.contacts != null) {
			for (Contact contact : this.contacts) {
				if (contact.getCcbill() != null && numOrder < contact.getNumeroOrdre()) {
					numOrder = contact.getNumeroOrdre();
					refContact = contact;
				}
			}
		}
		return refContact;
	}

	/**
	 * Gets the ref parent.
	 * 
	 * @return the ref parent
	 */
	public Client getRefParent() {
		return refParent;
	}

	/**
	 * Sets the ref parent.
	 * 
	 * @param refParent the new ref parent
	 */
	public void setRefParent(Client refParent) {
		this.refParent = refParent;
	}

	/**
	 * Gets the statut.
	 * 
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * Sets the statut.
	 * 
	 * @param statut the new statut
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * Gets the cost center.
	 * 
	 * @return the cost center
	 */
	public Long getCostCenter() {
		return costCenter;
	}

	/**
	 * Sets the cost center.
	 * 
	 * @param costCenter the new cost center
	 */
	public void setCostCenter(Long costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * Gets the balance.
	 * 
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * Sets the balance.
	 * 
	 * @param balance the new balance
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the cin.
	 * 
	 * @return the cin
	 */
	public String getCin() {
		return cin;
	}

	/**
	 * Sets the cin.
	 * 
	 * @param cin the new cin
	 */
	public void setCin(String cin) {
		this.cin = cin;
	}

}
