package ma.iam.payment.domaine.ge;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;

/**
 * The Class PaymentsGE. This is an object that contains data related to the SEMA_OP_PAYMENTS table. Do not modify this class because it will be
 * overwritten if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_PAYMENTS"
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentGE implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -281761443633352489L;

	/** The REF. */
	public static final String REF = "PaymentsGE";

	/** The LABE l_ ismultipay. */
	public static final String LABEL_ISMULTIPAY = "ismultipay";

	/** The LABE l_ status. */
	public static final String LABEL_STATUS = "status";

	/** The LABE l_ priorsystem. */
	public static final String LABEL_PRIORSYSTEM = "priorsystem";

	/** The LABE l_ sem a_ o p_ tr x_ type. */
	public static final String LABEL_SEMA_OP_TRX_TYPE = "semaOpTrxType";

	/** The LABE l_ bord u_ num. */
	public static final String LABEL_BORDU_NUM = "borduNum";

	/** The LABE l_ ne w_ sub. */
	public static final String LABEL_NEW_SUB = "newSub";

	/** The LABE l_ sem a_ o p_ pa y_ mode. */
	public static final String LABEL_SEMA_OP_PAY_MODE = "semaOpPayMode";

	/** The LABE l_ stam p_ amt. */
	public static final String LABEL_STAMP_AMT = "stampAmt";

	/** The LABE l_ caxact. */
	public static final String LABEL_CAXACT = "caxact";

	/** The LABE l_ chkdate. */
	public static final String LABEL_CHKDATE = "chkdate";

	/** The LABE l_ bankname. */
	public static final String LABEL_BANKNAME = "bankname";

	/** The LABE l_ glcode. */
	public static final String LABEL_GLCODE = "glcode";

	/** The LABE l_ chknum. */
	public static final String LABEL_CHKNUM = "chknum";

	/** The LABE l_ ribkey. */
	public static final String LABEL_RIBKEY = "ribkey";

	/** The LABE l_ tota l_ amt. */
	public static final String LABEL_TOTAL_AMT = "totalAmt";

	/** The LABE l_ custome r_ id. */
	public static final String LABEL_CUSTOMER_ID = "customerId";

	/** The LABE l_ tr x_ date. */
	public static final String LABEL_TRX_DATE = "trxDate";

	/** The LABE l_ bankaccno. */
	public static final String LABEL_BANKACCNO = "bankaccno";

	/** The LABE l_ ex p_ date. */
	public static final String LABEL_EXP_DATE = "expDate";

	/** The LABE l_ sem a_ o p_ cr d_ type. */
	public static final String LABEL_SEMA_OP_CRD_TYPE = "semaOpCrdType";

	/** The LABE l_ multisy s_ amt. */
	public static final String LABEL_MULTISYS_AMT = "multisysAmt";

	/** The LABE l_ cos t_ id. */
	public static final String LABEL_COST_ID = "costId";

	/** The LABE l_ ban k_ code. */
	public static final String LABEL_BANK_CODE = "bankCode";

	/** The LABE l_ pos t_ amt. */
	public static final String LABEL_POST_AMT = "postAmt";

	/** The LABE l_ blcheck. */
	public static final String LABEL_BLCHECK = "blcheck";

	/** The LABE l_ chequ e_ box. */
	public static final String LABEL_CHEQUE_BOX = "chequeBox";

	/** The LABE l_ branc h_ code. */
	public static final String LABEL_BRANCH_CODE = "branchCode";

	/** The LABE l_ banksubaccno. */
	public static final String LABEL_BANKSUBACCNO = "banksubaccno";

	/** The LABE l_ id. */
	public static final String LABEL_ID = "id";

	/** The LABE l_ sem a_ o p_ pa y_ details. */
	public static final String LABEL_SEMA_OP_PAY_DETAILS = "semaOpPayDetails";

	/** The LABE l_ sem a_ o p_ pa y_ status. */
	public static final String LABEL_SEMA_OP_PAY_STATUS = "semaOpPayStatus";

	/** The LABE l_ username. */
	public static final String LABEL_USERNAME = "username";

	// constructors
	/**
	 * Instantiates a new payments ge.
	 */
	public PaymentGE() {
	}

	/**
	 * Constructor for required fields.
	 * 
	 * @param caxact the caxact
	 * @param customerId the customer id
	 * @param semaOpPayMode the sema op pay mode
	 * @param semaOpTrxType the sema op trx type
	 * @param trxDate the trx date
	 * @param costId the cost id
	 * @param glcode the glcode
	 * @param postAmt the post amt
	 * @param stampAmt the stamp amt
	 * @param multisysAmt the multisys amt
	 * @param totalAmt the total amt
	 */
	public PaymentGE(java.lang.Long caxact, java.lang.Long customerId, PaymentMode semaOpPayMode,
			TransactionType semaOpTrxType, java.util.Date trxDate, java.lang.Integer costId, java.lang.String glcode, java.lang.Double postAmt,
			java.lang.Double stampAmt, java.lang.Double multisysAmt, java.lang.Double totalAmt) {

		this.caxact=caxact;
		this.customerId=customerId;
		this.semaOpPayMode=semaOpPayMode;
		this.semaOpTrxType=semaOpTrxType;
		this.trxDate=trxDate != null ? (Date) trxDate.clone() : null;
		this.costId=costId;
		this.glcode=glcode;
		this.postAmt=postAmt;
		this.stampAmt=stampAmt;
		this.multisysAmt=multisysAmt;
		this.totalAmt=totalAmt;
	}

	// fields
	/** The caxact. */
	private java.lang.Long caxact;

	/** The customer id. */
	private java.lang.Long customerId;

	/** The bankname. */
	private java.lang.String bankname;

	/** The bankaccno. */
	private java.lang.String bankaccno;

	/** The banksubaccno. */
	private java.lang.String banksubaccno;

	/** The chknum. */
	private java.lang.String chknum;

	/** The chkdate. */
	private java.util.Date chkdate;

	/** The ribkey. */
	private java.lang.String ribkey;

	/** The exp date. */
	private java.util.Date expDate;

	/** The sema op pay mode. */
	private PaymentMode semaOpPayMode;

	/** The sema op trx type. */
	private TransactionType semaOpTrxType;

	/** The trx date. */
	private java.util.Date trxDate;

	/** The cost id. */
	private java.lang.Integer costId;

	/** The glcode. */
	private java.lang.String glcode;

	/** The is multipay. */
	private java.lang.String isMultipay;

	/** The post amt. */
	private java.lang.Double postAmt;

	/** The stamp amt. */
	private java.lang.Double stampAmt;

	/** The multisys amt. */
	private java.lang.Double multisysAmt;

	/** The total amt. */
	private java.lang.Double totalAmt;

	/** The status. */
	private java.lang.String status;

	/** The bordu num. */
	private java.lang.String borduNum;

	/** The bank code. */
	private java.lang.String bankCode;

	/** The branch code. */
	private java.lang.String branchCode;

	/** The new sub. */
	private java.lang.String newSub;

	/** The blcheck. */
	private java.lang.String blcheck;

	/** The cheque box. */
	private java.lang.String chequeBox;

	/** The id. */
	private java.lang.Long id;

	/** The priorsystem. */
	private java.lang.String priorsystem;


	/** The sema op pay details. */
	private java.util.Collection<PaymentGEDetail> semaOpPayDetails;

	/** The sema op pay status. */
	private java.util.Collection semaOpPayStatus;

	/** The username. */
	private java.lang.String username;

	/** The cust name. */
	private java.lang.String custName;

	/** The nom tireur. */
	private java.lang.String nomTireur;
	
	/** The is pm. */
	private Integer isPM;

	/**
	 * Gets the nom tireur.
	 * 
	 * @return the nom tireur
	 */
	public java.lang.String getNomTireur() {
		return nomTireur;
	}

	/**
	 * Sets the nom tireur.
	 * 
	 * @param nomTireur the new nom tireur
	 */
	public void setNomTireur(java.lang.String nomTireur) {
		this.nomTireur = nomTireur;
	}

	/**
	 * Return the value associated with the column: CAXACT.
	 * 
	 * @return the caxact
	 */
	public java.lang.Long getCaxact() {
		return caxact;
	}

	/**
	 * Set the value related to the column: CAXACT.
	 * 
	 * @param caxact the CAXACT value
	 */
	public void setCaxact(java.lang.Long caxact) {
		this.caxact = caxact;
	}

	/**
	 * Return the value associated with the column: CUSTOMER_ID.
	 * 
	 * @return the customer id
	 */
	public java.lang.Long getCustomerId() {
		return customerId;
	}

	/**
	 * Set the value related to the column: CUSTOMER_ID.
	 * 
	 * @param customerId the CUSTOMER_ID value
	 */
	public void setCustomerId(java.lang.Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * Return the value associated with the column: BANKNAME.
	 * 
	 * @return the bankname
	 */
	public java.lang.String getBankname() {
		return bankname;
	}

	/**
	 * Set the value related to the column: BANKNAME.
	 * 
	 * @param bankname the BANKNAME value
	 */
	public void setBankname(java.lang.String bankname) {
		this.bankname = bankname;
	}

	/**
	 * Return the value associated with the column: BANKACCNO.
	 * 
	 * @return the bankaccno
	 */
	public java.lang.String getBankaccno() {
		return bankaccno;
	}

	/**
	 * Set the value related to the column: BANKACCNO.
	 * 
	 * @param bankaccno the BANKACCNO value
	 */
	public void setBankaccno(java.lang.String bankaccno) {
		this.bankaccno = bankaccno;
	}

	/**
	 * Return the value associated with the column: BANKSUBACCNO.
	 * 
	 * @return the banksubaccno
	 */
	public java.lang.String getBanksubaccno() {
		return banksubaccno;
	}

	/**
	 * Set the value related to the column: BANKSUBACCNO.
	 * 
	 * @param banksubaccno the BANKSUBACCNO value
	 */
	public void setBanksubaccno(java.lang.String banksubaccno) {
		this.banksubaccno = banksubaccno;
	}

	/**
	 * Return the value associated with the column: CHKNUM.
	 * 
	 * @return the chknum
	 */
	public java.lang.String getChknum() {
		return chknum;
	}

	/**
	 * Set the value related to the column: CHKNUM.
	 * 
	 * @param chknum the CHKNUM value
	 */
	public void setChknum(java.lang.String chknum) {
		this.chknum = chknum;
	}

	/**
	 * Return the value associated with the column: CHKDATE.
	 * 
	 * @return the chkdate
	 */
	public java.util.Date getChkdate() {
		return chkdate != null ? (Date) chkdate.clone() : null;
	}

	/**
	 * Set the value related to the column: CHKDATE.
	 * 
	 * @param chkdate the CHKDATE value
	 */
	public void setChkdate(java.util.Date chkdate) {
		this.chkdate = chkdate != null ? (Date)chkdate.clone() : null;
	}

	/**
	 * Return the value associated with the column: RIBKEY.
	 * 
	 * @return the ribkey
	 */
	public java.lang.String getRibkey() {
		return ribkey;
	}

	/**
	 * Set the value related to the column: RIBKEY.
	 * 
	 * @param ribkey the RIBKEY value
	 */
	public void setRibkey(java.lang.String ribkey) {
		this.ribkey = ribkey;
	}

	/**
	 * Return the value associated with the column: EXP_DATE.
	 * 
	 * @return the exp date
	 */
	public java.util.Date getExpDate() {
		return expDate != null ? (Date) expDate.clone() : null;
	}

	/**
	 * Set the value related to the column: EXP_DATE.
	 * 
	 * @param expDate the EXP_DATE value
	 */
	public void setExpDate(java.util.Date expDate) {
		this.expDate = expDate != null ? (Date) expDate.clone() : null;
	}

	/**
	 * Return the value associated with the column: PT_ID.
	 * 
	 * @return the sema op pay mode
	 */
	public PaymentMode getSemaOpPayMode() {
		return semaOpPayMode;
	}

	/**
	 * Set the value related to the column: PT_ID.
	 * 
	 * @param semaOpPayMode the sema op pay mode
	 */
	public void setSemaOpPayMode(PaymentMode semaOpPayMode) {
		this.semaOpPayMode = semaOpPayMode;
	}

	/**
	 * Return the value associated with the column: TRX_ID.
	 * 
	 * @return the sema op trx type
	 */
	public TransactionType getSemaOpTrxType() {
		return semaOpTrxType;
	}

	/**
	 * Set the value related to the column: TRX_ID.
	 * 
	 * @param semaOpTrxType the sema op trx type
	 */
	public void setSemaOpTrxType(TransactionType semaOpTrxType) {
		this.semaOpTrxType = semaOpTrxType;
	}

	/**
	 * Return the value associated with the column: TRX_DATE.
	 * 
	 * @return the trx date
	 */
	public Date getTrxDate() {
		return trxDate != null ? (Date) trxDate.clone() : null;
	}

	/**
	 * Set the value related to the column: TRX_DATE.
	 * 
	 * @param trxDate the TRX_DATE value
	 */
	public void setTrxDate(java.util.Date trxDate) {
		this.trxDate = trxDate != null ? (Date) trxDate.clone() : null;
	}

	/**
	 * Return the value associated with the column: COST_ID.
	 * 
	 * @return the cost id
	 */
	public java.lang.Integer getCostId() {
		return costId;
	}

	/**
	 * Set the value related to the column: COST_ID.
	 * 
	 * @param costId the COST_ID value
	 */
	public void setCostId(java.lang.Integer costId) {
		this.costId = costId;
	}

	/**
	 * Return the value associated with the column: GLCODE.
	 * 
	 * @return the glcode
	 */
	public java.lang.String getGlcode() {
		return glcode;
	}

	/**
	 * Set the value related to the column: GLCODE.
	 * 
	 * @param glcode the GLCODE value
	 */
	public void setGlcode(java.lang.String glcode) {
		this.glcode = glcode;
	}

	/**
	 * Return the value associated with the column: ISMULTIPAY.
	 * 
	 * @return the checks if is multipay
	 */
	public java.lang.String getIsMultipay() {
		return isMultipay;
	}

	/**
	 * Set the value related to the column: ISMULTIPAY.
	 * 
	 * @param isMultipay the is multipay
	 */
	public void setIsMultipay(java.lang.String isMultipay) {
		this.isMultipay = isMultipay;
	}

	/**
	 * Return the value associated with the column: POST_AMT.
	 * 
	 * @return the post amt
	 */
	public java.lang.Double getPostAmt() {
		return postAmt;
	}

	/**
	 * Set the value related to the column: POST_AMT.
	 * 
	 * @param postAmt the POST_AMT value
	 */
	public void setPostAmt(java.lang.Double postAmt) {
		this.postAmt = postAmt;
	}

	/**
	 * Return the value associated with the column: STAMP_AMT.
	 * 
	 * @return the stamp amt
	 */
	public java.lang.Double getStampAmt() {
		return stampAmt;
	}

	/**
	 * Set the value related to the column: STAMP_AMT.
	 * 
	 * @param stampAmt the STAMP_AMT value
	 */
	public void setStampAmt(java.lang.Double stampAmt) {
		this.stampAmt = stampAmt;
	}

	/**
	 * Return the value associated with the column: MULTISYS_AMT.
	 * 
	 * @return the multisys amt
	 */
	public java.lang.Double getMultisysAmt() {
		return multisysAmt;
	}

	/**
	 * Set the value related to the column: MULTISYS_AMT.
	 * 
	 * @param multisysAmt the MULTISYS_AMT value
	 */
	public void setMultisysAmt(java.lang.Double multisysAmt) {
		this.multisysAmt = multisysAmt;
	}

	/**
	 * Return the value associated with the column: TOTAL_AMT.
	 * 
	 * @return the total amt
	 */
	public java.lang.Double getTotalAmt() {
		return totalAmt;
	}

	/**
	 * Set the value related to the column: TOTAL_AMT.
	 * 
	 * @param totalAmt the TOTAL_AMT value
	 */
	public void setTotalAmt(java.lang.Double totalAmt) {
		this.totalAmt = totalAmt;
	}

	/**
	 * Return the value associated with the column: STATUS.
	 * 
	 * @return the status
	 */
	public java.lang.String getStatus() {
		return status;
	}

	/**
	 * Set the value related to the column: STATUS.
	 * 
	 * @param status the STATUS value
	 */
	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	/**
	 * Return the value associated with the column: BORDU_NUM.
	 * 
	 * @return the bordu num
	 */
	public java.lang.String getBorduNum() {
		return borduNum;
	}

	/**
	 * Set the value related to the column: BORDU_NUM.
	 * 
	 * @param borduNum the BORDU_NUM value
	 */
	public void setBorduNum(java.lang.String borduNum) {
		this.borduNum = borduNum;
	}

	/**
	 * Return the value associated with the column: BANK_CODE.
	 * 
	 * @return the bank code
	 */
	public java.lang.String getBankCode() {
		return bankCode;
	}

	/**
	 * Set the value related to the column: BANK_CODE.
	 * 
	 * @param bankCode the BANK_CODE value
	 */
	public void setBankCode(java.lang.String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 * Return the value associated with the column: BRANCH_CODE.
	 * 
	 * @return the branch code
	 */
	public java.lang.String getBranchCode() {
		return branchCode;
	}

	/**
	 * Set the value related to the column: BRANCH_CODE.
	 * 
	 * @param branchCode the BRANCH_CODE value
	 */
	public void setBranchCode(java.lang.String branchCode) {
		this.branchCode = branchCode;
	}

	/**
	 * Return the value associated with the column: NEW_SUB.
	 * 
	 * @return the new sub
	 */
	public java.lang.String getNewSub() {
		return newSub;
	}

	/**
	 * Set the value related to the column: NEW_SUB.
	 * 
	 * @param newSub the NEW_SUB value
	 */
	public void setNewSub(java.lang.String newSub) {
		this.newSub = newSub;
	}

	/**
	 * Return the value associated with the column: BLCHECK.
	 * 
	 * @return the blcheck
	 */
	public java.lang.String getBlcheck() {
		return blcheck;
	}

	/**
	 * Set the value related to the column: BLCHECK.
	 * 
	 * @param blcheck the BLCHECK value
	 */
	public void setBlcheck(java.lang.String blcheck) {
		this.blcheck = blcheck;
	}

	/**
	 * Return the value associated with the column: CHEQUE_BOX.
	 * 
	 * @return the cheque box
	 */
	public java.lang.String getChequeBox() {
		return chequeBox;
	}

	/**
	 * Set the value related to the column: CHEQUE_BOX.
	 * 
	 * @param chequeBox the CHEQUE_BOX value
	 */
	public void setChequeBox(java.lang.String chequeBox) {
		this.chequeBox = chequeBox;
	}

	/**
	 * Return the value associated with the column: ID.
	 * 
	 * @return the id
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * Set the value related to the column: ID.
	 * 
	 * @param id the ID value
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
	}

	/**
	 * Return the value associated with the column: PRIORSYSTEM.
	 * 
	 * @return the priorsystem
	 */
	public java.lang.String getPriorsystem() {
		return priorsystem;
	}

	/**
	 * Set the value related to the column: PRIORSYSTEM.
	 * 
	 * @param priorsystem the PRIORSYSTEM value
	 */
	public void setPriorsystem(java.lang.String priorsystem) {
		this.priorsystem = priorsystem;
	}

	/**
	 * Gets the sema op pay details.
	 * 
	 * @return the sema op pay details
	 */
	public java.util.Collection<PaymentGEDetail> getSemaOpPayDetails() {
		if (semaOpPayDetails == null) {
			return new HashSet();
		}
		return semaOpPayDetails;
	}

	/**
	 * Sets the sema op pay details.
	 * 
	 * @param semaOpPayDetails the new sema op pay details
	 */
	public void setSemaOpPayDetails(java.util.Collection<PaymentGEDetail> semaOpPayDetails) {
		this.semaOpPayDetails = semaOpPayDetails;
	}

	/**
	 * Gets the sema op pay status.
	 * 
	 * @return the sema op pay status
	 */
	public java.util.Collection getSemaOpPayStatus() {
		return semaOpPayStatus;
	}

	/**
	 * Sets the sema op pay status.
	 * 
	 * @param semaOpPayStatus the new sema op pay status
	 */
	public void setSemaOpPayStatus(java.util.Collection semaOpPayStatus) {
		this.semaOpPayStatus = semaOpPayStatus;
	}

	/**
	 * Gets the username.
	 * 
	 * @return the username
	 */
	public java.lang.String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 * 
	 * @param username the new username
	 */
	public void setUsername(java.lang.String username) {
		this.username = username;
	}

	/**
	 * Gets the cust name.
	 * 
	 * @return the cust name
	 */
	public java.lang.String getCustName() {
		return custName;
	}

	/**
	 * Sets the cust name.
	 * 
	 * @param custName the new cust name
	 */
	public void setCustName(java.lang.String custName) {
		this.custName = custName;
	}
	
	/**
	 * Checks if is pM.
	 *
	 * @return true, if is pM
	 */
	public Integer getIsPM() {
		if(isPM == null || isPM == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * Sets the pM.
	 *
	 * @param isPM the new pM
	 */
	public void setIsPM(Integer isPM) {
		this.isPM = isPM;
	}

	/**
	 * Equals.
	 * 
	 * @param obj the obj
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (!(obj instanceof PaymentGE)) {
			return false;
		} else {
			PaymentGE paymentGE = (PaymentGE) obj;
			if (null == this.getId() || null == paymentGE.getId()) {
				return false;
			} else {
				return this.getId().equals(paymentGE.getId());
			}
		}
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}

}