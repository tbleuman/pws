package ma.iam.payment.domaine.ge;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class SemaOpCcloc. This is an object that contains data related to the SEMA_OP_CCLOC table. Do not modify this class because it will be
 * overwritten if the configuration file related to this class is modified.
 * 
 * @hibernate.class table="SEMA_OP_CCLOC"
 * @author Atos Origin
 * @version 1.0
 */
public class SemaOpCcloc implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6431898503104342899L;
	// fields
	/** The LABE l_ vali d_ from. */
	public static final String LABEL_VALID_FROM = "validFrom";

	/** The LABE l_ lo c_ id. */
	public static final String LABEL_LOC_ID = "locId";

	/** The LABE l_ vali d_ till. */
	public static final String LABEL_VALID_TILL = "validTill";

	/** The LABE l_ sem a_ o p_ location. */
	public static final String LABEL_SEMA_OP_LOCATION = "semaOpLocation";

	/** The LABE l_ id. */
	public static final String LABEL_ID = "id";

	/** The LABE l_ system. */
	public static final String LABEL_SYSTEM = "system";

	/** The LABE l_ cos t_ id. */
	public static final String LABEL_COST_ID = "costId";

	// fields
	/** The sema op location. */
	private Location semaOpLocation;

	/** The cost id. */
	private Integer costId;

	/** The valid from. */
	private Date validFrom;

	/** The valid till. */
	private Date validTill;

	/** The id. */
	private long id;

	/** The system. */
	private String system;

	/**
	 * Return the value associated with the column: LOC_ID.
	 * 
	 * @return the sema op location
	 */
	public Location getSemaOpLocation() {
		return semaOpLocation;
	}

	/**
	 * Sets the sema op location.
	 * 
	 * @param semaOpLocation the new sema op location
	 */
	public void setSemaOpLocation(Location semaOpLocation) {
		this.semaOpLocation = semaOpLocation;
	}

	/**
	 * Gets the cost id.
	 * 
	 * @return the cost id
	 */
	public Integer getCostId() {
		return costId;
	}

	/**
	 * Sets the cost id.
	 * 
	 * @param costId the new cost id
	 */
	public void setCostId(Integer costId) {
		this.costId = costId;
	}

	/**
	 * Gets the valid from.
	 * 
	 * @return the valid from
	 */
	public java.util.Date getValidFrom() {
		return validFrom != null ? (Date) validFrom.clone() : null;
	}

	/**
	 * Sets the valid from.
	 * 
	 * @param validFrom the new valid from
	 */
	public void setValidFrom(java.util.Date validFrom) {
		this.validFrom = validFrom != null ? (Date) validFrom.clone() : null;
	}

	/**
	 * Gets the valid till.
	 * 
	 * @return the valid till
	 */
	public java.util.Date getValidTill() {
		return validTill != null ? (Date) validTill.clone() : null;
	}

	/**
	 * Sets the valid till.
	 * 
	 * @param validTill the new valid till
	 */
	public void setValidTill(java.util.Date validTill) {
		this.validTill = validTill != null ? (Date)validTill.clone() : null;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the system.
	 * 
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 * 
	 * @param system the new system
	 */
	public void setSystem(String system) {
		this.system = system;
	}

}