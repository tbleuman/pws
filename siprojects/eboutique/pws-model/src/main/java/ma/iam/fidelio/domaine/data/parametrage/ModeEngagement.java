package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class ModeEngagement extends BaseObject{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3106870876089261600L;

	/** The Constant EN_SERVICE. */
	public static final String EN_SERVICE = "enService";

	/** The Constant DUREE_EN_MOIS. */
	public static final String DUREE_EN_MOIS = "dureeEnMois";

	/** The Constant ENGAGEMENT_BSCS_ID. */
	public static final String ENGAGEMENT_BSCS_ID = "engagementBscsId";

	/** The libelle. */
	private String libelle;

	/** The duree en mois. */
	private Integer dureeEnMois;

	/** The engagement bscs id. */
	private Integer engagementBscsId;

	/** The en service. */
	private Boolean enService;

	/**
	 * Gets the libelle.
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * @param libelle the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the duree en mois.
	 * @return the duree en mois
	 */
	public Integer getDureeEnMois() {
		return dureeEnMois;
	}

	/**
	 * Sets the duree en mois.
	 * @param dureeEnMois the new duree en mois
	 */
	public void setDureeEnMois(Integer dureeEnMois) {
		this.dureeEnMois = dureeEnMois;
	}

	/**
	 * Gets the engagement bscs id.
	 * @return the engagement bscs id
	 */
	public Integer getEngagementBscsId() {
		return engagementBscsId;
	}

	/**
	 * Sets the engagement bscs id.
	 * @param engagementBscsId the new engagement bscs id
	 */
	public void setEngagementBscsId(Integer engagementBscsId) {
		this.engagementBscsId = engagementBscsId;
	}

	/**
	 * Gets the en service.
	 * @return the en service
	 */
	public Boolean getEnService() {
		return enService;
	}

	/**
	 * Sets the en service.
	 * @param enService the new en service
	 */
	public void setEnService(Boolean enService) {
		this.enService = enService;
	}
}
