package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;
import ma.iam.fidelio.domaine.data.parametrage.Qualite;

public class CompteClient extends BaseObject {
	
	public static final String ID_CLIENT = "idClient";

	/** ndActif. **/
	private String ndActif;

	/** idClient. **/
	private Integer idClient;

	/** soldeAcquis. **/
	private Long soldeAcquis;

	/** soldeEnCours. **/
	private Long soldeEnCours;

	/** dateMAJSolde. **/
	private Date dateMAJSolde;

	/** dateCreation. **/
	private Date dateCreation;

	/** codeFidelio. **/
	private Integer codeFidelio;

	/** La Référence Qualite. **/
	private Qualite refQualite;

	/**
	 * Modifier L'attribut ndActif.
	 * @param : ndActif
	 */
	public void setNdActif(String ndActif) {
		this.ndActif = ndActif;
	}
	
	/**
	 * Modifier L'attribut idClient.
	 * @param : idClient
	 */
	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}
	
	/**
	 * Modifier L'attribut soldeAcquis.
	 * @param : soldeAcquis
	 */
	public void setSoldeAcquis(Long soldeAcquis) {
		this.soldeAcquis = soldeAcquis;
	}
	
	/**
	 * Modifier L'attribut soldeEnCours.
	 * @param : soldeEnCours
	 */
	public void setSoldeEnCours(Long soldeEnCours) {
		this.soldeEnCours = soldeEnCours;
	}
	
	/**
	 * Modifier L'attribut dateMAJSolde.
	 * @param : dateMAJSolde
	 */
	public void setDateMAJSolde(Date dateMAJSolde) {
		this.dateMAJSolde = dateMAJSolde;
	}
	
	/**
	 * Modifier L'attribut dateCreation.
	 * @param : dateCreation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	/**
	 * Modifier L'attribut codeFidelio.
	 * @param : codeFidelio
	 */
	public void setCodeFidelio(Integer codeFidelio) {
		this.codeFidelio = codeFidelio;
	}
	
	/**
	 * Modifier l'objet Qualite.
	 * @param : refQualite
	 */
	public void setRefQualite(Qualite refQualite) {
		this.refQualite = refQualite;
	}
	
	/**
	 * Retourner la valeur de l'attribut ndActif.
	 * @return : ndActif
	 */
	public String getNdActif() {
		return ndActif;
	}
	
	/**
	 * Retourner la valeur de l'attribut idClient.
	 * @return : idClient
	 */
	public Integer getIdClient() {
		return idClient;
	}

	/**
	 * Retourner la valeur de l'attribut soldeAcquis.
	 * @return : soldeAcquis
	 */
	public Long getSoldeAcquis() {
		return soldeAcquis;
	}

	/**
	 * Retourner la valeur de l'attribut soldeEnCours.
	 * @return : soldeEnCours
	 */
	public Long getSoldeEnCours() {
		return soldeEnCours;
	}

	/**
	 * Retourner la valeur de l'attribut dateMAJSolde.
	 * @return : dateMAJSolde
	 */
	public Date getDateMAJSolde() {
		return dateMAJSolde;
	}

	/**
	 * Retourner la valeur de l'attribut dateCreation.
	 * @return : dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * Retourner la valeur de l'attribut codeFidelio.
	 * @return : codeFidelio
	 */
	public Integer getCodeFidelio() {
		return codeFidelio;
	}

	/**
	 * Retourner la valeur de l'attribut refQualite.
	 * @return : refQualite
	 */
	public Qualite getRefQualite() {
		return refQualite;
	}
}
