package ma.iam.fidelio.domaine.data.bscs;

import java.io.Serializable;


public class DetailFactureId implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2359301845630548860L;

	/** The id facture. */
	private Long idFacture;

	/** The numero ordre. */
	private Long numeroOrdre;

	/**
	 * Equals.
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DetailFactureId)) {
			return false;
		}
		final DetailFactureId obj = (DetailFactureId) other;
		return (obj.idFacture.equals(idFacture) && obj.numeroOrdre.equals(numeroOrdre));
	}

	/**
	 * Hash code.
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return 29 * super.hashCode() + idFacture.intValue() + numeroOrdre.intValue();
	}

	/**
	 * Gets the id facture.
	 * @return the id facture
	 */
	public Long getIdFacture() {
		return idFacture;
	}

	/**
	 * Sets the id facture.
	 * @param idFacture the new id facture
	 */
	public void setIdFacture(Long idFacture) {
		this.idFacture = idFacture;
	}

	/**
	 * Gets the numero ordre.
	 * @return the numero ordre
	 */
	public Long getNumeroOrdre() {
		return numeroOrdre;
	}

	/**
	 * Sets the numero ordre.
	 * @param numeroOrdre the new numero ordre
	 */
	public void setNumeroOrdre(Long numeroOrdre) {
		this.numeroOrdre = numeroOrdre;
	}

}
