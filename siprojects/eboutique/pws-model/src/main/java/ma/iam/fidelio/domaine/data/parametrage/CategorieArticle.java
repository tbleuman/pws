package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class CategorieArticle extends BaseObject {

	/** Le Code. **/
	private String code;

	/** Le Libelle. **/
	private String libelle;

	/**
	 * Modifier L'attribut code.
	 * @param : code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Modifier L'attribut libelle.
	 * @param : libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	/**
	 * Retourner la valeur de l'attribut code. 
	 * @return : code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Retourner la valeur de l'attribut libelle. 
	 * @return : libelle
	 */
	public String getLibelle() {
		return libelle;
	}
}
