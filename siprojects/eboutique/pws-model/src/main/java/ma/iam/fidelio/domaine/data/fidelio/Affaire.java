package ma.iam.fidelio.domaine.data.fidelio;

import java.util.Collection;
import java.util.Date;

import ma.iam.fidelio.domaine.data.commun.BaseObject;
import ma.iam.fidelio.domaine.data.parametrage.TypeAffaire;

public class Affaire extends BaseObject {

	/** L'utilisateur. **/
	private String utilisateur;

	/** La date de la création. **/
	private Date dateCreation;

	/** Le commentaire. **/
	private String commentaire;

	/** La référence CompteClient **/
	private CompteClient refCompteClient;

	/** La référence TypeAffaire. **/
	private TypeAffaire refTypeAffaire;

	/** La référence Collection HistStatutAffaires. **/
	private Collection<HistStatutAffaire> refHistStatutAffaires;
	
	/** La référence Collection Compensations. **/
	private Collection<Compensation> refCompensations;

	/**
	 * Retourner l'attribut utilisateur.
	 * @return : utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Modifier l'attribut utilisateur.
	 * @param : utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Retourner l'attribut dateCreation.
	 * @return : dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * Modifier l'attribut dateCreation.
	 * @param : dateCreation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * Retourner l'attribut commentaire.
	 * @return : commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * Modifier l'attribut commentaire.
	 * @param : commentaire
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * Retourner la référence CompteClient.
	 * @return : refCompteClient
	 */
	public CompteClient getRefCompteClient() {
		return refCompteClient;
	}

	/**
	 * Modifier la référence CompteClient.
	 * @param : refCompteClient
	 */
	public void setRefCompteClient(CompteClient refCompteClient) {
		this.refCompteClient = refCompteClient;
	}

	/**
	 * Retourner la référence Collection HistStatutAffaires.
	 * @return : refHistStatutAffaires
	 */
	public Collection<HistStatutAffaire> getRefHistStatutAffaires() {
		return refHistStatutAffaires;
	}

	/**
	 * Modifier la référence Collection HistStatutAffaires.
	 * @param : refHistStatutAffaires
	 */
	public void setRefHistStatutAffaires(Collection<HistStatutAffaire> refHistStatutAffaires) {
		this.refHistStatutAffaires = refHistStatutAffaires;
	}

	/**
	 * Retourner la référence TypeAffaire.
	 * @return : refTypeAffaire
	 */
	public TypeAffaire getRefTypeAffaire() {
		return refTypeAffaire;
	}

	/**
	 * Modifier la référence TypeAffaire.
	 * @param : refTypeAffaire
	 */
	public void setRefTypeAffaire(TypeAffaire refTypeAffaire) {
		this.refTypeAffaire = refTypeAffaire;
	}
	
	/**
	 * Retourner la référence Collection Compensations.
	 * @return : refCompensations
	 */
	public Collection<Compensation> getRefCompensations() {
		return refCompensations;
	}

	/**
	 * Modifier la référence Collection Compensations.
	 * @param refCompensations the new ref compensations
	 */
	public void setRefCompensations(Collection<Compensation> refCompensations) {
		this.refCompensations = refCompensations;
	}
}
