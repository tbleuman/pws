package ma.iam.fidelio.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ArticleDto implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = -5928151374184257156L;

	/** Id Article */
	private Integer idPoste;
	
	/** Code Article Sur BSCS */
	private String codeArticle;
	
	/** Nom Article */
	private String libelle;
	
	/** Etat De Supenseion Dans Fidelio*/
	private Boolean enService;
	
	/** Code Article SAP. */
	private String codeArticleSAP;
	
	/** Liste des Prix Standard */
	private PrixStandardDto prixStandardDto = new PrixStandardDto();
	
	/** Liste des Prix Avec Engagement */
	private List<PrixEngagementDto> listPrixEngagementDto = new ArrayList<PrixEngagementDto>();
	

	/**
	 * The getter method for the field idPoste.
	 * @return the idPoste.
	 */
	public Integer getIdPoste() {
		return idPoste;
	}

	/**
	 * The setter method for the field idPoste.
	 * @param idPoste the idPoste to set.
	 */
	public void setIdPoste(Integer idPoste) {
		this.idPoste = idPoste;
	}

	/**
	 * The getter method for the field codeArticle.
	 * @return the codeArticle.
	 */
	public String getCodeArticle() {
		return codeArticle;
	}

	/**
	 * The setter method for the field codeArticle.
	 * @param codeArticle the codeArticle to set.
	 */
	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	/**
	 * The getter method for the field libelle.
	 * @return the libelle.
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * The setter method for the field libelle.
	 * @param libelle the libelle to set.
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * The getter method for the field enService.
	 * @return the enService.
	 */
	public Boolean getEnService() {
		return enService;
	}

	/**
	 * The setter method for the field enService.
	 * @param enService the enService to set.
	 */
	public void setEnService(Boolean enService) {
		this.enService = enService;
	}
	
	/**
	 * The getter methode for the field codeArticleSAP.
	 * @return the codeArticleSAP
	 */
	public String getCodeArticleSAP() {
		return this.codeArticleSAP;
	}
	
	/**
	 * The setter method for the field codeArticleSAP.
	 * @param codeArticleSAP
	 */
	public void setCodeArticleSAP(String codeArticleSAP) {
		this.codeArticleSAP = codeArticleSAP;
	}

	/**
	 * The getter method for the field listPrixStandardDto.
	 * @return the listPrixStandardDto.
	 */
	public PrixStandardDto getPrixStandardDto() {
		return prixStandardDto;
	}

	/**
	 * The setter method for the field listPrixStandardDto.
	 * @param listPrixStandardDto the listPrixStandardDto to set.
	 */
	public void setPrixStandardDto(PrixStandardDto prixStandardDto) {
		this.prixStandardDto = prixStandardDto;
	}

	/**
	 * The getter method for the field listPrixEngagementDto.
	 * @return the listPrixEngagementDto.
	 */
	public List<PrixEngagementDto> getListPrixEngagementDto() {
		return listPrixEngagementDto;
	}

	/**
	 * The setter method for the field listPrixEngagementDto.
	 * @param listPrixEngagementDto the listPrixEngagementDto to set.
	 */
	public void setListPrixEngagementDto(
			List<PrixEngagementDto> listPrixEngagementDto) {
		this.listPrixEngagementDto = listPrixEngagementDto;
	}
}