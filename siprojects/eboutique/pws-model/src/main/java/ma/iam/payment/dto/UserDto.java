package ma.iam.payment.dto;

import java.io.Serializable;

/**
 * 
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class UserDto implements Serializable {

	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = 1944896056575121025L;
	
	private String login;
	
	private Integer costId;
	
	private Integer locationId;
	
	
	/**
	 * @param login
	 * @param costId
	 * @param locationId
	 */
	public UserDto(String login, Integer costId, Integer locationId) {
		super();
		this.login = login;
		this.costId = costId;
		this.locationId = locationId;
	}

	/**
	 * The getter method for the field login.
	 * @return the login.
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * The setter method for the field login.
	 * @param login the login to set.
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * The getter method for the field costId.
	 * @return the costId.
	 */
	public Integer getCostId() {
		return costId;
	}

	/**
	 * The setter method for the field costId.
	 * @param costId the costId to set.
	 */
	public void setCostId(Integer costId) {
		this.costId = costId;
	}

	/**
	 * The getter method for the field locationId.
	 * @return the locationId.
	 */
	public Integer getLocationId() {
		return locationId;
	}

	/**
	 * The setter method for the field locationId.
	 * @param locationId the locationId to set.
	 */
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

}
