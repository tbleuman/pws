package ma.iam.fidelio.domaine.data.bscs;

import java.util.Date;

public class FidelioFacture {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9156240224218105779L;

	/** The Constant MONTANT_OUVERT. */
	public static final String MONTANT_OUVERT = "montantOuvert";

	/** The Constant REF_CLIENT. */
	public static final String REF_CLIENT = "refClient";

	/** The Constant DATE_FACTURE. */
	public static final String DATE_FACTURE = "dateFacture";

	/** The Constant DATE_ECHEANCE. */
	public static final String DATE_ECHEANCE = "dateEcheance";

	/** The Constant FLAG_RECLAME. */
	public static final String FLAG_RECLAME = "flagReclame";

	/** The Constant SATUT_FACTURE. */
	public static final String SATUT_FACTURE = "statut";

	/** The id. */
	private Long id;

	/** The num reference. */
	private String numReference;

	/** The statut. */
	private String statut;

	/** The type. */
	private String type;

	/** The date facture. */
	private Date dateFacture;

	/** The motant facture. */
	private Float motantFacture;

	/** The date echeance. */
	private Date dateEcheance;

	/** The montant ouvert. */
	private Float montantOuvert;

	/** The flag reclame. */
	private String flagReclame;
	
	private FidelioClient refClient;

	/**
	 * Equals.
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FidelioFacture)) {
			return false;
		}
		final FidelioFacture obj = (FidelioFacture) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return 29 * super.hashCode() + id.intValue();
	}

	/**
	 * Gets the date echeance.
	 * @return the date echeance
	 */
	public Date getDateEcheance() {
		return dateEcheance;
	}

	/**
	 * Sets the date echeance.
	 * @param dateEcheance the new date echeance
	 */
	public void setDateEcheance(Date dateEcheance) {
		this.dateEcheance = dateEcheance;
	}

	/**
	 * Gets the date facture.
	 * @return the date facture
	 */
	public Date getDateFacture() {
		return dateFacture;
	}

	/**
	 * Sets the date facture.
	 * @param dateFacture the new date facture
	 */
	public void setDateFacture(Date dateFacture) {
		this.dateFacture = dateFacture;
	}

	/**
	 * Gets the flag reclame.
	 * @return the flag reclame
	 */
	public String getFlagReclame() {
		return flagReclame;
	}

	/**
	 * Sets the flag reclame.
	 * @param flagReclame the new flag reclame
	 */
	public void setFlagReclame(String flagReclame) {
		this.flagReclame = flagReclame;
	}

	/**
	 * Gets the id.
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the montant ouvert.
	 * @return the montant ouvert
	 */
	public Float getMontantOuvert() {
		return montantOuvert;
	}

	/**
	 * Sets the montant ouvert.
	 * @param montantOuvert the new montant ouvert
	 */
	public void setMontantOuvert(Float montantOuvert) {
		this.montantOuvert = montantOuvert;
	}

	/**
	 * Gets the motant facture.
	 * @return the motant facture
	 */
	public Float getMotantFacture() {
		return motantFacture;
	}

	/**
	 * Sets the motant facture.
	 * @param motantFacture the new motant facture
	 */
	public void setMotantFacture(Float motantFacture) {
		this.motantFacture = motantFacture;
	}

	/**
	 * Gets the num reference.
	 * @return the num reference
	 */
	public String getNumReference() {
		return numReference;
	}

	/**
	 * Sets the num reference.
	 * @param numReference the new num reference
	 */
	public void setNumReference(String numReference) {
		this.numReference = numReference;
	}

	/**
	 * Gets the statut.
	 * @return the statut
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * Sets the statut.
	 * @param statut the new statut
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * Gets the type.
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * The getter method for the field refClient.
	 * @return the refClient.
	 */
	public FidelioClient getRefClient() {
		return refClient;
	}

	/**
	 * The setter method for the field refClient.
	 * @param refClient the refClient to set.
	 */
	public void setRefClient(FidelioClient refClient) {
		this.refClient = refClient;
	}
}
