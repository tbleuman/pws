package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

public class Agence extends BaseObject {

	public static final String EN_SERVICE = "enService";
	public static final String COST_CENTER_ID = "costCenterId";
	/** libelle. **/
	private String libelle;

	/** L'utilisateur. **/
	private String utilisateur;

	/** En service. **/
	private Boolean enService;

	/** L'id du dealer. **/
	private Integer dealerId;

	/** L'id du costCenter. **/
	private Integer costCenterId;

	/**
	 * Retourner l'attribut libelle.
	 * @return : libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Modifier l'attribut libelle.
	 * @param : libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Retourner l'attribut utilisateur.
	 * @return : utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Modifier l'attribut utilisateur.
	 * @param : utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * Retourner l'attribut enService.
	 * @return : enService
	 */
	public Boolean getEnService() {
		return enService;
	}

	/**
	 * Modifier l'attribut enService.
	 * @param : enService
	 */
	public void setEnService(Boolean enService) {
		this.enService = enService;
	}

	/**
	 * Retourner l'attribut dealerId.
	 * @return : dealerId
	 */
	public Integer getDealerId() {
		return dealerId;
	}

	/**
	 * Modifier l'attribut dealerId.
	 * @param : dealerId
	 */
	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}

	/**
	 * Retourner l'attribut costCenterId.
	 * @return : costCenterId
	 */
	public Integer getCostCenterId() {
		return costCenterId;
	}

	/**
	 * Modifier l'attribut costCenterId.
	 * @param : costCenterId
	 */
	public void setCostCenterId(Integer costCenterId) {
		this.costCenterId = costCenterId;
	}
}
