package ma.iam.fidelio.domaine.data.parametrage;


/**
 * The Class BonusEngagement.
 * @version 1.0.0
 * @author Atos Origin
 */
public class BonusEngagement extends Parametre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -454987496784349687L;

	/** The Constant REF_MODE_ENGAGEMENT. */
	public static final String REF_MODE_ENGAGEMENT = "refModeEngagement";

	/** The bonus engagement. */
	private Integer bonusEngagement;

	/** The ref mode engagement. */
	private ModeEngagement refModeEngagement;

	/**
	 * Gets the bonus engagement.
	 * @return the bonus engagement
	 */
	public Integer getBonusEngagement() {
		return bonusEngagement;
	}

	/**
	 * Sets the bonus engagement.
	 * @param bonusEngagement the new bonus engagement
	 */
	public void setBonusEngagement(Integer bonusEngagement) {
		this.bonusEngagement = bonusEngagement;
	}

	/**
	 * Gets the ref mode engagement.
	 * @return the ref mode engagement
	 */
	public ModeEngagement getRefModeEngagement() {
		return refModeEngagement;
	}

	/**
	 * Sets the ref mode engagement.
	 * @param refModeEngagement the new ref mode engagement
	 */
	public void setRefModeEngagement(ModeEngagement refModeEngagement) {
		this.refModeEngagement = refModeEngagement;
	}

}
