package ma.iam.payment.domaine.bscs;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * <ul>
 * <li>Schema : SYSADM</li>
 * <li>Table : CASHDETAIL</li>
 * <li>Description : Détail paiement</li>
 * </ul>
 * </p>
 * .
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentBSCSDetail implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7874990406017132607L;

	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_NUM_TRANSACTION. */
	public static final String LABEL_NUM_TRANSACTION = "numTransaction";

	/** The Constant LABEL_NUM_DOC. */
	public static final String LABEL_NUM_DOC = "numDocument";

	/** The Constant LABEL_MONTANT. */
	public static final String LABEL_MONTANT = "montant";

	/** The Constant LABEL_CADAGLAR. */
	public static final String LABEL_CADAGLAR = "cadaglar";

	/** The Constant LABEL_CADASSOCXACT. */
	public static final String LABEL_CADASSOCXACT = "cadassocxact";

	/** The Constant LABEL_CADEXPCONVDATEEXCHANGE. */
	public static final String LABEL_CADEXPCONVDATEEXCHANGE = "cadexpconvdateExchange";

	/** The Constant LABEL_REC_VERSION. */
	public static final String LABEL_REC_VERSION = "recVersion";

	/** The Constant LABEL_PAY_DEVISE. */
	public static final String LABEL_PAY_DEVISE = "paymentDevise";

	/** The Constant LABEL_DOC_DEVISE. */
	public static final String LABEL_DOC_DEVISE = "documentDevise";

	/** The Constant LABEL_GL_DEVISE. */
	public static final String LABEL_GL_DEVISE = "glDevise";

	/** The Constant LABEL_CAD_AMT_DOC. */
	public static final String LABEL_CAD_AMT_DOC = "cadAmtDoc";

	/** The Constant LABEL_CAD_AMT_PAY. */
	public static final String LABEL_CAD_AMT_PAY = "cadAmtPay";

	/** The Constant LABEL_CAD_AMT_CUR_PAY. */
	public static final String LABEL_CAD_AMT_CUR_PAY = "cadCurAmtPay";

	/** The Constant LABEL_CADCONVDATEEXCHAGEGL. */
	public static final String LABEL_CADCONVDATEEXCHAGEGL = "cadConvDateExchangeGl";

	/** The Constant LABEL_CADCONVDATEEXCHAGEDOC. */
	public static final String LABEL_CADCONVDATEEXCHAGEDOC = "cadConvDateExchangeDoc";

	/** The Constant LABEL_CADCURAMTDOC. */
	public static final String LABEL_CADCURAMTDOC = "cadCurAmtDoc";

	/** The Constant LABEL_CADCURAMTGL. */
	public static final String LABEL_CADCURAMTGL = "cadCurAmtGl";

	/**
	 * <p>
	 * <ul>
	 * <li>Description : Identifiant</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private PaymentBSCSDetailId id;

	/**
	 * <p>
	 * <ul>
	 * <li>Description : Numero transation</li>
	 * <li>Champs : CADXACT</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long numTransaction;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADOXACT</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long numDocument;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADAMT_GL</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Double montant;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADGLAR</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private String cadaglar;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADASSOCXACT</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long cadassocxact;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADEXPCONVDATE_EXCHANGE</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date cadexpconvdateExchange;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : REC_VERSION</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long recVersion;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : PAYMENT_CURRENCY</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long paymentDevise = Long.valueOf(91);

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : DOCUMENT_CURRENCY</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long documentDevise = Long.valueOf(91);

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : GL_CURRENCY</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Long glDevise = Long.valueOf(91);

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADAMT_DOC</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Double cadAmtDoc;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADAMT_PAY</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Double cadAmtPay;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADCURAMT_PAY</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Double cadCurAmtPay;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADCONVDATE_EXCHANGE_GL</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date cadConvDateExchangeGl;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADCONVDATE_EXCHANGE_DOC</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Date cadConvDateExchangeDoc;

	/**
	 * <p>
	 * <ul>
	 * <li>Champs : CADCURAMT_DOC</li>
	 * </ul>
	 * </p>
	 * .
	 */
	private Double cadCurAmtDoc;

	/** The cad cur amt gl. */
	private Double cadCurAmtGl;

	/** The ref paiement. */
	private PaymentBSCS refPaiement;

	/** The ref facture. */
	private Facture refFacture;

	/**
	 * Gets the cad cur amt gl.
	 * 
	 * @return the cad cur amt gl
	 */
	public Double getCadCurAmtGl() {
		return cadCurAmtGl;
	}

	/**
	 * Sets the cad cur amt gl.
	 * 
	 * @param cadCurAmtGl the new cad cur amt gl
	 */
	public void setCadCurAmtGl(Double cadCurAmtGl) {
		this.cadCurAmtGl = cadCurAmtGl;
	}

	/**
	 * Equals.
	 * 
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PaymentBSCSDetail)) {
			return false;
		}
		final PaymentBSCSDetail obj = (PaymentBSCSDetail) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (id != null) {
			return 29 * super.hashCode() + id.hashCode();
		} else {
			return 29 * super.hashCode();
		}
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	protected PaymentBSCSDetailId getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(PaymentBSCSDetailId id) {
		this.id = id;
	}

	/**
	 * Gets the num document.
	 * 
	 * @return the num document
	 */
	public Long getNumDocument() {
		return numDocument;
	}

	/**
	 * Sets the num document.
	 * 
	 * @param numDocument the new num document
	 */
	public void setNumDocument(Long numDocument) {
		this.numDocument = numDocument;
	}

	/**
	 * Gets the num transaction.
	 * 
	 * @return the num transaction
	 */
	public Long getNumTransaction() {
		return numTransaction;
	}

	/**
	 * Sets the num transaction.
	 * 
	 * @param numTransaction the new num transaction
	 */
	public void setNumTransaction(Long numTransaction) {
		this.numTransaction = numTransaction;
	}

	/**
	 * Sets the id ordre.
	 * 
	 * @param idOrder the new id ordre
	 */
	public void setIdOrdre(Long idOrder) {
		id.setIdOrdre(idOrder);
	}

	/**
	 * Gets the id ordre.
	 * 
	 * @return the id ordre
	 */
	public Long getIdOrdre() {
		return id.getIdOrdre();
	}

	/**
	 * Gets the id paiement.
	 * 
	 * @return the id paiement
	 */
	public Long getIdPaiement() {
		return id.getIdPaiement();
	}

	/**
	 * Sets the id paiement.
	 * 
	 * @param idPaiement the new id paiement
	 */
	public void setIdPaiement(Long idPaiement) {
		id.setIdPaiement(idPaiement);
	}

	/**
	 * Gets the montant.
	 * 
	 * @return the montant
	 */
	public Double getMontant() {
		return montant;
	}

	/**
	 * Sets the montant.
	 * 
	 * @param montant the new montant
	 */
	public void setMontant(Double montant) {
		this.montant = montant;
	}

	/**
	 * Gets the cadaglar.
	 * 
	 * @return the cadaglar
	 */
	public String getCadaglar() {
		return cadaglar;
	}

	/**
	 * Sets the cadaglar.
	 * 
	 * @param cadaglar the new cadaglar
	 */
	public void setCadaglar(String cadaglar) {
		this.cadaglar = cadaglar;
	}

	/**
	 * Gets the cadassocxact.
	 * 
	 * @return the cadassocxact
	 */
	public Long getCadassocxact() {
		return cadassocxact;
	}

	/**
	 * Sets the cadassocxact.
	 * 
	 * @param cadassocxact the new cadassocxact
	 */
	public void setCadassocxact(Long cadassocxact) {
		this.cadassocxact = cadassocxact;
	}

	/**
	 * Gets the cadexpconvdate exchange.
	 * 
	 * @return the cadexpconvdate exchange
	 */
	public Date getCadexpconvdateExchange() {
		return cadexpconvdateExchange != null ? (Date) cadexpconvdateExchange.clone() : null;
	}

	/**
	 * Sets the cadexpconvdate exchange.
	 * 
	 * @param cadexpconvdateExchange the new cadexpconvdate exchange
	 */
	public void setCadexpconvdateExchange(Date cadexpconvdateExchange) {
		this.cadexpconvdateExchange = cadexpconvdateExchange != null ? (Date) cadexpconvdateExchange.clone() : null;
	}

	/**
	 * Gets the rec version.
	 * 
	 * @return the rec version
	 */
	public Long getRecVersion() {
		return recVersion;
	}

	/**
	 * Sets the rec version.
	 * 
	 * @param recVersion the new rec version
	 */
	public void setRecVersion(Long recVersion) {
		this.recVersion = recVersion;
	}

	/**
	 * Gets the payment devise.
	 * 
	 * @return the payment devise
	 */
	public Long getPaymentDevise() {
		return paymentDevise;
	}

	/**
	 * Sets the payment devise.
	 * 
	 * @param paymentDevise the new payment devise
	 */
	public void setPaymentDevise(Long paymentDevise) {
		this.paymentDevise = paymentDevise;
	}

	/**
	 * Gets the document devise.
	 * 
	 * @return the document devise
	 */
	public Long getDocumentDevise() {
		return documentDevise;
	}

	/**
	 * Sets the document devise.
	 * 
	 * @param documentDevise the new document devise
	 */
	public void setDocumentDevise(Long documentDevise) {
		this.documentDevise = documentDevise;
	}

	/**
	 * Gets the gl devise.
	 * 
	 * @return the gl devise
	 */
	public Long getGlDevise() {
		return glDevise;
	}

	/**
	 * Sets the gl devise.
	 * 
	 * @param glDevise the new gl devise
	 */
	public void setGlDevise(Long glDevise) {
		this.glDevise = glDevise;
	}

	/**
	 * Gets the cad amt doc.
	 * 
	 * @return the cad amt doc
	 */
	public Double getCadAmtDoc() {
		return cadAmtDoc;
	}

	/**
	 * Sets the cad amt doc.
	 * 
	 * @param cadAmtDoc the new cad amt doc
	 */
	public void setCadAmtDoc(Double cadAmtDoc) {
		this.cadAmtDoc = cadAmtDoc;
	}

	/**
	 * Gets the cad amt pay.
	 * 
	 * @return the cad amt pay
	 */
	public Double getCadAmtPay() {
		return cadAmtPay;
	}

	/**
	 * Sets the cad amt pay.
	 * 
	 * @param cadAmtPay the new cad amt pay
	 */
	public void setCadAmtPay(Double cadAmtPay) {
		this.cadAmtPay = cadAmtPay;
	}

	/**
	 * Gets the cad cur amt pay.
	 * 
	 * @return the cad cur amt pay
	 */
	public Double getCadCurAmtPay() {
		return cadCurAmtPay;
	}

	/**
	 * Sets the cad cur amt pay.
	 * 
	 * @param cadCurAmtPay the new cad cur amt pay
	 */
	public void setCadCurAmtPay(Double cadCurAmtPay) {
		this.cadCurAmtPay = cadCurAmtPay;
	}

	/**
	 * Gets the cad conv date exchange gl.
	 * 
	 * @return the cad conv date exchange gl
	 */
	public Date getCadConvDateExchangeGl() {
		return cadConvDateExchangeGl != null ? (Date) cadConvDateExchangeGl.clone() : null;
	}

	/**
	 * Sets the cad conv date exchange gl.
	 * 
	 * @param cadConvDateExchangeGl the new cad conv date exchange gl
	 */
	public void setCadConvDateExchangeGl(Date cadConvDateExchangeGl) {
		this.cadConvDateExchangeGl = cadConvDateExchangeGl != null ? (Date) cadConvDateExchangeGl.clone() : null;
	}

	/**
	 * Gets the cad conv date exchange doc.
	 * 
	 * @return the cad conv date exchange doc
	 */
	public Date getCadConvDateExchangeDoc() {
		return cadConvDateExchangeDoc != null ? (Date) cadConvDateExchangeDoc.clone() : null;
	}

	/**
	 * Sets the cad conv date exchange doc.
	 * 
	 * @param cadConvDateExchangeDoc the new cad conv date exchange doc
	 */
	public void setCadConvDateExchangeDoc(Date cadConvDateExchangeDoc) {
		this.cadConvDateExchangeDoc = cadConvDateExchangeDoc != null ? (Date) cadConvDateExchangeDoc.clone() : null;
	}

	/**
	 * Gets the cad cur amt doc.
	 * 
	 * @return the cad cur amt doc
	 */
	public Double getCadCurAmtDoc() {
		return cadCurAmtDoc;
	}

	/**
	 * Sets the cad cur amt doc.
	 * 
	 * @param cadCurAmtDoc the new cad cur amt doc
	 */
	public void setCadCurAmtDoc(Double cadCurAmtDoc) {
		this.cadCurAmtDoc = cadCurAmtDoc;
	}

	/**
	 * Gets the ref paiement.
	 * 
	 * @return the ref paiement
	 */
	public PaymentBSCS getRefPaiement() {
		return refPaiement;
	}

	/**
	 * Sets the ref paiement.
	 * 
	 * @param refPaiement the new ref paiement
	 */
	public void setRefPaiement(PaymentBSCS refPaiement) {
		this.refPaiement = refPaiement;
	}

	/**
	 * Gets the ref facture.
	 * 
	 * @return the ref facture
	 */
	public Facture getRefFacture() {
		return refFacture;
	}

	/**
	 * Sets the ref facture.
	 * 
	 * @param refFacture the new ref facture
	 */
	public void setRefFacture(Facture refFacture) {
		this.refFacture = refFacture;
	}

}
