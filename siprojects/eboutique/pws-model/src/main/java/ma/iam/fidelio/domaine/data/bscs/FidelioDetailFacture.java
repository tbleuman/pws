package ma.iam.fidelio.domaine.data.bscs;


public class FidelioDetailFacture {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4966972826828807052L;

	/** The id. */
	private DetailFactureId id;

	/** The numero. */
	private Long numero;

	/** The charge. */
	private String charge;

	/** The prix. */
	private Float prix;

	/** The montant ht. */
	private Float montantHT;

	/** The montant tva. */
	private Float montantTVA;

	/** The montant ttc. */
	private Float montantTTC;

	/** The ref facture. */
	private FidelioFacture refFacture;

	/**
	 * Equals.
	 * @param other the other
	 * @return true, if equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FidelioDetailFacture)) {
			return false;
		}
		final FidelioDetailFacture obj = (FidelioDetailFacture) other;
		return obj.id.equals(id);
	}

	/**
	 * Hash code.
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return 29 * super.hashCode() + id.hashCode();
	}

	/**
	 * Gets the id.
	 * @return the id
	 */
	protected DetailFactureId getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * @param id the new id
	 */
	protected void setId(DetailFactureId id) {
		this.id = id;
	}

	/**
	 * Gets the charge.
	 * @return the charge
	 */
	public String getCharge() {
		return charge;
	}

	/**
	 * Sets the charge.
	 * @param charge the new charge
	 */
	public void setCharge(String charge) {
		this.charge = charge;
	}

	/**
	 * Gets the montant ht.
	 * @return the montant ht
	 */
	public Float getMontantHT() {
		return montantHT;
	}

	/**
	 * Sets the montant ht.
	 * @param montantHT the new montant ht
	 */
	public void setMontantHT(Float montantHT) {
		this.montantHT = montantHT;
	}

	/**
	 * Gets the montant ttc.
	 * @return the montant ttc
	 */
	public Float getMontantTTC() {
		return montantTTC;
	}

	/**
	 * Sets the montant ttc.
	 * @param montantTTC the new montant ttc
	 */
	public void setMontantTTC(Float montantTTC) {
		this.montantTTC = montantTTC;
	}

	/**
	 * Gets the montant tva.
	 * @return the montant tva
	 */
	public Float getMontantTVA() {
		return montantTVA;
	}

	/**
	 * Sets the montant tva.
	 * @param montantTVA the new montant tva
	 */
	public void setMontantTVA(Float montantTVA) {
		this.montantTVA = montantTVA;
	}

	/**
	 * Gets the numero.
	 * @return the numero
	 */
	public Long getNumero() {
		return numero;
	}

	/**
	 * Sets the numero.
	 * @param numero the new numero
	 */
	public void setNumero(Long numero) {
		this.numero = numero;
	}

	/**
	 * Gets the prix.
	 * @return the prix
	 */
	public Float getPrix() {
		return prix;
	}

	/**
	 * Sets the prix.
	 * @param prix the new prix
	 */
	public void setPrix(Float prix) {
		this.prix = prix;
	}

	/**
	 * Gets the ref facture.
	 * @return the ref facture
	 */
	public FidelioFacture getRefFacture() {
		return refFacture;
	}

	/**
	 * Sets the ref facture.
	 * @param refFacture the new ref facture
	 */
	public void setRefFacture(FidelioFacture refFacture) {
		this.refFacture = refFacture;
	}

	/**
	 * Gets the id facture.
	 * @return the id facture
	 */
	public Long getIdFacture() {
		return id.getIdFacture();
	}

	/**
	 * Gets the numero ordre.
	 * @return the numero ordre
	 */
	public Long getNumeroOrdre() {
		return id.getNumeroOrdre();
	}
}
