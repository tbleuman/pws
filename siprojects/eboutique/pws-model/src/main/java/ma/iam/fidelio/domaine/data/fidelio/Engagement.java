package ma.iam.fidelio.domaine.data.fidelio;

import ma.iam.fidelio.domaine.data.commun.BaseObject;

/**
 * The Class Engagement.
 * @version 1.0.0
 * @author Atos Origin
 */
public class Engagement extends BaseObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5171446283423406667L;

	/** Prix Points. **/
	private Long prixPoints;

	/** L'id Article. **/
	private Integer articleId;

	/** Code Article. **/
	private String articleCode;

	/** Le Reliquat Hors Taxe. **/
	private Double reliquatHT;

	/** Libelle Article. **/
	private String articleLibelle;

	/** La Quantité. **/
	private String quantite;

	/** Les Points Acquis. **/
	private Integer ptsAquis;

	/** Les Points En Cours. **/
	private Integer ptsEnCours;
	
	/** Article En Service. **/
	private Boolean articleEnService;

	/** Le Type. **/
	private String type;
	
	private Integer dureeEnMois;
	
	private Integer modeEngagementId;
	
	private Integer categorieClientId;

	/**
	 * Retourner l'attribut articleEnService.
	 * @return : articleEnService
	 */
	public Boolean getArticleEnService() {
		return articleEnService;
	}

	/**
	 * Modifier l'attribut artcileEnService.
	 * @param : articleEnService
	 */
	public void setArticleEnService(Boolean articleEnService) {
		this.articleEnService = articleEnService;
	}


	/**
	 * Retourner l'attribut type.
	 * @return : type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Modifier l'attribut type.
	 * @param : type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Retourner l'attribut dureeEnMois.
	 * @return : dureeEnMois
	 */
	public Integer getDureeEnMois() {
		return dureeEnMois;
	}

	/**
	 * Modifier l'attribut dureeEnMois.
	 * @param : dureeEnMois
	 */
	public void setDureeEnMois(Integer dureeEnMois) {
		this.dureeEnMois = dureeEnMois;
	}

	/**
	 * Retourner l'attribut articleLibelle.
	 * @return : articleLibelle
	 */
	public String getArticleLibelle() {
		return articleLibelle;
	}

	/**
	 * Modifier l'attribut articleLibelle.
	 * @param : articleLibelle
	 */
	public void setArticleLibelle(String articleLibelle) {
		this.articleLibelle = articleLibelle;
	}

	/**
	 * Retourner l'attribut ptsAquis.
	 * @return : ptsAquis
	 */
	public Integer getPtsAquis() {
		return ptsAquis;
	}

	/**
	 * Modifier l'attribut ptsAquis.
	 * @param : ptsAquis
	 */
	public void setPtsAquis(Integer ptsAquis) {
		this.ptsAquis = ptsAquis;
	}

	/**
	 * Retourner l'attribut ptsEnCours.
	 * @return : ptsEnCours
	 */
	public Integer getPtsEnCours() {
		return ptsEnCours;
	}

	/**
	 * Modifier l'attribut ptsEnCours.
	 * @param : ptsEnCours
	 */
	public void setPtsEnCours(Integer ptsEnCours) {
		this.ptsEnCours = ptsEnCours;
	}

	/**
	 * Retourner l'attribut quantite.
	 * @return : quantite
	 */
	public String getQuantite() {
		return quantite;
	}

	/**
	 * Modifier l'attribut quantite.
	 * @param : quantite
	 */
	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

	/**
	 * Retourner l'attribut prixPoints.
	 * @return : prixPoints
	 */
	public Long getPrixPoints() {
		return prixPoints;
	}

	/**
	 * Modifier l'attribut prixPoints.
	 * @param : prixPoints
	 */
	public void setPrixPoints(Long prixPoints) {
		this.prixPoints = prixPoints;
	}

	/**
	 * Retourner l'attribut reliquatHT.
	 * @return : reliquatHT
	 */
	public Double getReliquatHT() {
		return reliquatHT;
	}

	/**
	 * Modifier l'attribut reliquatHT.
	 * @param : reliquatHT
	 */
	public void setReliquatHT(Double reliquatHT) {
		this.reliquatHT = reliquatHT;
	}

	/**
	 * Retourner l'attribut articleId.
	 * @return : articleId
	 */
	public Integer getArticleId() {
		return articleId;
	}

	/**
	 * Modifier l'attribut articleId.
	 * @param : articleId
	 */
	public void setArticleId(Integer num) {
		this.articleId = num;
	}

	/**
	 * Retourner l'attribut articleCode.
	 * @return : articleCode
	 */
	public String getArticleCode() {
		return articleCode;
	}

	/**
	 * Modifier l'attribut articleCode.
	 * @param : articleCode
	 */
	public void setArticleCode(String articleCode) {
		this.articleCode = articleCode;
	}

	/**
	 * @return the modeEngagementId
	 */
	public Integer getModeEngagementId() {
		return modeEngagementId;
	}

	/**
	 * @param modeEngagementId the modeEngagementId to set
	 */
	public void setModeEngagementId(Integer modeEngagementId) {
		this.modeEngagementId = modeEngagementId;
	}

	/**
	 * @return the categorieClientId
	 */
	public Integer getCategorieClientId() {
		return categorieClientId;
	}

	/**
	 * @param categorieClientId the categorieClientId to set
	 */
	public void setCategorieClientId(Integer categorieClientId) {
		this.categorieClientId = categorieClientId;
	}
}
