package ma.iam.fidelio.domaine.data.parametrage;

import ma.iam.fidelio.domaine.data.commun.BaseObject;


public class Qualite extends BaseObject {
	
	/** Le code. **/
	private String code;

	/** Libelle. **/
	private String libelle;

	/** L'utilisateur. **/
	private String utilisateur;

	/** En Service. **/
	private Boolean enService;

	/** La Référence Catégorie Client . **/
	private CategorieClient refCategorieClient;

	/**
	 * Modifier L'attribut code.
	 * @param : code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Modifier L'attribut libelle.
	 * @param : libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	/**
	 * Modifier L'attribut utilisateur.
	 * @param : utilisateur
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	/**
	 * Modifier L'attribut enService.
	 * @param : enService
	 */
	public void setEnService(Boolean enService) {
		this.enService = enService;
	}
	
	/**
	 * Modifier L'attribut refCategorieClient.
	 * @param : refCategorieClient
	 */
	public void setRefCategorieClient(CategorieClient refCategorieClient) {
		this.refCategorieClient = refCategorieClient;
	}
	
	/**
	 * Retourner la valeur de l'attribut code.
	 * @return : code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Retourner la valeur de l'attribut libelle.
	 * @return : libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Retourner la valeur de l'attribut utilisateur.
	 * @return : utilisateur
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * Retourner la valeur de l'attribut enService.
	 * @return : enService
	 */
	public Boolean getEnService() {
		return enService;
	}

	/**
	 * Retourner la valeur de l'attribut refCategorieClient.
	 * @return : refCategorieClient
	 */
	public CategorieClient getRefCategorieClient() {
		return refCategorieClient;
	}
}
