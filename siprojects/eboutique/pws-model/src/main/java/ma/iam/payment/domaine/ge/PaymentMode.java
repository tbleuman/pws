package ma.iam.payment.domaine.ge;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class PaymentMode.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public class PaymentMode implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4208554499009924642L;
	// Properties
	/** The Constant LABEL_ID. */
	public static final String LABEL_ID = "id";

	/** The Constant LABEL_SEMAOPPAYCAT. */
	public static final String LABEL_SEMAOPPAYCAT = "semaOpPayCat";

	/** The Constant LABEL_PTDESC. */
	public static final String LABEL_PTDESC = "ptDesc";

	/** The Constant LABEL_VALIDTILL. */
	public static final String LABEL_VALIDTILL = "validTill";

	/** The Constant LABEL_PRINTFLAG. */
	public static final String LABEL_PRINTFLAG = "printFlag";

	// Fields
	/** The id. */
	private Integer id;

	/** The pt desc. */
	private String ptDesc;

	/** The valid till. */
	private Date validTill;

	/** The print flag. */
	private String printFlag;

	// Getters & Setters
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the pt desc.
	 * 
	 * @return the pt desc
	 */
	public String getPtDesc() {
		return ptDesc;
	}

	/**
	 * Sets the pt desc.
	 * 
	 * @param ptDesc the new pt desc
	 */
	public void setPtDesc(String ptDesc) {
		this.ptDesc = ptDesc;
	}

	/**
	 * Gets the valid till.
	 * 
	 * @return the valid till
	 */
	public Date getValidTill() {
		return validTill != null ? (Date) validTill.clone() : null;
	}

	/**
	 * Sets the valid till.
	 * 
	 * @param validTill the new valid till
	 */
	public void setValidTill(Date validTill) {
		this.validTill = validTill != null ? (Date) validTill.clone() : null;
	}

	/**
	 * Gets the prints the flag.
	 * 
	 * @return the prints the flag
	 */
	public String getPrintFlag() {
		return printFlag;
	}

	/**
	 * Sets the prints the flag.
	 * 
	 * @param printFlag the new prints the flag
	 */
	public void setPrintFlag(String printFlag) {
		this.printFlag = printFlag;
	}
}
