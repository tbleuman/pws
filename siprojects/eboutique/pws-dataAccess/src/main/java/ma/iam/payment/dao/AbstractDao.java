package ma.iam.payment.dao;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.dao.PersistenceManager;
import ma.iam.common.Constantes;

/**
 * 
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public abstract class AbstractDao {
	
	/** Instance du LOGGER */
	protected static final Logger LOGGER = LogManager.getLogger(Constantes.TECHNICAL_PAYMENT_LOGGER_NAME);
	
	private PersistenceManager persistenceManager = null;
	
	/**
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @param bscsMobileManager
	 */
	public final void setPersistenceManager(PersistenceManager bscsMobileManager) {
		this.persistenceManager = bscsMobileManager;
	}
	
	/**
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @return
	 */
	protected final PersistenceManager getPersistenceManager() {
		return persistenceManager;
	}
	
	/**
	 * 
	 * @param sequenceKey
	 * @return
	 * @throws TechnicalException
	 */
	protected Long getAppSequence(String sequenceKey) throws TechnicalException {
		CallableStatement callableStatement = null;
		try {
			callableStatement = getPersistenceManager().getConnection().prepareCall(
					"{ CALL NEXTFREE.GETVALUE(?,?)}");
			callableStatement.registerOutParameter(2, Types.DOUBLE);
			callableStatement.setString(1, sequenceKey);
			callableStatement.executeQuery();
			return callableStatement.getLong(2);
		} catch (Exception e) {
			throw new TechnicalException(e.getMessage(), e);
		} finally {
			if (callableStatement != null) {
				try {
					callableStatement.close();
				} catch (SQLException e) {
					LOGGER.error(e.getMessage(), e);
				}
			}
		}
	}
}
