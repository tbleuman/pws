package ma.iam.fidelio.dao.client.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.common.Constantes;
import ma.iam.fidelio.dao.AbstractDao;
import ma.iam.fidelio.dao.client.ClientDao;
import ma.iam.fidelio.domaine.data.bscs.FidelioClient;
import ma.iam.fidelio.domaine.data.bscs.FidelioContrat;
import ma.iam.fidelio.domaine.data.bscs.FidelioFacture;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mutualisation.fwk.common.util.Utils;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;


public class ClientDaoImpl extends AbstractDao implements ClientDao {

	/**
	 * Méthode getAllNumAppels : Permet de récupérer les nuémros d'appel à
	 * partir d'un id client.
	 * 
	 * @param : idClient
	 * @return : List<String>
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public List<String> getAllNumAppels(Integer idClient)
			throws TechnicalException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerId", idClient);
		return getPersistenceManagerBSCS().getNamedQuery(
				Constantes.REQ_LISTE_NUMAPPEL_BY_CLIENT, params);
	}

	/**
	 * Méthode getContratByND : Permet de récupérer un objet de type
	 * FidelioContrat à partir d'un numéro d'appel.
	 * 
	 * @param : numeroAppel
	 * @return : Contrat
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public FidelioContrat getContratByND(String numeroAppel)
			throws TechnicalException {
		FidelioContrat fidelioContrat = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("numAppel", numeroAppel);

		List<FidelioContrat> list = getPersistenceManagerBSCS().getNamedQuery(
				Constantes.REQ_GET_CONTRAT_BY_NUM_APPEL, params);

		if (list != null && !list.isEmpty()) {
			fidelioContrat = list.get(0);
		}

		return fidelioContrat;
	}

	/**
	 * Méthode getListContratByNumAppel: Permet de récupérer la liste des
	 * contrats liés à un numéro d'appel à partir d'un numéro d'appel.
	 * 
	 * @param : numeroAppel
	 * @return : FidelioContract
	 * @throws : TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getListContratByNumAppel(String numeroAppel)
			throws TechnicalException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("numappel", numeroAppel);
		return getPersistenceManagerBSCS().getNamedQuery(
				Constantes.REQ_LISTE_CONTRAT_BY_NUMAPPEL, params);
	}

	/**
	 * Méthode getTotalImpaye : Permet de retourner le total impaye à partir
	 * d'un code client.
	 * 
	 * @param : codeClient
	 * @return : float
	 * @throws : TechnicalException
	 */
	public Float getTotalImpaye(Integer idClient) throws TechnicalException {
		Criteria criteria = getPersistenceManagerBSCS().createCriteria(
				FidelioFacture.class);
		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.MONTH, -2);
		Date dateFacture = Utils.stringToDate(Utils.dateToString(calendar
				.getTime()));
		criteria.add(Restrictions.ge(FidelioFacture.MONTANT_OUVERT,
				Float.valueOf(0)));
		criteria.add(Restrictions.le(FidelioFacture.DATE_FACTURE, dateFacture));
		criteria.add(Restrictions.eq(FidelioFacture.SATUT_FACTURE,
				Constantes.FACTURE_STATUT_IN));
		criteria.createCriteria(FidelioFacture.REF_CLIENT).add(
				Restrictions.eq(FidelioClient.ID, idClient));
		criteria.setProjection(Projections.sum(FidelioFacture.MONTANT_OUVERT));
		/*
		 * ftatbi change the type of result Float to Double
		 */
		// Float resultat = (Float) criteria.uniqueResult();
		// return (resultat != null ? resultat : 0F);

		Double resultat = (Double) criteria.uniqueResult();
		return (resultat != null ?  resultat.floatValue() : 0F);
	}

	/**
	 * (non javadoc) See @see
	 * ma.iam.fidelio.dao.commande.ClientDao#getClientByCode(java.lang.String).
	 * 
	 * @param codeClient
	 * @return
	 * @throws TechnicalException
	 */
	public FidelioClient getClientByCode(String codeClient)
			throws TechnicalException {
		FidelioClient fidelioClient = null;
		Criteria criteria = getPersistenceManagerBSCS().createCriteria(
				FidelioClient.class);
		criteria.add(Restrictions.eq("code", codeClient));
		List<FidelioClient> result = criteria.list();
		if (result != null && !result.isEmpty()) {
			fidelioClient = result.get(0);
		}
		return fidelioClient;
	}

}