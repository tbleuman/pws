package ma.iam.payment.dao.ge;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.payment.domaine.ge.Location;

/**
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public interface LocationDao {
	
	/**
	 * This method allows you to .</br></br>
	 * Created on Sprint #
	 * @param costId
	 * @return
	 * @throws TechnicalException
	 */
	public Location getLocationByCostId(Integer costId) throws TechnicalException;

}
