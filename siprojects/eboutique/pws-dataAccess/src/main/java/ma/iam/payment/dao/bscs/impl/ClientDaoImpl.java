package ma.iam.payment.dao.bscs.impl;

import java.util.ArrayList;
import java.util.List;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.bscs.ClientDao;
import ma.iam.payment.domaine.bscs.Client;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;


/**
 * The Class ClientDaoImpl.
 */
public class ClientDaoImpl extends AbstractDao implements ClientDao {
	

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.ClientDao#getClientById(java.lang.Long).
	 * @param idClient
	 * @return
	 * @throws TechnicalException
	 */
	public Client getClientById(Long idClient) throws TechnicalException {
		return (Client) getPersistenceManager().findById(Client.class, idClient, false);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.ClientDao#getClient(java.lang.String).
	 * @param codeClient
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Client getClient(String codeClient) throws TechnicalException {
		Client client = null;
		Criteria criteria = getPersistenceManager().createCriteria(Client.class);
		criteria.add(Restrictions.eq("code", codeClient));
		List<Client> result = criteria.list();
		if (result != null && !result.isEmpty()) {
			client = result.get(0);
		}
		return client;
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.ClientDao#updateBalanceClient(java.lang.Long, java.lang.Double).
	 * @param idClient
	 * @param montant
	 * @throws TechnicalException
	 */
	public void updateBalanceClient(Long idClient, Double montant) throws TechnicalException {
		List<Object> params = new ArrayList<Object>();
		params.add(montant);
		params.add(idClient);
		
		getPersistenceManager().executeUpdateQuery(
					"UPDATE CUSTOMER_ALL SET CSCURBALANCE = nvl(CSCURBALANCE,0) - ?, REC_VERSION = REC_VERSION + 1 WHERE CUSTOMER_ID = ?",
					params);
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.payment.dao.bscs.ClientDao#getResponsablePaiement(java.lang.String).
	 * @param codeClient
	 * @return
	 * @throws TechnicalException
	 */
	public Client getResponsablePaiement(String codeClient) throws TechnicalException {
		Client client = getClient(codeClient);

		// Premier Niveau
		if (client == null) {
			return null;
		}
		if (Constantes.FLAG_RESP_PAY.equals(client.getRespPaiement())) {
			return client;
		}

		// Deuxieme niveau
		client = client.getRefParent();
		if (client == null) {
			return null;
		}
		if (Constantes.FLAG_RESP_PAY.equals(client.getRespPaiement())) {
			return client;
		}

		// Troisieme niveau
		client = client.getRefParent();
		if (client == null) {
			return null;
		}
		if (Constantes.FLAG_RESP_PAY.equals(client.getRespPaiement())) {
			return client;
		}

		// Quatrieme niveau
		client = client.getRefParent();
		if (client == null) {
			return null;
		}
		if (Constantes.FLAG_RESP_PAY.equals(client.getRespPaiement())) {
			return client;
		}

		return client;
	}


	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.ClientDao#getClientByDN(java.lang.String).
	 * @param numAppel
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public Client getClientByDN(String numAppel) throws TechnicalException {
		Client client = null;
		Criteria criteria = getPersistenceManager().createCriteria(Client.class);
		criteria.add(Expression.sqlRestriction("EXISTS (SELECT 1 "+
           "FROM curr_co_msisdn ccm, contract_all ca, contr_services_cap csc, directory_number dn " +
           "WHERE this_.customer_id = ca.customer_id " +
             "AND ca.co_id = csc.co_id " +
             "AND csc.dn_id = dn.dn_id " +
             "AND dn.dn_num = '" + numAppel + "' " +
             "AND ccm.dn_num = dn.dn_num " +
             "AND ccm.customer_id = {alias}.customer_id " +
             "AND ccm.cs_deactiv_date IS NULL)"));
		List list = criteria.list();
		if (list != null && !list.isEmpty()) {
			client = (Client) list.get(0);
		}
		return client;
	}

}
