package ma.iam.payment.dao.bscs.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.common.Constantes;
import ma.iam.payment.dao.AbstractDao;
import ma.iam.payment.dao.bscs.FactureDao;
import ma.iam.payment.domaine.bscs.Facture;

/**
 * DAO Facture.
 * 
 * @author Atos
 * @version 1.0
 */
public class FactureDaoImpl extends AbstractDao implements FactureDao {
	

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.FactureDao#saveFacture(ma.iam.paiement.domaine.bscs.Facture).
	 * @param facture
	 * @return
	 * @throws TechnicalException
	 */
	public Facture saveFacture(Facture facture) throws TechnicalException {
		if (facture.getId() == null ) {
			facture.setId(getFactureSeq());
		}
		getPersistenceManager().save(facture);
		return facture;
	}

	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.FactureDao#getPeriodeFacture(java.lang.String).
	 * @return
	 * @throws TechnicalException
	 */
	@SuppressWarnings("unchecked")
	public String getPeriodeFacture() throws TechnicalException {
		String periode = null;
		Map<String, Object> params = new HashMap<String, Object>();
		List result = getPersistenceManager().getNamedQuery(Constantes.REQ_PERIODE_NEW_FACTURE, params);
		if (result != null && !result.isEmpty()) {
			periode = (String) result.get(0);
		}
		return periode;
	}


	/**
	 * (non javadoc)
	 * See @see ma.iam.paiement.dao.FactureDao#getFactureSeq().
	 * @return
	 * @throws TechnicalException
	 */
	public Long getFactureSeq() throws TechnicalException {

		/* for mobile system use nextfree to get next value */
		/* to avoid conflict with other systems using the   */
		/* same table */
		return super.getAppSequence(Constantes.ORDERHDR_ALL_SEQUENCE_KEY);
	
	}
	
}
