package ma.iam.services.catalogue.rs;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;


public class LignePointRS implements Serializable {


	/**
	 * This field serialVersionUID represents .
	 */
	private static final long serialVersionUID = 3572074374486793161L;
	
	private Integer pointsNu;
	private Integer points12Mois;
	private Integer points24Mois;
	
	
	
	
	/** Constructeur */
	public LignePointRS() {
		
	}




	/**
	 * @return the prixPointNu
	 */
	@XmlElement(nillable=true)
	public Integer getPointsNu() {
		return pointsNu;
	}




	/**
	 * @param prixPointNu the prixPointNu to set
	 */
	public void setPointsNu(Integer pointsNu) {
		this.pointsNu = pointsNu;
	}




	/**
	 * @return the point12Mois
	 */
	@XmlElement(nillable=true)
	public Integer getPoints12Mois() {
		return points12Mois;
	}




	/**
	 * @param point12Mois the point12Mois to set
	 */
	public void setPoints12Mois(Integer points12Mois) {
		this.points12Mois = points12Mois;
	}




	/**
	 * @return the point24Mois
	 */
	@XmlElement(nillable=true)
	public Integer getPoints24Mois() {
		return points24Mois;
	}




	/**
	 * @param point24Mois the point24Mois to set
	 */
	public void setPoints24Mois(Integer points24Mois) {
		this.points24Mois = points24Mois;
	}

}