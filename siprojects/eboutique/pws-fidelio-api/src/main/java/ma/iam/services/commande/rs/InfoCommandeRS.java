package ma.iam.services.commande.rs;

import java.io.Serializable;
import java.util.Date;

/**
 * @author A175029
 *
 */
public class InfoCommandeRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3596182973739633739L;

	/**
	 * N° de la commande E-Boutique
	 */
	private String numCommandeEB;
	
	/**
	 * Date livraison Fidelio
	 */
	private Date dateLivraison;

	public String getNumCommandeEB() {
		return numCommandeEB;
	}

	public void setNumCommandeEB(String numCommandeEB) {
		this.numCommandeEB = numCommandeEB;
	}

	public Date getDateLivraison() {
		return dateLivraison;
	}

	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}
	
}
