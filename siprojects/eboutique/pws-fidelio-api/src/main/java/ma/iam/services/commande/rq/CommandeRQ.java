package ma.iam.services.commande.rq;

import java.io.Serializable;

public class CommandeRQ implements Serializable {

	/** The Constant serialVersionID. */ 
	private static final long serialVersionUID = -1549909651068522006L;
	
	/** Numéro de la commande sur E-Boutique. */ 
	private String numCommande;
	
	/** Nombre De Point Acquis. */
	private Long pointsAcquis;
	
	/** Nombre De Point Encours. */
	private Long pointsEncours;
	
	/** Le complement d'argent. */
	private Double complement;
	
	/** Utilisateur E-Boutique. */ 
	private String utilisateur;
	
	/** Code de l'agence de livraison. */ 
	private Integer codeAgence;
	
	/** Numéro Appel. */
	private String numAppel;
	
	/** Id Article. */
	private Integer idArticle;
	
	/** Id Engagement. */
	private Integer idEngagement;
	
	/** Constructeur */
	public CommandeRQ() {
	}

	/**
	 * @return Retourner Le Numéro De La Commande.
	 */
	public String getNumCommande() {
		return numCommande;
	}

	/**
	 * @param numCommande Modifier Le Numéro De La Commande.
	 */
	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}

	/**
	 * @return Retourner L'utilisateur E-Boutique.
	 */
	public String getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param utilisateur Modifier L'utilisateur E-Boutique.
	 */
	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @return Retourner Le Code De L'agence De Livraison. 
	 */
	public Integer getCodeAgence() {
		return codeAgence;
	}

	/**
	 * @param codeAgence Modifier Le Code De L'agence De Livraison.
	 */
	public void setCodeAgence(Integer codeAgence) {
		this.codeAgence = codeAgence;
	}

	/**
	 * The getter method for the field pointsAcquis.
	 * @return the pointsAcquis.
	 */
	public Long getPointsAcquis() {
		return pointsAcquis;
	}

	/**
	 * The setter method for the field pointsAcquis.
	 * @param pointsAcquis the pointsAcquis to set.
	 */
	public void setPointsAcquis(Long pointsAcquis) {
		this.pointsAcquis = pointsAcquis;
	}

	/**
	 * The getter method for the field pointsEncours.
	 * @return the pointsEncours.
	 */
	public Long getPointsEncours() {
		return pointsEncours;
	}

	/**
	 * The setter method for the field pointsEncours.
	 * @param pointsEncours the pointsEncours to set.
	 */
	public void setPointsEncours(Long pointsEncours) {
		this.pointsEncours = pointsEncours;
	}

	/**
	 * The getter method for the field complement.
	 * @return the complement.
	 */
	public Double getComplement() {
		return complement;
	}

	/**
	 * The setter method for the field complement.
	 * @param complement the complement to set.
	 */
	public void setComplement(Double complement) {
		this.complement = complement;
	}

	/**
	 * @param numAppel the numAppel to set
	 */
	public void setNumAppel(String numAppel) {
		this.numAppel = numAppel;
	}

	/**
	 * @return the numAppel
	 */
	public String getNumAppel() {
		return numAppel;
	}

	/**
	 * @param idArticle the idArticle to set
	 */
	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}

	/**
	 * @return the idArticle
	 */
	public Integer getIdArticle() {
		return idArticle;
	}

	/**
	 * @param idEngagement the idEngagement to set
	 */
	public void setIdEngagement(Integer idEngagement) {
		this.idEngagement = idEngagement;
	}

	/**
	 * @return the idEngagement
	 */
	public Integer getIdEngagement() {
		return idEngagement;
	}
}
