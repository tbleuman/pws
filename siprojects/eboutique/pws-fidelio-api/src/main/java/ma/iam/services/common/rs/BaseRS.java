package ma.iam.services.common.rs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class BaseRS implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6949932616120253015L;
	
	private MessageRS[] messageErreur;

	/**
	 * @param messageErreur the messageErreur to set
	 */
	public void setMessageErreur(MessageRS[] messageErreur) {
		this.messageErreur = messageErreur;
	}

	/**
	 * @return the messageErreur
	 */
	public MessageRS[] getMessageErreur() {
		return messageErreur;
	}
	
	/**
	 * @param code
	 * @param message
	 */
	public void addErrorMessage(String code, String message) {
		List<MessageRS> listMessageRS = new ArrayList<MessageRS>();
		if (this.messageErreur != null && this.messageErreur.length != 0) {
			listMessageRS.addAll(Arrays.asList(this.messageErreur));
		}
		listMessageRS.add(new MessageRS(code, message));
		this.messageErreur = listMessageRS.toArray(new MessageRS[listMessageRS.size()]);
		listMessageRS = null;
	}
	

}
