package ma.iam.services.catalogue.rs;

import java.io.Serializable;

public class DetailComplementRS implements Serializable {

	/** The Constant serialVersionID. */
	private static final long serialVersionUID = -9179142666620723439L;
	
	/** Nombre De Point Acquis. */
	private Integer pointsAcquis;
	
	/** Nombre De Point Encours. */
	private Integer pointsEncours;
	
	/** Le complement d'argent. */
	private Float complement;
	

	/** Constructeur */
	public DetailComplementRS() {
	}

	/**
	 * @return Retourner Le Nombre De Points Acquis.
	 */
	public Integer getPointsAcquis() {
		return pointsAcquis;
	}

	/**
	 * @param pointsAcquis Modifier Le Nombre De Points Acquis.
	 */
	public void setPointsAcquis(Integer pointsAcquis) {
		this.pointsAcquis = pointsAcquis;
	}

	/**
	 * @return Retourner Le Nombre De Points Encours.
	 */
	public Integer getPointsEncours() {
		return pointsEncours;
	}

	/**
	 * @param pointsEncours Modifier Le Nombre De Points Encours.
	 */
	public void setPointsEncours(Integer pointsEncours) {
		this.pointsEncours = pointsEncours;
	}

	/**
	 * @return Retourner Le Complement d'argent.
	 */
	public Float getComplement() {
		return complement;
	}

	/**
	 * @param complement Modifier Le Complement d'argent.
	 */
	public void setComplement(Float complement) {
		this.complement = complement;
	}
}