package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.param.criteria.DansVoieCriteria;
import ma.iam.wiam.param.criteria.VoieCriteria;
import ma.iam.wiam.params.DansVoieParams;
import ma.iam.wiam.vo.DansVoieBean;
import ma.iam.wiam.ws.neto.model.PDansVoie;
/**Begin feature/wsTools*/
public class DansVoieMapper extends GenericMapper<PDansVoie, ReferentialBean>{

	public static final DansVoieCriteria mapParams2Criteria(DansVoieParams param) {
		DansVoieCriteria criteria =null;
		if (param != null) {
			criteria = new DansVoieCriteria();
			criteria.setCodeCommune(param.getCodeCommune());
			criteria.setCodeQuartier(param.getCodeQuartier());
			criteria.setCodeVoie(param.getCodeVoie());
		}
		return criteria;
	}
	
	public static final DansVoieBean mapPersist2Vo(PDansVoie persist) {
		DansVoieBean bean =null;
		if(persist != null) {
			bean = new DansVoieBean();
			bean.setCodeCommune(persist.getId().getCcom());
			bean.setCodeQuartier(persist.getId().getCquartier());
			bean.setCodeVoie(persist.getId().getCvoie());
			bean.setCodeDansVoie(persist.getId().getCdansVoie());
		}
		return bean;
	}
	
	public static final List<DansVoieBean> mapPersist2VoList(List<PDansVoie> list){
		List<DansVoieBean> listVos= new ArrayList<DansVoieBean>();
		if (CollectionUtils.isNotEmpty(list)) {
			for(PDansVoie persist : list) {
				listVos.add(mapPersist2Vo(persist));
			}
		}
		return listVos;
	}
	
	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(PDansVoie persist) {
		ReferentialBean bean = null;
		if(persist != null) {
			bean = new ReferentialBean();
			bean.setCode(persist.getId().getCdansVoie());
		}
		return bean;
	}

	@Override
	public PDansVoie mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		// TODO Auto-generated method stub
		return null;
	}
/**End feature/wsTools*/
}
