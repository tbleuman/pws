package ma.iam.wiam.params;
/**Begin feature/wsTools*/
public class DansVoieParams {

	private String codeCommune;
	private String codeQuartier;
	private String codeVoie;
	public String getCodeCommune() {
		return codeCommune;
	}
	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}
	public String getCodeQuartier() {
		return codeQuartier;
	}
	public void setCodeQuartier(String codeQuartier) {
		this.codeQuartier = codeQuartier;
	}
	public String getCodeVoie() {
		return codeVoie;
	}
	public void setCodeVoie(String codeVoie) {
		this.codeVoie = codeVoie;
	}
	
	
	/**End feature/wsTools*/
}
