package ma.iam.wiam.mapper;

import com.netonomy.blm.interfaces.util.CountryIF;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.ws.neto.model.Country;
/**Begin feature/wsTools**/
public class CountrysMapper extends GenericMapper<CountryIF,ReferentialBean>{

	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(CountryIF businesModel) {
		ReferentialBean bean = null;
		if(businesModel!=null) {
			bean = new ReferentialBean();
			bean.setCode(String.valueOf(businesModel.getIdentifier()));
			bean.setLibelle(businesModel.getName());
		}
		return bean;
	}

	@Override
	public CountryIF mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		// TODO Auto-generated method stub
		return null;
	}
/**End feature/wsTools**/
}
