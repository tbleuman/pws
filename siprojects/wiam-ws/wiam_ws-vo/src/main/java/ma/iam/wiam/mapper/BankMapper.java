package ma.iam.wiam.mapper;

import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.ws.neto.model.Bank;

public class BankMapper extends GenericMapper<Bank, ReferentialBean> {
	
	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(Bank bank) {
		ReferentialBean referentialBean = null;

		if (bank != null) {
			referentialBean = new ReferentialBean();
			referentialBean.setCode(bank.getCode());
			referentialBean.setLibelle(bank.getName());
		}
		return referentialBean;
	}

	@Override
	public Bank mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}
}
