package ma.iam.wiam.mapper;

import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.neto.bean.ReferentialBean;


public class ValueChoiceItemMapper extends GenericMapper<ValueChoiceItemIF, ReferentialBean>{
	
	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(ValueChoiceItemIF valueChoiceItem) {
		ReferentialBean referentialBean = null;

		if (valueChoiceItem == null)
			return referentialBean;

		referentialBean = new ReferentialBean();
		referentialBean.setCode(valueChoiceItem.getChoiceItem().getIdentifier().toString());
		referentialBean.setLibelle(valueChoiceItem.getChoiceItem().getDisplayValue());

		return referentialBean;
	}

	@Override
	public ValueChoiceItemIF mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}
}
