package ma.iam.wiam.mapper;

import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.param.criteria.BankAgencyCriteria;
import ma.iam.wiam.params.AgenceBancaireParams;
import ma.iam.wiam.ws.neto.model.BankAgency;

public class BankAgencyMapper extends GenericMapper<BankAgency, ReferentialBean> {
	
	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(BankAgency bankAgency) {
		ReferentialBean referentialBean = null;
		
		if (bankAgency != null) {
			referentialBean = new ReferentialBean();
			referentialBean.setCode(bankAgency.getCode());
			referentialBean.setLibelle(bankAgency.getName());
		}
		return referentialBean;
	}
	
	public static final BankAgencyCriteria mapParamsToCriteria(AgenceBancaireParams agenceBancaireParams) {
		BankAgencyCriteria bankAgencyCriteria = null;
		
		if (agenceBancaireParams != null) {
			bankAgencyCriteria = new BankAgencyCriteria();
			bankAgencyCriteria.setBankCode(agenceBancaireParams.getCodeBanque());
		}
		return bankAgencyCriteria;
	}

	@Override
	public BankAgency mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}
}
