package ma.iam.wiam.mapper;
/**Begin feature/wsTools*/

import ma.iam.wiam.vo.CcuBean;
import ma.iam.wiam.ws.neto.model.Ccu;

public class CcuMapper {

	public static final CcuBean mapPersist2Vo (Ccu persist) {
		
		CcuBean bean = null;
		if(persist != null) {
			bean = new CcuBean();
			bean.setCode(persist.getCode());
			bean.setBusinessName(persist.getBusinessName());
			bean.setCreationDate(persist.getCreationDate());
			bean.setCategory(persist.getCategory());
		}
		return bean;
	}
	
	
	
	
/**End feature/wsTools*/	
}
