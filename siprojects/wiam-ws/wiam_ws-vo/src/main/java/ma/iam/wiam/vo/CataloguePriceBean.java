package ma.iam.wiam.vo;

import java.math.BigDecimal;
import java.util.Date;

/**Begin feature/wsTools*/
public class CataloguePriceBean {

	private String modelId;
	private String rateplanCode;
	private Date dateDebut;
	private Date dateFin;
	private BigDecimal prixUnitaire;
	private BigDecimal minQte;
	private BigDecimal maxQte;
	private String anetoId;
	private String libelleAneto;
	private String description;
	private Integer venteTypeId;
	private String venteTypeName;
	private String codeCommercialType;
	private String libelleCommercialType;
	
	public String getCodeCommercialType() {
		return codeCommercialType;
	}
	public void setCodeCommercialType(String codeCommercialType) {
		this.codeCommercialType = codeCommercialType;
	}
	public String getLibelleCommercialType() {
		return libelleCommercialType;
	}
	public void setLibelleCommercialType(String libelleCommercialType) {
		this.libelleCommercialType = libelleCommercialType;
	}
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getRateplanCode() {
		return rateplanCode;
	}
	public void setRateplanCode(String rateplanCode) {
		this.rateplanCode = rateplanCode;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public BigDecimal getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(BigDecimal prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public BigDecimal getMinQte() {
		return minQte;
	}
	public void setMinQte(BigDecimal minQte) {
		this.minQte = minQte;
	}
	public BigDecimal getMaxQte() {
		return maxQte;
	}
	public void setMaxQte(BigDecimal maxQte) {
		this.maxQte = maxQte;
	}
	public String getAnetoId() {
		return anetoId;
	}
	public void setAnetoId(String anetoId) {
		this.anetoId = anetoId;
	}
	public String getLibelleAneto() {
		return libelleAneto;
	}
	public void setLibelleAneto(String libelleAneto) {
		this.libelleAneto = libelleAneto;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getVenteTypeId() {
		return venteTypeId;
	}
	public void setVenteTypeId(Integer venteTypeId) {
		this.venteTypeId = venteTypeId;
	}
	public String getVenteTypeName() {
		return venteTypeName;
	}
	public void setVenteTypeName(String venteTypeName) {
		this.venteTypeName = venteTypeName;
	}
	
/**End feature/wsTools*/	
}
