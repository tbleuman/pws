package ma.iam.wiam.vo;
/**Begin feature/wsTools*/
public class DansVoieBean {
	
	//fields
	private String codeCommune;
	private String codeQuartier;
	private String codeVoie;
	private String codeDansVoie;
	
	
	public String getCodeCommune() {
		return codeCommune;
	}
	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}
	public String getCodeQuartier() {
		return codeQuartier;
	}
	public void setCodeQuartier(String codeQuartier) {
		this.codeQuartier = codeQuartier;
	}
	public String getCodeVoie() {
		return codeVoie;
	}
	public void setCodeVoie(String codeVoie) {
		this.codeVoie = codeVoie;
	}
	public String getCodeDansVoie() {
		return codeDansVoie;
	}
	public void setCodeDansVoie(String codeDansVoie) {
		this.codeDansVoie = codeDansVoie;
	}
	
		
/**End feature/wsTools*/
}
