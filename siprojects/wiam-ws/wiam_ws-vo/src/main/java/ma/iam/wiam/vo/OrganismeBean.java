package ma.iam.wiam.vo;
/**Begin feature/wsTools*/
public class OrganismeBean {
	
	private Long codeOrganisme;
	private String libelleOrganisme;
	private String crgptcorg;
	
	public Long getCodeOrganisme() {
		return codeOrganisme;
	}
	public void setCodeOrganisme(Long codeOrganisme) {
		this.codeOrganisme = codeOrganisme;
	}
	public String getLibelleOrganisme() {
		return libelleOrganisme;
	}
	public void setLibelleOrganisme(String libelleOrganisme) {
		this.libelleOrganisme = libelleOrganisme;
	}
	public String getCrgptcorg() {
		return crgptcorg;
	}
	public void setCrgptcorg(String crgptcorg) {
		this.crgptcorg = crgptcorg;
	}
	
	
	
/**End feature/wsTools*/
}
