package ma.iam.wiam.vo;
/**Begin feature/wsTools*/
public class VendeurBean {

	private String loginId;
	private String loginName;
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
/**End feature/wsTools*/	
}
