package ma.iam.wiam.vo;

import java.util.Date;

/**Begin feature/wsTools*/
public class CcuBean {

	private String code;
	private String businessName;
	private Date creationDate; 
	private Integer category;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	
	
	
/**End feature/wsTools*/	
}
