package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.vo.OrganismeBean;
import ma.iam.wiam.ws.neto.model.Organisme;
/**Begin feature/wsTools*/
public class OrganismeMapper {
	
	public static final OrganismeBean mapPersist2Vo(Organisme persist) {
		OrganismeBean bean = null;
		if(persist!= null) {
			bean = new OrganismeBean();
			bean.setCodeOrganisme(persist.getCodeOrganisme());
			bean.setLibelleOrganisme(persist.getLibelleOrganisme());
			bean.setCrgptcorg(persist.getCrgptcorg());
		}
		return bean;
	}
	
	public static final List<OrganismeBean> mapPersit2VoList(List<Organisme> list){
		
		List<OrganismeBean> listVos = new ArrayList<OrganismeBean>();
		if (CollectionUtils.isNotEmpty(list)) {
			for(Organisme persist : list) {
				listVos.add(mapPersist2Vo(persist));
			}
		}
		return listVos;
	}
/**End feature/wsTools*/
}
