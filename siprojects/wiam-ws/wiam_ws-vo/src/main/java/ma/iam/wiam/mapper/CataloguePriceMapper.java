package ma.iam.wiam.mapper;
/**Begin feature/wsTools*/

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.vo.CataloguePriceBean;
import ma.iam.wiam.ws.neto.model.CataloguePrice;

public class CataloguePriceMapper {

	public static final CataloguePriceBean mapPersist2Vo(CataloguePrice persist) {
		
		CataloguePriceBean bean = null;
		if(persist != null) {
			bean = new CataloguePriceBean();
			bean.setModelId(persist.getModelId());
			bean.setRateplanCode(persist.getRateplanCode());
			bean.setDateDebut(persist.getDateDebut());
			bean.setDateFin(persist.getDateFin());
			bean.setPrixUnitaire(persist.getPrixUnitaire());
			bean.setMinQte(persist.getMinQte());
			bean.setMaxQte(persist.getMaxQte());
			bean.setAnetoId(persist.getAnetoId());
			bean.setLibelleAneto(persist.getLibelleAneto());
			bean.setDescription(persist.getDescription());
			bean.setVenteTypeId(persist.getVenteTypeId());
			bean.setVenteTypeName(persist.getVenteTypeName());
		}
		
		return bean;
		
	}
	
	public static final List<CataloguePriceBean> mapPersit2VoList (List<CataloguePrice> list){
		List<CataloguePriceBean> listVos = new ArrayList<CataloguePriceBean>();
		if(CollectionUtils.isNotEmpty(list)) {
			for(CataloguePrice persist : list) {
				listVos.add(mapPersist2Vo(persist));
			}
		}
		return listVos;
	}
	
	
	
	
	
/**End feature/wsTools*/	
}
