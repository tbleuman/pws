package ma.iam.wiam.vo;

public class BillingContact {

	private String businessName;
	private String lastName;
	private String firstName;
	private String tel;
	private String gsm;
	private String fax;

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getGsm() {
		return gsm;
	}

	public void setGsm(String gsm) {
		this.gsm = gsm;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Override
	public String toString() {
		return "BilligContact [businessName=" + businessName + ", lastName="
				+ lastName + ", firstName=" + firstName + ", tel=" + tel
				+ ", gsm=" + gsm + ", fax=" + fax + "]";
	}

}
