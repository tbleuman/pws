package ma.iam.wiam.mapper;
/**Begin feature/wsTools*/

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.vo.CommOfferBean;
import ma.iam.wiam.vo.OrganismeBean;
import ma.iam.wiam.ws.neto.model.CommofferInfoValue;
import ma.iam.wiam.ws.neto.model.Organisme;

public class CommOfferMapper {

	public static final CommOfferBean mapPersist2Vo(CommofferInfoValue persist) {
		CommOfferBean bean = null;
		if(persist !=null) {
			bean= new CommOfferBean();
			bean.setIdList(persist.getId_list());
			bean.setIdValue(persist.getId_value());
		}
		return bean;
	}
	
	public static final List<CommOfferBean> mapPersit2VoList(List<CommofferInfoValue> list){
		
		List<CommOfferBean> listVos = new ArrayList<CommOfferBean>();
		if(CollectionUtils.isNotEmpty(list)) {
			for(CommofferInfoValue persist : list) {
				listVos.add(mapPersist2Vo(persist));
			}
		}
		return listVos;
	}
/**End feature/wsTools*/	
}
