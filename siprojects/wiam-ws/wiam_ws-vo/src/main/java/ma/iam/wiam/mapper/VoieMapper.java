package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.param.criteria.VoieCriteria;
import ma.iam.wiam.params.VoieParams;
import ma.iam.wiam.vo.VoieBean;
import ma.iam.wiam.ws.neto.model.PVoie;

public class VoieMapper extends GenericMapper<PVoie, ReferentialBean> {

	public static final VoieCriteria mapParams2Criteria(VoieParams param) {
		VoieCriteria criteria = null;
		if (param != null) {
			criteria = new VoieCriteria();
			criteria.setCodeCommune(param.getCodeCommune());
			criteria.setCodeQuartier(param.getCodeQuartier());
		}
		return criteria;
	}

	public static final VoieBean mapPersist2Vo(PVoie persist) {
		VoieBean bean = null;
		if (persist != null) {
			bean = new VoieBean();

			bean.setLabelVoie(persist.getLvoie());
			bean.setCodeCommune(persist.getId().getCcom());
			bean.setCodeVoie(persist.getId().getCvoie());
			bean.setCodeQuartier(persist.getId().getCquartier());
		}
		return bean;
	}

	public static final List<VoieBean> mapPersist2VoList(List<PVoie> list) {
		List<VoieBean> listVos = new ArrayList<VoieBean>();

		if (CollectionUtils.isNotEmpty(list)) {
			for (PVoie persit : list) {
				listVos.add(mapPersist2Vo(persit));
			}
		}

		return listVos;
	}

	@Override
	public ReferentialBean mapBusinesModelToBusinesBean(PVoie persist) {
		ReferentialBean bean = null;
		if (persist != null) {
			bean = new ReferentialBean();

			bean.setLibelle(persist.getLvoie());
			bean.setCode(persist.getId().getCvoie());
		}
		return bean;
	}

	@Override
	public PVoie mapBusinesBeanToBusinesModel(ReferentialBean businesModel) {
		return null;
	}
}
