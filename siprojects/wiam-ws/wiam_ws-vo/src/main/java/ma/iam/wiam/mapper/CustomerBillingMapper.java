package ma.iam.wiam.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.iam.wiam.neto.bean.AdresseBean;
import ma.iam.wiam.neto.bean.BillingContactDetailBean;
import ma.iam.wiam.neto.bean.ContactDeFacturationBean;
import ma.iam.wiam.neto.bean.MethodeDePaiementBean;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.vo.BillingAddress;
import ma.iam.wiam.vo.BillingContact;
import ma.iam.wiam.vo.BillingInfoDiverse;
import ma.iam.wiam.vo.AbstractCustomerBillingInfoBean;
import ma.iam.wiam.vo.CustomerBillingInfoResponseBean;
import ma.iam.wiam.vo.PayementMethodInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * 
 * @author s.hroumti
 *
 */
public class CustomerBillingMapper {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CustomerBillingMapper.class);

	public static List<? extends AbstractCustomerBillingInfoBean> mapBusinessBean2VOCollection(
			List<ma.iam.wiam.neto.bean.CustomerBillingInfoBean> billingInfoList) {
		List<AbstractCustomerBillingInfoBean> list = null;
		if (billingInfoList != null && !billingInfoList.isEmpty()) {
			list = new ArrayList<AbstractCustomerBillingInfoBean>();
			for (ma.iam.wiam.neto.bean.CustomerBillingInfoBean customerBillingInfoBean : billingInfoList) {
				list.add(mapBusinessBean2VO(customerBillingInfoBean));
			}
		}

		return list;
	}

	private static AbstractCustomerBillingInfoBean mapBusinessBean2VO(
			ma.iam.wiam.neto.bean.CustomerBillingInfoBean customerBillingInfoBean) {
		AbstractCustomerBillingInfoBean result = new CustomerBillingInfoResponseBean();
		result.setNcli(customerBillingInfoBean.getNcli());
		if (result instanceof CustomerBillingInfoResponseBean) {
			((CustomerBillingInfoResponseBean) result)
					.setCategorieSs(customerBillingInfoBean.getCategorieSs());
		}
		if (customerBillingInfoBean.getBillingContactDetailBean() != null) {
			BillingInfoDiverse billingInfoDiverseBean = new BillingInfoDiverse();
			copyPropertiesFromBusinessBeanToVO(
					customerBillingInfoBean.getBillingContactDetailBean(),
					billingInfoDiverseBean);
			result.setBillingInfoDiverse(billingInfoDiverseBean);

			PayementMethodInfo payementMethodInfoBean = new PayementMethodInfo();
			copyPropertiesFromBusinessBeanToVO(
					customerBillingInfoBean.getBillingContactDetailBean(),
					payementMethodInfoBean);
			result.setPayementMethodInfo(payementMethodInfoBean);
			ContactDeFacturationBean contactDeFacturationBean = customerBillingInfoBean
					.getBillingContactDetailBean()
					.getContactDeFacturationBean();
			if (contactDeFacturationBean != null) {
				BillingContact billingContact = mapBusinessBeanToVO(customerBillingInfoBean);
				result.setBillingContact(billingContact);

				if (contactDeFacturationBean.getAdresse() != null) {
					result.setBillingAddress(mapBusinessBean2VO(contactDeFacturationBean
							.getAdresse()));
				}
			}
		}
		return result;
	}

	private static void copyPropertiesFromBusinessBeanToVO(
			BillingContactDetailBean billingContactDetailBean,
			BillingInfoDiverse billingInfoDiverseBean) {
		ContactDeFacturationBean contactDeFacturationBean = billingContactDetailBean
				.getContactDeFacturationBean();
		if (contactDeFacturationBean != null) {
			billingInfoDiverseBean.setConfidentialCode(contactDeFacturationBean
					.getCodeConfidentiel());

			billingInfoDiverseBean.setBillingCycleCode(contactDeFacturationBean
					.getCycleDeFacturation().getCode());
			if (contactDeFacturationBean.getOrganisation() != null) {
				billingInfoDiverseBean
						.setOrganisationCode(contactDeFacturationBean
								.getOrganisation().getCode());
			}
			if (contactDeFacturationBean.getSupportDeFacturation() != null) {
				billingInfoDiverseBean
						.setBillingSupportCode(contactDeFacturationBean
								.getSupportDeFacturation().getCode());
			}

			if (contactDeFacturationBean.getLangue() != null) {
				billingInfoDiverseBean.setLanguageCode(contactDeFacturationBean
						.getLangue().getCode());
			}
			if (contactDeFacturationBean.getTermeDePaiment() != null) {
				billingInfoDiverseBean
						.setPaymentTermCode(contactDeFacturationBean
								.getTermeDePaiment().getCode());
			}
			billingInfoDiverseBean.setExempted(contactDeFacturationBean
					.getExempte());
			billingInfoDiverseBean.setGrouperFacture(contactDeFacturationBean
					.getGrouperFacture());
		}
	}

	private static void copyPropertiesFromBusinessBeanToVO(
			BillingContactDetailBean billingContactDetailBean,
			PayementMethodInfo payementMethodInfoBean) {
		payementMethodInfoBean.setPaymentMethodCode(billingContactDetailBean
				.getPaymentMethodCode());
		MethodeDePaiementBean methodeDePaiementBean = billingContactDetailBean
				.getMethodeDePaiementBean();
		if (methodeDePaiementBean != null) {
			payementMethodInfoBean.setAccountHolder(methodeDePaiementBean
					.getTitulaireDeCompte());
			payementMethodInfoBean.setAccountNumber(methodeDePaiementBean
					.getNumeroDeCompte());
			if (methodeDePaiementBean.getAgenceBancaire() != null) {
				payementMethodInfoBean.setBankAgencyCode(methodeDePaiementBean
						.getAgenceBancaire().getCode());
			}
			if (methodeDePaiementBean.getBanque() != null) {
				payementMethodInfoBean.setBankCode(methodeDePaiementBean
						.getBanque().getCode());
			}
			payementMethodInfoBean.setRib(methodeDePaiementBean.getRib());
			if (!StringUtils.isEmpty(methodeDePaiementBean
					.getNumeroAutorisation())) {
				try {
					payementMethodInfoBean.setAutorizationNumber(Integer
							.parseInt(methodeDePaiementBean
									.getNumeroAutorisation()));
				} catch (NumberFormatException e) {
					LOGGER.debug(
							"The value : "
									+ methodeDePaiementBean
											.getNumeroAutorisation()
									+ " of autorization number is not valid ",
							e);
				}
			}
		}
	}

	private static BillingAddress mapBusinessBean2VO(AdresseBean address) {

		BillingAddress billingAddress = new BillingAddress();
		billingAddress.setBatiment(address.getBatiment());
		billingAddress.setBoitePostale(address.getBoitePostale());
		billingAddress.setCodePostal(address.getCodePostale());
		if (address.getCommune() != null) {
			billingAddress.setCommune(address.getCommune().getCode());
		}
		billingAddress.setCommuneEtranger(address.getCommuneEtrangere());
		billingAddress.setComplement(address.getComplementAdresse());
		billingAddress.setEscalier(address.getEscalier());
		billingAddress.setEtage(address.getEtage());
		billingAddress.setNumVoie(address.getNumeroVoie());
		if (address.getPays() != null) {
			billingAddress.setPays(address.getPays().getCode());
		}
		billingAddress.setPorte(address.getPorte());
		if (address.getQuartier() != null) {
			billingAddress.setQuartier(address.getQuartier().getCode());
		}
		billingAddress.setQuartierNonCodifiee(address.getQuartierNonCodifie());
		billingAddress.setTypeDestribution(address.getTypeDestribution()
				.getCode());
		// if (address.getTypeDestribution() != null) {
		// billingAddress.setTypeDestribution(ObjectIdUtil
		// .convertLongIdToLong(address.getTypeDestribution()
		// .getCode()));
		//
		// }
		if (address.getVoie() != null) {
			billingAddress.setVoie(address.getVoie().getCode());
		}
		billingAddress.setVoieNonCodifie(address.getVoieNonCodifie());
		return billingAddress;
	}

	private static BillingContact mapBusinessBeanToVO(
			ma.iam.wiam.neto.bean.CustomerBillingInfoBean customerBillingInfoBean) {
		BillingContact billingContact = new BillingContact();
		ContactDeFacturationBean contactDeFacturationBean = customerBillingInfoBean
				.getBillingContactDetailBean().getContactDeFacturationBean();
		if (contactDeFacturationBean != null) {
			billingContact.setBusinessName(contactDeFacturationBean
					.getRaisonSocial());
			billingContact.setFirstName(contactDeFacturationBean.getPrenom());
			billingContact.setLastName(contactDeFacturationBean.getNom());
			billingContact
					.setGsm(contactDeFacturationBean.getTelephoneMobile());
			billingContact.setTel(contactDeFacturationBean.getTelephoneFix());
			billingContact.setFax(contactDeFacturationBean.getFax());
		}
		return billingContact;
	}

	public static ma.iam.wiam.neto.bean.CustomerBillingInfoBean mapVO2BusinessBean(
			AbstractCustomerBillingInfoBean customerBillingInfoBean) {
		ma.iam.wiam.neto.bean.CustomerBillingInfoBean result = new ma.iam.wiam.neto.bean.CustomerBillingInfoBean();
		result.setNcli(customerBillingInfoBean.getNcli());
		if (customerBillingInfoBean instanceof CustomerBillingInfoResponseBean) {
			result.setCategorieSs(((CustomerBillingInfoResponseBean) customerBillingInfoBean)
					.getCategorieSs());
		}
		if (customerBillingInfoBean.getBillingInfoDiverse() != null
				|| customerBillingInfoBean.getPayementMethodInfo() != null
				|| customerBillingInfoBean.getBillingAddress() != null) {
			BillingContactDetailBean billingContactDetailBean = new BillingContactDetailBean();
			if (customerBillingInfoBean.getBillingInfoDiverse() != null) {
				copyPropertiesFromVOToBusinessBean(
						customerBillingInfoBean.getBillingInfoDiverse(),
						billingContactDetailBean);
			}
			copyPropertiesFromVOToBusinessBean(
					customerBillingInfoBean.getPayementMethodInfo(),
					billingContactDetailBean);

			result.setBillingContactDetailBean(billingContactDetailBean);
			if (customerBillingInfoBean.getBillingAddress() != null
					|| customerBillingInfoBean.getBillingContact() != null) {
				if (billingContactDetailBean.getContactDeFacturationBean() == null) {
					billingContactDetailBean
							.setContactDeFacturationBean(new ContactDeFacturationBean());
				}
				copyPropertiesFromVOToBusinessBean(
						customerBillingInfoBean.getBillingContact(),
						billingContactDetailBean.getContactDeFacturationBean());
				copyPropertiesFromVOToBusinessBean(
						customerBillingInfoBean.getBillingAddress(),
						billingContactDetailBean.getContactDeFacturationBean());
			}
		}
		return result;
	}

	private static void copyPropertiesFromVOToBusinessBean(
			PayementMethodInfo payementMethodInfo,
			BillingContactDetailBean billingContactDetailBean) {

		if (payementMethodInfo != null) {
			MethodeDePaiementBean methodeDePaiementBean = new MethodeDePaiementBean();
			if (payementMethodInfo != null) {
				methodeDePaiementBean.setTitulaireDeCompte(payementMethodInfo
						.getAccountHolder());
				methodeDePaiementBean.setNumeroDeCompte(payementMethodInfo
						.getAccountNumber());
				if (payementMethodInfo.getBankAgencyCode() != null) {
					methodeDePaiementBean
							.setAgenceBancaire(new ReferentialBean(
									payementMethodInfo.getBankAgencyCode()));
				}
				if (payementMethodInfo.getBankCode() != null) {
					methodeDePaiementBean.setBanque(new ReferentialBean(
							payementMethodInfo.getBankCode()));
				}
				methodeDePaiementBean.setRib(payementMethodInfo.getRib());
				if (payementMethodInfo.getAutorizationNumber() != null) {
					methodeDePaiementBean
							.setNumeroAutorisation(payementMethodInfo
									.getAutorizationNumber().toString());
				}
				if (payementMethodInfo.getPaymentMethodCode() != null) {
					methodeDePaiementBean.setCode(payementMethodInfo
							.getPaymentMethodCode().toString());
				}

				billingContactDetailBean
						.setPaymentMethodCode(payementMethodInfo
								.getPaymentMethodCode());
			}

			billingContactDetailBean
					.setMethodeDePaiementBean(methodeDePaiementBean);
		}

	}

	private static void copyPropertiesFromVOToBusinessBean(
			BillingInfoDiverse billingInfoDiverse,
			BillingContactDetailBean billingContactDetailBean) {

		ContactDeFacturationBean contactDeFacturationBean = new ContactDeFacturationBean();
		if (billingInfoDiverse.getBillingCycleCode() != null) {
			contactDeFacturationBean.setCycleDeFacturation(new ReferentialBean(
					billingInfoDiverse.getBillingCycleCode()));
		}
		if (billingInfoDiverse.getBillingSupportCode() != null) {
			contactDeFacturationBean
					.setSupportDeFacturation(new ReferentialBean(
							billingInfoDiverse.getBillingSupportCode()));
		}
		if (billingInfoDiverse.getOrganisationCode() != null) {
			contactDeFacturationBean.setOrganisation(new ReferentialBean(
					billingInfoDiverse.getOrganisationCode()));
		}
		if (billingInfoDiverse.getLanguageCode() != null) {
			contactDeFacturationBean.setLangue(new ReferentialBean(
					billingInfoDiverse.getLanguageCode()));
		}
		if (billingInfoDiverse.getPaymentTermCode() != null) {
			contactDeFacturationBean.setTermeDePaiment(new ReferentialBean(
					billingInfoDiverse.getPaymentTermCode()));
		}
		contactDeFacturationBean.setExempte(billingInfoDiverse.getExempted());
		contactDeFacturationBean.setGrouperFacture(billingInfoDiverse
				.getGrouperFacture());
		contactDeFacturationBean.setCodeConfidentiel(billingInfoDiverse
				.getConfidentialCode());
		billingContactDetailBean
				.setContactDeFacturationBean(contactDeFacturationBean);

	}

	private static void copyPropertiesFromVOToBusinessBean(
			BillingAddress billingAddress,
			ContactDeFacturationBean contactDeFacturationBean) {
		AdresseBean adresse = new AdresseBean();
		adresse.setBatiment(billingAddress.getBatiment());
		adresse.setBoitePostale(billingAddress.getBoitePostale());
		adresse.setCodePostale(billingAddress.getCodePostal());
		if (billingAddress.getCommune() != null) {
			adresse.setCommune(new ReferentialBean(billingAddress.getCommune()));
		}
		adresse.setCommuneEtrangere(billingAddress.getCommuneEtranger());
		adresse.setComplementAdresse(billingAddress.getComplement());
		adresse.setEscalier(billingAddress.getEscalier());
		adresse.setEtage(billingAddress.getEtage());
		adresse.setNumeroVoie(billingAddress.getNumVoie());
		if (billingAddress.getPays() != null) {
			adresse.setPays(new ReferentialBean(billingAddress.getPays()));
		}
		adresse.setPorte(billingAddress.getPorte());
		if (billingAddress.getQuartier() != null) {
			adresse.setQuartier(new ReferentialBean(billingAddress
					.getQuartier()));
		}
		adresse.setQuartierNonCodifie(billingAddress.getQuartierNonCodifiee());
		if (billingAddress.getTypeDestribution() != null) {
			adresse.setTypeDestribution(new ReferentialBean(billingAddress
					.getTypeDestribution().toString()));
		}
		if (billingAddress.getVoie() != null) {
			adresse.setVoie(new ReferentialBean(billingAddress.getVoie()));
		}
		adresse.setVoieNonCodifie(billingAddress.getVoieNonCodifie());
		contactDeFacturationBean.setAdresse(adresse);
	}

	private static void copyPropertiesFromVOToBusinessBean(
			BillingContact billingContact,
			ContactDeFacturationBean contactDeFacturationBean) {
		contactDeFacturationBean.setRaisonSocial(billingContact
				.getBusinessName());
		contactDeFacturationBean.setFax(billingContact.getFax());
		contactDeFacturationBean.setPrenom(billingContact.getFirstName());
		contactDeFacturationBean.setTelephoneMobile(billingContact.getGsm());
		contactDeFacturationBean.setNom(billingContact.getLastName());
		contactDeFacturationBean.setTelephoneFix(billingContact.getTel());

	}
}
