package ma.iam.wiam.vo;

/**Begin feature/wsTols*/
public class CommOfferBean {
	
	private String idList;
	private String idValue;
	public String getIdList() {
		return idList;
	}
	public void setIdList(String idList) {
		this.idList = idList;
	}
	public String getIdValue() {
		return idValue;
	}
	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}
	
	
/**End feature/wsTools*/
}
