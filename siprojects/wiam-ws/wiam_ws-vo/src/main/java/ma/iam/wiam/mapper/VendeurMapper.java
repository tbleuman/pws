package ma.iam.wiam.mapper;
/**Begin feature/wsTools*/

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import ma.iam.wiam.vo.VendeurBean;
import ma.iam.wiam.ws.neto.model.Vendeur;

public class VendeurMapper {

	public static final VendeurBean mapPersist2Vo(Vendeur persist) {
		VendeurBean bean = null;
		if(persist != null) {
			bean = new VendeurBean();
			bean.setLoginId(persist.getLoginId());
			bean.setLoginName(persist.getLoginName());
		}
		return bean;
	}
	
	public static final List<VendeurBean> mapPersit2VoList(List<Vendeur> list){
		List<VendeurBean> listVos = new ArrayList<VendeurBean>();
		if(CollectionUtils.isNotEmpty(list)) {
			for(Vendeur persist : list) {
				listVos.add(mapPersist2Vo(persist));
			}
		}
		return listVos;
	}
	
/**End feature/wsTools*/	
}
