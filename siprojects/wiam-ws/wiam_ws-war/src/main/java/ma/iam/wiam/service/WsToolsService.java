package ma.iam.wiam.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.neto.bean.ReferentialBeanList;
import ma.iam.wiam.params.AgenceBancaireParams;
import ma.iam.wiam.params.DansVoieParams;
import ma.iam.wiam.params.QuartierParams;
import ma.iam.wiam.params.VoieParams;
import ma.iam.wiam.vo.CataloguePriceBean;
import ma.iam.wiam.vo.CcuBean;
import ma.iam.wiam.vo.CommOfferBean;
import ma.iam.wiam.vo.CommuneBean;
import ma.iam.wiam.vo.OrganismeBean;
import ma.iam.wiam.vo.VendeurBean;

@WebService(serviceName = "wsToolsService", targetNamespace = "http://webservice.iam.ma/wiamws")
public interface WsToolsService extends BasicService {

	@WebMethod(operationName = "listerCommunes")
	@WebResult(name = "commune")
	public List<CommuneBean> getCommunes() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerQuartierByCommune")
	@WebResult(name = "quartier")
	public List<ReferentialBean> getQuartiersByCommune(
			@WebParam(name = "quartierParams") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") QuartierParams commune)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerVoieByQuartier")
	@WebResult(name = "voie")
	public List<ReferentialBean> getVoieByQuartier(
			@WebParam(name = "quartier") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") VoieParams voieParam)
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerCodesPersonnes")
	@WebResult(name = "codePersonne")
	public List<ReferentialBean> getCodesPersonnes()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerEtatsCiviles")
	@WebResult(name = "etatCivile")
	public List<ReferentialBean> getEtatsCiviles() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerIdentifiantsResidentiels")
	@WebResult(name = "identifiantResidentiel")
	public List<ReferentialBean> getIdentifiantsResidentiels()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerIdentifiantsProfessionels")
	@WebResult(name = "identifiantProfessionel")
	public List<ReferentialBean> getIdentifiantsProf()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerIdentifiantsEntreprises")
	@WebResult(name = "identifiantEntreprise")
	public List<ReferentialBean> getIdentifiantsEntreprise()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerProfessions")
	@WebResult(name = "profession")
	public List<ReferentialBean> getProfessions() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerEntitesGestionnaires")
	@WebResult(name = "entitesGestionnaire")
	public List<ReferentialBean> getEntityManagers()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listSegments1")
	@WebResult(name = "segment1")
	public List<ReferentialBean> getSegments1() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listSegments2")
	@WebResult(name = "segment2")
	public List<ReferentialBean> getSegments2() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerInfosProspections")
	@WebResult(name = "infoProspection")
	public List<ReferentialBean> getProspectionInfos()
			throws FunctionnalException, TechnicalException;

	@WebMethod(operationName = "listerBanques")
	@WebResult(name = "banque")
	public List<ReferentialBean> getBanks() throws FunctionnalException,
			TechnicalException;

	@WebMethod(operationName = "listerAgencesBancaires")
	@WebResult(name = "agenceBancaire")
	public List<ReferentialBean> getAgenciesByParams(
			@WebParam(name = "agenceBancaireParams") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") AgenceBancaireParams agenceBancaireParams)
			throws FunctionnalException, TechnicalException;

	// @WebMethod(operationName = "byPass")
	// @WebResult(name = "statut")
	// public String modeByPass();
	@WebMethod(operationName = "getConfigAll")
	@WebResult(name = "referentialBeanList")
	public ReferentialBeanList getConfigAll()throws FunctionnalException, TechnicalException;
	
	/**Begin feature/wsTools**/
	@WebMethod(operationName = "listerSex")
	@WebResult(name = "Sex")
	public List<ReferentialBean> getSex() throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "listerSupportFacturation")
	@WebResult(name = "SupportFacturation")
	public List<ReferentialBean> getSupportFacturation() throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "listerLangue")
	@WebResult(name = "Langue")
	public List<ReferentialBean> getLangue() throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "listerCountry")
	@WebResult(name = "country")
	public List<ReferentialBean> getCountrys() throws FunctionnalException,
			TechnicalException;
	
	@WebMethod(operationName = "getNumeroVoieByVoie")
	@WebResult(name = "NumeroVoie")
	public List<ReferentialBean> getNumeroVoieByVoie(
			@WebParam(name = "voie") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") DansVoieParams dansVoieParams)
			throws FunctionnalException, TechnicalException;
	
	@WebMethod(operationName = "listerTypeDistribution")
	@WebResult(name = "TypeDistribution")
	public List<ReferentialBean> getTypeDistribution()throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "listerOperationWinBack")
	@WebResult(name = "OperationWinBack")
	public List<ReferentialBean> getOperateurWinBack()throws FunctionnalException,
	TechnicalException; 
	
	@WebMethod(operationName = "listerOrganisme")
	@WebResult(name = "organismes")
	public List<OrganismeBean> getOrganismes() throws FunctionnalException,
			TechnicalException;
	
	@WebMethod(operationName = "listerAdresseAnnuaire")
	@WebResult(name = "AdresseAnnuaire")
	public List<CommOfferBean> getAdresseAnnuaire()throws FunctionnalException,
	TechnicalException;
	/**Begin feature_Changement_Req_ACICGC*/
	@WebMethod(operationName = "getListACICGC")
	@WebResult(name = "Logins")
	public List<VendeurBean> getListACICGC(
			@WebParam(name = "agenceCode") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String agenceCode
			)throws FunctionnalException,
	TechnicalException;
	/**End feature_Changement_Req_ACICGC*/
	@WebMethod(operationName = "verifierExistanceCCU")
	@WebResult(name = "CCU")
	public CcuBean verifierExistanceCCU(
			@WebParam(name = "codeCcu") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String codeCcu
			)throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "getListeEquipementByOperationND")
	@WebResult(name = "Modeles")
	public List<CataloguePriceBean>  getListeEquipementByOperationND(
			@WebParam(name = "NdLogin") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String NdLogin
			)throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "getListeEquipementByOperationNA_RP")
	@WebResult(name = "Modeles")
	public List<CataloguePriceBean> getListeEquipementByOperationNA_RP(
			@WebParam(name = "rateplanCode") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String rateplanCode,
			@WebParam(name = "typeCommercial") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String typeCommercial
			)throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "VerifierNonExistanceLoginInternet")
	@WebResult(name = "Existe")
	public Boolean VerifierNonExistanceLoginInternet(
			@WebParam(name = "login") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String login,
			@WebParam(name = "serviceId") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String serviceId
			)throws FunctionnalException,
	TechnicalException; 
	
	@WebMethod(operationName = "VerifierNonExistanceAdressMail")
	@WebResult(name = "Existe")
	public Boolean VerifierNonExistanceAdressMail(
			@WebParam(name = "email") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String email,
			@WebParam(name = "serviceId") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String serviceId
			)throws FunctionnalException,
	TechnicalException; 
	
	@WebMethod(operationName = "VerifierNonExistanceLoginSoftSwitch")
	@WebResult(name = "Existe")
	public Boolean VerifierNonExistanceLoginSoftSwitch(
			@WebParam(name = "login") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String login
			)throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "VerifierEquipementCompatibleND")
	@WebResult(name = "Compatible")
	public Boolean VerifierEquipementCompatibleND(
			@WebParam(name = "NdLogin") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String NdLogin,
			@WebParam(name = "ANetoId") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String ANetoId
			)throws FunctionnalException,
	TechnicalException;
	
	@WebMethod(operationName = "getEngagementContrat")
	@WebResult(name = "DateFinEngagement")
	public java.util.Date getEngagementContrat (
			@WebParam(name = "NdLogin") @XmlElement(required = true, namespace = "http://webservice.iam.ma/wiamws") String NdLogin
			)throws FunctionnalException,
	TechnicalException;
	/**End feature/wsTools**/
}
