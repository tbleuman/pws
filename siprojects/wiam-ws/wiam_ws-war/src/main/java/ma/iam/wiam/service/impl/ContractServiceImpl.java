package ma.iam.wiam.service.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.iam.security.SecuredObject;
import ma.iam.wiam.business.ContractManagerBiz;
import ma.iam.wiam.business.CustomerBillingManagerBiz;
import ma.iam.wiam.business.NetoIntManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.business.TemplateManagerBiz;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.mapper.CustomerBillingMapper;
import ma.iam.wiam.neto.bean.ContractBean;
import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.service.ContractService;
import ma.iam.wiam.vo.CustomerBillingInfoResponseBean;
import ma.iam.wiam.ws.neto.model.Contrat;
import ma.iam.wiam.ws.neto.model.Template;

/**
 * 
 * @author s.hroumti
 *
 */
@SecuredObject
@Component(value = "contractService")
public class ContractServiceImpl extends BasicServiceImpl implements
		ContractService {

	@Autowired
	private ContractManagerBiz contractManagerBiz;
	
	/**Begin  feature/Template**/
	@Autowired
	private TemplateManagerBiz templateManagerBiz;
	/**End  feature/Template**/	
	
	public ReferentialBean getStatus(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		return contractManagerBiz.getStatus(ncli, nd);
	}

	/**<!--  feature/Ajouter_Neto_Int -->**/
	@Override
	public String createContract(CreateContractOrder createContractOrder, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {
		return  contractManagerBiz.createContract(createContractOrder);
		
	}

	/**Begin  feature/Template**/
	@Override
	public Template getTemplateById(Long templateID, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return templateManagerBiz.getTemplateById(templateID);
	}

	@Override
	public List<Template> getAllTemplate() throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return templateManagerBiz.getAllTemplate();
	}
	
	/**End  feature/Template**/
	
	/**Begin : feature/Recherche_Contrat_Client */
	@Override
	public List<Contrat> getContractClient(TicketObject ticketObject, String numCli, String typeIdentifiant, String identifiant)
			throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		
			return contractManagerBiz.getContractByClient(numCli, typeIdentifiant,identifiant);
		
			
	}
	
	@Override
	public ContractBean getContractWithServices(TicketObject ticketObject, String numCli, String nd_login,
			String typeIdentifiant, String identifiant) throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return contractManagerBiz.getContractBean(numCli, nd_login, typeIdentifiant, identifiant);
		
		 
	}

	/**End : feature/Recherche_Contrat_Client */

	
}
