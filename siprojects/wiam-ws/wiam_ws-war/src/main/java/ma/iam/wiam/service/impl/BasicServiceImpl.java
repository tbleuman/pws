package ma.iam.wiam.service.impl;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.xml.ws.WebServiceContext;

import ma.iam.wiam.business.ByPassManagerBiz;
import ma.iam.wiam.service.BasicService;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Properties;

@HandlerChain(file = "handlers.xml")
public abstract class BasicServiceImpl implements BasicService {

	@Autowired
	private ByPassManagerBiz byPassManagerBiz;
	@Autowired
	private Properties git;
	@Resource
	private WebServiceContext ctx;

	public String modeByPass() {
		return byPassManagerBiz.modeByPasse();
	}

	public String getUser() {
		return (String) ctx.getMessageContext().get("USERNAME");
	}
	@Override
	public String infos() {
		return "branch: "+ git.getProperty("git.branch") +
				", closest.tag.name : "+git.getProperty("git.closest.tag.name") +
				", build.time : "+git.getProperty("git.build.time") +
				", commit.id : "+ git.getProperty("git.commit.id");
	}
}
