package ma.iam.wiam.service.impl;

import java.util.List;

import ma.iam.security.SecuredObject;
import ma.iam.wiam.business.OrderManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.mapper.ServiceMapper;
import ma.iam.wiam.neto.bean.CheckOrderResponseBean;
import ma.iam.wiam.neto.bean.CommandeBean;
import ma.iam.wiam.neto.bean.ServiceList;
import ma.iam.wiam.neto.bean.ServiceRulesRequestBean;
import ma.iam.wiam.service.ServiceOperationsService;
import ma.iam.wiam.vo.ServiceRule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author s.hroumti
 *
 */
@SecuredObject
@Component(value = "serviceOperationsService")
public class ServiceOperationsServiceImpl extends BasicServiceImpl implements
		ServiceOperationsService {

	@Autowired
	private ServiceManagerBiz serviceManagerBiz;

	@Autowired
	private OrderManagerBiz orderManagerBiz;

	public ServiceList getServicesSouscrits(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		// return ServiceMapper.mapBusinessBeanToVO(serviceManagerBiz
		// .getServicesSouscrits(ncli, nd));
		return serviceManagerBiz.getServicesSouscrits(ncli, nd);
	}

	public ServiceList getAddableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		// return ServiceMapper.mapBusinessBeanToVO(serviceManagerBiz
		// .getAddableServices(ncli, nd));
		return serviceManagerBiz.getAddableServices(ncli, nd);
	}

	public ServiceList getRemovableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		// return ServiceMapper.mapBusinessBeanToVO(serviceManagerBiz
		// .getRemovableServices(ncli, nd));
		return serviceManagerBiz.getRemovableServices(ncli, nd);
	}

	public ServiceList getEditableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		// return ServiceMapper.mapBusinessBeanToVO(serviceManagerBiz
		// .getEditableServices(ncli, nd));
		return serviceManagerBiz.getEditableServices(ncli, nd);
	}

	public ServiceList getServiceParamsModify(String ncli, String nd,
			List<String> serviceIds) throws FunctionnalException,
			TechnicalException {
		// return ServiceMapper.mapBusinessBeanToVO(serviceManagerBiz
		// .getServiceParamsModify(ncli, nd, serviceIds));
		return serviceManagerBiz.getServiceParamsModify(ncli, nd, serviceIds);
	}

	public ServiceList getServiceParamsAdd(String ncli, String nd,
			List<String> serviceIds) throws FunctionnalException,
			TechnicalException {
		// return ServiceMapper.mapBusinessBeanToVO(serviceManagerBiz
		// .getServiceParamsAdd(ncli, nd, serviceIds));
		return serviceManagerBiz.getServiceParamsAdd(ncli, nd, serviceIds);
	}

	public List<ServiceRule> getDependanceAndIncompatibleServiceRules(
			ServiceRulesRequestBean serviceRulesRequestBean)
			throws FunctionnalException, TechnicalException {
		return ServiceMapper.mapServiceRuleBusinessBeanToVO(serviceManagerBiz
				.getServiceRules(serviceRulesRequestBean));
	}

	public CheckOrderResponseBean checkOrder(CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException {
		return orderManagerBiz.checkOrder(commandeRequestBean);
	}

	public CheckOrderResponseBean createOrder(CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException {
		return orderManagerBiz.createOrder(commandeRequestBean, getUser());
	}

	public CheckOrderResponseBean test(CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException {
		return orderManagerBiz.checkOrder(commandeRequestBean);
	}

	public CheckOrderResponseBean cancelOrder(CommandeBean commandeRequestBean)
			throws FunctionnalException, TechnicalException {
		return orderManagerBiz.cancelOrder(commandeRequestBean, getUser());
	}
}
