package ma.iam.wiam.service.impl;

import ma.iam.security.SecuredObject;
import ma.iam.wiam.neto.bean.TicketObject;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



import ma.iam.wiam.business.ByPassManagerBiz;
import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.LevelManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.constants.Constants;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.neto.bean.RequestBean;
import ma.iam.wiam.service.CustomerService;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.OrganizationTypeEnum;


@SecuredObject
@Component(value = "customerService")
public class CustomerServiceImpl extends BasicServiceImpl implements
		CustomerService {

	@Autowired
	private LevelManagerBiz levelManagerBiz;

	@Autowired
	ByPassManagerBiz byPassManagerBiz;

	//@todo feature/24: ajouter ReferentielManagerBiz
	@Autowired
	ReferentialManagerBiz referentialManagerBiz;
	
	/**feature/Changement_Categorie_Client*/
	@Autowired
	ClientManagerBiz clientManagerBiz;

	public String createGPCustomer(NiveauBean organisationBean, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {

		if (organisationBean == null) {
			throw new TechnicalException(
					ExceptionCodeTypeEnum.REQUIRED_FIELD_WS,
					"Organisation: objet null");
		}
		//@todo feature/24: utiliser une liste dynamique --changer avec getItemFromReferential(String code)
		String[] sousCategories = (String[]) referentialManagerBiz.getItemFromReferential(Constants.SOUS_CATEGORY_EREV);
		if (organisationBean.getType() == null
				|| !ArrayUtils.contains(
						sousCategories, organisationBean.getType()
								.getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE,
					"Organisation: type invalide");
		}
		 String result = levelManagerBiz.createLevel(organisationBean,
				ActionType.CREATE_CUSTOMER,ticketObject,"creationClientGrandPublic",getUser());
		 return result;
	}

	public String createProfessionnelCustomer(NiveauBean organisationBean, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {
		if (organisationBean == null) {
			throw new TechnicalException(
					ExceptionCodeTypeEnum.REQUIRED_FIELD_WS,
					"Organisation: objet null");
		}

		if (organisationBean.getType() == null
				|| !ArrayUtils.contains(
						new String[] { OrganizationTypeEnum.PROFESSIONNEL
								.toString() }, organisationBean.getType()
								.getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE,
					"Organisation: type invalide");
		}
		String result =levelManagerBiz.createLevel(organisationBean,
				ActionType.CREATE_CUSTOMER,ticketObject,"creationClientProfessionnel",getUser());
		return result;
	}

	public String createEntrepriseCustomer(NiveauBean organisationBean, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {
		if (organisationBean == null) {
			throw new TechnicalException(
					ExceptionCodeTypeEnum.REQUIRED_FIELD_WS,
					"Organisation: objet null");
		}

		if (organisationBean.getType() == null
				|| !ArrayUtils.contains(
						new String[] { OrganizationTypeEnum.ENTREPRISE
								.toString() }, organisationBean.getType()
								.getCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.INVALID_TYPE,
					"Organisation: type invalide");
		}
		String result =levelManagerBiz.createLevel(organisationBean,
				ActionType.CREATE_CUSTOMER,ticketObject,"creationClientEntreprise",getUser());
		return result;
	}

	public String createLevel(NiveauBean organisationBean, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {
		String result =levelManagerBiz.createLevel(organisationBean,
				ActionType.ADD_LEVEL,ticketObject,"createLevel",getUser());
		return result;
	}

	// public String modeByPass() {
	// return byPassManagerBiz.modeByPasse();
	// }

	public NiveauBean linkLevelWithCcu(RequestBean requestBean, TicketObject ticketObject)
			throws FunctionnalException, TechnicalException {
		NiveauBean createdLevel = levelManagerBiz.linkLevelWithCcu(requestBean,ticketObject);

		return createdLevel;
	}

	/**Begin feature Changement_Categorie_Client**/
	@Override
	public String changeCategoryCustomer(TicketObject ticketObject, String ncli, String codeSousCategorieDest)
			throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		if(clientManagerBiz.changeCategoryClient(ncli, codeSousCategorieDest))
			return ticketObject.getTicket();
		else
			throw new FunctionnalException(ExceptionCodeTypeEnum.CHANGE_CATEGORIE_ECHEC);
		
	}
	/**End feature Changement_Categorie_Client**/
}
