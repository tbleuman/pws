package ma.iam.wiam.service;

import javax.jws.WebMethod;
import javax.jws.WebResult;

public interface BasicService {

	@WebMethod(operationName = "byPass")
	@WebResult(name = "statut")
	public String modeByPass();

	String getUser();

	public String infos() ;
}
