/*
 * @author AtoS
 */
package ma.iam.wiam.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;

/**
 * The listener interface for receiving initializationContext events. The class
 * that is interested in processing a initializationContext event implements
 * this interface, and the object created with that class is registered with a
 * component using the component's
 * <code>addInitializationContextListener<code> method. When
 * the initializationContext event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see InitializationContextEvent
 */
public class InitializationContextListener implements ServletContextListener {

	private static final Logger LOG = LoggerFactory.getLogger(InitializationContextListener.class);

	public void contextDestroyed(final ServletContextEvent servlet) {

	}

	public void contextInitialized(final ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		initLogger();
		loadReferential(context);
	}
	
	private void loadReferential(ServletContext context) {
	}

	private void initLogger() {
		final LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		context.reset();
		context.putProperty("application-name", "wiam-ws");
		final JoranConfigurator jc = new JoranConfigurator();
		jc.setContext(context);
		try {
			final String logconfigfile = System.getProperty("logback.configurationFile");
			jc.doConfigure(logconfigfile);
		} catch (final Exception e) {
			LOG.error("Exception in method InitializationContextListener.contextInitialized(servlet) :", e);
		}
		final String name = context.getProperty("application-name");
		LOG.debug("Application name : " + name);
	}

}
