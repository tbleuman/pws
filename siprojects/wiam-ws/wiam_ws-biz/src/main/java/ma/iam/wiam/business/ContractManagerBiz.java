package ma.iam.wiam.business;

import java.util.List;

import com.netonomy.blm.api.Contract.ContractF;
import com.netonomy.blm.util.BlmSecurityException;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContractBean;
import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.ws.neto.model.Client;
import ma.iam.wiam.ws.neto.model.Contrat;
import ma.iam.wiam.ws.neto.model.Organisme;

public interface ContractManagerBiz {

	String getContractById(Long Id) throws BlmSecurityException,
			NumberFormatException, TechnicalException;

	ReferentialBean getStatus(String ncli, String nd)
			throws FunctionnalException, TechnicalException;

	ContractF getContract(String ncli, String nd) throws TechnicalException,
			FunctionnalException;

	public ContractF getContract(Client client, String nd)
			throws TechnicalException, FunctionnalException;
	
	/**<!--Begin feature/Ajouter_Neto_Int -->**/ 
	String createContract(CreateContractOrder createContractOrder)throws TechnicalException, FunctionnalException;
	/**<!--End feature/Ajouter_Neto_Int -->**/ 
	
	/**Begin : feature/Recherche_Contrat_Client */
	List<Contrat> getContractByClient(String numClient,String typeIdentifiant,String identifiant)throws TechnicalException, FunctionnalException;
	ContractBean getContractBean(String numCli,String nd,String typeIdentifiant,String identifiant )throws TechnicalException, FunctionnalException;
	/**End : feature/Recherche_Contrat_Client */
	
	/**Begin feature/wsTools*/
	List<Organisme> getListOrganisme()throws TechnicalException, FunctionnalException;
	/**End feature/wsTools*/
}
