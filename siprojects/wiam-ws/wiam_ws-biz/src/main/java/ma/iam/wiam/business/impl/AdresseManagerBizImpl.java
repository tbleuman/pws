package ma.iam.wiam.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.iam.wiam.business.AdresseManagerBiz;
import ma.iam.wiam.business.constants.Constants;
import ma.iam.wiam.dao.CommuneDao;
import ma.iam.wiam.dao.ContactAddInfoDao;
import ma.iam.wiam.dao.CountryDao;
import ma.iam.wiam.dao.DansVoieDao;
import ma.iam.wiam.dao.QuartierDao;
import ma.iam.wiam.dao.VoieDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.param.criteria.DansVoieCriteria;
import ma.iam.wiam.param.criteria.QuartierCriteria;
import ma.iam.wiam.param.criteria.VoieCriteria;
import ma.iam.wiam.util.StringUtil;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Contact;
import ma.iam.wiam.ws.neto.model.ContactAddInfo;
import ma.iam.wiam.ws.neto.model.Country;
import ma.iam.wiam.ws.neto.model.PCommune;
import ma.iam.wiam.ws.neto.model.PDansVoie;
import ma.iam.wiam.ws.neto.model.PQuartier;
import ma.iam.wiam.ws.neto.model.PVoie;
import ma.iam.wiam.ws.neto.model.Parameter;

@Component("adresseManagerBiz")
public class AdresseManagerBizImpl implements AdresseManagerBiz {

	@Autowired
	CommuneDao communeDao;

	@Autowired
	QuartierDao quartierDao;

	@Autowired
	VoieDao voieDao;
	
	@Autowired
	CountryDao countryDao;
	
	@Autowired
	ContactAddInfoDao contactAddInfoDao;
	/**Begin feature/wsTools*/
	@Autowired
	DansVoieDao dansVoieDao;
	/**End feature/wsTools*/
	public List<PCommune> getListCommune() throws FunctionnalException, TechnicalException {
		List<PCommune> list = communeDao.getCommuneList();
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}

	public List<PVoie> getVoieByByCriteria(VoieCriteria voieCriteria) throws FunctionnalException, TechnicalException {

		if (voieCriteria == null || StringUtil.isNullOrEmpty(voieCriteria.getCodeCommune())
				|| StringUtil.isNullOrEmpty(voieCriteria.getCodeQuartier())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_SEARCH_WS);
		}

		List<PVoie> list = voieDao.listVoieByCommuneAndQuartier(voieCriteria.getCodeCommune(),
				voieCriteria.getCodeQuartier());
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}
	
	/**Begin feature/wsTools*/
	public List<PDansVoie> getDansVoieByCriteria(DansVoieCriteria dansVoieCriteria)
			throws FunctionnalException, TechnicalException {
		if (dansVoieCriteria == null || StringUtil.isNullOrEmpty(dansVoieCriteria.getCodeCommune())
				|| StringUtil.isNullOrEmpty(dansVoieCriteria.getCodeQuartier())
				|| StringUtil.isNullOrEmpty(dansVoieCriteria.getCodeVoie())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_SEARCH_WS);
		}
		List<PDansVoie> list = dansVoieDao.getNumeroDansVoieByVoie(dansVoieCriteria.getCodeCommune(),
				dansVoieCriteria.getCodeQuartier(), dansVoieCriteria.getCodeVoie());
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}
	/**End feature/wsTools*/
	public List<PQuartier> getQuartiersByCriteria(QuartierCriteria quartier)
			throws FunctionnalException, TechnicalException {

		if (quartier == null || StringUtil.isNullOrEmpty(quartier.getCodeCommune())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_SEARCH_WS);
		}
		List<PQuartier> list = quartierDao.listQuartierByCodeCommune(quartier.getCodeCommune());
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}

	@SuppressWarnings("serial")
	public String getAdresseAsString(Contact contact) throws FunctionnalException, TechnicalException {

		StringBuffer sAdr = new StringBuffer();
		String ccom = contact.getState();
		String cquartier = contact.getAddAddress3();
		String cvoie = contact.getAddAddress2();

		Country country = countryDao.getCountryById(contact.getCountryId());
		if (country != null) {
			if (!StringUtil.isNullOrEmpty(country.getCountryName())) {
				sAdr.append(country.getCountryName());
				sAdr.append(", ");
			}
		}
		// Foreigner Adresse
		if (country != null && country.getId() != Constants.MOROCCO_ID) {
			List<ContactAddInfo> list = contactAddInfoDao.getContactAddInfoByContactAndCodeParams(contact,
					(new ArrayList<String>() {
						{
							add(Parameter.ETRG_COMMUNE_CONTACT);
						}
					}));
			if (list != null && list.size() > 0 && list.get(0).getValue() != null) {
				sAdr.append(StringUtil.nonNullOrEmpty(list.get(0).getValue().getValueString()));
			}
		} else {
			if (!StringUtil.isNullOrEmpty(ccom)) {

				PCommune commune = communeDao.getCommuneByCode(ccom);

				sAdr.append((commune.getLcom() == null) ? "" : commune.getLcom());
			}
		}
		if (!StringUtil.isNullOrEmpty(cquartier)) {
			sAdr.append(", ");
			if (cquartier.equals(Constants.NONE) || cquartier.equals(Constants.XXXX))
				sAdr.append((StringUtil.isNullOrEmpty(contact.getAddAddress1())) ? "" : contact.getAddAddress1());
			else {

				PQuartier quartier = quartierDao.getQuartierByCodeCommuneAndCodeQuartier(ccom, cquartier);

				if (quartier != null) {
					sAdr.append((StringUtil.isNullOrEmpty(quartier.getLquartier())) ? "" : quartier.getLquartier());
				} else {
					sAdr.append((StringUtil.isNullOrEmpty(contact.getAddAddress1())) ? "" : contact.getAddAddress1());
				}
			}
		} else {
			sAdr.append(", ");
			sAdr.append((StringUtil.isNullOrEmpty(contact.getAddAddress1())) ? "" : contact.getAddAddress1());
		}
		if ((cvoie != null) && (!(cvoie.length() == 0))) {
			sAdr.append(", ");
			if (cvoie.equals(Constants.NONE) || cvoie.equals(Constants.XXXX))
				sAdr.append((StringUtil.isNullOrEmpty(contact.getStreetAddress())) ? "" : contact.getStreetAddress());
			else {

				PVoie voie = voieDao.getVoie(ccom, cquartier, cvoie);

				if (voie != null)
					sAdr.append(((StringUtil.isNullOrEmpty(voie.getLvoie())) ? "" : voie.getLvoie()));
				else
					sAdr.append(
							(StringUtil.isNullOrEmpty(contact.getStreetAddress())) ? "" : contact.getStreetAddress());
			}
		} else {
			sAdr.append(", ");
			sAdr.append((StringUtil.isNullOrEmpty(contact.getStreetAddress())) ? "" : contact.getStreetAddress());
		}
		if (!StringUtil.isNullOrEmpty(contact.getStreetNumber())) {
			sAdr.append(", ");
			sAdr.append(contact.getStreetNumber());
		}

		List<ContactAddInfo> list = contactAddInfoDao.getContactAddInfoByContactAndCodeParams(contact,
				getAdresseParameterContact());

		String[] strArraysParam = new String[7];
		for (ContactAddInfo addInfo : list) {
			if (addInfo.getId().getParamId().getParamCode().equals(Parameter.BATIMENT_CONTACT)
					&& addInfo.getValue() != null) {
				strArraysParam[0] = StringUtil.nonNullOrEmpty(addInfo.getValue().getValueString());
			}
			if (addInfo.getId().getParamId().getParamCode().equals(Parameter.ESCALIER_CONTACT)
					&& addInfo.getValue() != null) {
				strArraysParam[1] = StringUtil.nonNullOrEmpty(addInfo.getValue().getValueString());
			}

			if (addInfo.getId().getParamId().getParamCode().equals(Parameter.ETAGE_CONTACT)
					&& addInfo.getValue() != null) {
				strArraysParam[2] = StringUtil.nonNullOrEmpty(addInfo.getValue().getValueString());
			}
			if (addInfo.getId().getParamId().getParamCode().equals(Parameter.PORTE_CONTACT)
					&& addInfo.getValue() != null) {
				strArraysParam[3] = StringUtil.nonNullOrEmpty(addInfo.getValue().getValueString());
			}

			if (addInfo.getId().getParamId().getParamCode().equals(Parameter.BP_CONTACT)
					&& addInfo.getValue() != null) {
				strArraysParam[4] = StringUtil.nonNullOrEmpty(addInfo.getValue().getValueString());
			}
			if (addInfo.getId().getParamId().getParamCode().equals(Parameter.COMPL_ADR_CONTACT)
					&& addInfo.getValue() != null) {
				strArraysParam[5] = StringUtil.nonNullOrEmpty(addInfo.getValue().getValueString());
			}

		}

		if (strArraysParam[0] != null && !"".equals(strArraysParam[0])) {
			sAdr.append(Constants.ADRESSE_BATIMENT);
			sAdr.append(strArraysParam[0]);
		}
		if (strArraysParam[1] != null && !"".equals(strArraysParam[1])) {
			sAdr.append(Constants.ADRESSE_ESC);
			sAdr.append(strArraysParam[1]);
		}
		if (strArraysParam[2] != null && !"".equals(strArraysParam[2])) {
			sAdr.append(Constants.ADRESSE_ETAGE);
			sAdr.append(strArraysParam[2]);
		}
		if (strArraysParam[3] != null && !"".equals(strArraysParam[3])) {
			sAdr.append(Constants.ADRESSE_PORTE);
			sAdr.append(strArraysParam[3]);
		}
		if (strArraysParam[4] != null && !"".equals(strArraysParam[4])) {
			sAdr.append(Constants.ADRESSE_BP);
			sAdr.append(strArraysParam[4]);
		}
		if (strArraysParam[5] != null && !"".equals(strArraysParam[5])) {
			sAdr.append(", ");
			sAdr.append(strArraysParam[5]);
		}

		return sAdr.toString();
	}

	private List<String> getAdresseParameterContact() {
		String a[] = new String[] { Parameter.BATIMENT_CONTACT, Parameter.ESCALIER_CONTACT, Parameter.ETAGE_CONTACT,
				Parameter.PORTE_CONTACT, Parameter.BP_CONTACT, Parameter.COMPL_ADR_CONTACT };
		return Arrays.asList(a);
	}

	

	
}
