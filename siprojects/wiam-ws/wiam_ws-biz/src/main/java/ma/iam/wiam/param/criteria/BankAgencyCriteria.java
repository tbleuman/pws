package ma.iam.wiam.param.criteria;

public class BankAgencyCriteria {

	public String bankCode;

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
}
