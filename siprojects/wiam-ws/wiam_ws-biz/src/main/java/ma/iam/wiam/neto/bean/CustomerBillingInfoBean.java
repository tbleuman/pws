package ma.iam.wiam.neto.bean;

public class CustomerBillingInfoBean {
	private String ncli;
	private String categorieSs;
	private BillingContactDetailBean billingContactDetailBean;

	public String getNcli() {
		return ncli;
	}

	public void setNcli(String ncli) {
		this.ncli = ncli;
	}

	public String getCategorieSs() {
		return categorieSs;
	}

	public void setCategorieSs(String categorieSs) {
		this.categorieSs = categorieSs;
	}

	public BillingContactDetailBean getBillingContactDetailBean() {
		return billingContactDetailBean;
	}

	public void setBillingContactDetailBean(
			BillingContactDetailBean billingContactDetailBean) {
		this.billingContactDetailBean = billingContactDetailBean;
	}

	@Override
	public String toString() {
		return "CustomerBillingInfoBean [ncli=" + ncli + ", categorieSs="
				+ categorieSs + ", billingContactDetailBean="
				+ billingContactDetailBean + "]";
	}

}
