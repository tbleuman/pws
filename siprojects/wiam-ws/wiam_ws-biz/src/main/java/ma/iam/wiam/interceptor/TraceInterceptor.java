package ma.iam.wiam.interceptor;

public interface TraceInterceptor {
    public void performTrace(Object respObject, String operation, String userName, String ticket, String userNameId) throws TraceException ;
	
}
