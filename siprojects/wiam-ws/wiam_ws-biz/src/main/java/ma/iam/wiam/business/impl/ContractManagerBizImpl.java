package ma.iam.wiam.business.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.*;
import javax.xml.namespace.QName;

import ma.iam.wiam.constants.Constants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import com.netonomy.blm.api.Contract.ContractF;
import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.interfaces.contact.ContactIF;
import com.netonomy.blm.interfaces.contract.ContractIF;
import com.netonomy.blm.interfaces.contract.ContractStatusIF;
import com.netonomy.blm.interfaces.parameters.ParameterDescriptorIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.service.ContractedServiceIF;
import com.netonomy.blm.interfaces.service.RatePlanServiceIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.MailingAddressIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.blm.util.BlmSecurityException;
import com.netonomy.util.ObjectId;

import ma.iam.wiam.adresse.AdresseMgr;
import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.ContractManagerBiz;
import ma.iam.wiam.business.CustomerBillingManagerBiz;
import ma.iam.wiam.business.NetoIntManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.contrat.ContratHelper;
import ma.iam.wiam.customer.ContactWIAM;
import ma.iam.wiam.dao.ActionDao;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.dao.OrigineDao;
import ma.iam.wiam.dao.StatusDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.AdresseBean;
import ma.iam.wiam.neto.bean.ContractBean;
import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.neto.bean.ItemList;
import ma.iam.wiam.neto.bean.ParameterDescriptor;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.neto.bean.ServiceBean;
import ma.iam.wiam.neto.bean.ServiceList;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.ContractDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.services.ServiceHelper;
import ma.iam.wiam.services.dao.SvcFeeParamValue;
import ma.iam.wiam.services.dao.SvcFeeParamValueListBuilder;
import ma.iam.wiam.services.service.FeeCalcultatorHelper;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.IdentifiantType;
import ma.iam.wiam.ws.neto.enums.OrganizationTypeEnum;
import ma.iam.wiam.ws.neto.model.Client;
import ma.iam.wiam.ws.neto.model.Contrat;
import ma.iam.wiam.ws.neto.model.NetoInt;
import ma.iam.wiam.ws.neto.model.Organisme;
import ma.iam.wiam.dao.ContratDao;
import ma.iam.wiam.dao.OrganismeDao;


 
@Service("contractManagerBiz")
public class ContractManagerBizImpl implements ContractManagerBiz {
	
	@Autowired
	private ContractDao contractDao;

	@Autowired
	private ClientManagerBiz clientManagerBiz;

	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;
	
	/**<!-- @todo feature/Ajouter_Neto_Int -->**/
	//Begin feature/Ajouter_Neto_Int
	@Autowired
	private NetoIntManagerBiz netoIntManagerBiz;
	@Autowired
	private ActionDao actionDao;
	@Autowired
	private OrigineDao origineDao;
	@Autowired
	private StatusDao statusDao;
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ContractManagerBizImpl.class);
	private static  JAXBContext JAXB_CONTEXT = null;

	static {
		try {
			JAXB_CONTEXT = JAXBContext.newInstance(CreateContractOrder.class);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	//End feature/Ajouter_Neto_Int
	/**Begin : feature/Recherche_Contrat_Client */
	@Autowired(required=true)
	private ContratDao contratDao;
	@Autowired
	private ClientDao clientDao;
	@Autowired
	private LevelFDao levelFDao;
	/**End : feature/Recherche_Contrat_Client */
	/**Begin feature/wsTools*/
	@Autowired
	private OrganismeDao organismeDao;
	/**End feature/wsTools*/
	
	public String getContractById(Long Id) throws BlmSecurityException,
			NumberFormatException, TechnicalException {
		String str = null;
		if (Id != null) {
			ContractF contractF = contractDao.getContractById(Id);
			// new ContractF(contractDao.getCurrentSession(), new
			// LongId(Long.parseLong("" + Id)));
			str = contractF.toString();
		}
		return str;
	}

	public ReferentialBean getStatus(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		ContractF contractF = getContract(ncli, nd);
		ContractStatusIF status = contractF.getStatus();

		return new ReferentialBean(status.getIdentifier().getString(),
				status.getName());
	}

	public ContractF getContract(String ncli, String nd)
			throws TechnicalException, FunctionnalException {
		Client client = clientManagerBiz.getClient(ncli);
		ContractF contract = getContract(client, nd);
		return contract;
	}

	public ContractF getContract(Client client, String nd)
			throws TechnicalException, FunctionnalException {
		String contractId;
		try {
			contractId = ContratHelper.getContractByNDAndOrgId(nd, client
					.getId().toString());
		} catch (SQLException e) {
			LOGGER.error("Cannot get contract : ", e);
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

		if (contractId == null) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,
					"Aucun contrat ne correspond au critères (numClient :"
							+ client.getNumClient() + ",nd:" + nd + ")");
		}
		ContractF contract = contractDao.getContractById(Long
				.parseLong(contractId));
		return contract;
	}
	
	/**<!--Begin  feature/Ajouter_Neto_Int -->**/
	public String createContract(CreateContractOrder createContractOrder)
			throws TechnicalException, FunctionnalException {

		try {

			Marshaller jaxbMarshaller = JAXB_CONTEXT.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			JAXBElement<CreateContractOrder> jaxbElement =
		            new JAXBElement<CreateContractOrder>( new QName("", "create-contract-order"),
		            		CreateContractOrder.class,
		            		createContractOrder);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(jaxbElement, sw);
			String vXMLData = sw.toString();

			NetoInt netoInt = new NetoInt();
			// netoInt.setRequestID(new Long(1));
			netoInt.setOrigine(origineDao.getById(Constants.NETOCMDREQUEST_ORIGIN_ID));
			netoInt.setStatus(statusDao.getById(Constants.NETOCMDREQUEST_STATUS_TO_BE_PROCESSED_2));
			netoInt.setAction(actionDao.getById(Constants.NETOCMDREQUEST_ACTION_CREATE_CONTRACT));
			netoInt.setPriority(new Long(2));

			netoInt.setInsertionDate(new java.util.Date());
			netoInt.setStatusUpdateDate(new java.util.Date());
			netoInt.setInputData(vXMLData);

			// netoInt.setParentRequestID(null);
			//netoInt.setAdditionalInfo("Info Additionnel DUMMY");
			//netoInt.setCoID(new Long(82107485));
			/*if (createContractOrder.getFixeOrder()!=null)
				netoInt.setCustomerID(Long.valueOf(createContractOrder.getFixeOrder().getCustomerCode()));*/
			//netoInt.setDestID(new Long(25413654));
			netoInt.setUserName("EREV_WS");
			netoInt.setEntryDate(new java.util.Date());
			netoInt.setRequestMode("A");

			netoIntManagerBiz.createNetoInt(netoInt);
			return String.valueOf(netoInt.getRequestID());
		} catch ( ma.iam.mutualisation.fwk.common.exception.TechnicalException  | JAXBException e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	/**<!--End feature/Ajouter_Neto_Int -->**/
	
	
	/**Begin : feature/Recherche_Contrat_Client */
	@Override
	public List<Contrat> getContractByClient(String numClient, String typeIdentifiant, String identifiant)
			throws TechnicalException, FunctionnalException {
		
		
			Client client = clientManagerBiz.getClient(numClient);
			List<Contrat> lst = null;
			LevelF orgF =null;
			ParameterIF[] lParams = null;
			ContactIF legalContact =null;
			String typeIdentifiantClient=null;
			String identifiantClient=null;
			String categorie = null;
			if (client != null) {
				categorie = contratDao.getOrganizationTypeByNumCli(numClient);
				orgF = levelFDao.get(ObjectId.newLongId(client.getId()).toString());
				legalContact = orgF.getLegalContact();
				lParams = legalContact.getAdditionalParameters();
				if("RESIDENTIEL".equals(categorie)) {
					typeIdentifiantClient = ContactWIAM.getTypeIdentifiantGP(lParams, false);
					identifiantClient = ContactWIAM.getIdentifiant(lParams);
				}else if("PROFESSIONNEL".equals(categorie)) {
					typeIdentifiantClient = ContactWIAM.getTypeIdentifiantPRF(lParams, false);
					identifiantClient = ContactWIAM.getIdentifiant(lParams);
				}else if("ENTREPRISE".equals(categorie)) {
					typeIdentifiantClient = ContactWIAM.getTypeIdentifiantENT(lParams, false);
					identifiantClient = ContactWIAM.getIdentifiant(lParams);
				}else {
					throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,"Le client ne fait pas partie des catégorie éligible");
				}
				if(typeIdentifiantClient ==null || identifiantClient == null || !typeIdentifiantClient.equals(typeIdentifiant) || !identifiantClient.equals(identifiant)) {
					throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,"le type d'identifiant ou l'identifiant est erroné");
				}
				lst = contratDao.getContratByClientNumber(numClient);
				
			} else {
				 throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,"Le Numéro du Client est éronné");
			}
			return (CollectionUtils.isNotEmpty(lst) ? lst : null);

	}

	private ServiceBean getContractedServiceBean(ContractIF contract,
			ContractedServiceIF contractedServiceIF) {
		ServiceBean contractedServiceBean = new ServiceBean();
		contractedServiceBean.setAccessFee(FeeCalcultatorHelper
				.calculateContractedService(ActionMgrIF.FEE_ACCESS,
						contractedServiceIF));
		contractedServiceBean.setSubscriptionFee(FeeCalcultatorHelper
				.calculateContractedService(ActionMgrIF.FEE_SUBSCRIPTION,
						contractedServiceIF));
		contractedServiceBean.setServiceCode(contractedServiceIF.getService()
				.getCode());
		contractedServiceBean.setServiceName(ServiceHelper.getServiceName(
				contract.getMyRatePlan().getCode(),
				contractedServiceIF.getService()));
		contractedServiceBean.setServiceId(contractedServiceIF.getIdentifier()
				.toString());

		ParameterIF[] parameters = contractedServiceIF.getParameters();
		if (parameters != null) {
			contractedServiceBean.setParameters(getParameterDescriptorList(
					contractedServiceIF, parameters, false));
		}
		return contractedServiceBean;
	}
	private List<ParameterDescriptor> getParameterDescriptorList(
			RatePlanServiceIF contractedServiceIF, ParameterIF[] parameters,
			boolean withPossibleValues) {
		List<ParameterDescriptor> serviceParameterDescriptors = new ArrayList<ParameterDescriptor>();
		for (ParameterIF parameterIF : parameters) {
			Boolean isHidden = ServiceHelper.isParamHidden(parameterIF
					.getParameterDescriptor().getCode(), contractedServiceIF
					.getService().getCode());
			if (isHidden) {
				continue;
			}
			ParameterDescriptorIF parameterDescriptor = parameterIF
					.getParameterDescriptor();
			ParameterDescriptor serviceParameterDescriptor = new ParameterDescriptor();
			serviceParameterDescriptor.setCode(parameterDescriptor.getCode());
			serviceParameterDescriptor.setDescription(parameterDescriptor
					.getDescription());
			serviceParameterDescriptor.setIdentifier(parameterDescriptor
					.getIdentifier().toString());
			serviceParameterDescriptor.setInputFormat(parameterDescriptor
					.getInputFormat());
			serviceParameterDescriptor.setName(parameterDescriptor.getName());
			serviceParameterDescriptor.setShortDescription(parameterDescriptor
					.getShortDescription());
			serviceParameterDescriptor.setType(parameterDescriptor.getType());

			try {
				serviceParameterDescriptor.setValue(WIAMHelper
						.getWIAMParameterValues(
								new ParameterIF[] { parameterIF }, 0, false));
			} catch (IOException e) {
				LOGGER.debug("Cannot get the value of param {} : {}",
						parameterDescriptor.getIdentifier().toString(), e);
			} catch (SQLException e) {
				LOGGER.debug("Cannot get the value of param {} : {}",
						parameterDescriptor.getIdentifier().toString(), e);
			}

			if (withPossibleValues) {
				setPossibleValues(contractedServiceIF, parameterIF,
						serviceParameterDescriptor);
			}

			serviceParameterDescriptors.add(serviceParameterDescriptor);

		}
		return serviceParameterDescriptors;
	}
	private void setPossibleValues(RatePlanServiceIF contractedServiceIF,
			ParameterIF parameterIF,
			ParameterDescriptor serviceParameterDescriptor) {
		int paramType = parameterIF.getParameterDescriptor().getType();
		switch (paramType) {
		case ParameterDescriptorIF.CHOICE:
			setValueChoiceItems(contractedServiceIF, parameterIF,
					serviceParameterDescriptor);
			break;
		}
	}

	private void setValueChoiceItems(RatePlanServiceIF contractedServiceIF,
			ParameterIF parameterIF,
			ParameterDescriptor serviceParameterDescriptor) {
		List<ValueChoiceItemIF> valueChoiceItems = getValueChoiceItems(
				contractedServiceIF, parameterIF);
		ItemList possibleValues = new ItemList();
		serviceParameterDescriptor.setPossibleValues(possibleValues);
		for (ValueChoiceItemIF valueChoiceItemIF : valueChoiceItems) {
			String choiceId = valueChoiceItemIF.getChoiceItem().getIdentifier()
					.toString();
			String choiceDisplayValue = valueChoiceItemIF.getChoiceItem()
					.getDisplayValue();

			possibleValues.getItems().add(
					new ReferentialBean(choiceId, choiceDisplayValue));
		}
	}
	private List<ValueChoiceItemIF> getValueChoiceItems(
			RatePlanServiceIF service, ParameterIF param) {

		List<ValueChoiceItemIF> aList = new ArrayList<ValueChoiceItemIF>();

		@SuppressWarnings("unchecked")
		List<SvcFeeParamValue> list = (List<SvcFeeParamValue>) SvcFeeParamValueListBuilder
				.getParamValuesToDisplay(service, ActionMgrIF.FEE_ACCESS);
		if (list != null) {
			ValueChoiceItemIF items[] = ((ValueChoiceIF) param).getItems();
			for (SvcFeeParamValue feeParamValue : list) {
				if (feeParamValue.getParamID() == ObjectId.convertToLong(
						param.getParameterDescriptor().getIdentifier())
						.longValue()) {
					for (int j = 0; j < items.length; j++)
						if (feeParamValue.getChoiceItemID() == ObjectId
								.convertToLong(
										items[j].getChoiceItem()
												.getIdentifier()).longValue()
								&& !aList.contains(items[j]))
							aList.add(items[j]);
				}
			}
		}
		return aList;

	}
	@Override
	public ContractBean getContractBean(String numCli, String nd, String typeIdentifiant, String identifiant)
			throws TechnicalException, FunctionnalException {
		
		ContractBean bean = null;
		Client client = clientManagerBiz.getClient(numCli);
		if(client != null) {
			LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId()).toString());
			ContactIF legalContact = orgF.getLegalContact();
			ParameterIF[] lParams = legalContact.getAdditionalParameters();
			String typeIdentifiantClient = null;
			String identifiantClient = null;
			String categorie = contratDao.getOrganizationTypeByNumCli(numCli);
			if ("RESIDENTIEL".equals(categorie)) {
				typeIdentifiantClient = ContactWIAM.getTypeIdentifiantGP(lParams, false);
				identifiantClient = ContactWIAM.getIdentifiant(lParams);
			} else if ("PROFESSIONNEL".equals(categorie)) {
				typeIdentifiantClient = ContactWIAM.getTypeIdentifiantPRF(lParams, false);
				identifiantClient = ContactWIAM.getIdentifiant(lParams);
			} else if ("ENTREPRISE".equals(categorie)) {
				typeIdentifiantClient = ContactWIAM.getTypeIdentifiantENT(lParams, false);
				identifiantClient = ContactWIAM.getIdentifiant(lParams);
			} else {
				throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,
						"Le client ne fait pas partie des catégorie éligible");
			}

			if (typeIdentifiantClient == null || identifiantClient == null
					|| !typeIdentifiantClient.equals(typeIdentifiant) || !identifiantClient.equals(identifiant)) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,
						"le type d'identifiant ou l'identifiant est erroné");
			}
			ContractIF contract = getContract(numCli, nd);
			ActionMgrIF actionMgr = ObjectMgr.createActionManager();
			ContractedServiceIF[] services = contract.getContractedServices(actionMgr, null);
			List<ServiceBean> result = new ArrayList<ServiceBean>();
			for (ContractedServiceIF contractedServiceIF : services) {
				ServiceBean contractedServiceBean = getContractedServiceBean(contract, contractedServiceIF);
				result.add(contractedServiceBean);
			}
			Contrat contrat = contratDao.getContratByNdNumCli(numCli, nd);
			AdresseBean adresseBean = new AdresseBean();
			MailingAddressIF legalAddress = legalContact.getAddress();
			adresseBean.setPays(new ReferentialBean(legalAddress.getCountry().getIdentifier().getString()));
			adresseBean.setCommune(new ReferentialBean(AdresseMgr.getCommune(legalAddress)));
			adresseBean.setCommuneEtrangere(ContactWIAM.getCommuneEtranger(lParams));
			adresseBean.setCodePostale(AdresseMgr.getCodePostale(legalAddress));
			adresseBean.setQuartier(new ReferentialBean(AdresseMgr.getCodeQuartier(legalAddress)));
			adresseBean.setQuartierNonCodifie(AdresseMgr.getQuartierName(legalAddress));
			adresseBean.setVoie(new ReferentialBean(AdresseMgr.getCodeVoie(legalAddress)));
			adresseBean.setVoieNonCodifie(AdresseMgr.getVoieName(legalAddress));
			adresseBean.setNumeroVoie(AdresseMgr.getVoieNumber(legalAddress));
			adresseBean.setBatiment(ContactWIAM.getBatiment(lParams));
			adresseBean.setEscalier(ContactWIAM.getEscalier(lParams));
			adresseBean.setEtage(ContactWIAM.getEtage(lParams));
			adresseBean.setPorte(ContactWIAM.getPorte(lParams));
			adresseBean.setTypeDestribution(new ReferentialBean(ContactWIAM.getTypeDistribution(lParams, false)));
			adresseBean.setBoitePostale(ContactWIAM.getBoitePostale(lParams));
			adresseBean.setComplementAdresse(ContactWIAM.getComplementAdresse(lParams));
			
			bean = new ContractBean(contrat.getId(), contrat.getNd_login(), contrat.getRateplan(),contrat.getRateplanCode(),new ServiceList(result),adresseBean);
			
		}else {
			throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND,"le Client est intouvable");
		}
		return bean;
		
	}

	/**End : feature/Recherche_Contrat_Client */

	/**Begin feature/wsTools*/
	@Override
	public List<Organisme> getListOrganisme() throws TechnicalException, FunctionnalException {
		List<Organisme> list = organismeDao.getListOrganisme();
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}
	/**End feature/wsTools*/

}
