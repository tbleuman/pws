package ma.iam.wiam.business;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.neto.bean.RequestBean;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.ws.neto.enums.ActionType;

public interface LevelManagerBiz {

	String  createLevel(NiveauBean niveauBean, final ActionType actionType, TicketObject ticketObject,String webMethod,String userName) throws FunctionnalException, TechnicalException;
	
	NiveauBean linkLevelWithCcu(RequestBean requestBean,TicketObject ticketObject) throws FunctionnalException, TechnicalException;
}
