package ma.iam.wiam.business;

public interface ByPassManagerBiz {

	String modeByPasse();

	String bdModeByPasse();

	String sessionModeByPasse();
}
