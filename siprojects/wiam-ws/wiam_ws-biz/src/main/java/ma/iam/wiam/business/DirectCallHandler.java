package ma.iam.wiam.business;

import java.util.concurrent.TimeoutException;

import ma.iam.wiam.util.directCall.DirectCallContext;

public interface DirectCallHandler {

	public final static String DIRECTCALL_REPLY_ERROR = "DIRECTCALL_REPLY_ERROR";
	public final static String DIRECTCALL_REPLY_OK = "DIRECTCALL_REPLY_OK";
	public final static String DIRECTCALL_NO_REPLY = "DIRECTCALL_NO_REPLY";

	public String handle(DirectCallContext ctx) throws TimeoutException;
}
