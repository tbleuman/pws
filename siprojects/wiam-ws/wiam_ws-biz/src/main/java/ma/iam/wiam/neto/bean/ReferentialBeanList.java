package ma.iam.wiam.neto.bean;

import java.io.Serializable;
import java.util.List;



public class ReferentialBeanList implements Serializable {

    private List<ReferentialBean> codesPersonnes;
    private List<ReferentialBean> etatsCiviles ;
    private List<ReferentialBean> identifiantsResidentiels;
    private List<ReferentialBean> identifiantsProf;
    private List<ReferentialBean> identifiantsEntreprise;
    private List<ReferentialBean> professions ;
    private List<ReferentialBean> segments1 ;
    private List<ReferentialBean> segments2 ;
    private List<ReferentialBean> prospectionInfos;
    private List<ReferentialBean> banks ;
    /**Begin feature/ReferencialBeanList*/
    private List<ReferentialBean> sex;
    private List<ReferentialBean> supportFacturation;
    private List<ReferentialBean> langue;
    private List<ReferentialBean> countrys;
    private List<ReferentialBean> typeDistribution;
    private List<ReferentialBean> operateurWinBack;
    
    
    public List<ReferentialBean> getSex() {
		return sex;
	}

	public void setSex(List<ReferentialBean> sex) {
		this.sex = sex;
	}

	public List<ReferentialBean> getSupportFacturation() {
		return supportFacturation;
	}

	public void setSupportFacturation(List<ReferentialBean> supportFacturation) {
		this.supportFacturation = supportFacturation;
	}

	public List<ReferentialBean> getLangue() {
		return langue;
	}

	public void setLangue(List<ReferentialBean> langue) {
		this.langue = langue;
	}

	public List<ReferentialBean> getCountrys() {
		return countrys;
	}

	public void setCountrys(List<ReferentialBean> countrys) {
		this.countrys = countrys;
	}

	public List<ReferentialBean> getTypeDistribution() {
		return typeDistribution;
	}

	public void setTypeDistribution(List<ReferentialBean> typeDistribution) {
		this.typeDistribution = typeDistribution;
	}

	public List<ReferentialBean> getOperateurWinBack() {
		return operateurWinBack;
	}

	public void setOperateurWinBack(List<ReferentialBean> operateurWinBack) {
		this.operateurWinBack = operateurWinBack;
	}


	/**End feature/ReferencialBeanList*/
    public List<ReferentialBean> getCodesPersonnes() {
        return codesPersonnes;
    }

    public void setCodesPersonnes(List<ReferentialBean> codesPersonnes) {
        this.codesPersonnes = codesPersonnes;
    }

    public List<ReferentialBean> getEtatsCiviles() {
        return etatsCiviles;
    }

    public void setEtatsCiviles(List<ReferentialBean> etatsCiviles) {
        this.etatsCiviles = etatsCiviles;
    }

    public List<ReferentialBean> getIdentifiantsResidentiels() {
        return identifiantsResidentiels;
    }

    public void setIdentifiantsResidentiels(List<ReferentialBean> identifiantsResidentiels) {
        this.identifiantsResidentiels = identifiantsResidentiels;
    }

    public List<ReferentialBean> getIdentifiantsProf() {
        return identifiantsProf;
    }

    public void setIdentifiantsProf(List<ReferentialBean> identifiantsProf) {
        this.identifiantsProf = identifiantsProf;
    }

    public List<ReferentialBean> getIdentifiantsEntreprise() {
        return identifiantsEntreprise;
    }

    public void setIdentifiantsEntreprise(List<ReferentialBean> identifiantsEntreprise) {
        this.identifiantsEntreprise = identifiantsEntreprise;
    }

    public List<ReferentialBean> getProfessions() {
        return professions;
    }

    public void setProfessions(List<ReferentialBean> professions) {
        this.professions = professions;
    }

    public List<ReferentialBean> getSegments1() {
        return segments1;
    }

    public void setSegments1(List<ReferentialBean> segments1) {
        this.segments1 = segments1;
    }

    public List<ReferentialBean> getSegments2() {
        return segments2;
    }

    public void setSegments2(List<ReferentialBean> segments2) {
        this.segments2 = segments2;
    }

    public List<ReferentialBean> getProspectionInfos() {
        return prospectionInfos;
    }

    public void setProspectionInfos(List<ReferentialBean> prospectionInfos) {
        this.prospectionInfos = prospectionInfos;
    }

    public List<ReferentialBean> getBanks() {
        return banks;
    }

    public void setBanks(List<ReferentialBean> banks) {
        this.banks = banks;
    }
}
