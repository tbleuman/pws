package ma.iam.wiam.business;

import java.util.Date;
import java.util.List;

import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.param.criteria.BankAgencyCriteria;
import ma.iam.wiam.ws.neto.model.AppConfigSousCategorie;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;
import ma.iam.wiam.ws.neto.model.CataloguePrice;
import ma.iam.wiam.ws.neto.model.Ccu;
import ma.iam.wiam.ws.neto.model.CommofferInfoValue;
import ma.iam.wiam.ws.neto.model.Country;
import ma.iam.wiam.ws.neto.model.Vendeur;

public interface EntityManagerBiz {
	
	List<ValueChoiceItemIF> getSegments1() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getSegments2() throws FunctionnalException, TechnicalException;

	List<Object[]> getAllMTAgenciesInRegie() throws FunctionnalException, TechnicalException;

	List<ValueChoiceItemIF> getProspectionInfos() throws FunctionnalException, TechnicalException;

	List<Bank> getBanks() throws FunctionnalException, TechnicalException;

	List<BankAgency> getAgenciesByCriteria(BankAgencyCriteria bankAgencyCriteria) throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getPayerQualities() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getBillingCycles() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getPaymentTerms() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getBillingSupports() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getBillingLanguages() throws FunctionnalException, TechnicalException;
	
	//feature/24:Ajouter la methode ici
	String[] getSousCategory() throws FunctionnalException, TechnicalException;
	
	/**Begin feature/wsTools*/
	List<CommofferInfoValue> getListCommOfferValueByCode(String code)throws FunctionnalException, TechnicalException;
	/**Begin feature_Changement_Req_ACICGC*/
	List<Vendeur> getListACICGC(String agenceCode)throws FunctionnalException, TechnicalException;
	/**End feature_Changement_Req_ACICGC*/
	Ccu verifierExistanceCCU(String codeCCU)throws FunctionnalException, TechnicalException;
	
	List<CataloguePrice> getListeEquipementByOperationND (String NdLogin)throws FunctionnalException, TechnicalException;
	
	List<CataloguePrice> getListeEquipementByOperationNA_RP(String rateplanCode,String typeCommercial)throws FunctionnalException, TechnicalException;
	
	Boolean VerifierNonExistanceIdentifiant(String login , String serviceId)throws FunctionnalException, TechnicalException;
	
	Boolean VerifierNonExistanceLoginSoftSwitch(String login) throws FunctionnalException, TechnicalException;
	
	Boolean VerifierEquipementCompatibleND(String NdLogin,String ANetoId) throws FunctionnalException, TechnicalException;
	
	Date getEngagementContrat (String NdLogin)throws FunctionnalException, TechnicalException;
	/**End feature/wsTools*/
	
}
