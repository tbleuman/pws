package ma.iam.wiam.business.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.ContractManagerBiz;
import ma.iam.wiam.business.DirectCallHandler;
import ma.iam.wiam.business.OrderManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.business.constants.Constants;
import ma.iam.wiam.business.constants.ProjectProperties;
import ma.iam.wiam.contrat.ContractWIAM;
import ma.iam.wiam.contrat.ContratHandler;
import ma.iam.wiam.contrat.ContratHelper;
import ma.iam.wiam.customer.BillingAccountWIAM;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.encaissement.OrderMgr;
import ma.iam.wiam.encaissement.PaymentMgr;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.identification.IdentifiantMgr;
import ma.iam.wiam.neto.bean.CheckOrderResponseBean;
import ma.iam.wiam.neto.bean.CommandeBean;
import ma.iam.wiam.neto.bean.ErrorList;
import ma.iam.wiam.neto.bean.ParameterDescriptor;
import ma.iam.wiam.neto.bean.ServiceBean;
import ma.iam.wiam.neto.bean.ServiceRulesRequestBean;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.offre.CommercialOfferWIAM;
import ma.iam.wiam.services.ServiceHelper;
import ma.iam.wiam.services.regle.bean.ServiceRule;
import ma.iam.wiam.services.service.FeeCalcultatorHelper;
import ma.iam.wiam.util.StringUtil;
import ma.iam.wiam.util.Utils;
import ma.iam.wiam.util.WIAMConstants;
import ma.iam.wiam.util.WIAMException;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.util.WSValidator;
import ma.iam.wiam.util.directCall.DirectCallContext;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Client;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netonomy.blm.api.BillingAccount.BillingAccountF;
import com.netonomy.blm.api.Contract.ContractF;
import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.blm.api.utils.DescriptorOidIF;
import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.contract.ContractIF;
import com.netonomy.blm.interfaces.contract.ContractStatusIF;
import com.netonomy.blm.interfaces.offer.CommercialOfferIF;
import com.netonomy.blm.interfaces.request.RequestIF;
import com.netonomy.blm.interfaces.service.ContractedServiceIF;
import com.netonomy.blm.interfaces.service.RatePlanServiceIF;
import com.netonomy.blm.interfaces.service.ServiceIF;
import com.netonomy.blm.interfaces.util.ActionItemIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.blm.interfaces.util.PersistentActionMgrCategoryIF;
import com.netonomy.blm.interfaces.util.PersistentActionMgrIF;
import com.netonomy.util.ObjectId;

/**
 * 
 * @author s.hroumti
 *
 */
@Service
public class OrderManagerBizImpl implements OrderManagerBiz {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OrderManagerBizImpl.class);

	private static final SimpleDateFormat SIMPLE_DATE_SHORT_FORMAT = new SimpleDateFormat(
			ProjectProperties.SHORT_FORMATTER);

	private static final SimpleDateFormat SIMPLE_DATE_FULL_FORMAT = new SimpleDateFormat(
			ProjectProperties.FULL_FORMATTER);
	private final static String DIRECTCALL_TIME_OUT = "DIRECTCALL_TIME_OUT";

	@Value("#{config['ma.iam.wiam.directCallReply.period']}")
	private long directCallReplyPeriod;
	@Value("#{config['ma.iam.wiam.service.timeout']}")
	private long timeout;

	@Autowired
	private ServiceManagerBiz serviceManagerBiz;

	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;

	@Autowired
	private ClientDao clientDao;

	@Autowired
	private ReferentialManagerBiz referentialManagerBiz;

	@Autowired
	private LevelFDao levelFDao;

	@Autowired
	private ClientManagerBiz clientManagerBiz;

	@Autowired
	private ContractManagerBiz contractManagerBiz;

	@Autowired
	private DirectCallHandler directCallHandler;

	public CheckOrderResponseBean checkOrder(CommandeBean commandeBean)
			throws FunctionnalException, TechnicalException {
		if (commandeBean == null
				|| StringUtil.isNullOrEmpty(commandeBean.getNcli())
				|| StringUtil.isNullOrEmpty(commandeBean.getNd())) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_SEARCH_WS,
					"Les criteres de vérification sont vides");
		}

		ContractF contract = contractManagerBiz.getContract(
				commandeBean.getNcli(), commandeBean.getNd());

		baseNetoDao.getCurrentSession();

		CheckOrderResponseBean result = new CheckOrderResponseBean();
		initErrorList(result);
		ErrorList errorList = result.getErrorList();

		PersistentActionMgrIF order = findOrdersForLineNo(commandeBean.getNd(),
				commandeBean.getNcli());
		if (order != null) {
			ParameterIF[] params = order.getAdditionalParameters();
			String type = OrderMgr.getOrderTypeCode(params);
			try {
				String dictionaryStringValue = PaymentMgr
						.getDictionaryStringValue(PaymentMgr.ORDER_TYPE, type);
				errorList.getErrorMessages().add(
						"commande existante : "
								+ order.getIdentifier().getString() + " "
								+ dictionaryStringValue);
			} catch (SQLException e) {
				LOGGER.error(
						"Impossible de récupérer la description du type de la commande : {}",
						e);
				errorList.getErrorMessages().add(
						"commande existante : "
								+ order.getIdentifier().getString() + " "
								+ type);
			}
			result.setIsValid(false);
			return result;
		}

		if (commandeBean.getInfosCommande() != null) {
			checkDateDepot(commandeBean, errorList);
			checkDateEffet(commandeBean, errorList);
			checkComment(commandeBean, errorList);
		}

		checkServices(commandeBean, errorList);
		checkParameters(commandeBean, result, contract);

		result.setIsValid(errorList == null
				|| errorList.getErrorMessages() == null
				|| errorList.getErrorMessages().isEmpty());
		return result;
	}

	private void checkComment(CommandeBean commandeBean, ErrorList errorList) {
		if (commandeBean.getInfosCommande().getCommentaire() != null
				&& commandeBean.getInfosCommande().getCommentaire().length() > 100) {
			errorList
					.getErrorMessages()
					.add("Le champ \"Commentaire\" n'est pas conforme au format standard et doit contenir moins de "
							+ Constants.COMMENT_MAX_SIZE);
		}
	}

	private void checkServices(CommandeBean commandeBean, ErrorList errorList)
			throws FunctionnalException, TechnicalException {
		if (commandeBean.getServicesToAdd() != null
				|| commandeBean.getServicesToModify() != null
				|| commandeBean.getServicesToRemove() != null) {
			ServiceRulesRequestBean serviceRulesRequestBean = new ServiceRulesRequestBean();
			serviceRulesRequestBean.setNcli(commandeBean.getNcli());
			serviceRulesRequestBean.setNd(commandeBean.getNd());
			serviceRulesRequestBean.setServicesToAdd(commandeBean
					.getServicesToAdd());
			serviceRulesRequestBean.setServicesToModify(commandeBean
					.getServicesToModify());
			serviceRulesRequestBean.setServicesToRemove(commandeBean
					.getServicesToRemove());
			List<ServiceRule> serviceRules = serviceManagerBiz
					.getServiceRules(serviceRulesRequestBean);
			if (serviceRules != null && !serviceRules.isEmpty()) {
				for (ServiceRule serviceRule : serviceRules) {
					errorList.getErrorMessages().add(
							serviceRule.getMessageRule());
				}
			}

		}
	}

	private void checkParameters(CommandeBean commandeBean,
			CheckOrderResponseBean checkOrderResponseBean, ContractF contract)
			throws FunctionnalException, TechnicalException {

		ActionMgrIF actionMgr = ObjectMgr.createOrder(null);

		if (commandeBean.getServicesToModify() != null
				&& commandeBean.getServicesToModify().getServices() != null) {
			for (ServiceBean serviceBean : commandeBean.getServicesToModify()
					.getServices()) {

				ContractedServiceIF serv = contract.getContractedService(
						actionMgr,
						ObjectId.instantiate(serviceBean.getServiceId()));
				compareAllParameters(serv.getParameters(),
						serviceBean.getParameters(), checkOrderResponseBean);
			}
		}
	}

	private void checkDateEffet(CommandeBean commandeBean, ErrorList errorList) {
		if (commandeBean.getInfosCommande().getDateDepot() != null) {
			try {
				Date dateEffet = WSValidator.stringToDate(commandeBean
						.getInfosCommande().getDateEffet(),
						ProjectProperties.SHORT_FORMATTER);

				if (DateUtils.truncatedCompareTo(dateEffet, new Date(),
						Calendar.DAY_OF_MONTH) < 0) {

					errorList.getErrorMessages().add(
							"Le champ \"Date effet\" doit être une date valide après "
									+ SIMPLE_DATE_SHORT_FORMAT
											.format(new Date()) + ".");
				}
			} catch (ParseException e) {
				errorList.getErrorMessages().add(
						"Le champ \"Date depot\" doit être une date valide .");
			}
		}
	}

	private void checkDateDepot(CommandeBean commandeBean, ErrorList errorList) {
		if (commandeBean.getInfosCommande().getDateDepot() != null) {

			try {
				Date dateDepot = WSValidator.stringToDate(commandeBean
						.getInfosCommande().getDateDepot(),
						ProjectProperties.SHORT_FORMATTER);

				if (DateUtils.truncatedCompareTo(dateDepot, new Date(),
						Calendar.DAY_OF_MONTH) > 0) {

					errorList.getErrorMessages().add(
							"Le champ \"Date depot\" doit être une date valide entre 01/01/1900 et "
									+ SIMPLE_DATE_SHORT_FORMAT
											.format(new Date()) + ".");
				}
			} catch (ParseException e) {
				errorList.getErrorMessages().add(
						"Le champ \"Date depot\" doit être une date valide .");
			}
		}
	}

	/**
	 * find and return not cancelled order for lineNO and orgRef
	 * 
	 * @param lineNo
	 * @param orgRef
	 * @return
	 * @throws TechnicalException
	 * @throws SQLException
	 */
	private PersistentActionMgrIF findOrdersForLineNo(String lineNo,
			String orgRef) throws TechnicalException {

		LOGGER.debug("checking for orders for line no {} : orgRef : {}",
				lineNo, orgRef);

		// checking for orders
		PersistentActionMgrIF[] order;
		try {
			order = OrderMgr.getOrderByLineNumberFast(lineNo, orgRef);
			if (order != null && order.length > 0) {
				for (int i = 0; i < order.length; i++) {
					ParameterIF[] params = order[i].getAdditionalParameters();
					String statusId = OrderMgr.getOrderStatusCode(params);
					String type = OrderMgr.getOrderTypeCode(params);

					if (statusId != null
							&& !statusId.equals(OrderMgr.ORDER_STATUS_ANNULE)
							&& type != null) {
						return order[i];
					}

				}
			}
			return null;
		} catch (SQLException e) {
			LOGGER.error(
					"cannot check the existence of order for  line no {} : orgRef : {}",
					lineNo, orgRef);
			LOGGER.error("error detail : {}", e);
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"Cannot check the existence of order");
		}
	}

	private float getCartFee(ActionMgrIF actionMgr, LevelF level) {

		BillingAccountF[] billing = level.getBillingAccounts();

		boolean bExempte = false;
		if (billing != null && billing.length > 0) {
			ParameterIF[] params = billing[0].getAdditionalParameters();
			Boolean exempteFlag = BillingAccountWIAM.isExemptionTaxe(params);
			if (exempteFlag != null)
				bExempte = exempteFlag.booleanValue();
		}

		float priceTax = 0;
		float price = (float) FeeCalcultatorHelper.calculateCartFee(
				ActionMgrIF.FEE_SUBSCRIPTION, actionMgr);
		LOGGER.debug("CART FEE (HT) :" + price);
		if (!bExempte)
			priceTax = (float) FeeCalcultatorHelper.calculateCartFeeTax(
					ActionMgrIF.FEE_SUBSCRIPTION, actionMgr);
		price += priceTax;

		price = Float.parseFloat(WIAMHelper.floatToString(new Float(price)));

		LOGGER.debug("CART TAX :" + priceTax);
		LOGGER.debug("CART FEE (TTC):" + price);
		return price;
	}

	public CheckOrderResponseBean createOrder(CommandeBean commandeBean,
			String login) throws FunctionnalException, TechnicalException {

		CheckOrderResponseBean checkOrderResponseBean = checkOrder(commandeBean);
//		 CheckOrderResponseBean checkOrderResponseBean = new
//		 CheckOrderResponseBean();
//		 checkOrderResponseBean.setIsValid(true);
		if (checkOrderResponseBean != null
				&& !Boolean.TRUE.equals(checkOrderResponseBean.getIsValid())) {
			return checkOrderResponseBean;
		}
		checkOrderResponseBean.setIsValid(true);
		initErrorList(checkOrderResponseBean);
		Client client = clientManagerBiz.getClient(commandeBean.getNcli());
		ContractF contract = contractManagerBiz.getContract(client,
				commandeBean.getNd());

		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());

		ActionMgrIF actionMgr = ObjectMgr.createOrder(null);

		addServices(commandeBean, contract, actionMgr);
		removeServices(commandeBean, contract, actionMgr);
		modifyServices(commandeBean, checkOrderResponseBean, contract,
				actionMgr);

		if (!checkOrderResponseBean.getIsValid()) {
			return checkOrderResponseBean;
		}

		String orderType = OrderMgr.ORDER_TYPE_MODIF_SERVICE;

		// set additional parameters
		String operationId = "9";
		String commentaire = commandeBean.getInfosCommande().getCommentaire();
		String dateEffet = commandeBean.getInfosCommande().getDateEffet();
		String dateDepot = commandeBean.getInfosCommande().getDateDepot();
		Vector paramsVect = initOrderParamsVector(commandeBean, contract, orgF,
				orderType, operationId, dateDepot, dateEffet, commentaire);

		actionMgr.setOptParameters(WIAMHelper.vectorToParameter(paramsVect));

		// String accessType =
		// CommercialOfferWIAM.getAccessTypeInfo(ObjectRefMgr
		// .getRatePlanService(ObjectId.instantiate(serviceId))
		// .getRatePlan().getCode());
		String accessType;
		try {
			accessType = CommercialOfferWIAM.getAccessTypeInfo(contract
					.getMyRatePlan().getCode());

			PersistentActionMgrIF order = saveOrder(actionMgr, orderType, null,
					contract.getPhoneNumber(), accessType, orgF, login);
			checkOrderResponseBean
					.setOrderId(order.getIdentifier().getString());
			DirectCallContext ctx = directCall(client, contract, actionMgr,
					checkOrderResponseBean, login);
			// if (Boolean.TRUE.equals(checkOrderResponseBean.getIsValid())) {
			onDirectCallResponse(client, contract, actionMgr, ctx, login);
			// }
			return checkOrderResponseBean;
		} catch (SQLException e) {
			LOGGER.error("Error in createOrder : ", e);
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		} catch (Throwable e) {
			LOGGER.error("Error in createOrder : ", e);
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	private void modifyServices(CommandeBean commandeBean,
			CheckOrderResponseBean checkOrderResponseBean, ContractF contract,
			ActionMgrIF actionMgr) throws FunctionnalException,
			TechnicalException {
		if (commandeBean.getServicesToModify() != null
				&& commandeBean.getServicesToModify().getServices() != null) {
			for (ServiceBean serviceBean : commandeBean.getServicesToModify()
					.getServices()) {

				ContractedServiceIF serv = contract.getContractedService(
						actionMgr,
						ObjectId.instantiate(serviceBean.getServiceId()));

				ParameterIF[] parameters = serv.getParameters();
				for (ParameterIF parameterIF : parameters) {
					WIAMHelper.setParameter(
							parameterIF,
							getRequestedParamValue(parameterIF
									.getParameterDescriptor().getCode(),
									serviceBean.getParameters()));
				}
				contract.modifyService(actionMgr, serv, parameters,
						new ParameterIF[] {});
				try {
					ContratHandler.deleteSRVRemiseTBKF(actionMgr, contract,
							null, null, null, null);
					// ContratHandler.addNetWorkService(contract, actionMgr, new
					// HttpRequestHandlerServlet(),
					// jspHelper);
					// ContratHandler.addRemiseInfinifix(actionMgr, contract,
					// null,
					// session, request, response, jspHelper, errors);
				} catch (Throwable e) {
					LOGGER.error(
							"Cannot deleteSRVRemiseTBKF service : {} : {}",
							serv.getCode(), e);
					throw new TechnicalException(
							ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
				}

			}
		}
	}

	private void removeServices(CommandeBean commandeBean, ContractF contract,
			ActionMgrIF actionMgr) {
		if (commandeBean.getServicesToRemove() != null
				&& commandeBean.getServicesToRemove().getServices() != null) {
			for (ServiceBean serviceBean : commandeBean.getServicesToRemove()
					.getServices()) {

				ContractedServiceIF serv = contract.getContractedService(
						actionMgr,
						ObjectId.instantiate(serviceBean.getServiceId()));
				if (!ServiceHelper.getImpactedServicesInActionMgrItems(
						actionMgr, serv.getService().getCode().toString()))
					contract.removeService(actionMgr, serv,
							serv.getDefaultParameters());
			}
		}
	}

	private void addServices(CommandeBean commandeBean, ContractF contract,
			ActionMgrIF actionMgr) {
		if (commandeBean.getServicesToAdd() != null
				&& commandeBean.getServicesToAdd().getServices() != null) {
			for (ServiceBean serviceBean : commandeBean.getServicesToAdd()
					.getServices()) {
				RatePlanServiceIF ratePlanService = ObjectRefMgr
						.getRatePlanService(ObjectId.instantiate(serviceBean
								.getServiceId()));

				contract.addService(actionMgr, ratePlanService,
						new ParameterIF[] {}, 1,
						CommercialOfferIF.TYPE_PRIORITY_TO_DEDICATED,
						new ParameterIF[] {});
			}
		}
	}

	private void initErrorList(CheckOrderResponseBean checkOrderResponseBean) {
		if (checkOrderResponseBean.getErrorList() == null) {
			checkOrderResponseBean.setErrorList(new ErrorList());
		}
		if (checkOrderResponseBean.getErrorList().getErrorMessages() == null) {
			checkOrderResponseBean.getErrorList().setErrorMessages(
					new ArrayList<String>());
		}
	}

	private boolean compareAllParameters(ParameterIF[] parameters,
			List<ParameterDescriptor> requestedParams,
			CheckOrderResponseBean checkOrderResponseBean)
			throws FunctionnalException, TechnicalException {
		if (parameters == null || requestedParams == null) {
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean.getErrorList().getErrorMessages()
					.add("Nombre de paramètres reçus n'est pas correcte.");
			return false;
		}

		if (parameters.length != requestedParams.size()) {
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean.getErrorList().getErrorMessages()
					.add("Nombre de paramètres reçus n'est pas correcte.");
			return false;
		}

		Boolean isParamChanged = false;
		for (ParameterDescriptor parameterDescriptor : requestedParams) {
			int indexOf = getParameterIndex(parameters,
					parameterDescriptor.getCode());
			if (indexOf < 0) {
				checkOrderResponseBean.setIsValid(false);
				checkOrderResponseBean
						.getErrorList()
						.getErrorMessages()
						.add("Impossible de trouver le paramètre "
								+ parameterDescriptor.getCode());
				return false;
			}

			try {
				String oldValue = WIAMHelper.getWIAMParameterValues(parameters,
						indexOf, false);
				String newValue = parameterDescriptor.getValue();
				if (!StringUtils.equals(oldValue, newValue)) {
					isParamChanged = true;
				}
			} catch (IOException e) {
				LOGGER.error("ERROR WHILE compareAllParameters : {}", e);
				throw new TechnicalException(
						ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			} catch (SQLException e) {
				LOGGER.error("ERROR WHILE compareAllParameters : {}", e);
				throw new TechnicalException(
						ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			}

		}

		if (!isParamChanged) {
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean.getErrorList().getErrorMessages()
					.add("Aucun paramètre n'a été modifié.");
			return false;
		}
		return true;
	}

	private int getParameterIndex(ParameterIF[] parameters, String code) {
		LOGGER.debug("search param index for " + code);
		int i = 0;
		for (ParameterIF parameterIF : parameters) {
			LOGGER.debug("Test with :"
					+ parameterIF.getParameterDescriptor().getCode()
					+ "##"
					+ parameterIF.getParameterDescriptor().getIdentifier()
							.toString());
			if (parameterIF.getParameterDescriptor().getCode().equals(code)) {
				return i;
			}
			i++;
		}
		return -1;
	}

	private PersistentActionMgrIF saveOrder(ActionMgrIF actionMgr,
			String orderType, String orderContext, String lineNo,
			String accessType, LevelF level, String login) throws Throwable {

		PersistentActionMgrIF order = actionMgr.getBackupPersistentActionMgr();
		String orderStatusCode = null;
		String historyEventComment;
		if (order == null) {

			ParameterIF[] actionMgrParams = actionMgr.getOptParameters(null);
			Integer opID = OrderMgr.getOrderOperationId(actionMgrParams);
			Integer rpID = OrderMgr.getOrderRatePlanId(actionMgrParams);
			Integer ptID = OrderMgr.getOrderTypeProduitId(actionMgrParams);
			String agenceCode = OrderMgr
					.getOrderAgenceGestionnaire(actionMgrParams);

			String orgReferenceID = level.getReferenceId();
			if (agenceCode == null) {
				agenceCode = orgReferenceID;
			}

			order = ObjectMgr.createPersistentActionManager();
			PersistentActionMgrCategoryIF persistentActionMgrCat = ObjectRefMgr
					.getPersistentActionMgrCategoryByCode("CORE_BACKUP");
			order.setCategory(persistentActionMgrCat);

			// generate a unique id corresponding to the command
			String comOrderId = new Integer(
					ContratHelper.getNumCommandComercialSeq()).toString();
			order.setName(comOrderId);

			if (orgReferenceID != null) {
				order.setDescription(orgReferenceID);
			}
			if (accessType != null
					&& (accessType.equals("CVP") || accessType.equals("CTXIP")
							|| accessType.equals("PCTXIP") || accessType
								.equals("CVPINT"))) {
				orderStatusCode = OrderMgr.ORDER_STATUS_REALISABLE;
			} else {
				orderStatusCode = OrderMgr.ORDER_STATUS_EN_COURS;
			}

			Vector vectParams = new Vector();
			OrderMgr.setOrderStatusCode(vectParams, orderStatusCode);
			OrderMgr.setOrderTypeCode(vectParams, orderType);
			if (orderContext == null) {
				orderContext = orderType;
			}
			OrderMgr.setOrderContext(vectParams, orderContext);
			if (opID != null)
				OrderMgr.setOrderOperationId(vectParams, String.valueOf(opID));
			if (rpID != null)
				OrderMgr.setOrderRatePlanId(vectParams, String.valueOf(rpID));
			if (ptID != null)
				OrderMgr.setOrderTypeProduitId(vectParams, String.valueOf(ptID));
			OrderMgr.setOrderAgenceGestionnaire(vectParams, agenceCode);

			OrderMgr.setOrderStatusDate(vectParams,
					SIMPLE_DATE_FULL_FORMAT.format(order.getCreationDate()));
			if (lineNo != null)
				OrderMgr.setOrderLineNumber(vectParams, lineNo);

			// float
			// price=(float)FeeCalcultatorHelper.calculateCartFee(ActionMgrIF.FEE_SUBSCRIPTION,actionMgr);
			float price = getCartFee(actionMgr, level);
			String priceString = null;
			String paidFlag = null;
			if (price >= 0) {
				priceString = String.valueOf(price);
				paidFlag = "false";
			}
			OrderMgr.setOrderTotalAmount(vectParams, priceString);
			// OrderMgr.setOrderPaid(vectParams,paidFlag);

			ParameterIF[] paramsOrder = WIAMHelper
					.vectorToParameter(vectParams);
			order.setAdditionalParameters(paramsOrder);//OrderMgr.getOrderComment(actionMgr.getOptParameters(null))

			historyEventComment = "Nouvelle demande";

		} else {
			ParameterIF[] orderParameters = order.getAdditionalParameters();
			orderStatusCode = OrderMgr.getOrderStatusCode(orderParameters);
			if (OrderMgr.ORDER_STATUS_REALISABLE.equals(orderStatusCode)) {
				Vector paramsVect = WIAMHelper
						.parameterToVector(orderParameters);
				OrderMgr.setOrderStatusCode(paramsVect,
						OrderMgr.ORDER_STATUS_REALISABLE_MODIFIED);
				order.setAdditionalParameters(WIAMHelper
						.vectorToParameter(paramsVect));
			}
			historyEventComment = "Demande modifie";
		}

		LOGGER.debug("Persisting order: " + order.getIdentifier().toString()
				+ " login: " + login);
		//OrderMgr.getOrderComment(paramsOrder);
		if(true){
			throw new TechnicalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		order.persist(actionMgr);
		actionMgr.setBackupPersistentActionMgr(order);
		OrderMgr.addOrderHistoryEvent(order.getIdentifier().getString(),
				orderStatusCode, login, historyEventComment);

		return order;
	}

	private Vector initOrderParamsVector(CommandeBean commandeBean,
			ContractF contract, LevelF orgF, String orderType,
			String operationId, String dateDepot, String dateEffet,
			String commentaire) {
		Vector paramsVect = new Vector();
		String rateplanId = contract.getMyRatePlan().getIdentifier()
				.getString();
		String productTypeId = contract.getContractType().getIdentifier()
				.getString();
		// String orderContext;
		//
		// if (orderContext == null)
		// orderContext = orderType;

		if (orderType != null) {
			OrderMgr.setOrderTypeCode(paramsVect, orderType);
		}
		if (operationId != null) {
			OrderMgr.setOrderOperationId(paramsVect, operationId);
		}
		if (rateplanId != null) {
			OrderMgr.setOrderRatePlanId(paramsVect, rateplanId);
		}
		if (productTypeId != null) {
			OrderMgr.setOrderTypeProduitId(paramsVect, productTypeId);
		}
		// the agence of the user that created the order
		OrderMgr.setOrderAgenceGestionnaire(paramsVect, orgF.getReferenceId());

		if (dateEffet != null) {
			OrderMgr.setDateDebutOrder(paramsVect, dateEffet);
		}

		if (dateDepot != null) {
			OrderMgr.setDateDepotDossier(paramsVect, dateDepot);
		}

		// if(dateFin!=null)
		// OrderMgr.setDateFinOrder(paramsVect,dateFin);
		//
		// if(extremite!=null)
		// OrderMgr.setOrderAdresseExtrem(paramsVect,extremite);

		if (commentaire != null) {
			OrderMgr.setOrderComment(paramsVect, commentaire);
		}
		return paramsVect;
	}

	private DirectCallContext directCall(Client client, ContractIF contract,
			ActionMgrIF actionMgr,
			CheckOrderResponseBean checkOrderResponseBean, String login)
			throws Throwable {

		String accessType = CommercialOfferWIAM.getAccessTypeInfo(contract
				.getMyRatePlan().getCode());

		PersistentActionMgrIF order = actionMgr.getBackupPersistentActionMgr();
		ParameterIF[] orderParameters = null;
		if (order != null) {
			orderParameters = order.getAdditionalParameters();
		} else {
			orderParameters = actionMgr.getOptParameters(null);
		}

		String orderTypeCode = OrderMgr.getOrderTypeCode(orderParameters);
		String orderContext = OrderMgr.getOrderContext(orderParameters);
		if (orderContext == null) {
			orderContext = orderTypeCode;
		}
		String reqOrderStatus = null;

		reqOrderStatus = OrderMgr.ORDER_STATUS_REALISABLE;

		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());
		Hashtable<String, Object> orderParams = getOrderInfoSCForCall(
				actionMgr, false);

		orderParams.put("accessType", accessType);

		orderParams.put("agentId", login);

		// YFI FC5033 Begin
		String typeOffre = CommercialOfferWIAM.getRateplanInfo(null, contract
				.getMyRatePlan().getCode(), "RTP_FIBRE_OPTIQUE");
		if ("1".equals(typeOffre)) {
			orderParams.put("type_offre_Spec", "FO");
		}
		String typeOffre_cloud = CommercialOfferWIAM.getRateplanInfo(null,
				contract.getMyRatePlan().getCode(), "FLAG_RTP_CLOUD");
		if ("1".equals(typeOffre_cloud)) {
			orderParams.put("type_offre_Spec", "CLOUD");
			orderParams.put("organisation_id", orgF.getLegacyIdentifier()
					.getString());
			// TODO
			// String adressemail = CloudDNSMgr.getAdresseMailClient(orgF
			// .getLegacyIdentifier().getString());
			// if (adressemail == null)
			// adressemail = "";
			// orderParams.put("prerequis_EMail", adressemail);
		}

		setAddressParams(contract, orderParams);

		// test for temporary
		ParameterIF[] contractParams = ((ContractF) contract)
				.getAdditionalParameters();
		Boolean bTemp = ContractWIAM.isTemporaireContrat(contractParams);
		if (bTemp != null && bTemp.equals(Boolean.TRUE)) {
			Date dateEnd = OrderMgr.getDateFinOrder(contractParams);
			if (dateEnd != null) {
				orderParams.put("dateResTemp",
						SIMPLE_DATE_SHORT_FORMAT.format(dateEnd));
			}
		}

		if (orderTypeCode.equals(OrderMgr.ORDER_TYPE_MODIF_SERVICE)) {
			ActionItemIF[] itemAddService = actionMgr.findActionItemsByAction(
					DescriptorOidIF.ACTION_ADD_SERVICE, true);
			if (itemAddService != null) {
				for (int i = 0; i < itemAddService.length; i++) {
					ServiceIF service = null;
					service = ((RequestIF.AddService) (itemAddService[i]
							.getRequest().getAdditionalParameter()))
							.getRatePlanService().getService();
					String serviceRemise = CommercialOfferWIAM.getServiceInfo(
							null, null, service.getCode().toString(),
							"SVC_INITIAL_ENG_FLAG");
					if ("1".equals(serviceRemise)) {
						Vector contractParamsVect = WIAMHelper
								.parameterToVector(contractParams);
						ContractWIAM.setDureeEngagementDate(contractParamsVect,
								"");
						((ContractF) contract).modifyContract(actionMgr,
								ContractIF.LEVEL_ITEM.charValue(),
								((ContractF) contract).getLevel(), WIAMHelper
										.vectorToParameter(contractParamsVect));
					}
				}
			}
		}

		DirectCallContext ctx = makeDirectCall(actionMgr, contract, orgF,
				orderTypeCode, orderContext, reqOrderStatus, orderParams,
				order, login);

		String directCallReply;
		try {
			directCallReply = directCallHandler.handle(ctx);
		} catch (TimeoutException e) {
			directCallReply = DIRECTCALL_TIME_OUT;
		}

		checkDireclCallResponse(directCallReply, ctx, checkOrderResponseBean);

		return ctx;
	}

	private void checkDireclCallResponse(String directCallReply,
			DirectCallContext ctx, CheckOrderResponseBean checkOrderResponseBean) {
		if (directCallReply.equals(DirectCallHandler.DIRECTCALL_REPLY_ERROR)) {
			// Return Direct No-Reply Error
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean
					.getErrorList()
					.getErrorMessages()
					.add("Problème au niveau du module Wimtech, veuillez contactez l'administrateur."
							+ "102|" + directCallReply);
			return;
		} else if (directCallReply.equals(DIRECTCALL_TIME_OUT)) {
			// Return Direct No-Reply Error
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean
					.getErrorList()
					.getErrorMessages()
					.add("Le module Wimtech ne repond pas dans le temps, veuillez contactez l'administrateur.");
			return;
		}

		Hashtable reply = ctx.getReply();
		LOGGER.debug("GETTING REPLY  = " + reply);

		String replyMap = "request:dataarea:doaddorder";
		String status = (String) Utils.getObjectFromMap(reply, replyMap
				+ ":return_status");

		LOGGER.debug("direct call status = " + status);

		if (status == null) {
			// Pas de Reponse
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean.getErrorList().getErrorMessages()
					.add("101|" + "Reponse Status is null");
			return;

		} else if (status.equals("0")) {
			// Reponse OK
			String etatDem = "";
			etatDem = (String) Utils.getObjectFromMap(reply,
					"request:dataarea:doaddorder:demande:etat_demande");
			checkOrderResponseBean.setIsValid(true);
			checkOrderResponseBean.setOrderStatus("0|" + etatDem);
			// return "0|" + etatDem;
			return;
		} else if (status.equals("1")) {
			// Reponse KO => Erreur
			String errorDesc = "";
			String errorCode = "";
			errorCode = (String) Utils.getObjectFromMap(reply,
					"request:dataarea:doaddorder:error:error_code");
			errorDesc = (String) Utils.getObjectFromMap(reply,
					"request:dataarea:doaddorder:error:error_description");
			if (errorCode == null)
				errorCode = "";
			if (errorDesc == null)
				errorDesc = "";
			String errorMsg = "Erreur code:" + errorCode + " " + errorDesc;
			LOGGER.debug("Direct Call Reply Error = " + errorMsg);

			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean.getErrorList().getErrorMessages()
					.add(errorMsg);
			// ********************************************************************
			// 7 Commande sur le contrat déjà en cours
			// if (errorCode.equals("20102")) {
			// return "20102|" + errorMsg;
			// }
			// if (errorMsg.indexOf("Dossier suspendu ou restreint") > 0)
			// return "12|" + errorMsg;
			// if (errorMsg.indexOf("Aucun dossier en service") > 0)
			// return "13|" + errorMsg;
			// if ((errorMsg
			// .indexOf("demande DN ou MS+DN en cours pour le meme ND") > 0)
			// || (errorMsg.indexOf("demande MS en cours pour le meme ND") > 0))
			// return "7|" + errorMsg;
			//
			// return "101|" + errorMsg;
		} else {
			checkOrderResponseBean.setIsValid(false);
			checkOrderResponseBean.getErrorList().getErrorMessages()
					.add("101|No Valid Reply Received");
			return;
		}

	}

	private void setAddressParams(ContractIF contract, Hashtable orderParams)
			throws SQLException, WIAMException {
		ParameterIF[] optionParameters = null;
		optionParameters = ((ContractF) contract).getAdditionalParameters();
		String commune = ContractWIAM.getAdrInstCodeCommuneA(optionParameters);
		String codePostal = null;

		// TODO decommenter ces lignes
		// if (commune != null)
		// codePostal = AdresseMgr.getCommuneByCode(commune).getCodePostale();
		// if (codePostal != null)
		// orderParams.put("code_postal", codePostal);
		String quartier = ContractWIAM
				.getAdrInstCodeQuartierA(optionParameters);
		if (quartier != null)
			orderParams.put("quartier", quartier);
		String quartierNonCodifiee = ContractWIAM
				.getAdrInstQuartierNameA(optionParameters);
		if (quartierNonCodifiee != null)
			orderParams.put("Quartier_non_codifiee", quartierNonCodifiee);
		String voie = ContractWIAM.getAdrInstCodeVoieA(optionParameters);
		if (voie != null)
			orderParams.put("voie", voie);
		String voieNonCodifie = ContractWIAM
				.getAdrInstVoieNameA(optionParameters);
		if (voieNonCodifie != null)
			orderParams.put("Voie_non_codifie", voieNonCodifie);
		String voieNumero = ContractWIAM.getAdrInstNumVoieA(optionParameters);
		if (voieNumero != null)
			orderParams.put("numVoie", voieNumero);

		String batiment = ContractWIAM.getAdrInstBatimentA(optionParameters);
		if (batiment != null)
			orderParams.put("batiment", batiment);
		String escalier = ContractWIAM.getAdrInstEscalierA(optionParameters);
		if (escalier != null)
			orderParams.put("escalier", escalier);
		String etage = ContractWIAM.getAdrInstEtageA(optionParameters);
		if (etage != null)
			orderParams.put("etage", etage);
		String porte = ContractWIAM.getAdrInstPorteA(optionParameters);
		if (porte != null)
			orderParams.put("porte", porte);
	}

	private Hashtable<String, Object> getOrderInfoSCForCall(
			ActionMgrIF actionMgr, boolean fromOrder) {
		Hashtable<String, Object> res = new Hashtable<String, Object>();

		ParameterIF[] params = null;
		PersistentActionMgrIF order = actionMgr.getBackupPersistentActionMgr();
		if (order == null || !fromOrder) {
			params = actionMgr.getOptParameters(null);
		} else {
			params = order.getAdditionalParameters();
		}

		Date dateDepot = OrderMgr.getDateDepotDossier(params);
		Date dateDebut = OrderMgr.getDateDebutOrder(params);
		Date dateFin = OrderMgr.getDateFinOrder(params);

		Date currentDate = new Date();
		res.put("date_status", SIMPLE_DATE_SHORT_FORMAT.format(currentDate));
		res.put("date_enregist", SIMPLE_DATE_SHORT_FORMAT.format(currentDate));

		if (dateDepot != null) {
			res.put("date_depot", SIMPLE_DATE_SHORT_FORMAT.format(dateDepot));
		}
		if (dateDebut != null) {
			res.put("date_effet", SIMPLE_DATE_SHORT_FORMAT.format(dateDebut));
		}
		if (dateFin != null) {
			res.put("date_fin", SIMPLE_DATE_SHORT_FORMAT.format(dateFin));
		}

		String comment = OrderMgr.getOrderComment(params);
		if (comment != null) {
			res.put("comment", comment);
		}

		Boolean minIla = OrderMgr.isOrderADT(params);
		if (minIla != null) {
			if (minIla.equals(Boolean.TRUE)) {
				res.put("min_ila", "true");
			} else {
				res.put("min_ila", "false");
			}
		}

		String dem2 = OrderMgr.getOrderNDem2(params);
		if (dem2 != null) {
			res.put("dem2", dem2);
		}
		String dem3 = OrderMgr.getOrderNDem3(params);
		if (dem3 != null) {
			res.put("dem3", dem3);
		}

		ActionItemIF[] ai = actionMgr.findActionItemsByAction(
				DescriptorOidIF.ACTION_MODIFY_CONTRACTSTATUS, true);
		if (ai != null && ai.length > 0) {

			ContractStatusIF status = ((RequestIF.ModifyContractStatus) ai[0]
					.getRequest().getAdditionalParameter()).getContractStatus();
			if (status != null) {
				res.put("raison", status.getLegacyIdentifier().getString());
			}

		}
		return res;
	}

	private DirectCallContext makeDirectCall(ActionMgrIF actionMgr,
			ContractIF contract, LevelF orgF, String orderType,
			String orderContext, String orderReqStatus,
			Hashtable<String, Object> orderParams, PersistentActionMgrIF order,
			String login) throws Throwable {

		LOGGER.debug("makeDirectCall");

		String pamId = ObjectId.convertToLong(order.getIdentifier()).toString();
		String pamId2 = null;
		String pamId3 = null;

		if (orderType.equals(OrderMgr.ORDER_TYPE_MODIF_SERVICE)) {
			// FC5340
			// ActionMgrIF actionMgr = getCurrentActionMgr (request, response,
			// jspHelper);
			String ratePCode = null;
			String gponFlag = CommercialOfferWIAM.getRateplanInfo(null,
					ratePCode, "RTP_FIBRE_OPTIQUE");
			if ((gponFlag == null || "".equals(gponFlag))
					&& !"INT00".equals(ratePCode) && !"INT10".equals(ratePCode)) {

				ParameterIF[] paramsOrder = order.getAdditionalParameters();
				Vector paramOrder = WIAMHelper.parameterToVector(paramsOrder);
				pamId2 = OrderMgr.getOrderNDem2(paramsOrder);
				pamId3 = OrderMgr.getOrderNDem3(paramsOrder);
				boolean bUpdate = false;
				if (pamId2 == null) {

					pamId2 = ObjectId.convertToLong(
							ObjectMgr.createPersistentActionManager()
									.getIdentifier()).toString();
					OrderMgr.setOrderNDem2(paramOrder, pamId2);
					bUpdate = true;
				}

				if (pamId3 == null) {
					pamId3 = ObjectId.convertToLong(
							ObjectMgr.createPersistentActionManager()
									.getIdentifier()).toString();
					OrderMgr.setOrderNDem3(paramOrder, pamId3);
					bUpdate = true;
				}
				if (bUpdate) {
					LOGGER.debug("Updating order: "
							+ order.getIdentifier().toString() + " login: "
							+ login);
					ParameterIF[] paramOrders = WIAMHelper
							.vectorToParameter(paramOrder);
					order.setAdditionalParameters(paramOrders);
					order.update();
				}

				orderParams.put("dem2", pamId2);
				orderParams.put("dem3", pamId3);
			} else {
				ParameterIF[] paramsOrder = order.getAdditionalParameters();
				pamId2 = OrderMgr.getComTelSupport(paramsOrder);
				if (pamId2 != null && !"".equals(pamId2)) {
					orderParams.put("dem2", pamId2);
					orderParams.put("dem3", "AVEC");
				}
			}
		}

		boolean modifyOrder = true;
		String orderStatus = getOrderStatus(order);
		if (orderStatus != null
				&& orderStatus.equals(OrderMgr.ORDER_STATUS_EN_COURS)) {
			modifyOrder = false;
		}
		// String modif_order = (String) baseNetoDao.getCurrentSession()
		// .getAttribute("modifyOrder");
		// if (modif_order != null && !"".equals(modif_order)) {
		// modifyOrder = true;
		// }

		String catagorie = orgF.getType().getIdentifier().getString();
		String legacyId = orgF.getHierarchyRoot().getLegacyIdentifier()
				.getString();

		Hashtable input = createDirectCallHashtable(actionMgr, orderType,
				orderParams, login, legacyId + "|" + catagorie,
				contract.getPhoneNumber(), orderReqStatus, pamId, modifyOrder);

		// configuration of the direct call
		// set the parameters for the direct call
		String requestQ = "MQSERIES_QUEUE_07";
		String replyQ = "MQSERIES_QUEUE_08";
		String redir = "ON_DIRECTCALL_OK";
		String errRedir = "ON_DIRECTCALL_OK";

		String topic = null;
		// FC 6066
		if (!modifyOrder)
			topic = "REQADDORDER";
		else
			topic = "REQMODIFYORDER";

		DirectCallContext ctx = new DirectCallContext(requestQ, replyQ, topic,
				input, redir, errRedir);

		return ctx;
	}

	private Hashtable createDirectCallHashtable(ActionMgrIF actionMgr,
			String operation, Hashtable orderParams, String login,
			String orgLegacyID, String lineNo, String status, String pamId,
			boolean modifyOrder) throws Throwable {

		Hashtable<String, Object> callParams = new Hashtable<String, Object>();
		if (login != null)
			callParams.put("login", login);
		if (orgLegacyID != null && orgLegacyID.indexOf("|") != -1) {

			callParams.put("orgRefId",
					orgLegacyID.substring(0, orgLegacyID.indexOf("|")));
			String idCategorie = orgLegacyID
					.substring(orgLegacyID.indexOf("|") + 1);
			LOGGER.debug("la categorie est : " + idCategorie);
			if (idCategorie != null) {
				callParams.put("orgCategorie", idCategorie);
			} else {
				callParams.put("orgCategorie", "UNDEFINIED");
			}
		} else if (orgLegacyID != null) {
			callParams.put("orgRefId", orgLegacyID);
		}

		/**
		 * List of paramernames to copy from orderParams to callParams with same
		 * name
		 */
		List<String> paramNames = Arrays.asList("packMT", "NNG", "LOGIN_TRPL",
				"PASSWORD_TRPL", "commune", "codePostal", "quartier", "voie",
				"batiment", "etage", "porte", "CLIENTPERE", "SOUSCLIENT",
				"NOMBRE3G", "IPROUTEUR", "NOMCONTACT", "PRENOMCONTACT",
				"TELCONTACT", "GSMCONTACT", "FAXCONTACT", "MAILCONTACT",
				"ancien_nd", "nouveau_nd", "liste_hierarchie",
				"liste_membre_add", "liste_hierarchie_rm", "liste_membre_rm",
				"ndmobileadd", "ndmobilerem", "ndmobileadded",
				"ndmobileremved", "flotte_num", "flottenummobile",
				"servicecode", "agentId", "doEtudeAutomatique", "dateResTemp",
				"nDosInst", "ngrpt", "f_tr", "prerequis_os", "prerequis_EMail",
				"organisation_id", "oldprerequis_EMail", "accessType",
				"type_offre", "type_offre_Spec",
				WIAMConstants.ParameterNeto.PRM_KEEP_ND);

		for (String paramName : paramNames) {
			if (orderParams.get(paramName) != null) {
				callParams.put(paramName, orderParams.get(paramName));
			}
		}

		/**
		 * List of paramernames to copy from orderParams to callParams with
		 * different name
		 */
		Map<String, String> paramNamesHash = new HashMap<String, String>();

		paramNamesHash.put("date_status", "dateChangeEtat");
		paramNamesHash.put("date_depot", "dateDepot");
		paramNamesHash.put("date_enregist", "dateEnregistDem");
		paramNamesHash.put("date_effet", "dateEffet");
		paramNamesHash.put("comment", "commentInst");
		paramNamesHash.put("dem2", "nDem2");
		paramNamesHash.put("dem3", "nDem3");
		paramNamesHash.put("abrv_natlig", "abrvNatlig");

		for (String paramName : paramNamesHash.keySet()) {
			if (orderParams.get(paramName) != null) {
				callParams.put(paramNamesHash.get(paramName),
						orderParams.get(paramName));
			}
		}

		if (status != null) {
			callParams.put("etatDem", status);
		}

		if ("PABX".equals(orderParams.get("accessType"))
				&& orderParams.get("nd_ref") != null) {
			callParams.put("ndRef", orderParams.get("nd_ref"));
		} else if (lineNo != null) {
			callParams.put("ndRef", lineNo);
		}

		if (orderParams.get("min_ila") != null) {
			if (orderParams.get("min_ila").equals("true")) {
				callParams.put("fADT", "1");
			} else {
				callParams.put("fADT", "0");
			}
		}

		if (modifyOrder) {
			callParams.put("orderOperation", OrderMgr.MODIFY_ORDER);
		} else {
			callParams.put("orderOperation", OrderMgr.ADD_ORDER);
		}

		if (pamId != null) {
			callParams.put("nDem", pamId);
		}
		if (orderParams.get("abrvOperation") != null) {
			callParams.put("abrvOperation", orderParams.get("abrvOperation"));
		} else {
			callParams.put("abrvOperation", operation);
		}

		return OrderMgr.getDemandeModifServiceHash(actionMgr, callParams);
	}

	private String getOrderStatus(PersistentActionMgrIF order) {
		String orderStatus = null;
		ParameterIF[] params = order.getAdditionalParameters();
		if (params != null)
			orderStatus = OrderMgr.getOrderStatusCode(params);
		return orderStatus;
	}

	private void onDirectCallResponse(Client client, ContractF contract,
			ActionMgrIF actionMgr, DirectCallContext ctx, String login)
			throws Throwable {

		PersistentActionMgrIF order = actionMgr.getBackupPersistentActionMgr();
		ParameterIF[] paramsOrder = order.getAdditionalParameters();

		String orderType = OrderMgr.getOrderTypeCode(paramsOrder);

		String errorMsg = null;
		String newOrderStatus = null;
		String errorCode = null;
		String errorDescription;
		String newRatePlanCode = null;
		if (ctx.getStatus() == DirectCallContext.ENDED_ERROR) {

			errorCode = ctx.getErr();
			errorDescription = ctx.getErr();

			if (errorCode == null)
				errorCode = "";
			if (errorDescription == null)
				errorDescription = "";
			errorMsg = "Erreur code:" + errorCode + " " + errorDescription;
			newOrderStatus = OrderMgr.ORDER_STATUS_IRREALISABLE;
			if (errorCode.equals("4410")) {
				newOrderStatus = OrderMgr.ORDER_STATUS_ENREGISTRE;
			}
		} else if (ctx.getStatus() == DirectCallContext.ENDED_SUCCESS) {

			errorMsg = "";
			if (orderType.equals(OrderMgr.ORDER_TYPE_MODIF_SERVICE)
					|| orderType.equals(OrderMgr.ORDER_TYPE_MODIF_TECHNIQUE)) {
				errorCode = ctx.getErr();
				ContractF fixeContract = contract;
				String op1 = isServToAddOrRemoveFromFixeEtInt(actionMgr,
						"RS110");
				if ("REM".equals(op1)) {
					RatePlanServiceIF jsadslService = ObjectRefMgr
							.getRatePlanServiceByCode(fixeContract
									.getMyRatePlan().getCode() + "." + "RS200");
					if (jsadslService != null) {// SEY: ANO 24976
						ParameterIF[] parameters = jsadslService
								.getDefaultParameters();
						ContractedServiceIF csv = fixeContract
								.getContractedService(null, jsadslService
										.getService().getIdentifier());
						if (csv != null
								&& !ServiceHelper
										.getImpactedServicesInActionMgrItems(
												actionMgr, csv.getService()
														.getCode().toString()))
							fixeContract.removeService(actionMgr, csv,
									new ParameterIF[] {});
					}
				}
				String servCode = null;
				if (orderType.equals(OrderMgr.ORDER_TYPE_MODIF_TECHNIQUE)) {
					servCode = getServToAddOrRemoveFromFixeEtInt(newRatePlanCode);

					if (servCode == null)
						servCode = getServToAddOrRemoveFromFixeEtInt(fixeContract
								.getMyRatePlan().getCode().toString());
				} else
					servCode = getServToAddOrRemoveFromFixeEtInt(fixeContract
							.getMyRatePlan().getCode().toString());
				if (servCode != null) {
					String op = isServToAddOrRemoveFromFixeEtInt(actionMgr,
							servCode);
					boolean isToAdd = false;
					boolean isToRemove = false;
					if ("ADD".equals(op))
						isToAdd = true;
					if ("REM".equals(op))
						isToRemove = true;
					if (isToAdd || isToRemove) {
						LOGGER.debug("FC4757: Aljazeera Sport ADSL exist ....",
								null, 3);
						ActionMgrIF actionMgr2 = ObjectMgr.createOrder(null);
						ContractF contractInternet = IdentifiantMgr
								.getInternetContractByFixeContract(fixeContract);
						String rateplanCodeInt = contractInternet
								.getMyRatePlan().getCode();
						LOGGER.debug("YFI FC4757 : rateplanCodeInt contrat internet = "
								+ rateplanCodeInt);
						String nd = contractInternet.getPhoneNumber();
						LOGGER.debug("YFI FC4757 : nd contrat internet = " + nd);
						String contractStatus = contractInternet.getStatus()
								.getIdentifier().getString();
						LOGGER.debug("YFI FC4757 : status contrat internet = "
								+ contractStatus);
						String servCodeInt = getServToAddOrRemoveFromFixeEtInt(contractInternet
								.getMyRatePlan().getCode().toString());
						RatePlanServiceIF jsadslService = ObjectRefMgr
								.getRatePlanServiceByCode(contractInternet
										.getMyRatePlan().getCode()
										+ "."
										+ servCodeInt);
						ParameterIF[] parameters = jsadslService
								.getDefaultParameters();

						try {
							ParameterIF[] addParams = contractInternet
									.getAdditionalParameters();
							contractInternet.modifyContract(actionMgr2,
									ContractIF.LEVEL_ITEM.charValue(),
									contractInternet.getLevel(), addParams);
							if (isToAdd)// ajout du service au contrat
										// internet
								contractInternet
										.addService(
												actionMgr2,
												jsadslService,
												parameters,
												1,
												CommercialOfferIF.TYPE_PRIORITY_TO_DEDICATED,
												new ParameterIF[] {});
							else if (isToRemove) { // suppression du
													// service du
													// contrat internet
								ContractedServiceIF csv = contractInternet
										.getContractedService(null,
												jsadslService.getService()
														.getIdentifier());
								contractInternet.removeService(actionMgr2, csv,
										new ParameterIF[] {});
							}
						} catch (Exception e) {
							LOGGER.debug("Attention, Une modification en double sur le contrat Internet :"
									+ e.getMessage());
						}
						String accessType = CommercialOfferWIAM
								.getAccessTypeInfo(contractInternet
										.getMyRatePlan().getCode());
						// TODO
						PersistentActionMgrIF order2 = saveOrder(actionMgr2,
								orderType, orderType, nd, accessType,
								contractInternet.getLevel(), login);

						Vector actionMgrparamsVect = WIAMHelper
								.parameterToVector(order2
										.getAdditionalParameters());
						if (actionMgrparamsVect == null)
							actionMgrparamsVect = new Vector();
						OrderMgr.setComTelSupport(actionMgrparamsVect, order
								.getIdentifier().getString());

						String dateEffet = SIMPLE_DATE_SHORT_FORMAT
								.format(new Date());
						String dateDepot = SIMPLE_DATE_SHORT_FORMAT
								.format(new Date());
						String dateFin = SIMPLE_DATE_SHORT_FORMAT
								.format(new Date());
						String comment = "Commande d'ajout Aljazeera Sport ADSL";
						if (isToRemove)
							comment = "Commande de suppression Aljazeera Sport ADSL";
						OrderMgr.setOrderTypeCode(actionMgrparamsVect,
								OrderMgr.ORDER_TYPE_MODIF_SERVICE);
						if (dateEffet != null)
							OrderMgr.setDateDebutOrder(actionMgrparamsVect,
									dateEffet);
						if (dateDepot != null)
							OrderMgr.setDateDepotDossier(actionMgrparamsVect,
									dateDepot);
						if (dateFin != null)
							OrderMgr.setDateFinOrder(actionMgrparamsVect,
									dateFin);
						if (comment != null)
							OrderMgr.setOrderComment(actionMgrparamsVect,
									comment);
						OrderMgr.setOrderOperationId(
								actionMgrparamsVect,
								""
										+ OrderMgr.getOrderOperationId(order
												.getAdditionalParameters()));
						OrderMgr.setComTelSupport(actionMgrparamsVect, order
								.getIdentifier().getString());
						OrderMgr.setOrderTypeProduitId(actionMgrparamsVect,
								contractInternet.getContractType()
										.getIdentifier().getString());
						OrderMgr.setOrderRatePlanId(actionMgrparamsVect,
								contractInternet.getMyRatePlan()
										.getIdentifier().getString());
						actionMgr2.setOptParameters(WIAMHelper
								.vectorToParameter(actionMgrparamsVect));
						actionMgr2.setBackupPersistentActionMgr(order2);
						ParameterIF[] orderParameters = order2
								.getAdditionalParameters();
						Vector paramsVect = WIAMHelper
								.parameterToVector(orderParameters);
						if (paramsVect == null)
							paramsVect = new Vector();
						paramsVect = WIAMHelper.parameterToVector(order2
								.getAdditionalParameters());
						if (paramsVect == null)
							paramsVect = new Vector();
						OrderMgr.setOrderTypeCode(paramsVect,
								OrderMgr.ORDER_TYPE_MODIF_SERVICE);
						if (dateEffet != null)
							OrderMgr.setDateDebutOrder(paramsVect, dateEffet);
						if (dateDepot != null)
							OrderMgr.setDateDepotDossier(paramsVect, dateDepot);
						if (dateFin != null)
							OrderMgr.setDateFinOrder(paramsVect, dateFin);
						if (comment != null)
							OrderMgr.setOrderComment(paramsVect, comment);
						OrderMgr.setComTelSupport(paramsVect, order
								.getIdentifier().getString());
						OrderMgr.setOrderOperationId(
								paramsVect,
								""
										+ OrderMgr.getOrderOperationId(order
												.getAdditionalParameters()));
						OrderMgr.setOrderTypeProduitId(paramsVect,
								contractInternet.getContractType()
										.getIdentifier().getString());
						OrderMgr.setOrderRatePlanId(paramsVect,
								contractInternet.getMyRatePlan()
										.getIdentifier().getString());
						order2.setAdditionalParameters(WIAMHelper
								.vectorToParameter(paramsVect));
						LOGGER.debug("Persisting order Internet: "
								+ order2.getIdentifier().toString()
								+ " login: " + login);
						order2.persist(actionMgr2);
						actionMgrparamsVect = WIAMHelper
								.parameterToVector(actionMgr
										.getOptParameters(null));
						if (actionMgrparamsVect == null)
							actionMgrparamsVect = new Vector();
						OrderMgr.setInternetSubOrder(actionMgrparamsVect,
								order2.getIdentifier().getString());
						OrderMgr.setInternetSubOrderType(actionMgrparamsVect,
								OrderMgr.getInternetSubOrderType(order2
										.getAdditionalParameters()));
						actionMgr.setOptParameters(WIAMHelper
								.vectorToParameter(actionMgrparamsVect));
						actionMgr.setBackupPersistentActionMgr(order);
						paramsVect = WIAMHelper.parameterToVector(order
								.getAdditionalParameters());
						if (paramsVect == null)
							paramsVect = new Vector();
						OrderMgr.setInternetSubOrder(paramsVect, order2
								.getIdentifier().getString());
						OrderMgr.setInternetSubOrderType(paramsVect, OrderMgr
								.getInternetSubOrderType(order2
										.getAdditionalParameters()));
						OrderMgr.setOrderCustomerRef(paramsVect, fixeContract
								.getLevel().getReferenceId());
						order.setAdditionalParameters(WIAMHelper
								.vectorToParameter(paramsVect));
						LOGGER.debug("Persisting order Fixe : "
								+ order.getIdentifier().toString() + " login: "
								+ login);
						order.persist(actionMgr);
					}
				}
			}
			newOrderStatus = OrderMgr.ORDER_STATUS_REALISABLE;

		} else {
			errorMsg = "statut_indefini";
			newOrderStatus = OrderMgr.ORDER_STATUS_IRREALISABLE;
		}

		// if (ctx.getStatus() == DirectCallContext.ENDED_SUCCESS)
		// {
		Vector paramOrder = WIAMHelper.parameterToVector(paramsOrder);

		OrderMgr.setOrderStatusDate(paramOrder, dateToString(Calendar
				.getInstance().getTime()));
		// newOrderStatus = OrderMgr.ORDER_STATUS_REALISABLE;
		OrderMgr.setOrderStatusCode(paramOrder, newOrderStatus);

		// update order
		ParameterIF[] paramOrders = WIAMHelper.vectorToParameter(paramOrder);
		order.setAdditionalParameters(paramOrders);
		LOGGER.debug("Updating order: " + order.getIdentifier().toString()
				+ " login: " + login);
		order.update();
		// }

		OrderMgr.addOrderHistoryEvent(order.getIdentifier().getString(),
				getOrderStatus(order), login, errorMsg);
	}

	/**
	 * Fonction permettant de savoir si actionMgr contient une action d'ajout ou
	 * suppression d'un service
	 *
	 * @param actionMgr
	 * @return
	 * @throws Throwable
	 *
	 */

	private String isServToAddOrRemoveFromFixeEtInt(ActionMgrIF actionMgr,
			String servCode) throws Throwable {
		if (servCode != null) {
			ActionItemIF[] itemsAdd = actionMgr.findActionItemsByAction(
					DescriptorOidIF.ACTION_ADD_SERVICE, true);
			if (itemsAdd != null) {
				for (int j = 0; j < itemsAdd.length; j++) {
					RatePlanServiceIF service = ((RequestIF.AddService) (itemsAdd[j]
							.getRequest().getAdditionalParameter()))
							.getRatePlanService();
					if (service != null) {
						/*
						 * String serviceCode = service.getCode(); serviceCode =
						 * serviceCode.substring(serviceCode .lastIndexOf(".") +
						 * 1);
						 */
						if (servCode.equals(service.getService().getCode())) {
							return "ADD";
						}
					}
				}
			}
			ActionItemIF[] itemsRem = actionMgr.findActionItemsByAction(
					DescriptorOidIF.ACTION_REMOVE_SVC, true);
			if (itemsRem != null) {
				for (int j = 0; j < itemsRem.length; j++) {
					RatePlanServiceIF service = ((RequestIF.RemoveService) (itemsRem[j]
							.getRequest().getAdditionalParameter()))
							.getRatePlanService();
					if (service != null) {
						/*
						 * String serviceCode = service.getCode(); serviceCode =
						 * serviceCode.substring(serviceCode .lastIndexOf(".") +
						 * 1);
						 */
						if (servCode.equals(service.getService().getCode())) {
							return "REM";
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Fonction permettant de recup�rer le service code du service � ajouter sur
	 * les deux contrats fixe et internet
	 *
	 * @param contract
	 * @return servCode
	 * @throws Throwable
	 *
	 */
	public String getServToAddOrRemoveFromFixeEtInt(String rtpCode)
			throws Throwable {
		String servCode = CommercialOfferWIAM.getRateplanInfo(null, rtpCode,
				"FLAG_RTP_4757");
		if (servCode == null)
			servCode = CommercialOfferWIAM.getRateplanInfo(null, rtpCode,
					"FLAG_RTP_4757_NET");
		return servCode;
	}

	public String dateToString(Date date) {

		try {
			return SIMPLE_DATE_FULL_FORMAT.format(date);
		} catch (java.lang.IllegalArgumentException ex) {
			return "";
		}
	}

	public CheckOrderResponseBean cancelOrder(CommandeBean commandeRequestBean,
			String login) throws FunctionnalException, TechnicalException {
		CheckOrderResponseBean checkOrderResponseBean = checkCancelOrder(commandeRequestBean);
		if (checkOrderResponseBean != null
				&& !Boolean.TRUE.equals(checkOrderResponseBean.getIsValid())) {
			return checkOrderResponseBean;
		}
		Client client = clientManagerBiz.getClient(commandeRequestBean
				.getNcli());
		ContractF contract = contractManagerBiz.getContract(client,
				commandeRequestBean.getNd());

		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());

		String requestQ = "MQSERIES_QUEUE_02";
		String replyQ = "MQSERIES_QUEUE_04";
		String topic = "REQREMOVEORDER";
		String redir = "ON_DIRECTCALL_OK";
		String errRedir = "ON_DIRECTCALL_OK";

		try {
			Date dateTemp = new Date();

			String legacyId = orgF.getHierarchyRoot().getLegacyIdentifier()
					.getString();
			Hashtable input = OrderMgr.getRemoveOrder(login,
					commandeRequestBean.getNumCommande(), legacyId,
					SIMPLE_DATE_FULL_FORMAT.format(dateTemp),
					commandeRequestBean.getInfosCommande().getMotif(),
					commandeRequestBean.getInfosCommande().getCommentaire());

			DirectCallContext ctx = new DirectCallContext(requestQ, replyQ,
					topic, input, redir, errRedir);

			String directCallReply;
			try {
				directCallReply = directCallHandler.handle(ctx);
			} catch (TimeoutException e) {
				directCallReply = DIRECTCALL_TIME_OUT;
			}

			checkDireclCallResponse(directCallReply, ctx,
					checkOrderResponseBean);

			return checkOrderResponseBean;
		} catch (Exception ex) {
			LOGGER.error(
					"ERROR WHILE GENERATING INPUT FOR CANCEL ORDER DC : {}", ex);
			throw new TechnicalException(
					ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
					"Erreur lors de l'annulation de la commande, veuillez contactez l'administrateur.");
		}
	}

	private CheckOrderResponseBean checkCancelOrder(
			CommandeBean commandeRequestBean) {
		CheckOrderResponseBean result = new CheckOrderResponseBean();
		result.setIsValid(true);
		return null;
	}

	private ParameterDescriptor getRequestedParam(String paramName,
			List<ParameterDescriptor> parameters) {
		for (ParameterDescriptor parameterDescriptor : parameters) {
			if (parameterDescriptor.getCode().equals(paramName)) {
				return parameterDescriptor;
			}
		}
		return null;
	}

	private String getRequestedParamValue(String codeParam,
			List<ParameterDescriptor> parameters) {
		ParameterDescriptor requestedParam = getRequestedParam(codeParam,
				parameters);
		if (requestedParam != null) {
			return requestedParam.getValue();
		}
		return null;
	}
}
