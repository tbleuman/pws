package ma.iam.wiam.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;

import ma.iam.wiam.business.EntityManagerBiz;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.dao.AppConfigSousCategorieDao;
import ma.iam.wiam.dao.BankAgencyDao;
import ma.iam.wiam.dao.BankDao;
import ma.iam.wiam.dao.CataloguePriceDao;
import ma.iam.wiam.dao.CcuDao;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.dao.ContratDao;
import ma.iam.wiam.dao.CountryDao;
import ma.iam.wiam.dao.IdentifiantDao;
import ma.iam.wiam.dao.LevelDao;
import ma.iam.wiam.dao.ReferentielCommOfferDao;
import ma.iam.wiam.dao.VendeurDao;
import ma.iam.wiam.dao.impl.IdentifiantDaoImpl;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.ValueChoiceItemDao;
import ma.iam.wiam.param.criteria.BankAgencyCriteria;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.AppConfigSousCategorie;
import ma.iam.wiam.ws.neto.model.Bank;
import ma.iam.wiam.ws.neto.model.BankAgency;
import ma.iam.wiam.ws.neto.model.CataloguePrice;
import ma.iam.wiam.ws.neto.model.Ccu;
import ma.iam.wiam.ws.neto.model.Client;
import ma.iam.wiam.ws.neto.model.CommofferInfoValue;
import ma.iam.wiam.ws.neto.model.Contrat;
import ma.iam.wiam.ws.neto.model.Country;
import ma.iam.wiam.ws.neto.model.Vendeur;

@Component
public class EntityManagerBizImpl implements EntityManagerBiz {

	@Value("#{config['ma.iam.wiam.bank.restrictedList']}")
	private String restrictedBanks;

	@Autowired
	private BankDao bankDao;

	@Autowired
	private BankAgencyDao bankAgencyDao;

	@Autowired
	private ValueChoiceItemDao valueChoiceItemDao;

	@Autowired
	private LevelDao levelDao;
	
	// feature/24: Autowiring avec la sous categorie dao
	@Autowired
	private AppConfigSousCategorieDao appConfigSousCategorieDao;
	
	/**Begin feature/wsTools*/
	@Autowired
	private ReferentielCommOfferDao referentielCommOfferDao;
	
	@Autowired
	private VendeurDao vendeurDao; 
	
	@Autowired
	private CcuDao ccuDao;
	
	@Autowired
	private ClientDao clientDao;
	
	@Autowired
	private ContratDao contratDao;
	
	@Autowired
	private CataloguePriceDao cataloguePriceDao;
	
	@Autowired
	private IdentifiantDao identifiantDao;
	
	/**End feature/wsTools*/
	
	
	public List<ValueChoiceItemIF> getSegments1() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_SEGMENT);
	}

	public List<ValueChoiceItemIF> getSegments2() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_SEGMENT2);
	}

	public List<Object[]> getAllMTAgenciesInRegie() throws FunctionnalException, TechnicalException {
		return levelDao.getAllMTAgenciesInRegie();
	}

	public List<ValueChoiceItemIF> getProspectionInfos() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_LIB_PROSPECT);
	}

	public List<Bank> getBanks() throws FunctionnalException, TechnicalException {
		String separator = ",";
		String[] restrictedBanksArray = restrictedBanks.split(separator);

		Arrays.asList(restrictedBanksArray);

		List<Bank> notRestrictedBanks = bankDao.getBanksNotInCodesList(Arrays.asList(restrictedBanksArray));

		return notRestrictedBanks;
	}

	public List<BankAgency> getAgenciesByCriteria(BankAgencyCriteria bankAgencyCriteria)
			throws FunctionnalException, TechnicalException {
		if (StringUtils.isEmpty(bankAgencyCriteria.getBankCode())) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_SEARCH_WS);
		}
		List<BankAgency> bankAgencies = new ArrayList<BankAgency>(bankAgencyDao.getAgenciesByCodeBank(bankAgencyCriteria.getBankCode()));
		return bankAgencies;
	}

	public List<ValueChoiceItemIF> getPayerQualities() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.ORG_QUALITE_PAYEUR);
	}

	public List<ValueChoiceItemIF> getBillingCycles() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_CYCLE_FACTURATION);
	}

	public List<ValueChoiceItemIF> getPaymentTerms() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_TERME_PAIEMENT);
	}

	public List<ValueChoiceItemIF> getBillingSupports() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_SUPPORT_FACTURE);
	}

	public List<ValueChoiceItemIF> getBillingLanguages() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(Constants.BA_LANGUE_FACTURE);
	}
	
	// feature/24: ajouter la methode de sous categorie qui sera appeler
	public String[] getSousCategory() throws FunctionnalException, TechnicalException {
		return appConfigSousCategorieDao.getSousCategorie(Constants.SOUS_CATEGORY_EREV);
	}

	/**Begin feature/wsTools*/
	@Override
	public List<CommofferInfoValue> getListCommOfferValueByCode(String code)
			throws FunctionnalException, TechnicalException {
		
		return referentielCommOfferDao.getListCommOfferValueByCode(code);
	}

	/**Begin feature_Changement_Req_ACICGC*/
	@Override
	public List<Vendeur> getListACICGC(String agenceCode) throws FunctionnalException, TechnicalException {
		
		return vendeurDao.getListAcicgcByAgenceId(agenceCode);
	}
	/**End feature_Changement_Req_ACICGC*/
	@Override
	public Ccu verifierExistanceCCU(String codeCCU) throws FunctionnalException, TechnicalException {
		
		try {
			return ccuDao.getById(codeCCU);
		}catch(Throwable e) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
	}

	@Override
	public List<CataloguePrice> getListeEquipementByOperationND(String NdLogin)
			throws FunctionnalException, TechnicalException {
		
		List<CataloguePrice> listCatalogue =null;
		Contrat ctr=null;
		List<Client> list = clientDao.findClientByNdOrLogin(NdLogin);
		for(Client cli : list) {
			ctr = contratDao.getContratByNdNumCli(cli.getNumClient(), NdLogin);
			if(ctr != null) {	
				break;
			}
		}
		if(ctr == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS,"le Contrat du Nd :"+NdLogin+" n'a pas le Status Actif");
		}
		listCatalogue = cataloguePriceDao.getListeEquipementByOperationND(ctr.getRateplanCode());
		
		return listCatalogue;
	}

	@Override
	public List<CataloguePrice> getListeEquipementByOperationNA_RP(String rateplanCode, String typeCommercial)
			throws FunctionnalException, TechnicalException {
		
		return cataloguePriceDao.getListEquipementNAByTypeComm(rateplanCode, typeCommercial);
	}

	@Override
	public Boolean VerifierNonExistanceIdentifiant(String login , String serviceId) throws FunctionnalException, TechnicalException {
		

		return identifiantDao.VerifierNonExistanceIdentifiant(login,serviceId);
	}

	@Override
	public Boolean VerifierNonExistanceLoginSoftSwitch(String login) throws FunctionnalException, TechnicalException {
		
		return identifiantDao.VerifierNonExistanceLoginSoftSwitch(login);
	}

	@Override
	public Boolean VerifierEquipementCompatibleND(String NdLogin, String ANetoId)
			throws FunctionnalException, TechnicalException {
		
		Contrat ctr=null;
		List<Client> list = clientDao.findClientByNdOrLogin(NdLogin);
		for(Client cli : list) {
			ctr = contratDao.getContratByNdNumCli(cli.getNumClient(), NdLogin);
			if(ctr != null) {	
				break;
			}
		}
		if(ctr == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS,"le Contrat du Nd :"+NdLogin+" n'a pas le Status Actif");
		}
		
		return cataloguePriceDao.VerifierEquipementCompatibleND(ctr.getRateplanCode(), ANetoId); 
	}

	@Override
	public Date getEngagementContrat(String NdLogin) throws FunctionnalException, TechnicalException {
		Contrat ctr=null;
		List<Client> list = clientDao.findClientByNdOrLogin(NdLogin);
		for(Client cli : list) {
			ctr = contratDao.getContratByNdNumCli(cli.getNumClient(), NdLogin);
			if(ctr != null) {	
				break;
			}
		}
		if(ctr == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS,"le Contrat du Nd :"+NdLogin+" n'a pas le Status Actif");
		}
		
		return contratDao.getDateFinEngagementContrat(ctr.getId());	
	}
	
	
	
	
	
	
	
	
	
	
	
	/**End feature/wsTools*/
}
