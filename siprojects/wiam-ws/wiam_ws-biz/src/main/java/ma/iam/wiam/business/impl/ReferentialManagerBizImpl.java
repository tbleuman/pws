package ma.iam.wiam.business.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netonomy.blm.api.utils.ObjectRefMgr;

import ma.iam.wiam.business.AdresseManagerBiz;
import ma.iam.wiam.business.EntityManagerBiz;
import ma.iam.wiam.business.IdentityManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.dao.LevelDao;
import ma.iam.wiam.dao.TitleDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.ValueChoiceItemDao;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.springframework.stereotype.Repository;

//@Component
@Repository
public class ReferentialManagerBizImpl implements ReferentialManagerBiz {
	
	@Autowired
	TitleDao titleDao;
	
	@Autowired
	LevelDao levelDao;
	
	@Autowired
	ValueChoiceItemDao valueChoiceItemDao;
	
	Map<String, Object> data;
	
	@Autowired
	EntityManagerBiz entityManagerBiz;
	
	@Autowired
	AdresseManagerBiz adresseManagerBiz;
	
	@Autowired
	IdentityManagerBiz identityManagerBiz;
	
	private static final Logger LOG = LoggerFactory.getLogger(ReferentialManagerBizImpl.class);
	
	@PostConstruct
	private void init() throws FunctionnalException, TechnicalException {
	    try {
            data = new HashMap<String, Object>();

            //START-REMARK--If web services needed for this Lists
            //Please get them from a manager biz instead of dao directly
            data.put(Constants.TITLES, titleDao.getAll());
            data.put(Constants.SMART_FACT_ORGANIZATIONS, levelDao.getSmartFactOrganizations());
            //END-REMARK

            data.put(Constants.SEGMENTS1, entityManagerBiz.getSegments1());
            data.put(Constants.MT_AGENCIES_IN_REGIE, entityManagerBiz.getAllMTAgenciesInRegie());
            data.put(Constants.PROSPECTION_INFOS, entityManagerBiz.getProspectionInfos());
            data.put(Constants.BANKS, entityManagerBiz.getBanks());
            data.put(Constants.PAYER_QUALITIES, entityManagerBiz.getPayerQualities());
            data.put(Constants.BILLING_CYCLES, entityManagerBiz.getBillingCycles());
            data.put(Constants.PAYMENT_TERMS, entityManagerBiz.getPaymentTerms());
            data.put(Constants.BILLING_SUPPORTS, entityManagerBiz.getBillingSupports());
            data.put(Constants.BILLING_LANGUAGES, entityManagerBiz.getBillingLanguages());

            data.put(Constants.COMMUNES, adresseManagerBiz.getListCommune());

            data.put(Constants.PERSON_CODES, identityManagerBiz.getCodesPersonnes());
            data.put(Constants.MARITAL_STATUS, identityManagerBiz.getEtatsCiviles());
            data.put(Constants.GP_IDENTITY_TYPES, identityManagerBiz.getIdentifiantsResidentiels());
            data.put(Constants.PRO_IDENTITY_TYPES, identityManagerBiz.getIdentifiantsProf());
            data.put(Constants.ENT_IDENTITY_TYPES, identityManagerBiz.getIdentifiantsEntreprise());
            data.put(Constants.PROFESSIONS, identityManagerBiz.getProfessions());
            data.put(Constants.GENDERS, identityManagerBiz.getGenders());

            data.put(Constants.ALL_COUNTRIES, ObjectRefMgr.getAllCountries());
          //feature/24: ajout de la list des sous-categorie
            data.put(Constants.SOUS_CATEGORY_EREV, entityManagerBiz.getSousCategory());

            LOG.debug(" ====> Initialization complete [OK]");
        }catch(Throwable e){
	    	e.printStackTrace();
	        throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,e);
        }
	}

	public Object getItemFromReferential(String code) {
		return data.get(code);
	}

	public void setItemInReferential(String code, Object value) throws TechnicalException {
		if(code ==  null || value ==  null) {
			throw new TechnicalException(ExceptionCodeTypeEnum.REF_NULL_ITEM);
		}
		data.put(code, value);
	}
}
