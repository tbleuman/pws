package ma.iam.wiam.business;

import java.util.Collection;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContractBean;
import ma.iam.wiam.ws.neto.model.NetoInt;
import ma.iam.wiam.ws.neto.model.base.BaseNetoInt;
/**<!-- @todo feature/Ajouter_Neto_Int -->**/
public interface NetoIntManagerBiz {
	
	void createNetoInt(NetoInt netoint)throws FunctionnalException, TechnicalException;
	void updateNetoInt(NetoInt netoint)throws FunctionnalException, TechnicalException;
	//void deleteNetoInt(NetoInt netoint)throws FunctionnalException, TechnicalException;
	//void deleteNetoIntByID(Long id)throws FunctionnalException, TechnicalException;
	NetoInt getNetoIntByID(Long id)throws FunctionnalException, TechnicalException;
	Collection<NetoInt> getAllNetoInt()throws FunctionnalException, TechnicalException;
	
	

}
