package ma.iam.wiam.business.impl;

import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import ma.iam.wiam.business.AsyncActionManagerBiz;
import ma.iam.wiam.dao.*;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.util.StringUtil;
import ma.iam.wiam.util.Utils;
import ma.iam.wiam.ws.neto.model.NetoInt;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.blm.api.Organization.OrganizationF;
import com.netonomy.blm.api.utils.DescriptorOidIF;
import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.api.utils.SessionF;
import com.netonomy.blm.interfaces.contact.ContactIF;
import com.netonomy.blm.interfaces.organization.LevelIF;
import com.netonomy.blm.interfaces.organization.OrganizationTypeCategoryIF;
import com.netonomy.blm.interfaces.organization.OrganizationTypeIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.request.RequestIF;
import com.netonomy.blm.interfaces.util.ActionItemIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.util.ObjectId;

import ma.iam.wiam.business.ContactManagerBiz;
import ma.iam.wiam.business.LevelManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.business.constants.ProjectProperties;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.customer.ClientEntreprise;
import ma.iam.wiam.customer.ClientGPMultiResidence;
import ma.iam.wiam.customer.ClientGPSimple;
import ma.iam.wiam.customer.ClientMT;
import ma.iam.wiam.customer.ClientProfessionnel;
import ma.iam.wiam.customer.OrganisationWIAM;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ContactBean;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.neto.bean.RequestBean;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.util.WSValidator;
import ma.iam.wiam.validators.OrganizationValidator;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.NiveauType;
import ma.iam.wiam.ws.neto.enums.OrganizationCategoryEnum;
import ma.iam.wiam.ws.neto.exception.ServiceException;
import ma.iam.wiam.ws.neto.model.Ccu;
import ma.iam.wiam.ws.neto.model.User;
import ma.iam.wiam.ws.neto.session.NetoSessionFactory;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;

/**
 * Organization service 
 * @author y.eddaalous
 *
 */
@Service
public class LevelManagerBizImpl implements LevelManagerBiz {

	private static final SimpleDateFormat fullFormatter = new SimpleDateFormat(ProjectProperties.FULL_FORMATTER);
	private static final SimpleDateFormat shortFormatter = new SimpleDateFormat(ProjectProperties.SHORT_FORMATTER);
	private static JAXBContext JAXB_CONTEXT = null;

	static {
		try {
			JAXB_CONTEXT = JAXBContext.newInstance(NiveauBean.class);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private CcuDao ccuDao;
	
	@Autowired
	private LevelFDao levelFDao;
	
	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;
	
	@Autowired
	private ReferentialManagerBiz referentialManagerBiz;
	
	@Autowired
	private ContactManagerBiz contactManagerBiz;
	@Autowired
	private NetoIntDao netoIntDao;
	@Autowired
	private ActionDao actionDao;
	@Autowired
	private OrigineDao origineDao;
	@Autowired
	private StatusDao statusDao;
	@Autowired
	private AsyncActionManagerBiz asyncActionManagerBiz;

  	public String createLevel(NiveauBean niveauBean, final ActionType actionType, TicketObject ticketObject,String webMethod,String userName) throws FunctionnalException, TechnicalException {
	    return prepareLevel(niveauBean, actionType,  ticketObject, webMethod, userName);
	}
	
	public NiveauBean linkLevelWithCcu(RequestBean requestBean, TicketObject ticketObject) throws FunctionnalException, TechnicalException {
		RequestIF requestIF =ObjectMgr.getRequest(ObjectId.instantiate(requestBean.getId()));
		
		if(requestIF == null) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.OBJECT_NOT_FOUND);
		}
		
		if (requestIF.getStatus().equals(RequestIF.request_status_DONE)){
			String levelId= requestIF.getCreatedId().toString();
			
			LevelF level = levelFDao.get(levelId);
			
			ParameterIF[] parameterIFs = level.getAdditionalParameters();
			
			String ccu = OrganisationWIAM.getCCU(parameterIFs);
			String entityManager = ClientGPSimple.getAgenceMTGest(parameterIFs);
			
			if (StringUtils.isNotEmpty(ccu)) {
				ccu = ccu.split(";")[0];
				SessionF sessionF;
				try {
					sessionF = NetoSessionFactory.getInstance();
					OrganisationWIAM.updateCCUByOrganization(level.getLegacyIdentifier().getString(),ccu.toUpperCase().trim(), sessionF.getUserF().getLogin(), entityManager);
				} catch (Throwable e) {
					throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
				}
			}else {
				throw new FunctionnalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			}
			return new NiveauBean(levelId);
		}else{
			throw new FunctionnalException(ExceptionCodeTypeEnum.REQUEST_NOT_YET_TREATED);
		} 
	}
	
	private String prepareLevel(final NiveauBean niveauBean, final ActionType actionType, TicketObject ticketObject,String webMethod,String userName) throws FunctionnalException, TechnicalException {
		OrganizationValidator.validateOrganization(niveauBean, actionType);
        baseNetoDao.getCurrentSession();
        OrganizationTypeIF organizationTypeIF = ObjectRefMgr.getOrganizationTypeByCode(niveauBean.getType().getCode());

        if(organizationTypeIF == null) {
            throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: type " + niveauBean.getType().getCode() + "  non trouve dans le referentiel dans le referentiel");
        }
        OrganizationTypeCategoryIF organizationTypeCategoryIF = organizationTypeIF.getCategory();
        OrganizationValidator.validateContact(niveauBean.getContactLegal(), organizationTypeCategoryIF.getCode());
        OrganizationValidator.validateContact(niveauBean.getContactDeFacturation(), organizationTypeCategoryIF.getCode());
		NetoInt netoInt = new NetoInt();
		try {
			Marshaller jaxbMarshaller = JAXB_CONTEXT.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			JAXBElement<NiveauBean> jaxbElement =
					new JAXBElement<NiveauBean>( new QName("", "createCustomerOrder"),
							NiveauBean.class,
							niveauBean);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(jaxbElement, sw);
			String vXMLData = sw.toString();

			netoInt.setOrigine(origineDao.getById(Constants.NETOCMDREQUEST_ORIGIN_ID));
			netoInt.setStatus(statusDao.getById(Constants.NETOCMDREQUEST_STATUS_BEING_PROCESSED_99));
			netoInt.setAction(actionDao.getById(Constants.NETOCMDREQUEST_ACTION_CREATE_CUSTOMER));
			netoInt.setPriority(new Long(2));
			netoInt.setInsertionDate(new java.util.Date());
			netoInt.setStatusUpdateDate(new java.util.Date());
			netoInt.setInputData(vXMLData);
			netoInt.setUserName(userName);
			netoInt.setEntryDate(new java.util.Date());
			netoInt.setRequestMode("A");
			netoIntDao.save(netoInt);
		}catch( ma.iam.mutualisation.fwk.common.exception.TechnicalException e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}catch(JAXBException e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		asyncActionManagerBiz.addLevels( netoInt,niveauBean,actionType,ticketObject,webMethod,userName);
		return String.valueOf(netoInt.getRequestID());
	}






}
