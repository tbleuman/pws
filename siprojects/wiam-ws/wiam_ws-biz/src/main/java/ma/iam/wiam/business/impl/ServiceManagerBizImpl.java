package ma.iam.wiam.business.impl;

import static ma.iam.wiam.business.constants.Constants.SERVICE_INFO_CODE_SVC_ALLOW_MODIF;
import static ma.iam.wiam.business.constants.Constants.SERVICE_INFO_CODE_SVC_MINCOM_FLAG;
import static ma.iam.wiam.business.constants.Constants.SERVICE_INFO_CODE_SVC_NOT_HIDDEN_MS;
import static ma.iam.wiam.business.constants.Constants.SERVICE_INFO_CODE_SVC_PACK_MODIF;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.ContractManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.business.ServiceManagerBiz;
import ma.iam.wiam.contrat.ContractWIAMDataHelper;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.dao.CommuneDao;
import ma.iam.wiam.dao.QuartierDao;
import ma.iam.wiam.dao.VoieDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.ItemList;
import ma.iam.wiam.neto.bean.ParameterDescriptor;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.neto.bean.ServiceBean;
import ma.iam.wiam.neto.bean.ServiceList;
import ma.iam.wiam.neto.bean.ServiceRulesRequestBean;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.ContractDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.offre.CommercialOfferWIAM;
import ma.iam.wiam.services.ServiceHelper;
import ma.iam.wiam.services.dao.SvcFeeParamValue;
import ma.iam.wiam.services.dao.SvcFeeParamValueListBuilder;
import ma.iam.wiam.services.regle.ServiceRulesMgr;
import ma.iam.wiam.services.regle.bean.ServiceRule;
import ma.iam.wiam.services.service.FeeCalcultatorHelper;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.contract.ContractIF;
import com.netonomy.blm.interfaces.parameters.ParameterDescriptorIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.service.AddableServiceIF;
import com.netonomy.blm.interfaces.service.ContractedServiceIF;
import com.netonomy.blm.interfaces.service.RatePlanServiceIF;
import com.netonomy.blm.interfaces.service.ServiceIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.util.ObjectId;

/**
 * 
 * @author s.hroumti
 *
 */
@Service
public class ServiceManagerBizImpl implements ServiceManagerBiz {

	private static final String CONSTANT_1 = "1";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ServiceManagerBizImpl.class);

	@Autowired
	private CommuneDao communeDao;

	@Autowired
	private QuartierDao quartierDao;

	@Autowired
	private VoieDao voieDao;

	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;

	@Autowired
	private ContractDao contractDao;

	@Autowired
	private ClientDao clientDao;

	@Autowired
	private ReferentialManagerBiz referentialManagerBiz;

	@Autowired
	private LevelFDao levelFDao;

	@Autowired
	private ClientManagerBiz clientManagerBiz;

	@Autowired
	private ContractManagerBiz contractManagerBiz;

	public ServiceList getServicesSouscrits(String ncli, String nd)
			throws FunctionnalException, TechnicalException {

		ActionMgrIF actionMgr = ObjectMgr.createActionManager();

		ContractIF contract = contractManagerBiz.getContract(ncli, nd);

		ContractedServiceIF[] services = contract.getContractedServices(
				actionMgr, null);
		List<ServiceBean> result = new ArrayList<ServiceBean>();
		for (ContractedServiceIF contractedServiceIF : services) {
			ServiceBean contractedServiceBean = getContractedServiceBean(
					contract, contractedServiceIF);

			result.add(contractedServiceBean);
		}
		return new ServiceList(result);
	}

	private ServiceBean getContractedServiceBean(ContractIF contract,
			ContractedServiceIF contractedServiceIF) {
		ServiceBean contractedServiceBean = new ServiceBean();
		contractedServiceBean.setAccessFee(FeeCalcultatorHelper
				.calculateContractedService(ActionMgrIF.FEE_ACCESS,
						contractedServiceIF));
		contractedServiceBean.setSubscriptionFee(FeeCalcultatorHelper
				.calculateContractedService(ActionMgrIF.FEE_SUBSCRIPTION,
						contractedServiceIF));
		contractedServiceBean.setServiceCode(contractedServiceIF.getService()
				.getCode());
		contractedServiceBean.setServiceName(ServiceHelper.getServiceName(
				contract.getMyRatePlan().getCode(),
				contractedServiceIF.getService()));
		contractedServiceBean.setServiceId(contractedServiceIF.getIdentifier()
				.toString());

		ParameterIF[] parameters = contractedServiceIF.getParameters();
		if (parameters != null) {
			contractedServiceBean.setParameters(getParameterDescriptorList(
					contractedServiceIF, parameters, false));
		}
		return contractedServiceBean;
	}

	private List<ParameterDescriptor> getParameterDescriptorList(
			RatePlanServiceIF contractedServiceIF, ParameterIF[] parameters,
			boolean withPossibleValues) {
		List<ParameterDescriptor> serviceParameterDescriptors = new ArrayList<ParameterDescriptor>();
		for (ParameterIF parameterIF : parameters) {
			Boolean isHidden = ServiceHelper.isParamHidden(parameterIF
					.getParameterDescriptor().getCode(), contractedServiceIF
					.getService().getCode());
			if (isHidden) {
				continue;
			}
			ParameterDescriptorIF parameterDescriptor = parameterIF
					.getParameterDescriptor();
			ParameterDescriptor serviceParameterDescriptor = new ParameterDescriptor();
			serviceParameterDescriptor.setCode(parameterDescriptor.getCode());
			serviceParameterDescriptor.setDescription(parameterDescriptor
					.getDescription());
			serviceParameterDescriptor.setIdentifier(parameterDescriptor
					.getIdentifier().toString());
			serviceParameterDescriptor.setInputFormat(parameterDescriptor
					.getInputFormat());
			serviceParameterDescriptor.setName(parameterDescriptor.getName());
			serviceParameterDescriptor.setShortDescription(parameterDescriptor
					.getShortDescription());
			serviceParameterDescriptor.setType(parameterDescriptor.getType());

			try {
				serviceParameterDescriptor.setValue(WIAMHelper
						.getWIAMParameterValues(
								new ParameterIF[] { parameterIF }, 0, false));
			} catch (IOException e) {
				LOGGER.debug("Cannot get the value of param {} : {}",
						parameterDescriptor.getIdentifier().toString(), e);
			} catch (SQLException e) {
				LOGGER.debug("Cannot get the value of param {} : {}",
						parameterDescriptor.getIdentifier().toString(), e);
			}

			if (withPossibleValues) {
				setPossibleValues(contractedServiceIF, parameterIF,
						serviceParameterDescriptor);
			}

			serviceParameterDescriptors.add(serviceParameterDescriptor);

		}
		return serviceParameterDescriptors;
	}

	private void setPossibleValues(RatePlanServiceIF contractedServiceIF,
			ParameterIF parameterIF,
			ParameterDescriptor serviceParameterDescriptor) {
		int paramType = parameterIF.getParameterDescriptor().getType();
		switch (paramType) {
		case ParameterDescriptorIF.CHOICE:
			setValueChoiceItems(contractedServiceIF, parameterIF,
					serviceParameterDescriptor);
			break;
		}
	}

	private void setValueChoiceItems(RatePlanServiceIF contractedServiceIF,
			ParameterIF parameterIF,
			ParameterDescriptor serviceParameterDescriptor) {
		List<ValueChoiceItemIF> valueChoiceItems = getValueChoiceItems(
				contractedServiceIF, parameterIF);
		ItemList possibleValues = new ItemList();
		serviceParameterDescriptor.setPossibleValues(possibleValues);
		for (ValueChoiceItemIF valueChoiceItemIF : valueChoiceItems) {
			String choiceId = valueChoiceItemIF.getChoiceItem().getIdentifier()
					.toString();
			String choiceDisplayValue = valueChoiceItemIF.getChoiceItem()
					.getDisplayValue();

			possibleValues.getItems().add(
					new ReferentialBean(choiceId, choiceDisplayValue));
		}
	}

	// private Client getClient(String ncli) throws TechnicalException,
	// FunctionnalException {
	//
	// List<Client> list = clientDao.findClientByClientNum(ncli);
	// if (list == null || list.isEmpty()) {
	// throw new FunctionnalException(
	// ExceptionCodeTypeEnum.EMPTY_RESULT_WS,
	// "aucun client trouvé");
	// }
	// if (list.size() > 1) {
	// throw new FunctionnalException(
	// ExceptionCodeTypeEnum.MANY_RESULT_WS,
	// "Plusieurs résultats trouvés");
	//
	// }
	//
	// return list.get(0);
	// }

	@SuppressWarnings("unchecked")
	public ServiceList getAddableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		ActionMgrIF actionMgr = ObjectMgr.createActionManager();

		Client client = clientManagerBiz.getClient(ncli);
		ContractIF contract = contractManagerBiz.getContract(client, nd);

		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());

		AddableServiceIF[] services = contract.getAddableServices(actionMgr,
				null, null, null);

		String ratePlanCode = contract.getMyRatePlan().getCode();

		List<ServiceBean> result = new ArrayList<ServiceBean>();
		if (services != null) {

			List<AddableServiceIF> addService = new ArrayList<AddableServiceIF>();

			for (int i = 0; i < services.length; i++) {
				String serviceCode = services[i].getService().getCode();
				// YFI FC4838 begin
				try {
					String svcFlag = CommercialOfferWIAM.getServiceInfo(null,
							ratePlanCode, serviceCode,
							SERVICE_INFO_CODE_SVC_NOT_HIDDEN_MS);
					if ("1".equals(svcFlag)
							|| !ServiceHelper.isServiceHidden(orgF.getType()
									.getCode(), ratePlanCode, serviceCode)) { // YFI
																				// FC4838
																				// end
						addService.add(services[i]);
					}
				} catch (SQLException e) {
					LOGGER.error("cannot check visibility of service : {}", e);
					throw new TechnicalException(
							ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
				}
			}

			try {
				addService = ContractWIAMDataHelper
						.checkCompatibleServiceInActionMgr(actionMgr,
								addService);
			} catch (Throwable e) {
				LOGGER.error("cannot checkCompatibleServiceInActionMgr : {}", e);
				throw new TechnicalException(
						ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
			}

			for (AddableServiceIF addableService : addService) {
				ServiceBean contractedServiceBean = getAddableServiceBean(
						contract, addableService);

				result.add(contractedServiceBean);

			}
		}
		return new ServiceList(result);
	}

	private ServiceBean getAddableServiceBean(ContractIF contract,
			AddableServiceIF addableService) {
		LOGGER.debug("construct ContractServiceBean for addable service : {}",
				addableService.getName());
		ServiceBean contractedServiceBean = new ServiceBean();
		contractedServiceBean.setAccessFee(getAccessFees(addableService));
		contractedServiceBean
				.setSubscriptionFee(getSubscriptionFee(addableService));

		contractedServiceBean.setServiceId(addableService
				.getRatePlanServiceIdentifier().toString());

		ParameterIF[] parameters = addableService.getDefaultParameters();
		if (parameters != null) {
			contractedServiceBean.setParameters(getParameterDescriptorList(
					addableService, parameters, false));
		}

		contractedServiceBean.setServiceCode(addableService.getService()
				.getCode());
		contractedServiceBean
				.setServiceName(ServiceHelper.getServiceName(contract
						.getMyRatePlan().getCode(), addableService.getService()));
		return contractedServiceBean;
	}

	public double getAccessFees(AddableServiceIF service) {

		if (FeeCalcultatorHelper.isFeeDependingOnParam(ActionMgrIF.FEE_ACCESS,
				service)) {
			return FeeCalcultatorHelper.getSvcMinFee(ActionMgrIF.FEE_ACCESS,
					service.getRatePlan().getIdentifier().getString(), service
							.getIdentifier().getString());
		} else {

			return FeeCalcultatorHelper.calculateSvcFee(ActionMgrIF.FEE_ACCESS,
					service, service.getDefaultParameters());
		}
	}

	public double getSubscriptionFee(AddableServiceIF service) {
		if (FeeCalcultatorHelper.isFeeDependingOnParam(
				ActionMgrIF.FEE_SUBSCRIPTION, service)) {
			return FeeCalcultatorHelper.getSvcMinFee(
					ActionMgrIF.FEE_SUBSCRIPTION, service.getRatePlan()
							.getIdentifier().getString(), service
							.getIdentifier().getString());
		} else {
			return FeeCalcultatorHelper.calculateSvcFee(
					ActionMgrIF.FEE_SUBSCRIPTION, service,
					service.getDefaultParameters());
		}
	}

	public ServiceList getRemovableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException {

		LOGGER.info("START getRemovableServices ncli:{}  nd:{}", ncli, nd);

		ActionMgrIF actionMgr = ObjectMgr.createActionManager();

		Client client = clientManagerBiz.getClient(ncli);
		ContractIF contract = contractManagerBiz.getContract(client, nd);
		ContractedServiceIF[] contractedServiceIFArray = contract
				.getContractedServices(actionMgr, null);

		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());
		String levelType = orgF.getType().getCode();
		String ratePlanCode = contract.getMyRatePlan().getCode();

		List<ContractedServiceIF> contractedServiceIFList = new ArrayList<ContractedServiceIF>();
		if (contractedServiceIFArray != null) {
			for (ContractedServiceIF contractedServiceIF : contractedServiceIFArray) {
				String serviceId = contractedServiceIF.getIdentifier()
						.toString();
				LOGGER.debug("Test if the service {} is removable", serviceId);
				if (contractedServiceIF.getCategory().getIdentifier()
						.getString().equals("4")) {
					LOGGER.debug(
							"The service {} is not removable because category = 4",
							serviceId);
					continue;
				}
				if (contractedServiceIF.isRemovable()) {

					// FC5580 yaa Begin
					boolean isOk = (ServiceHelper
							.isServiceforOnlySup(contractedServiceIF
									.getService().getCode()) || ServiceHelper
							.isServiceforOnlySup(ratePlanCode,
									contractedServiceIF.getService().getCode()));
					if (isOk) {
						LOGGER.debug(
								"The service {} is removable because isServiceforOnlySup",
								serviceId);
						contractedServiceIFList.add(contractedServiceIF);
						continue;
					}
					// FC5580 yaa End

					// the services for min commitment cannot be removed or
					// modified
					String minCom;
					try {
						minCom = CommercialOfferWIAM.getServiceInfo(levelType,
								ratePlanCode, contractedServiceIF.getCode(),
								SERVICE_INFO_CODE_SVC_MINCOM_FLAG);

						if (minCom != null && minCom.equals(CONSTANT_1)) {
							LOGGER.debug(
									"The service {} is not removable because min commitment",
									serviceId);
							continue;
						}

						// YFI FC4838 begin
						if (ServiceHelper.isServiceHidden(ratePlanCode,
								contractedServiceIF.getService().getCode())) {
							String svcFlag = CommercialOfferWIAM
									.getServiceInfo(null, ratePlanCode,
											contractedServiceIF.getService()
													.getCode(),
											SERVICE_INFO_CODE_SVC_NOT_HIDDEN_MS);
							if (!ServiceHelper.isServiceforOnlyModif(
									ratePlanCode, contractedServiceIF
											.getService().getCode())
									&& !CONSTANT_1.equals(svcFlag) /*
																	 * YFi
																	 * FC4838
																	 */) { // FC
								// 4231
								// -
								// MMA
								LOGGER.debug(
										"The service {} is not removable because SVC_NOT_HIDDEN_MS=1",
										serviceId);
								continue;
							}
						}
					} catch (SQLException e) {
						LOGGER.error("Cannot getServiceInfo : ", e);
						continue;
					}

					contractedServiceIFList.add(contractedServiceIF);
				}
			}
		}

		List<ServiceBean> result = new ArrayList<ServiceBean>();
		for (ContractedServiceIF contractedServiceIF : contractedServiceIFList) {
			ServiceBean contractedServiceBean = getContractedServiceBean(
					contract, contractedServiceIF);

			result.add(contractedServiceBean);
		}
		LOGGER.info(
				"End getRemovableServices ncli:{}  nd:{} result contains {} services.",
				new Object[] { ncli, nd, contractedServiceIFList.size() });

		return new ServiceList(result);
	}

	public ServiceList getEditableServices(String ncli, String nd)
			throws FunctionnalException, TechnicalException {
		LOGGER.info("START getEditableServices ncli:{}  nd:{}", ncli, nd);

		ActionMgrIF actionMgr = ObjectMgr.createActionManager();

		Client client = clientManagerBiz.getClient(ncli);
		ContractIF contract = contractManagerBiz.getContract(client, nd);
		ContractedServiceIF[] ContractedServiceIFArray = contract
				.getContractedServices(actionMgr, null);

		String ratePlanCode = contract.getMyRatePlan().getCode();

		List<ContractedServiceIF> contractedServiceIFList = new ArrayList<ContractedServiceIF>();
		if (ContractedServiceIFArray != null) {
			for (ContractedServiceIF contractedServiceIF : ContractedServiceIFArray) {
				String serviceId = contractedServiceIF.getIdentifier()
						.toString();
				LOGGER.debug("Test if the service {} is editable", serviceId);

				if (contractedServiceIF.getCategory().getIdentifier()
						.getString().equals("4")) {
					LOGGER.debug(
							"The service {} is not editable because category = 4",
							serviceId);
					continue;
				}

				try {
					if (ServiceHelper.isServiceReadOnly(contractedServiceIF
							.getService().getCode())
							|| ServiceHelper.isServiceHidden(ratePlanCode,
									contractedServiceIF.getService().getCode())) {
						if (!CONSTANT_1.equals(CommercialOfferWIAM
								.getServiceInfo(null, ratePlanCode,
										contractedServiceIF.getService()
												.getCode(),
										SERVICE_INFO_CODE_SVC_PACK_MODIF))) {
							LOGGER.debug(
									"The service {} is not editable because SVC_PACK_MODIF != 1",
									serviceId);
							continue;
						}
					}
					// FC5892
					if (CONSTANT_1.equals(CommercialOfferWIAM.getServiceInfo(
							null, ratePlanCode, contractedServiceIF
									.getService().getCode(),
							SERVICE_INFO_CODE_SVC_ALLOW_MODIF))
							|| CONSTANT_1.equals(CommercialOfferWIAM
									.getServiceInfo(null, null,
											contractedServiceIF.getService()
													.getCode(),
											SERVICE_INFO_CODE_SVC_ALLOW_MODIF))) {
						LOGGER.debug(
								"The service {} is not editable because SVC_ALLOW_MODIF = 1",
								serviceId);
						continue;
					}
				} catch (SQLException e) {
					LOGGER.error("Cannot getServiceInfo : ", e);
					continue;
				}

				// verify if it is modifiable
				ParameterIF[] parameters = contractedServiceIF.getParameters();
				if (parameters == null || parameters.length == 0) {
					LOGGER.debug(
							"The service {} is not editable because parameters are null or empty",
							serviceId);
					continue;
				}

				contractedServiceIFList.add(contractedServiceIF);
			}
		}

		List<ServiceBean> result = new ArrayList<ServiceBean>();
		for (ContractedServiceIF contractedServiceIF : contractedServiceIFList) {
			ServiceBean contractedServiceBean = getContractedServiceBean(
					contract, contractedServiceIF);

			result.add(contractedServiceBean);
		}
		LOGGER.info(
				"End getRemovableServices ncli:{}  nd:{} result contains {} services.",
				new Object[] { ncli, nd, contractedServiceIFList.size() });

		return new ServiceList(result);
	}

	public ServiceList getServiceParamsModify(String ncli, String nd,
			List<String> serviceIds) throws FunctionnalException,
			TechnicalException {
		ActionMgrIF actionMgr = ObjectMgr.createActionManager();

		ContractIF contract = contractManagerBiz.getContract(ncli, nd);

		List<ServiceBean> result = new ArrayList<ServiceBean>();
		for (String serviceId : serviceIds) {
			ServiceBean serviceResponseBean = new ServiceBean();
			serviceResponseBean.setServiceId(serviceId);

			ContractedServiceIF contractedServiceIF = contract
					.getContractedService(actionMgr,
							ObjectId.instantiate(serviceId));

			ParameterIF[] parameters = getParameters(contractedServiceIF);
			if (parameters != null) {
				serviceResponseBean.setParameters(getParameterDescriptorList(
						contractedServiceIF, parameters, true));
			}
			result.add(serviceResponseBean);
		}

		return new ServiceList(result);
	}

	private ParameterIF[] getParameters(ServiceIF service)
			throws FunctionnalException {
		if (service == null) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_SEARCH_WS);
		}
		if (service instanceof ContractedServiceIF) {
			return ((ContractedServiceIF) service).getParameters();
		} else {
			return service.getDefaultParameters();
		}
	}

	public List<ValueChoiceItemIF> getValueChoiceItems(
			RatePlanServiceIF service, ParameterIF param) {

		List<ValueChoiceItemIF> aList = new ArrayList<ValueChoiceItemIF>();

		@SuppressWarnings("unchecked")
		List<SvcFeeParamValue> list = (List<SvcFeeParamValue>) SvcFeeParamValueListBuilder
				.getParamValuesToDisplay(service, ActionMgrIF.FEE_ACCESS);
		if (list != null) {
			ValueChoiceItemIF items[] = ((ValueChoiceIF) param).getItems();
			for (SvcFeeParamValue feeParamValue : list) {
				if (feeParamValue.getParamID() == ObjectId.convertToLong(
						param.getParameterDescriptor().getIdentifier())
						.longValue()) {
					for (int j = 0; j < items.length; j++)
						if (feeParamValue.getChoiceItemID() == ObjectId
								.convertToLong(
										items[j].getChoiceItem()
												.getIdentifier()).longValue()
								&& !aList.contains(items[j]))
							aList.add(items[j]);
				}
			}
		}
		return aList;

	}

	public List<ServiceRule> getServiceRules(
			ServiceRulesRequestBean serviceRulesRequestBean)
			throws FunctionnalException, TechnicalException {
		ContractIF contract = contractManagerBiz.getContract(
				serviceRulesRequestBean.getNcli(),
				serviceRulesRequestBean.getNd());

		List<ServiceRule> result = new ArrayList<ServiceRule>();
		result.addAll(getServiceRulesForRemovedServices(
				serviceRulesRequestBean, contract));

		result.addAll(getServiceRulesForAddedServices(serviceRulesRequestBean,
				contract));

		return result;
	}

	@SuppressWarnings("unchecked")
	private List<ServiceRule> getServiceRulesForAddedServices(
			ServiceRulesRequestBean serviceRulesRequestBean, ContractIF contract)
			throws TechnicalException {
		ContractedServiceIF[] contractedServ = contract.getContractedServices(
				null, null);
		Vector<ServiceIF> subscribedServices = getContractedServicesAsVector(contractedServ);

		Vector<ServiceIF> removableServices = getServicesToRemove(
				serviceRulesRequestBean, subscribedServices);
		if (!removableServices.isEmpty()) {
			subscribedServices.removeAll(removableServices);
		}

		List<String> addedServicesCodes = getAddedServicesCodes(serviceRulesRequestBean);

		List<ServiceRule> result = new ArrayList<ServiceRule>();
		if (addedServicesCodes != null && !addedServicesCodes.isEmpty()) {
			List<ServiceRule> serviceErrors = null;
			Vector<ServiceIF> listServicesToCheck = new Vector<ServiceIF>();
			LOOP: for (String serviceId : addedServicesCodes) {
				serviceErrors = null;

				// JBA BEGIN FC4756
				// MHA - 01/04/11 - DI4391 FC4146
				List listIncompatibleOrNeededServiceRules;
				try {
					listIncompatibleOrNeededServiceRules = ServiceRulesMgr
							.checkServiceRules(
									ObjectRefMgr
											.getRatePlanService(
													ObjectId.instantiate(serviceId))
											.getIdentifier().getString(),
									ServiceRulesMgr.SERVICE_RULE_NEED, null,
									null, null);

					// END MHA - 01/04/11 - DI4391 FC4146

					List listIncompatibleServiceRules = ServiceRulesMgr
							.checkServiceRules(
									ObjectRefMgr
											.getRatePlanService(
													ObjectId.instantiate(serviceId))
											.getIdentifier().getString(),
									ServiceRulesMgr.SERVICE_RULE_INCOMPATIBLE,
									null, null, null);

					ServiceRulesMgr.ajouterListeAUneAutre(
							listIncompatibleOrNeededServiceRules,
							listIncompatibleServiceRules);

					// JBA
					if (listIncompatibleOrNeededServiceRules != null
							&& listIncompatibleOrNeededServiceRules.size() > 0) {
						// MHA FC 4657
						serviceErrors = ServiceRulesMgr
								.checkIncompatibleServiceOrNeededService(
										listIncompatibleOrNeededServiceRules,
										subscribedServices, addedServicesCodes
												.toArray(new String[0]));

					}

					// JBA END FC4756
					// ne pas oublier que ceci pour un NA et MS seulement
					List<ServiceRule> listSuffisantServiceRules = ServiceRulesMgr
							.checkServiceRules(
									ObjectRefMgr
											.getRatePlanService(
													ObjectId.instantiate(serviceId))
											.getIdentifier().getString(),
									ServiceRulesMgr.SERVICE_RULE_EXIGE_UN_PARMI_PLUSIEURS,
									null, null, null);// 3747

					// keep only null rateplan yaa FC5460 begin
					List<ServiceRule> finalList = new ArrayList<ServiceRule>();
					for (ServiceRule itemList : listSuffisantServiceRules) {
						if (itemList.getRatePlanID() == null) {
							itemList.setRatePlanCode(contract.getMyRatePlan()
									.getIdentifier().getString());
							finalList.add(itemList);
						}
					}
					listSuffisantServiceRules = finalList;
					List<ServiceRule> listSuffisantServiceRulesFC5460 = ServiceRulesMgr
							.checkServiceRules(
									ObjectRefMgr
											.getRatePlanService(
													ObjectId.instantiate(serviceId))
											.getIdentifier().getString(),
									ServiceRulesMgr.SERVICE_RULE_EXIGE_UN_PARMI_PLUSIEURS,
									contract.getMyRatePlan(), null, null); // yaa
																			// FC5460
					listSuffisantServiceRules
							.addAll(listSuffisantServiceRulesFC5460);
					// keep only null rateplan yaa FC5460 end
					if ((listSuffisantServiceRules != null && listSuffisantServiceRules
							.size() > 0)) {

						List<ServiceRule> serviceSuffisantErrors = ServiceRulesMgr
								.checkServiceSuffisant(
										listSuffisantServiceRules,
										subscribedServices, addedServicesCodes
												.toArray(new String[0]));

						if (serviceSuffisantErrors != null
								&& serviceSuffisantErrors.size() > 0) {
							if (serviceErrors == null) {
								serviceErrors = new ArrayList<ServiceRule>();
							}
							serviceErrors.addAll(serviceSuffisantErrors);
						}

					}

					if (serviceErrors != null && serviceErrors.size() > 0) {

						for (int k = 0; k < serviceErrors.size(); k++) {
							ServiceRule serviceError = (ServiceRule) serviceErrors
									.get(k); // MHA FC 4657

							if (serviceError.getRuleId().equals("5")) {
								String serviceName2 = "";
								String serviceID2 = "";
								String serviceCode2 = "";

								for (int j = 0; j < listSuffisantServiceRules
										.size(); j++) {

									serviceID2 = serviceID2
											+ ((ServiceRule) listSuffisantServiceRules
													.get(j)).getServiceID2();
									serviceName2 = serviceName2
											+ "["
											+ ((ServiceRule) listSuffisantServiceRules
													.get(j)).getServiceName2()
											+ "]";
									serviceCode2 = serviceCode2
											+ ((ServiceRule) listSuffisantServiceRules
													.get(j)).getServiceCode2();
									if (j != (listSuffisantServiceRules.size() - 1)) {
										serviceID2 = serviceID2 + ",";
										serviceCode2 = serviceCode2 + ",";
										serviceName2 = serviceName2 + ",";
									}

								}

								serviceError.setServiceID2(serviceID2);
								serviceError.setServiceCode2(serviceCode2);
								serviceError.setServiceName2(serviceName2);

								serviceError.setMessageRule("LE SERVICE ["
										+ serviceError.getServiceName1() + "] "
										+ serviceError.getRuleDescription()
										+ " " + serviceError.getServiceName2());

							}

							if (serviceError.getRatePlanName() != null) {
								serviceError.setMessageRule(serviceError
										.getMessageRule()
										+ " pour le plan tarifaire ["
										+ serviceError.getRatePlanName() + "]");
							}
						}

						break LOOP;
					}

					// FIN JBA FC4756

					listServicesToCheck.add(ObjectRefMgr.getRatePlanService(
							ObjectId.instantiate(serviceId)).getService());
				} catch (Exception e) {
					LOGGER.error(
							"Une erreur est survenue lors du test de la compatibilité et la dépendance entre services : ",
							e);
					throw new TechnicalException(
							ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,
							"Une erreur est survenue lors du test de la compatibilité et la dépendance entre services. Contactez l'administrateur.");
				}
			}

			if (serviceErrors != null && serviceErrors.size() > 0) {
				result.addAll(serviceErrors);
			} else {
				listServicesToCheck = orderAddServiceListBasedOnSvcRules(listServicesToCheck);

				Hashtable<String, Hashtable<ServiceIF, Vector<ServiceIF>>> errorTable2 = new Hashtable<String, Hashtable<ServiceIF, Vector<ServiceIF>>>();
				ServiceHelper.checkServices(listServicesToCheck,
						subscribedServices, true, errorTable2);
				if (!errorTable2.isEmpty()) {
					result.addAll(getServiceRules(errorTable2));
				}

			}
		}

		return result;
	}

	private List<String> getAddedServicesCodes(
			ServiceRulesRequestBean serviceRulesRequestBean) {
		List<String> addedServicesCodes = new ArrayList<String>();
		if (serviceRulesRequestBean.getServicesToAdd() != null
				&& serviceRulesRequestBean.getServicesToAdd().getServices() != null) {
			for (ServiceBean serviceBean : serviceRulesRequestBean
					.getServicesToAdd().getServices()) {
				addedServicesCodes.add(serviceBean.getServiceId());
			}

		}
		return addedServicesCodes;
	}

	private Vector<ServiceIF> getContractedServicesAsVector(
			ContractedServiceIF[] contractedServ) {
		Vector<ServiceIF> subscribedServices = new Vector<ServiceIF>();
		if (contractedServ != null) {
			for (int i = 0; i < contractedServ.length; i++) {
				subscribedServices.add(contractedServ[i].getService());
			}
		}
		return subscribedServices;
	}

	private List<ServiceRule> getServiceRulesForRemovedServices(
			ServiceRulesRequestBean serviceRulesRequestBean, ContractIF contract) {

		List<ServiceRule> result = new ArrayList<ServiceRule>();
		if (serviceRulesRequestBean.getServicesToRemove() != null
				&& serviceRulesRequestBean.getServicesToRemove().getServices() != null) {
			ContractedServiceIF[] contractedServ = contract
					.getContractedServices(null, null);
			Vector<ServiceIF> subscribedServices = getContractedServicesAsVector(contractedServ);

			List<String> addedServicesCodes = getAddedServicesCodes(serviceRulesRequestBean);

			if (addedServicesCodes != null) {
				for (String serviceId : addedServicesCodes) {
					subscribedServices.add(ObjectRefMgr.getRatePlanService(
							ObjectId.instantiate(serviceId)).getService());
				}
			}

			Hashtable<String, Hashtable<ServiceIF, Vector<ServiceIF>>> errorTable = new Hashtable<String, Hashtable<ServiceIF, Vector<ServiceIF>>>();

			ServiceHelper.checkServices(
					getServicesToRemove(serviceRulesRequestBean,
							subscribedServices), subscribedServices, false,
					errorTable);
			if (!errorTable.isEmpty()) {
				result.addAll(getServiceRules(errorTable));
			}
		}
		return result;
	}

	private Vector<ServiceIF> getServicesToRemove(
			ServiceRulesRequestBean serviceRulesRequestBean,
			Vector<ServiceIF> subscribedServices) {
		Vector<ServiceIF> removableServices = new Vector<ServiceIF>();
		if (serviceRulesRequestBean.getServicesToRemove() != null
				&& serviceRulesRequestBean.getServicesToRemove().getServices() != null) {
			for (ServiceBean serviceToRemove : serviceRulesRequestBean
					.getServicesToRemove().getServices()) {
				for (ServiceIF subscribedService : subscribedServices) {
					if (subscribedService
							.getIdentifier()
							.getString()
							.equals(ObjectId.instantiate(
									serviceToRemove.getServiceId()).getString())) {
						removableServices.add(subscribedService);
					}
				}
			}
		}

		return removableServices;
	}

	private List<ServiceRule> getServiceRules(
			Hashtable<String, Hashtable<ServiceIF, Vector<ServiceIF>>> errorTable) {
		List<ServiceRule> result = new ArrayList<ServiceRule>();

		if (errorTable.get("incompatible") != null) {
			result.addAll(getServiceRules("incompatible",
					errorTable.get("incompatible")));
		}

		if (errorTable.get("required") != null) {
			result.addAll(getServiceRules("required",
					errorTable.get("required")));
		}

		return result;
	}

	private List<ServiceRule> getServiceRules(
			String requiredOrIncompatible,
			Hashtable<ServiceIF, Vector<ServiceIF>> requiredOrIncompatibleServices) {
		List<ServiceRule> rules = new ArrayList<ServiceRule>();
		Enumeration<ServiceIF> keys = requiredOrIncompatibleServices.keys();
		while (keys.hasMoreElements()) {
			ServiceIF service = keys.nextElement();
			Vector<ServiceIF> incVector = requiredOrIncompatibleServices
					.get(service);
			ServiceRule serviceRule = new ServiceRule();

			if (incVector != null && incVector.size() > 0) {
				for (int i = 0; i < incVector.size(); i++) {

					serviceRule.setServiceName1(service.getName());
					serviceRule.setServiceID1(service.getIdentifier()
							.toString());
					serviceRule.setServiceCode1(service.getCode());

					ServiceIF service2 = incVector.get(i);
					serviceRule.setServiceName2(service2.getName());
					serviceRule.setServiceID2(service2.getIdentifier()
							.toString());
					serviceRule.setServiceCode2(service2.getCode());

					if ("incompatible".equalsIgnoreCase(requiredOrIncompatible)) {

						serviceRule.setMessageRule(serviceRule
								.getServiceName1()
								+ " est incompatible avec "
								+ serviceRule.getServiceName2());
						serviceRule.setRuleDescription("incompatible");
					} else {
						serviceRule.setMessageRule(serviceRule
								.getServiceName1()
								+ " exige "
								+ serviceRule.getServiceName2());
						serviceRule.setRuleDescription("exige");
					}
					rules.add(serviceRule);
				}
			}
		}
		return rules;
	}

	public Vector orderAddServiceListBasedOnSvcRules(
			Vector<ServiceIF> serviceList) {

		Vector rule = null;
		ServiceIF tmpSvc = null;
		Vector<ServiceIF> serviceListOrdered = new Vector<ServiceIF>();

		int index = -1;

		// check for every service if it needs orher services in the list and
		// add all those services before it in the orderesd list
		for (ServiceIF service : serviceList) {
			tmpSvc = service;
			// add it at the end of the ordered list
			if (!serviceListOrdered.contains(service)) {
				serviceListOrdered.add(service);
			}
			index = serviceListOrdered.indexOf(service);

			// check if there are service needed that are not before it in the
			// list.
			rule = ServiceHelper.checkRequired(tmpSvc, new Vector(
					serviceListOrdered.subList(0, index)));
			for (int j = 0; j < rule.size(); j++) {
				if (!serviceListOrdered.contains((ServiceIF) rule.get(j))
						&& serviceList.contains((ServiceIF) rule.get(j))) {
					// add the needed service before
					serviceListOrdered.add(index, (ServiceIF) rule.get(j));
				}

			}
		}

		return serviceListOrdered;
	}

	public ServiceList getServiceParamsAdd(String ncli, String nd,
			List<String> serviceIds) throws FunctionnalException,
			TechnicalException {

		List<ServiceBean> result = new ArrayList<ServiceBean>();
		for (String serviceId : serviceIds) {

			ServiceBean serviceResponseBean = new ServiceBean();
			serviceResponseBean.setServiceId(serviceId);

			RatePlanServiceIF service = ObjectRefMgr
					.getRatePlanService(ObjectId.instantiate(serviceId));
			ParameterIF[] parameters = getParameters(service);
			if (parameters != null) {
				serviceResponseBean.setParameters(getParameterDescriptorList(
						service, parameters, true));
			}
			result.add(serviceResponseBean);
		}
		return new ServiceList(result);
	}
}
