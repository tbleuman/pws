package ma.iam.wiam.business.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netonomy.blm.interfaces.contact.TitleIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.util.CountryIF;

import ma.iam.wiam.business.IdentityManagerBiz;
import ma.iam.wiam.dao.CountryDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.netoapi.dao.TitlesDao;
import ma.iam.wiam.netoapi.dao.ValueChoiceItemDao;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;

@Component("identityManagerBiz")
public class IdentityManagerBizImpl implements IdentityManagerBiz {

	private static final String CONTACT_PRO_GP_SEX = "CONTACT_PRO_GP_SEX";

	private static final String CONTACT_GP_TYPE_IDENT = "CONTACT_GP_TYPE_IDENT";

	private static final String CONTACT_PRO_GP_ETAT_CIVL = "CONTACT_PRO_GP_ETAT_CIVL";

	private static final String CONTACT_JOB = "CONTACT_JOB";

	private static final String CONTACT_ENT_TYPE_IDENT = "CONTACT_ENT_TYPE_IDENT";
	private static final String CONTACT_PRF_TYPE_IDENT = "CONTACT_PRF_TYPE_IDENT";
	/**Begin feature/wsTools**/
	private static final String BA_SUPPORT_FACTURE = "BA_SUPPORT_FACTURE";
	private static final String BA_LANGUE_FACTURE = "BA_LANGUE_FACTURE";
	private static final String CONTACT_TYPE_DIST = "CONTACT_TYPE_DIST";
	private static final String OPERATEUR_WINBACK_A ="6.593.2";
	private static final String OPERATEUR_WINBACK_B ="6.595.2";
	private static final String OPERATEUR_WINBACK_C ="6.597.2";
	/**End feature/wsTools**/

	@Autowired
	private ValueChoiceItemDao valueChoiceItemDao;

	@Autowired
	private TitlesDao titlesDao;
	/**Begin feature/wsTools**/
	@Autowired
	private CountryDao countryDao;
	/**End feature/wsTools**/
	public List<TitleIF> getCodesPersonnes() throws FunctionnalException, TechnicalException {
		List<TitleIF> list = titlesDao.getAllTitles();
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}

	public List<ValueChoiceItemIF> getEtatsCiviles() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(CONTACT_PRO_GP_ETAT_CIVL);
	}

	public List<ValueChoiceItemIF> getIdentifiantsResidentiels() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(CONTACT_GP_TYPE_IDENT);
	}

	public List<Object> getIdentifiantsProfAndEntreprise() throws FunctionnalException, TechnicalException {
		throw new UnsupportedOperationException();
	}

	public List<ValueChoiceItemIF> getProfessions() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(CONTACT_JOB);
	}

	public List<ValueChoiceItemIF> getIdentifiantsProf() throws FunctionnalException, TechnicalException {
		List<ValueChoiceItemIF> list = valueChoiceItemDao.getValueChoiceItems(CONTACT_PRF_TYPE_IDENT);
		return list;
	}

	public List<ValueChoiceItemIF> getIdentifiantsEntreprise() throws FunctionnalException, TechnicalException {
		List<ValueChoiceItemIF> list = valueChoiceItemDao.getValueChoiceItems(CONTACT_ENT_TYPE_IDENT);
		return list;
	}
	
	public List<ValueChoiceItemIF> getGenders() throws FunctionnalException, TechnicalException {
		return valueChoiceItemDao.getValueChoiceItems(CONTACT_PRO_GP_SEX);
	}

	/**Begin feature/wsTools**/
	@Override
	public List<ValueChoiceItemIF> getSupportFacturation() throws FunctionnalException, TechnicalException {
		
		return valueChoiceItemDao.getValueChoiceItems(BA_SUPPORT_FACTURE);
	}

	@Override
	public List<ValueChoiceItemIF> getLangue() throws FunctionnalException, TechnicalException {
		
		return valueChoiceItemDao.getValueChoiceItems(BA_LANGUE_FACTURE);
	}
	@Override
	public List<ValueChoiceItemIF> getTypeDistribution() throws FunctionnalException, TechnicalException {
		
		return valueChoiceItemDao.getValueChoiceItems(CONTACT_TYPE_DIST);
	}
	
	@Override
	public List<ValueChoiceItemIF> getOperateurWinBack() throws FunctionnalException, TechnicalException {
		// TODO Auto-generated method stub
		return valueChoiceItemDao.getValueChoiceItems(OPERATEUR_WINBACK_B);
		
	}
	
	
	
	@Override
	public List<CountryIF> getCountrys() throws FunctionnalException, TechnicalException {
		List<CountryIF> list =countryDao.getAllCountrys();
		if (CollectionUtils.isEmpty(list)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT_WS);
		}
		return list;
	}
	/**End feature/wsTools**/

	

	
	

	
}
