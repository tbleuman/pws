package ma.iam.wiam.interceptor;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class InTraceInterceptorImpl extends AbstractPhaseInterceptor implements TraceInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(InTraceInterceptorImpl.class);
	private BaseDAO baseDAO;

	private String ticket = "ticket";

	private String userName = "userName";

	private String pwsTraceQuery = "SELECT COUNT(1) from  PWS_TRACES WHERE REQUEST_EXTERNAL_REFERENCE = :request_external_reference";

	public InTraceInterceptorImpl() {
		super(Phase.PRE_INVOKE);
	}

	public void handleMessage(Message message) throws Fault {
		MessageContentsList inObjects = MessageContentsList.getContentsList(message);
		if (inObjects != null && !inObjects.isEmpty()){
			for (Iterator<Object> it = inObjects.iterator(); it.hasNext() ;){
				Object ob = it.next();
				if( ob !=null) {
                    try {
                        String ticketId = BeanUtils.getProperty(ob, ticket);
                        String userNameId = BeanUtils.getProperty(ob, userName);
                        if (ticketId != null && !"".equals(ticketId.trim()) && ticketId.matches("[0-9]+")) {
                            Map<String, String> namedParameters = new HashMap<String, String>();
                            namedParameters.put("request_external_reference", ticketId);
                            int countTicket = baseDAO.getNamedParameterJdbcTemplate().queryForObject(pwsTraceQuery, namedParameters,
                                    Integer.class);
                            LOG.debug("******************* " + ob.getClass().getName() + " ++++++++  " + countTicket);
                            message.getExchange().put("ma.iam.ticket", ticketId + "");
                            message.getExchange().put("ma.iam.userName", userNameId + "");
                            System.out.println(message.getExchange().get("ma.iam.ticket"));
                            if (countTicket > 0)
                                throw new Fault(new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_TICKET,"Ticket déjà traité : " + ticketId));


                        }
                    } catch (NoSuchMethodException e) {
                        LOG.debug("NoSuchMethodException : "+e.getMessage());
                    } catch (IllegalAccessException e) {
                        LOG.debug("IllegalAccessException : "+e.getMessage());
                    } catch (InvocationTargetException e) {
                        LOG.debug("InvocationTargetException : "+e.getMessage());
                    }
                }

			}
		}
	}


    public BaseDAO getBaseDAO() {
        return baseDAO;
    }

    public void setBaseDAO(BaseDAO baseDAO) {
        this.baseDAO = baseDAO;
    }

    public String getPwsTraceQuery() {
        return pwsTraceQuery;
    }

    public void setPwsTraceQuery(String pwsTraceQuery) {
        this.pwsTraceQuery = pwsTraceQuery;
    }

    public String getTicket() {        return ticket;    }

    public void setTicket(String ticket) {        this.ticket = ticket;    }

    @Override
    public void performTrace(Object respObject, String operation, String userName, String ticket, String userNameId) throws TraceException {

    }
}
