package ma.iam.wiam.business;

import java.util.List;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.param.criteria.DansVoieCriteria;
import ma.iam.wiam.param.criteria.QuartierCriteria;
import ma.iam.wiam.param.criteria.VoieCriteria;
import ma.iam.wiam.ws.neto.model.Contact;
import ma.iam.wiam.ws.neto.model.PCommune;
import ma.iam.wiam.ws.neto.model.PDansVoie;
import ma.iam.wiam.ws.neto.model.PQuartier;
import ma.iam.wiam.ws.neto.model.PVoie;

public interface AdresseManagerBiz {

	List<PCommune> getListCommune() throws FunctionnalException, TechnicalException;

	List<PVoie> getVoieByByCriteria(VoieCriteria voieCriteria) throws FunctionnalException, TechnicalException;

	List<PQuartier> getQuartiersByCriteria(QuartierCriteria quartier) throws FunctionnalException, TechnicalException;

	String getAdresseAsString(Contact contact) throws FunctionnalException, TechnicalException;
	
	/**Begin feature/wsTools*/
	List<PDansVoie> getDansVoieByCriteria(DansVoieCriteria dansVoieCriteria)throws FunctionnalException, TechnicalException;
	/**End feature/wsTools*/
}
