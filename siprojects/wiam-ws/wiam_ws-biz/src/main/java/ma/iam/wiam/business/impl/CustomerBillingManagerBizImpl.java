package ma.iam.wiam.business.impl;

import static ma.iam.wiam.business.constants.Constants.EMPTY_STRING;
import static ma.iam.wiam.business.constants.Constants.ENTREPRISE;
import static ma.iam.wiam.business.constants.Constants.GRANDS_COMPTE;
import static ma.iam.wiam.business.constants.Constants.MODE_PAIEMENT_PRELEVEMENT_CODE;
import static ma.iam.wiam.business.constants.Constants.PROFESSIONNEL;
import static ma.iam.wiam.business.constants.Constants.TELEBOUTIQUE;

import java.util.ArrayList;
import java.util.List;

import ma.iam.wiam.adresse.AdresseMgr;
import ma.iam.wiam.business.AddressManagerBiz;
import ma.iam.wiam.business.ClientManagerBiz;
import ma.iam.wiam.business.ContactManagerBiz;
import ma.iam.wiam.business.CustomerBillingManagerBiz;
import ma.iam.wiam.customer.BillingAccountWIAM;
import ma.iam.wiam.customer.ContactWIAM;
import ma.iam.wiam.customer.PaymentInfoWIAM;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.AdresseBean;
import ma.iam.wiam.neto.bean.BillingContactDetailBean;
import ma.iam.wiam.neto.bean.ContactDeFacturationBean;
import ma.iam.wiam.neto.bean.CustomerBillingInfoBean;
import ma.iam.wiam.neto.bean.MethodeDePaiementBean;
import ma.iam.wiam.neto.bean.ReferentialBean;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.param.criteria.ClientSearchCriteria;
import ma.iam.wiam.util.StringUtil;
import ma.iam.wiam.validators.CustomerBillingAccountValidator;
import ma.iam.wiam.validators.OrganizationValidator;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.netonomy.blm.api.BillingAccount.BillingAccountF;
import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.interfaces.billing.PaymentMethodIF;
import com.netonomy.blm.interfaces.contact.ContactIF;
import com.netonomy.blm.interfaces.request.RequestIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.MailingAddressIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import com.netonomy.util.LongId;
import com.netonomy.util.ObjectId;

/**
 * 
 * @author s.hroumti
 *
 */
@Component("customerBillingManagerBizImpl")
public class CustomerBillingManagerBizImpl implements CustomerBillingManagerBiz {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CustomerBillingManagerBizImpl.class);

	@Autowired
	private ClientDao clientDao;
	@Autowired
	private LevelFDao levelFDao;

	@Autowired
	@Qualifier("baseNetoDAOImpl")
	private BaseNetoDao baseNetoDao;

	@Autowired
	private ContactManagerBiz contactManagerBiz;

	@Autowired
	private AddressManagerBiz addressManagerBiz;

	@Autowired
	private ClientManagerBiz clientManagerBiz;
	

	public List<CustomerBillingInfoBean> getBillingInfos(
			ClientSearchCriteria criteria) throws FunctionnalException,
			TechnicalException {
		String ncli = criteria.getNcli();
		if (StringUtil.isNullOrEmpty(ncli)) {
			throw new FunctionnalException(
					ExceptionCodeTypeEnum.EMPTY_SEARCH_WS,
					"critére de recherches est vide");
		}

		Client client = clientManagerBiz.getClient(ncli);
		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());

		// CommonValidator
		// .validateCategory(orgF.getType().getCategory().getCode());

		CustomerBillingInfoBean customerBillingInfoBean = new CustomerBillingInfoBean();
		customerBillingInfoBean.setNcli(ncli);
		customerBillingInfoBean.setCategorieSs(client.getOrgTypeName());
		customerBillingInfoBean
				.setBillingContactDetailBean(constructBillingContactDetailBean(orgF));

		List<CustomerBillingInfoBean> result = new ArrayList<CustomerBillingInfoBean>();
		result.add(customerBillingInfoBean);
		return result;
	}

	private BillingContactDetailBean constructBillingContactDetailBean(
			LevelF orgF) {
		BillingContactDetailBean billingContactDetailBean = new BillingContactDetailBean();

		BillingAccountF billingAccount = null;
		BillingAccountF[] billing = orgF.getBillingAccounts();
		if (billing != null) {
			billingAccount = billing[0];
		} else {
			return null;
		}

		setBillingContactPaymentParams(billingContactDetailBean, billingAccount);

		billingContactDetailBean
				.setContactDeFacturationBean(constructContactBean(orgF,
						billingAccount));

		return billingContactDetailBean;
	}

	private void setBillingContactPaymentParams(
			BillingContactDetailBean billingContactDetailBean,
			BillingAccountF billingAccount) {

		PaymentMethodIF pmIF = billingAccount.getPaymentMethod();
		if (pmIF == null) {
			LOGGER.debug("The payment method is null");
			return;
		}
		String modePaiement = pmIF.getIdentifier().getString();

		if (!StringUtil.isNullOrEmpty(modePaiement)) {
			try {
				billingContactDetailBean.setPaymentMethodCode(Integer
						.parseInt(modePaiement));
			} catch (NumberFormatException e) {
				LOGGER.debug("The value : " + modePaiement
						+ " of mode payment is not valid ", e);
			}
		}

		if (MODE_PAIEMENT_PRELEVEMENT_CODE.equals(modePaiement)) {

			MethodeDePaiementBean methodeDePaiementBean = new MethodeDePaiementBean();
			ParameterIF[] pParams = billingAccount.getPaymentMethodParameters();

			String codeBanque = PaymentInfoWIAM.getCodeBanque(pParams);
			if (codeBanque != null) {
				methodeDePaiementBean
						.setBanque(new ReferentialBean(codeBanque));
			}

			String codeAgence = PaymentInfoWIAM.getCodeAgence(pParams);
			if (codeAgence != null) {
				methodeDePaiementBean.setAgenceBancaire(new ReferentialBean(
						codeAgence));
			}

			String numCompte = PaymentInfoWIAM.getNumeroCompte(pParams);
			// if (numCompte == null) {
			// numCompte = EMPTY_STRING;
			// }
			methodeDePaiementBean.setNumeroDeCompte(numCompte);

			String cleRib = PaymentInfoWIAM.getCleBancaire(pParams);
			// if (cleRib == null) {
			// cleRib = EMPTY_STRING;
			// }
			methodeDePaiementBean.setRib(cleRib);

			String titulaireCompte = PaymentInfoWIAM
					.getNomTitulaireCompte(pParams);
			// if (titulaireCompte == null) {
			// titulaireCompte = EMPTY_STRING;
			// }
			methodeDePaiementBean.setTitulaireDeCompte(titulaireCompte);
			String authNumber = PaymentInfoWIAM.getNumeroAutorisation(pParams);
			methodeDePaiementBean.setNumeroAutorisation(authNumber);

			billingContactDetailBean
					.setMethodeDePaiementBean(methodeDePaiementBean);
		}

	}

	private String getBillingCycleCode(LevelF orgF) {
		BillingAccountF rootBillingAccount = null;
		BillingAccountF[] rootBilling = orgF.getHierarchyRoot()
				.getBillingAccounts();
		if (rootBilling != null) {
			rootBillingAccount = rootBilling[0];
		} else {
			return null;
		}

		ParameterIF[] rootLParams = rootBillingAccount
				.getAdditionalParameters();
		String cycleFactur = BillingAccountWIAM.getCycleFacturation(
				rootLParams, false);

		if (cycleFactur == null) {
			cycleFactur = EMPTY_STRING;
		}
		return cycleFactur;
	}

	private String getBillingSuppportCode(LevelF orgF,
			BillingAccountF billingAccount) {
		ParameterIF[] lParams = billingAccount.getAdditionalParameters();
		String supportFacturation = BillingAccountWIAM.getSupportFacture(
				lParams, false);
		if (supportFacturation == null) {
			supportFacturation = EMPTY_STRING;
		}

		String organisationCode = BillingAccountWIAM
				.getSmartFactOrgCode(lParams);
		if (organisationCode == null) {
			organisationCode = EMPTY_STRING;
		}

		if (organisationCode.equals(EMPTY_STRING)
				&& supportFacturation.equals(EMPTY_STRING)) {
			// getting the parent organization code
			LevelF orgParent = (LevelF) orgF.getParentLevel();
			if (orgParent != null) {
				// getting the organization code of the parent
				BillingAccountF[] billingParent = orgParent
						.getBillingAccounts();
				BillingAccountF billingAccountParent = null;
				if (billingParent != null) {
					billingAccountParent = billingParent[0];
					ParameterIF[] lParamsP = billingAccountParent
							.getAdditionalParameters();
					supportFacturation = BillingAccountWIAM.getSupportFacture(
							lParamsP, false);
					if (supportFacturation == null) {
						supportFacturation = EMPTY_STRING;
					}

					// organisationCode = BillingAccountWIAM
					// .getSmartFactOrgCode(lParamsP);
					// if (organisationCode == null) {
					// organisationCode = EMPTY_STRING;
					// }
				}
			}
		}

		return supportFacturation;
	}

	private ContactDeFacturationBean constructContactBean(LevelF orgF,
			BillingAccountF billingAccount) {

		ContactIF billingContact = orgF.getBillingContact();
		if (billingContact == null) {
			LOGGER.debug("billing contact is null");
			return null;
		}

		ContactDeFacturationBean contactDeFacturationBean = new ContactDeFacturationBean();
		String categorie = orgF.getType().getCategory().getCode();
		if (categorie != null
				&& (categorie.equalsIgnoreCase(PROFESSIONNEL)
						|| categorie.equalsIgnoreCase(ENTREPRISE)
						|| categorie.equalsIgnoreCase(TELEBOUTIQUE) || categorie
							.equalsIgnoreCase(GRANDS_COMPTE))) {
			contactDeFacturationBean.setRaisonSocial(billingContact
					.getBusinessName());

		}
		contactDeFacturationBean.setPrenom(billingContact.getFirstName());
		contactDeFacturationBean.setNom(billingContact.getLastName());
		if (billingContact.getPhoneNumber() != null) {
			contactDeFacturationBean.setTelephoneFix(billingContact
					.getPhoneNumber().getNumber());
		}
		if (billingContact.getPhoneNumber2() != null) {
			contactDeFacturationBean.setTelephoneMobile(billingContact
					.getPhoneNumber2().getNumber());
		}
		if (billingContact.getFaxNumber() != null) {
			contactDeFacturationBean.setFax(billingContact.getFaxNumber()
					.getNumber());
		}

		AdresseBean addressBean = constructAddressBean(billingContact);
		contactDeFacturationBean.setAdresse(addressBean);

		ParameterIF[] lParams = billingAccount.getAdditionalParameters();

		contactDeFacturationBean.setExempte(Boolean.TRUE
				.equals(BillingAccountWIAM.isExemptionTaxe(lParams)));

		contactDeFacturationBean.setGrouperFacture(Boolean.TRUE
				.equals(BillingAccountWIAM.isGroupemetFact(lParams)));

		contactDeFacturationBean.setSupportDeFacturation(new ReferentialBean(
				getBillingSuppportCode(orgF, billingAccount)));

		contactDeFacturationBean.setLangue(new ReferentialBean(
				BillingAccountWIAM.getLangue(lParams, false)));

		contactDeFacturationBean.setCycleDeFacturation(new ReferentialBean(
				getBillingCycleCode(orgF)));

		contactDeFacturationBean.setTermeDePaiment(new ReferentialBean(
				BillingAccountWIAM.getTermePaiement(lParams, false)));

		ParameterIF[] pParams = billingAccount.getPaymentMethodParameters();

		contactDeFacturationBean.setCodeConfidentiel(PaymentInfoWIAM
				.getCodeConfidentiel(pParams));

		return contactDeFacturationBean;
	}

	private AdresseBean constructAddressBean(ContactIF billingContact) {
		MailingAddressIF billingAddress = billingContact.getAddress();

		AdresseBean addressBean = null;
		if (billingAddress != null) {
			addressBean = new AdresseBean();
			addressBean.setPays(new ReferentialBean(billingAddress.getCountry()
					.getIdentifier().getString()));

			addressBean.setCommune(new ReferentialBean(AdresseMgr
					.getCommune(billingAddress)));
			ParameterIF[] lParams = billingContact.getAdditionalParameters();
			addressBean.setCommuneEtrangere(ContactWIAM
					.getCommuneEtranger(lParams));

			addressBean.setCodePostale(AdresseMgr
					.getCodePostale(billingAddress));

			addressBean.setQuartier(new ReferentialBean(AdresseMgr
					.getCodeQuartier(billingAddress)));
			addressBean.setQuartierNonCodifie(AdresseMgr
					.getQuartierName(billingAddress));
			addressBean.setVoie(new ReferentialBean(AdresseMgr
					.getCodeVoie(billingAddress)));
			addressBean.setVoieNonCodifie(AdresseMgr
					.getVoieName(billingAddress));
			addressBean.setNumeroVoie(AdresseMgr.getVoieNumber(billingAddress));

			addressBean.setBatiment(ContactWIAM.getBatiment(lParams));
			addressBean.setEscalier(ContactWIAM.getEscalier(lParams));
			addressBean.setEtage(ContactWIAM.getEtage(lParams));
			addressBean.setPorte(ContactWIAM.getPorte(lParams));
			addressBean.setTypeDestribution(new ReferentialBean(ContactWIAM
					.getTypeDistribution(lParams, false)));
			addressBean.setBoitePostale(ContactWIAM.getBoitePostale(lParams));
			addressBean.setComplementAdresse(ContactWIAM
					.getComplementAdresse(lParams));
		} else {

			LOGGER.debug("billing address is null");
		}
		return addressBean;
	}

	public String modifyBillingAccountInfo(
			CustomerBillingInfoBean customerBillingInfoBean)
			throws FunctionnalException, TechnicalException {

		CustomerBillingAccountValidator.validateBillingAccount(
				customerBillingInfoBean,
				ActionType.UPDATE_CUSTOMER_BILLING_ACCOUNT);

		Client client = clientManagerBiz.getClient(customerBillingInfoBean
				.getNcli());

		baseNetoDao.getCurrentSession();

		ActionMgrIF actionMgr = ObjectMgr.createOrder(null);

		LevelF orgF = levelFDao.get(ObjectId.newLongId(client.getId())
				.toString());

		// CommonValidator
		// .validateCategory(orgF.getType().getCategory().getCode());

		BillingAccountF billingAccount = orgF.getBillingAccounts()[0];

		modifyPaymentInfo(customerBillingInfoBean, actionMgr, billingAccount);

		if (customerBillingInfoBean.getBillingContactDetailBean()
				.getContactDeFacturationBean() != null) {

			modifyBillingAccount(customerBillingInfoBean, actionMgr, orgF,
					billingAccount);

			modifyBillingContact(customerBillingInfoBean, actionMgr, orgF);
		}

		actionMgr.submit(RequestIF.SUBMIT_MODE_NORMAL);

		return customerBillingInfoBean.getNcli();
	}

	private void modifyBillingContact(
			CustomerBillingInfoBean customerBillingInfoBean,
			ActionMgrIF actionMgr, LevelF orgF) throws TechnicalException,
			FunctionnalException {
		ContactIF contactIF = orgF.getBillingContact();
		MailingAddressIF mailingAddress = contactIF.getAddress();

		ContactDeFacturationBean contactDeFacturationBean = customerBillingInfoBean
				.getBillingContactDetailBean().getContactDeFacturationBean();
		addressManagerBiz.createAddress(contactDeFacturationBean
				.getAdresse(), mailingAddress, orgF.getType().getCategory()
				.getCode());

		ParameterIF[] addressParameterIFs = addressManagerBiz
				.setAdditionalInfosForAddress(contactDeFacturationBean.getAdresse());
		
		OrganizationValidator.validateContact(contactDeFacturationBean, orgF.getType().getCategory().getCode());
		contactManagerBiz.updateContact(contactDeFacturationBean, orgF.getType().getCategory().getCode(), contactIF);
		
		orgF.modifyBillingContact(actionMgr, contactIF, addressParameterIFs);
	}

	private void modifyBillingAccount(
			CustomerBillingInfoBean customerBillingInfoBean,
			ActionMgrIF actionMgr, LevelF orgF, BillingAccountF billingAccount)
			throws FunctionnalException {
		ParameterIF[] additionnalInfosForBillingContact = contactManagerBiz
				.setAdditionnalInfosForBillingContact(customerBillingInfoBean
						.getBillingContactDetailBean()
						.getContactDeFacturationBean(), orgF.getType()
						.getCategory().getCode());

		billingAccount.modifyBillingAccount(actionMgr,
				billingAccount.getPaymentResponsible(),
				additionnalInfosForBillingContact);
	}

	private void modifyPaymentInfo(
			CustomerBillingInfoBean customerBillingInfoBean,
			ActionMgrIF actionMgr, BillingAccountF billingAccount)
			throws FunctionnalException {

		customerBillingInfoBean
				.getBillingContactDetailBean()
				.getMethodeDePaiementBean()
				.setCodeConfidentiel(
						customerBillingInfoBean.getBillingContactDetailBean()
								.getContactDeFacturationBean()
								.getCodeConfidentiel());

		ParameterIF[] paymentMethodParameterIFs = contactManagerBiz
				.setPaymentMethodInfosForBillingContact(customerBillingInfoBean
						.getBillingContactDetailBean()
						.getMethodeDePaiementBean());
		billingAccount.modifyPaymentInfo(actionMgr,
				ObjectRefMgr.getPaymentMethod(new LongId(
						customerBillingInfoBean.getBillingContactDetailBean()
								.getPaymentMethodCode())),
				paymentMethodParameterIFs, null);
	}
	
}
