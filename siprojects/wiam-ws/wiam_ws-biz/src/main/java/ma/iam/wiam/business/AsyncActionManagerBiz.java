package ma.iam.wiam.business;

import com.netonomy.blm.interfaces.organization.LevelIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.model.NetoInt;

public interface AsyncActionManagerBiz {
    void processLevel(final ActionMgrIF actionMgr, final LevelIF parentLevelIF, final NiveauBean niveauBean, final NiveauBean parentNiveauBean, final String organizationTypeCategoryCode, final ActionType actionType) throws TechnicalException, FunctionnalException ;
    void addLevels(NetoInt netoInt,final NiveauBean niveauBean, final ActionType actionType, TicketObject ticketObject, String webMethod, String userName) throws FunctionnalException, TechnicalException ;
    }
