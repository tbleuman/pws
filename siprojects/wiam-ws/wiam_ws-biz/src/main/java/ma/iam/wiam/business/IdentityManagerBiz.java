package ma.iam.wiam.business;

import java.util.List;

import com.netonomy.blm.interfaces.contact.TitleIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.util.CountryIF;

import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;

public interface IdentityManagerBiz {
	
	List<TitleIF> getCodesPersonnes() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getEtatsCiviles() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getIdentifiantsResidentiels() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getIdentifiantsProf() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getIdentifiantsEntreprise() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getProfessions() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getGenders() throws FunctionnalException, TechnicalException;
	/**Begin feature/wsTools**/
	List<CountryIF> getCountrys() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getSupportFacturation() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getLangue() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getTypeDistribution() throws FunctionnalException, TechnicalException;
	
	List<ValueChoiceItemIF> getOperateurWinBack() throws FunctionnalException, TechnicalException;
	/**End feature/wsTools**/
	
}