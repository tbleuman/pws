/*
 * @author AtoS
 */
package ma.iam.wiam.neto.bean;

import java.io.Serializable;

/**
 * @author y.eddaalous
 */
public class ReferentialBean implements Serializable {

	/** La constante serialVersionUID. */
	private static final long serialVersionUID = -8616113284323002751L;

	private String code;
	private String libelle;

	public ReferentialBean() {
	}

	public ReferentialBean(String code) {
		this.code = code;
	}

	public ReferentialBean(String code, String libelle) {
		this.code = code;
		this.libelle = libelle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String label) {
		this.libelle = label;
	}
}
