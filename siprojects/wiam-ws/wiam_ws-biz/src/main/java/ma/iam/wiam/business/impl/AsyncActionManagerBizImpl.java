package ma.iam.wiam.business.impl;

import com.netonomy.blm.api.Hierarchy.LevelF;
import com.netonomy.blm.api.Organization.OrganizationF;
import com.netonomy.blm.api.utils.DescriptorOidIF;
import com.netonomy.blm.api.utils.ObjectMgr;
import com.netonomy.blm.api.utils.ObjectRefMgr;
import com.netonomy.blm.api.utils.SessionF;
import com.netonomy.blm.interfaces.contact.ContactIF;
import com.netonomy.blm.interfaces.organization.LevelIF;
import com.netonomy.blm.interfaces.organization.OrganizationTypeCategoryIF;
import com.netonomy.blm.interfaces.organization.OrganizationTypeIF;
import com.netonomy.blm.interfaces.parameters.ValueChoiceItemIF;
import com.netonomy.blm.interfaces.request.RequestIF;
import com.netonomy.blm.interfaces.util.ActionItemIF;
import com.netonomy.blm.interfaces.util.ActionMgrIF;
import com.netonomy.blm.interfaces.util.ParameterIF;
import ma.iam.wiam.business.AsyncActionManagerBiz;
import ma.iam.wiam.business.ContactManagerBiz;
import ma.iam.wiam.business.ReferentialManagerBiz;
import ma.iam.wiam.business.constants.ProjectProperties;
import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.customer.*;
import ma.iam.wiam.dao.*;
import ma.iam.wiam.exceptions.FunctionnalException;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.interceptor.TraceException;
import ma.iam.wiam.interceptor.TraceInterceptor;
import ma.iam.wiam.neto.bean.ContactBean;
import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.neto.bean.NiveauBean;
import ma.iam.wiam.neto.bean.TicketObject;
import ma.iam.wiam.netoapi.dao.BaseNetoDao;
import ma.iam.wiam.netoapi.dao.LevelFDao;
import ma.iam.wiam.util.WIAMHelper;
import ma.iam.wiam.util.WSValidator;
import ma.iam.wiam.validators.OrganizationValidator;
import ma.iam.wiam.ws.neto.enums.ActionType;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.enums.NiveauType;
import ma.iam.wiam.ws.neto.enums.OrganizationCategoryEnum;
import ma.iam.wiam.ws.neto.exception.ServiceException;
import ma.iam.wiam.ws.neto.model.Ccu;
import ma.iam.wiam.ws.neto.model.Client;
import ma.iam.wiam.ws.neto.model.NetoInt;
import ma.iam.wiam.ws.neto.model.User;
import ma.iam.wiam.ws.neto.session.NetoSessionFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

@Service
public class AsyncActionManagerBizImpl implements AsyncActionManagerBiz {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(AsyncActionManagerBizImpl.class);
    private static final SimpleDateFormat fullFormatter = new SimpleDateFormat(ProjectProperties.FULL_FORMATTER);
    private static final SimpleDateFormat shortFormatter = new SimpleDateFormat(ProjectProperties.SHORT_FORMATTER);
    private static JAXBContext JAXB_CONTEXT = null;

    static {
        try {
            JAXB_CONTEXT = JAXBContext.newInstance(Client.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private UserDao userDao;

    @Autowired
    private CcuDao ccuDao;

    @Autowired
    private LevelFDao levelFDao;

    @Autowired
    @Qualifier("baseNetoDAOImpl")
    private BaseNetoDao baseNetoDao;

    @Autowired
    private ReferentialManagerBiz referentialManagerBiz;

    @Autowired
    private ContactManagerBiz contactManagerBiz;
    @Autowired
    @Qualifier("traceInterceptor")
    private TraceInterceptor traceInterceptor;

    @Autowired
    private NetoIntDao netoIntDao;
    @Autowired
    private ActionDao actionDao;
    @Autowired
    private OrigineDao origineDao;
    @Autowired
    private StatusDao statusDao;

    @Autowired
    private ClientDao clientDao;

    private String submitActionMgr(ActionMgrIF actionMgr, String webMethod, TicketObject ticketObject,String userName) {
        RequestIF requestIF = actionMgr.getRequest();
        actionMgr.submit(RequestIF.SUBMIT_MODE_NORMAL);
        String response = requestIF.getParentRequest().getIdentifier().toString();
        try {
            traceInterceptor.performTrace(response, webMethod, userName, ticketObject.getTicket(), ticketObject.getUserName());
        }catch (TraceException t){
            LOGGER.error("Can't tace operation createLevel ");
        }
        return response;
    }

    @Async
    public void addLevels(NetoInt netoInt,final NiveauBean niveauBean, final ActionType actionType, TicketObject ticketObject,String webMethod,String userName) throws FunctionnalException, TechnicalException {


        baseNetoDao.getCurrentSession();

        ActionMgrIF actionMgr = ObjectMgr.createOrder(null);

        if(actionType.equals(ActionType.CREATE_CUSTOMER)) {
            processLevel(actionMgr, null, niveauBean, null, null, actionType);
        } else if (actionType.equals(ActionType.ADD_LEVEL)) {
            LevelF parentLevel = levelFDao.get(niveauBean.getId());

            for (NiveauBean sousNiveauBean : niveauBean.getNiveaux().getNiveau()) {
                processLevel(actionMgr.createChild(), parentLevel, sousNiveauBean, niveauBean, parentLevel.getType().getCategory().getCode(), actionType);
            }
            String result=  submitActionMgr(actionMgr,webMethod,ticketObject,userName);
            updateNetoCmd(result,netoInt);
            return;
        }
        String result=  submitActionMgr(actionMgr,webMethod,ticketObject,userName);
        updateNetoCmd(result,netoInt);

    }
    private void updateNetoCmd(String result,NetoInt netoInt) throws TechnicalException{
        try{
            System.out.println("Calling updateNetoCmd for result : "+result);
            if(result!=null && result.contains(":")) {
                String requestId= result.split(":")[1];
                int retry = 10;
                List<Client> clients = null;
                do {
                    retry --;
                    Thread.sleep(10000);
                   clients = clientDao.findClientByRequest(requestId);
                    if (clients != null && clients.size() > 0) {
                        netoInt.setCustomerID(clients.get(0).getId());
                        try {
                            Marshaller jaxbMarshaller = JAXB_CONTEXT.createMarshaller();
                            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                            JAXBElement<Client> jaxbElement =
                                    new JAXBElement<Client>(new QName("", "customer"),
                                            Client.class,
                                            clients.get(0));
                            StringWriter sw = new StringWriter();
                            jaxbMarshaller.marshal(jaxbElement, sw);
                            String vXMLData = sw.toString();
                            netoInt.setOutputData(vXMLData);
                        } catch (JAXBException e) {
                            LOGGER.error("Marshelling client impossible");
                        }
                    }
                }while ((clients == null || clients.isEmpty()) && retry >0);

                netoInt.setStatus(statusDao.getById(Constants.NETOCMDREQUEST_STATUS_OK_1));
            }else{
                netoInt.setStatus(statusDao.getById(Constants.NETOCMDREQUEST_STATUS_KO_0));
                netoInt.setErrorMessage("Echec de la creation du client");
            }

            netoInt.setEntryDate(new java.util.Date());
            netoInt.setAdditionalInfo(result);

            netoIntDao.save(netoInt);
        }catch( ma.iam.mutualisation.fwk.common.exception.TechnicalException | InterruptedException e){
            throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
        }
    }
    public void processLevel(final ActionMgrIF actionMgr, final LevelIF parentLevelIF, final NiveauBean niveauBean, final NiveauBean parentNiveauBean, final String organizationTypeCategoryCode, final ActionType actionType) throws TechnicalException, FunctionnalException {

        LevelIF levelIF;

        if(parentNiveauBean == null) {
            OrganizationTypeIF organizationTypeIF = ObjectRefMgr.getOrganizationTypeByCode(niveauBean.getType().getCode());

            if(organizationTypeIF == null) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: type " + niveauBean.getType().getCode() + "  non trouve dans le referentiel dans le referentiel");
            }

            OrganizationTypeCategoryIF organizationTypeCategoryIF = organizationTypeIF.getCategory();
            ParameterIF[] organizationParameters = setAdditionalInfosForLevel(niveauBean, organizationTypeCategoryIF.getCode(), actionType);

            levelIF = OrganizationF.createCustomer(actionMgr, organizationTypeIF, new Long(0), organizationParameters);

            contactManagerBiz.processContact(actionMgr, levelIF, niveauBean.getContactLegal(), organizationTypeCategoryIF.getCode());
            contactManagerBiz.processContact(actionMgr, levelIF, niveauBean.getContactDeFacturation(), organizationTypeCategoryIF.getCode());

            if(niveauBean.getNiveaux() != null && CollectionUtils.isNotEmpty(niveauBean.getNiveaux().getNiveau())) {
                for (NiveauBean sousNiveauBean : niveauBean.getNiveaux().getNiveau()) {
                    if( sousNiveauBean!=null)
                        processLevel(actionMgr.createChild(), levelIF, sousNiveauBean, niveauBean, organizationTypeCategoryIF.getCode(), actionType);
                }
            }

            if(!levelIF.isPaymentResponsible()) {
                levelIF.setPaymentResponsible(actionMgr, true, null);
            }
        } else {
            OrganizationValidator.validateSubLevel(niveauBean, organizationTypeCategoryCode, actionType);

            if (ArrayUtils.contains(new String[] {OrganizationCategoryEnum.GRAND_PUBLIC.toString(), OrganizationCategoryEnum.PROFESSIONNEL.toString()}, organizationTypeCategoryCode)) {

                ParameterIF[] siteParameterIFs = setAdditionalInfosForLevel(parentLevelIF, niveauBean, parentNiveauBean, organizationTypeCategoryCode, actionType);
                ParameterIF[] parentLegalContactParameterIFs = null;
                ContactIF parentLegalContact = parentLevelIF.getLegalContact();

                levelIF = parentLevelIF.createLevel(actionMgr, siteParameterIFs);

                if(actionType.equals(ActionType.CREATE_CUSTOMER)) {
                    ActionItemIF[]  actionItemIFs = actionMgr.getActionMgr().findActionItemsByAction(DescriptorOidIF.ACTION_ADD_LEGAL_CONTACT, false);
                    parentLegalContactParameterIFs = actionItemIFs[0].getRequest().getOptParameters();
                } else if(actionType.equals(ActionType.ADD_LEVEL)) {
                    parentLegalContactParameterIFs = parentLevelIF.getLegalContact().getAdditionalParameters();
                }

                levelIF.createLegalContact(actionMgr, parentLegalContact, parentLegalContactParameterIFs);
                contactManagerBiz.processContact(actionMgr, levelIF, niveauBean.getContactDeFacturation(), organizationTypeCategoryCode);

                if(!levelIF.isPaymentResponsible()) {
                    levelIF.setPaymentResponsible(actionMgr, true, null);
                }
            } else if (ArrayUtils.contains(new String[] {OrganizationCategoryEnum.ENTREPRISE.toString()}, organizationTypeCategoryCode)) {

                ParameterIF[] levelParameterIFs = setAdditionalInfosForLevel(niveauBean, organizationTypeCategoryCode, actionType);

                levelIF = parentLevelIF.createLevel(actionMgr, levelParameterIFs);

                contactManagerBiz.processContact(actionMgr, levelIF, niveauBean.getContactLegal(), organizationTypeCategoryCode);
                contactManagerBiz.processContact(actionMgr, levelIF, niveauBean.getContactDeFacturation(), organizationTypeCategoryCode);

                if(!levelIF.isPaymentResponsible()) {
                    levelIF.setPaymentResponsible(actionMgr, true, null);
                }
            }
        }
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private ParameterIF[] setAdditionalInfosForLevel(final NiveauBean niveauBean, final String organizationTypeCategoryCode, final ActionType actionType) throws FunctionnalException, TechnicalException {
        try {
            OrganizationValidator.validateAdditionalInfosForLevel(niveauBean, organizationTypeCategoryCode, actionType);

            Vector levelParameters = new Vector();

            if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.GRAND_PUBLIC.toString())) {

                OrganisationWIAM.setCCU(levelParameters, "");

                if ("RETRAITE".equals(niveauBean.getType().getCode())) {
                    try {
                        Date retirementDate = WSValidator.stringToDate(niveauBean.getDateDeRetraite(), ProjectProperties.SHORT_FORMATTER);
                        ClientMT.setDateRetraite(levelParameters, fullFormatter.format(retirementDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                List<Object[]> mtAgenciesInRegie = (List<Object[]>) referentialManagerBiz.getItemFromReferential(Constants.MT_AGENCIES_IN_REGIE);
                Object[] mtAgency = getMTAgencyFromList(niveauBean.getEntiteGestionnaire().getCode(), mtAgenciesInRegie);

                if (mtAgency == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: entiteGestionnaire " + niveauBean.getEntiteGestionnaire().getCode() + "  non trouve dans le referentiel");
                }

                ClientGPMultiResidence.setAgenceMTGest(levelParameters, niveauBean.getEntiteGestionnaire().getCode());

                if (niveauBean.getSegment1() != null && StringUtils.isNotEmpty(niveauBean.getSegment1().getCode())) {

                    List<ValueChoiceItemIF> segments1 = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.SEGMENTS1);
                    ValueChoiceItemIF segment1 = getValueChoiceItemFromList(niveauBean.getSegment1().getCode(), segments1);

                    if (segment1 == null) {
                        throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: segment1 " + niveauBean.getSegment1().getCode() + "  non trouve dans le referentiel");
                    }
                    ClientGPMultiResidence.setSegment1(levelParameters, niveauBean.getSegment1().getCode());
                }

                if (niveauBean.getNotificationSms() != null) {
                    ClientMT.setNotificationBySMS(levelParameters, niveauBean.getNotificationSms() ? Constants.ON : Constants.OFF);
                } else {
                    ClientMT.setNotificationBySMS(levelParameters, null);
                }

                if (niveauBean.getNotificaionMail() != null) {
                    ClientMT.setNotificationByEmail(levelParameters, niveauBean.getNotificaionMail() ? Constants.ON : Constants.OFF);
                } else {
                    ClientMT.setNotificationByEmail(levelParameters, null);
                }

                List<ValueChoiceItemIF> prospectionInfos = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.PROSPECTION_INFOS);
                ValueChoiceItemIF prospectionInfo = getValueChoiceItemFromList(niveauBean.getInfoDeProspection().getCode(), prospectionInfos);

                if (prospectionInfo == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: infoDeProspection " + niveauBean.getInfoDeProspection().getCode() + "  non trouve dans le referentiel");
                }

                ClientMT.setInfosProspection(levelParameters, niveauBean.getInfoDeProspection().getCode());

                ClientGPMultiResidence.setCommentaire(levelParameters, niveauBean.getCommentaire());

                OrganisationWIAM.setAC_ICGC(levelParameters, "");

                List<ValueChoiceItemIF> payerQualities = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.PAYER_QUALITIES);

                if (CollectionUtils.isNotEmpty(payerQualities)) {
                    ClientGPMultiResidence.setQualitePayeur(levelParameters, payerQualities.get(0).getChoiceItem().getIdentifier().toString());
                } else {
                    throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
                }

                ClientGPMultiResidence.setSubscriber(levelParameters, "false");
            } else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.PROFESSIONNEL.toString())) {

                OrganisationWIAM.setCCU(levelParameters, "");

                List<Object[]> mtAgenciesInRegie = (List<Object[]>) referentialManagerBiz.getItemFromReferential(Constants.MT_AGENCIES_IN_REGIE);
                Object[] mtAgency = getMTAgencyFromList(niveauBean.getEntiteGestionnaire().getCode(), mtAgenciesInRegie);

                if (mtAgency == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: entiteGestionnaire " + niveauBean.getEntiteGestionnaire().getCode() + "  non trouve dans le referentiel");
                }

                ClientProfessionnel.setAgenceMTGest(levelParameters, niveauBean.getEntiteGestionnaire().getCode());

                if (niveauBean.getSegment1() != null && StringUtils.isNotEmpty(niveauBean.getSegment1().getCode())) {

                    List<ValueChoiceItemIF> segments1 = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.SEGMENTS1);
                    ValueChoiceItemIF segment1 = getValueChoiceItemFromList(niveauBean.getSegment1().getCode(), segments1);

                    if (segment1 == null) {
                        throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: segment1 " + niveauBean.getSegment1().getCode() + "  non trouve dans le referentiel");
                    }
                    ClientProfessionnel.setSegment1(levelParameters, niveauBean.getSegment1().getCode());
                }

                if (niveauBean.getNotificationSms() != null) {
                    ClientMT.setNotificationBySMS(levelParameters, niveauBean.getNotificationSms() ? Constants.ON : Constants.OFF);
                } else {
                    ClientMT.setNotificationBySMS(levelParameters, null);
                }

                if (niveauBean.getNotificaionMail() != null) {
                    ClientMT.setNotificationByEmail(levelParameters, niveauBean.getNotificaionMail() ? Constants.ON : Constants.OFF);
                } else {
                    ClientMT.setNotificationByEmail(levelParameters, null);
                }

                List<ValueChoiceItemIF> prospectionInfos = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.PROSPECTION_INFOS);
                ValueChoiceItemIF prospectionInfo = getValueChoiceItemFromList(niveauBean.getInfoDeProspection().getCode(), prospectionInfos);

                if (prospectionInfo == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: infoDeProspection " + niveauBean.getInfoDeProspection().getCode() + "  non trouve dans le referentiel");
                }

                ClientMT.setInfosProspection(levelParameters, niveauBean.getInfoDeProspection().getCode());

                ClientProfessionnel.setCommentaire(levelParameters, niveauBean.getCommentaire());

                OrganisationWIAM.setAC_ICGC(levelParameters, "");

                List<ValueChoiceItemIF> payerQualities = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.PAYER_QUALITIES);

                if (CollectionUtils.isNotEmpty(payerQualities)) {
                    ClientProfessionnel.setQualitePayeur(levelParameters, payerQualities.get(0).getChoiceItem().getIdentifier().toString());
                } else {
                    throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
                }
                ClientProfessionnel.setSubscriber(levelParameters, "false");

            } else if (organizationTypeCategoryCode.equals(OrganizationCategoryEnum.ENTREPRISE.toString())) {

                Ccu ccu = ccuDao.getById(niveauBean.getCcu());

                if (ccu == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: ccu " + niveauBean.getCcu() + "  non trouve dans le referentiel");
                }
                OrganisationWIAM.setCCU(levelParameters, buildCcu(niveauBean.getCcu()));

                List<Object[]> mtAgenciesInRegie = (List<Object[]>) referentialManagerBiz.getItemFromReferential(Constants.MT_AGENCIES_IN_REGIE);
                Object[] mtAgency = getMTAgencyFromList(niveauBean.getEntiteGestionnaire().getCode(), mtAgenciesInRegie);

                if (mtAgency == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: entiteGestionnaire " + niveauBean.getEntiteGestionnaire().getCode() + "  non trouve dans le referentiel");
                }

                ClientEntreprise.setAgenceMTGest(levelParameters, niveauBean.getEntiteGestionnaire().getCode());

                if (niveauBean.getSegment1() != null && StringUtils.isNotEmpty(niveauBean.getSegment1().getCode())) {

                    List<ValueChoiceItemIF> segments1 = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.SEGMENTS1);
                    ValueChoiceItemIF segment1 = getValueChoiceItemFromList(niveauBean.getSegment1().getCode(), segments1);

                    if (segment1 == null) {
                        throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: segment1 " + niveauBean.getSegment1().getCode() + "  non trouve dans le referentiel");
                    }
                    ClientEntreprise.setSegment1(levelParameters, niveauBean.getSegment1().getCode());
                }

                if (niveauBean.getNotificationSms() != null) {
                    ClientMT.setNotificationBySMS(levelParameters, niveauBean.getNotificationSms() ? Constants.ON : Constants.OFF);
                } else {
                    ClientMT.setNotificationBySMS(levelParameters, null);
                }

                if (niveauBean.getNotificaionMail() != null) {
                    ClientMT.setNotificationByEmail(levelParameters, niveauBean.getNotificaionMail() ? Constants.ON : Constants.OFF);
                } else {
                    ClientMT.setNotificationByEmail(levelParameters, null);
                }

                List<ValueChoiceItemIF> prospectionInfos = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.PROSPECTION_INFOS);
                ValueChoiceItemIF prospectionInfo = getValueChoiceItemFromList(niveauBean.getInfoDeProspection().getCode(), prospectionInfos);

                if (prospectionInfo == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: infoDeProspection " + niveauBean.getInfoDeProspection().getCode() + "  non trouve dans le referentiel");
                }

                if (actionType.equals(ActionType.CREATE_CUSTOMER)) {
                    ClientEntreprise.setActivite(levelParameters, niveauBean.getActivite());
                    ClientEntreprise.setPortefeuille(levelParameters, niveauBean.getPorteFeuille());
                    ClientEntreprise.setNomAttacheCommercialMT(levelParameters, niveauBean.getAttacheCommercial());
                    ClientEntreprise.setNombreSite(levelParameters, niveauBean.getNombreDeSites());
                    ClientEntreprise.setActionnariat(levelParameters, niveauBean.getActionnariat());
                    ClientEntreprise.setProcessusDecisionCentralise(levelParameters, niveauBean.getProssesusDecisionnelCentralise().toString());
                    ClientEntreprise.setProduitMarque(levelParameters, niveauBean.getProduit());
                    ClientEntreprise.setEffectif(levelParameters, niveauBean.getEffectif());
                    ClientEntreprise.setChiffreAffaire(levelParameters, niveauBean.getChiffreAffaire() != null ? niveauBean.getChiffreAffaire().toString() : null);
                    ClientEntreprise.setCroissance(levelParameters, niveauBean.getCroissance() != null ? niveauBean.getCroissance().toString() : null);
                    ClientEntreprise.setMultinationale(levelParameters, niveauBean.getMultinationale().toString());
                    ClientEntreprise.setSecteurConcurentiel(levelParameters, niveauBean.getSecteurConcurrentiel().toString());
                }

                ClientEntreprise.setCommentaire(levelParameters, niveauBean.getCommentaire());

                User user = userDao.getById(niveauBean.getAcIcgc());

                if (user == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.REF_FIELD_NOT_FOUND_WS, "Organisation: acIcgc " + niveauBean.getAcIcgc() + "  non trouve dans le referentiel");
                }

                OrganisationWIAM.setAC_ICGC(levelParameters, user.getLogin());

                ContactBean technicalContact = niveauBean.getContactTechnique();
                ContactBean commercialContact = niveauBean.getContactCommercial();

                OrganizationValidator.validateContact(technicalContact, organizationTypeCategoryCode);
                OrganizationValidator.validateContact(commercialContact, organizationTypeCategoryCode);

                ClientEntreprise.setContactITLTechnique(levelParameters, technicalContact.getNom(), technicalContact.getPrenom(), technicalContact.getTelephoneFix(), technicalContact.getTelephoneMobile(), technicalContact.getFax(), technicalContact.getEmail());
                ClientEntreprise.setContactITLCommercial(levelParameters, commercialContact.getNom(), commercialContact.getPrenom(), commercialContact.getTelephoneFix(), commercialContact.getTelephoneMobile(), commercialContact.getFax(), commercialContact.getEmail());

                List<ValueChoiceItemIF> payerQualities = (List<ValueChoiceItemIF>) referentialManagerBiz.getItemFromReferential(Constants.PAYER_QUALITIES);

                if (CollectionUtils.isNotEmpty(payerQualities)) {
                    ClientEntreprise.setQualitePayeur(levelParameters, payerQualities.get(0).getChoiceItem().getIdentifier().toString());
                } else {
                    throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
                }

                ClientEntreprise.setNewReference(levelParameters, "false");

                if (ArrayUtils.contains(new NiveauType[]{NiveauType.INTERNET, NiveauType.VOIX_DATA}, niveauBean.getNiveauType())) {
                    ClientEntreprise.setSubscriber(levelParameters, "true");
                    ClientMT.setSubscriberType(levelParameters, niveauBean.getNiveauType().getType());
                } else {
                    ClientEntreprise.setSubscriber(levelParameters, "false");
                }
            }

            String creationDate = shortFormatter.format(new Date());
            OrganisationWIAM.setOrgCreationDate(levelParameters, creationDate);

            ParameterIF[] organizationParameterIFs = WIAMHelper.vectorToParameter(levelParameters);

            return organizationParameterIFs;
        }catch(FunctionnalException | TechnicalException e){
            throw e;
        }
        catch(Throwable e){
            throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
        }
    }

    @SuppressWarnings("rawtypes")
    private ParameterIF[] setAdditionalInfosForLevel(final LevelIF parentLevelIF, final NiveauBean niveauBean, final NiveauBean parentNiveauBean, final String organizationTypeCategoryCode, final ActionType actionType) throws FunctionnalException, TechnicalException {

        OrganizationValidator.validateAdditionalInfosForSite(niveauBean, organizationTypeCategoryCode);

        Vector siteParameters = new Vector();
        String entityManager = null;

        if(actionType.equals(ActionType.CREATE_CUSTOMER)) {
            entityManager = parentNiveauBean.getEntiteGestionnaire().getCode();
        } else if(actionType.equals(ActionType.ADD_LEVEL)) {
            entityManager = ClientGPSimple.getAgenceMTGest(((LevelF) parentLevelIF).getAdditionalParameters());
        }

        ClientProfessionnel.setAgenceMTGest(siteParameters, entityManager);

        ClientMT.setSubscriber(siteParameters, "true");

        ClientMT.setSubscriberType(siteParameters, niveauBean.getNiveauType().getType());

        ParameterIF[] siteParameterIFs = WIAMHelper.vectorToParameter(siteParameters);

        return siteParameterIFs;
    }

    private String buildCcu(final String parentCcu) throws TechnicalException  {
        SessionF sessionF;
        try {
            sessionF = NetoSessionFactory.getInstance();
            String ccu = parentCcu.toUpperCase().trim() + ";" + sessionF.getUserF().getLogin() + ";" + sessionF.getUserF().getLevel().getLegalContact().getBusinessName().toUpperCase();
            return ccu;
        } catch (ServiceException e) {
            throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
        }
    }

    private ValueChoiceItemIF getValueChoiceItemFromList(final String code, final List<ValueChoiceItemIF> valueChoiceItemIFs) {
        if(code != null && CollectionUtils.isNotEmpty(valueChoiceItemIFs)) {
            for (ValueChoiceItemIF valueChoiceItemIF : valueChoiceItemIFs) {
                if(code.equals(valueChoiceItemIF.getChoiceItem().getIdentifier().toString())) {
                    return valueChoiceItemIF;
                }
            }
        }

        return null;
    }

    private Object[] getMTAgencyFromList(final String code, final List<Object[]> mtAgencies) {
        if(code != null && CollectionUtils.isNotEmpty(mtAgencies)) {
            for (Object[] mtAgency : mtAgencies) {
                if(code.equals(mtAgency[0])) {
                    return mtAgency;
                }
            }
        }

        return null;
    }
}
