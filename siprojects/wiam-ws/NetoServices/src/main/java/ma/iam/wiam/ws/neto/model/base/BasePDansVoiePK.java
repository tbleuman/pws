package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**Begin feature/wsTools*/
public abstract class BasePDansVoiePK implements Serializable{

	protected int hashCode = Integer.MIN_VALUE;
	
	public static final String PROP_CCOM = "Ccom";
	public static final String PROP_CQUARTIER = "Cquartier";
	public static final String PROP_CVOIE = "Cvoie";
	public static final String PROP_CDANSVOIE = "CdansVoie";
	
	private String ccom;
	private String cquartier;
	private String cvoie;
	private String cdansVoie;
	public BasePDansVoiePK() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BasePDansVoiePK(String ccom, String cquartier, String cvoie, String cdansVoie) {
		super();
		this.ccom = ccom;
		this.cquartier = cquartier;
		this.cvoie = cvoie;
		this.cdansVoie = cdansVoie;
	}
	public String getCcom() {
		return ccom;
	}
	public void setCcom(String ccom) {
		this.ccom = ccom;
	}
	public String getCquartier() {
		return cquartier;
	}
	public void setCquartier(String cquartier) {
		this.cquartier = cquartier;
	}
	public String getCvoie() {
		return cvoie;
	}
	public void setCvoie(String cvoie) {
		this.cvoie = cvoie;
	}
	public String getCdansVoie() {
		return cdansVoie;
	}
	public void setCdansVoie(String cdansVoie) {
		this.cdansVoie = cdansVoie;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ccom == null) ? 0 : ccom.hashCode());
		result = prime * result + ((cdansVoie == null) ? 0 : cdansVoie.hashCode());
		result = prime * result + ((cquartier == null) ? 0 : cquartier.hashCode());
		result = prime * result + ((cvoie == null) ? 0 : cvoie.hashCode());
		result = prime * result + hashCode;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasePDansVoiePK other = (BasePDansVoiePK) obj;
		if (ccom == null) {
			if (other.ccom != null)
				return false;
		} else if (!ccom.equals(other.ccom))
			return false;
		if (cdansVoie == null) {
			if (other.cdansVoie != null)
				return false;
		} else if (!cdansVoie.equals(other.cdansVoie))
			return false;
		if (cquartier == null) {
			if (other.cquartier != null)
				return false;
		} else if (!cquartier.equals(other.cquartier))
			return false;
		if (cvoie == null) {
			if (other.cvoie != null)
				return false;
		} else if (!cvoie.equals(other.cvoie))
			return false;
		if (hashCode != other.hashCode)
			return false;
		return true;
	}
	
	
	
	
	
	/**End feature/wsTools*/
}
