package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseAppConfig;
// feature/24:création du modele bean
public class AppConfigSousCategorie extends BaseAppConfig{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AppConfigSousCategorie() {
		super();
	}
	
	public AppConfigSousCategorie(String id_list, String id_value) {
		super(id_list,id_value);
	}
	
}
