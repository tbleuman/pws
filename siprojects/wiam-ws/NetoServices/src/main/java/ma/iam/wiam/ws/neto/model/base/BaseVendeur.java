package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**Begin feature/wsTools*/
public abstract class BaseVendeur implements Serializable{

	
	protected String loginId;
	protected String loginName;
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public BaseVendeur() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BaseVendeur(String loginId, String loginName) {
		super();
		this.loginId = loginId;
		this.loginName = loginName;
	}
	
	
/**End feature/wsTools*/	
}
