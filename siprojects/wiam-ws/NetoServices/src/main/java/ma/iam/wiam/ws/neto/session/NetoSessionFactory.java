package ma.iam.wiam.ws.neto.session;

import ma.iam.wiam.ws.neto.exception.ServiceException;
import ma.iam.wiam.ws.neto.utils.ParamGetter;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import com.netonomy.blm.api.User.UserF;
import com.netonomy.blm.api.utils.SessionF;

/**
 * 12/10/2015
 * 
 * Neto Services
 * 
 * Description :
 * 
 * Sadik BARKANI
 * 
 */
public class NetoSessionFactory {
    /**
     * Logger
     */
    private static Logger log = Logger.getLogger(NetoSessionFactory.class);
    /**
     * the instance of the session
     */
    private static SessionF factory;

    /***************************************************************************
     * the session factory
     * 
     * @return the session factory
     * @throws HibernateException
     */
    public synchronized static SessionF getInstance() throws ServiceException {
	if (factory == null) {
	    factory = buildInstance();
	}
	return factory;
    }

    /**
     * private constructeur
     * 
     */
    private NetoSessionFactory() {
    }

    /**
     * build the session factory
     * 
     * @return the sesssion factory
     */
    private static SessionF buildInstance() throws ServiceException {
	ParamGetter paramGetter = new ParamGetter();
	factory = new SessionF();
	boolean userAuthenticated = true;
	log.log(Priority.INFO, " Init Blmsession ");
	try {
	    factory.authenticate(paramGetter.getBatchProperties().getProperty("netoUser"), paramGetter
		    .getBatchProperties().getProperty("netoPassword"));
	    UserF user = factory.getUserF();

	    factory.add();
	    user.isEnabled();
	} catch (Exception e) {
	    e.printStackTrace();
	    userAuthenticated = false;
	}

	if (!userAuthenticated) {
	    // BLM SESSION IS NULL : System.exit
	    log.fatal(" Init Blmsession :: ERROR - failed userAuthentication");
	} else {
	    log.log(Priority.INFO, " Init Blmsession :: OK ");
	}
	return factory;
    }

}
