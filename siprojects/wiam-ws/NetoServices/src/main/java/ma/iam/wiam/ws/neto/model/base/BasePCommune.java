package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the P_COMMUNE table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="P_COMMUNE"
 */

public abstract class BasePCommune  implements Serializable {

	public static String REF = "PCommune";
	public static String PROP_CPOSTAL = "Cpostal";
	public static String PROP_CRGPT_COM = "CrgptCom";
	public static String PROP_LCOM = "Lcom";
	public static String PROP_CCOM = "Ccom";
	public static String PROP_CREGION = "Cregion";
	public static String PROP_F_CODIF = "FCodif";


	// constructors
	public BasePCommune () {
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BasePCommune (
		java.lang.String ccom) {

		this.setCcom(ccom);
		initialize();
	}

	protected void initialize () {}



	// fields
	private java.lang.String ccom;
	private java.lang.String lcom;
	private java.lang.String crgptCom;
	private java.lang.String cpostal;
	private java.lang.Integer cregion;
	private java.lang.String fCodif;






	/**
	 * Return the value associated with the column: CCOM
	 */
	public java.lang.String getCcom () {
		return ccom;
	}

	/**
	 * Set the value related to the column: CCOM
	 * @param ccom the CCOM value
	 */
	public void setCcom (java.lang.String ccom) {
		this.ccom = ccom;
	}



	/**
	 * Return the value associated with the column: LCOM
	 */
	public java.lang.String getLcom () {
		return lcom;
	}

	/**
	 * Set the value related to the column: LCOM
	 * @param lcom the LCOM value
	 */
	public void setLcom (java.lang.String lcom) {
		this.lcom = lcom;
	}



	/**
	 * Return the value associated with the column: CRGPT_COM
	 */
	public java.lang.String getCrgptCom () {
		return crgptCom;
	}

	/**
	 * Set the value related to the column: CRGPT_COM
	 * @param crgptCom the CRGPT_COM value
	 */
	public void setCrgptCom (java.lang.String crgptCom) {
		this.crgptCom = crgptCom;
	}



	/**
	 * Return the value associated with the column: CPOSTAL
	 */
	public java.lang.String getCpostal () {
		return cpostal;
	}

	/**
	 * Set the value related to the column: CPOSTAL
	 * @param cpostal the CPOSTAL value
	 */
	public void setCpostal (java.lang.String cpostal) {
		this.cpostal = cpostal;
	}



	/**
	 * Return the value associated with the column: CREGION
	 */
	public java.lang.Integer getCregion () {
		return cregion;
	}

	/**
	 * Set the value related to the column: CREGION
	 * @param cregion the CREGION value
	 */
	public void setCregion (java.lang.Integer cregion) {
		this.cregion = cregion;
	}



	/**
	 * Return the value associated with the column: F_CODIF
	 */
	public java.lang.String getFCodif () {
		return fCodif;
	}

	/**
	 * Set the value related to the column: F_CODIF
	 * @param fCodif the F_CODIF value
	 */
	public void setFCodif (java.lang.String fCodif) {
		this.fCodif = fCodif;
	}







	public String toString () {
		return super.toString();
	}


}