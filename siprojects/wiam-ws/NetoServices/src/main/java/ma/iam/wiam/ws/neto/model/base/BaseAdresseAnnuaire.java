package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
/**Begin feature/wsTools**/
public abstract class BaseAdresseAnnuaire implements Serializable{

	public static final String ID_LIST = "id_list";

	public static final String ID_VALUE = "id_value";
	
	protected String id_list;

	protected String id_value;

	public String getId_list() {
		return id_list;
	}

	public void setId_list(String id_list) {
		this.id_list = id_list;
	}

	public String getId_value() {
		return id_value;
	}

	public void setId_value(String id_value) {
		this.id_value = id_value;
	}

	public BaseAdresseAnnuaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseAdresseAnnuaire(String id_list, String id_value) {
		super();
		this.id_list = id_list;
		this.id_value = id_value;
	}
	
	
	
/**End feature/wsTools*/	
}
