package ma.iam.wiam.ws.neto.enums;

import java.util.ArrayList;
import java.util.List;

public enum OrganizationTypeEnum {
	RESIDENTIEL, PROFESSIONNEL, ENTREPRISE;

	public static List<String> categoriesElligibleForModification = new ArrayList<String>();
	
	static {
		categoriesElligibleForModification.add(RESIDENTIEL.name());
		categoriesElligibleForModification.add(PROFESSIONNEL.name());
		categoriesElligibleForModification.add(ENTREPRISE.name());
	}

}
