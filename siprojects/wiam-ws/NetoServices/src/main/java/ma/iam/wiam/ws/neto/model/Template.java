package ma.iam.wiam.ws.neto.model;

import java.util.Date;

import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.ws.neto.model.base.BaseTemplate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**<!--  @todo feature/Template -->**/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace="http://webservice.iam.ma/wiamws")
public class Template extends BaseTemplate{

	public Template(Long templateID, StatusTemplate statusTemplateID, String inputData, String categorie,
			String rateplan, Date statusUpdateDate, String description, String shdes,
			CreateContractOrder createContractOrder) {
		super(templateID, statusTemplateID, inputData, categorie, rateplan, statusUpdateDate, description, shdes,
				createContractOrder);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Template() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Template(Long templateID, StatusTemplate statusTemplateID, String inputData, String categorie,
			String rateplan, Date statusUpdateDate, String description, String shdes) {
		super(templateID, statusTemplateID, inputData, categorie, rateplan, statusUpdateDate, description, shdes);
		// TODO Auto-generated constructor stub
	}

	public Template(Long templateID) {
		super(templateID);
		// TODO Auto-generated constructor stub
	}

}
