package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BasePQuartier;

public class PQuartier extends BasePQuartier {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public PQuartier () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public PQuartier (PQuartierPK id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}