package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.PQuartier;
import ma.iam.wiam.ws.neto.model.PQuartierPK;


/**
 * This is an object that contains data related to the P_QUARTIER table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="P_QUARTIER"
 */

public abstract class BasePQuartier  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static String REF = "PQuartier";
	public static String PROP_LQUARTIER = "Lquartier";
	public static String PROP_F_CODIF_VOIE = "FCodifVoie";
	public static String PROP_ID = "Id";


	// constructors
	public BasePQuartier () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BasePQuartier (PQuartierPK id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private PQuartierPK id;

	// fields
	private java.lang.String lquartier;
	private java.lang.String fCodifVoie;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     */
	public PQuartierPK getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (PQuartierPK id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: LQUARTIER
	 */
	public java.lang.String getLquartier () {
		return lquartier;
	}

	/**
	 * Set the value related to the column: LQUARTIER
	 * @param lquartier the LQUARTIER value
	 */
	public void setLquartier (java.lang.String lquartier) {
		this.lquartier = lquartier;
	}



	/**
	 * Return the value associated with the column: F_CODIF_VOIE
	 */
	public java.lang.String getFCodifVoie () {
		return fCodifVoie;
	}

	/**
	 * Set the value related to the column: F_CODIF_VOIE
	 * @param fCodifVoie the F_CODIF_VOIE value
	 */
	public void setFCodifVoie (java.lang.String fCodifVoie) {
		this.fCodifVoie = fCodifVoie;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof PQuartier)) return false;
		else {
			PQuartier pQuartier = (PQuartier) obj;
			if (null == this.getId() || null == pQuartier.getId()) return false;
			else return (this.getId().equals(pQuartier.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}