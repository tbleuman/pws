package ma.iam.wiam.ws.neto.exception;

public class ServiceException extends Exception {
	private static final long serialVersionUID = 0;
	/**
	 * Constructeur par d&eacute;faut
	 * 
	 */
	public ServiceException() {
		super();
	}

	/**
	 * @param message
	 *            le message &agrave; remont&eacute; au utilisateur
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * @param message
	 *            le message &agrave; remont&eacute; au utilisateur
	 */
	public ServiceException(Exception exception) {
		super(exception);
	}
}
