package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

import ma.iam.wiam.ws.neto.model.PDansVoiePK;
/**Begin feature/wsTools**/
public abstract class BasePDansVoie implements Serializable{

	public static String REF = "PDansVoie";
	public static String PROP_ID = "Id";
	
	private PDansVoiePK id;
	
	
	
	
	
	
	
	public PDansVoiePK getId() {
		return id;
	}

	public void setId(PDansVoiePK id) {
		this.id = id;
	}


	public BasePDansVoie(PDansVoiePK id) {
		super();
		this.id = id;initialize();
	}

	public BasePDansVoie() {
		super();initialize();
		// TODO Auto-generated constructor stub
	}
	
	protected void initialize() {
	}
	/**End feature/wsTools**/
}
