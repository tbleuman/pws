/*
 * @author AtoS
 */
package ma.iam.wiam.ws.neto.enums;

/**
 * The Enum ExceptionCodeTypeEnum.
 */
public enum ExceptionCodeTypeEnum {

	/** THE ERROR WS. */
	SYSTEM_ERROR_WS("ERR_WIAMWS_00_001", "Erreur système au niveau du webservice"),
	/** The EMPT y_ resul t_ ws. */
    EMPTY_RESULT_WS("ERR_WIAMWS_00_002", "Aucun résultat trouvé"),
    
    MANY_RESULT_WS("ERR_WIAMWS_00_004", "Plusieurs résultats trouvés"),
    /** The ERROR in PARAM_ ws. */
    EMPTY_SEARCH_WS("ERR_WIAMWS_00_003", "Problème dans les paramétres de recherches"),
    /** The QUER y_ error. */
    WS_SYNTAX_ERROR("ERR_WIAMWS_00_006", "Erreur de format de la requête utilisée"),
    
    REQUIRED_FIELD_WS("ERR_WIAMWS_00_004", "Champ obligatoire manquant"),
    
    REF_FIELD_NOT_FOUND_WS("ERR_WIAMWS_00_005", "Valeur non trouvée"),
    
    REF_NULL_ITEM("ERR_WIAMWS_00_007", "Referentiel ne permet pas des codes et valeurs null"),
    
    INVALID_TYPE("ERR_WIAMWS_00_008", "Type invalide"),
    

    RESTRICTION_ERROR("ERR_WIAMWS_00_009", "Restriction"),
    
    OBJECT_NOT_FOUND("ERR_WIAMWS_00_010", "Object non trouve"),
	PROBLEM_TICKET("ERR_WIAMWS_00_011", "Problème de ticket"),
	PROBLEM_COMMAND_INVALID("ERR_WIAMWS_00_012", "Problème de ticket"),
    REQUEST_NOT_YET_TREATED("ERR_WIAMWS_00_013", "Request non encore traitée"),
    
    CHANGE_CATEGORIE_ECHEC("ERR_WIAMWS_00_014", "Echec de l'opération du cnangement de la catégorie")
	/** The error code. */
	;

	/** The error code. */
	private final String errorCode;

	/** The error context. */
	private final String errorContext;

	/**
	 * Instantiates a new exception code type enum.
	 * 
	 * @param errorCode
	 *            the error code
	 * @param errorContext
	 *            the error context
	 */
	ExceptionCodeTypeEnum(final String errorCode, final String errorContext) {
		this.errorCode = errorCode;
		this.errorContext = errorContext;
	}

	/**
	 * Gets the error code.
	 * 
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error name.
	 * 
	 * @return the error name
	 */
	public String getErrorContext() {
		return errorContext;
	}

}
