package ma.iam.wiam.ws.neto.model;

import ma.iam.wiam.ws.neto.model.base.BaseBankAgency;



public class BankAgency extends BaseBankAgency {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public BankAgency () {
		super();
	}

	/**
	 * Constructor for required fields
	 */
	public BankAgency (String code) {
		super (code);
	}

/*[CONSTRUCTOR MARKER END]*/
}