package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;

/**Begin feature/wsTools*/
/**@Hibernate class for TABLE ------> CUSTOM_WIAM.P_CORGAPA */
public abstract class BaseOrganisme implements Serializable {

	public static String REF = "Organisme";
	public static String PROP_CODEORGANISME = "CodeOrganisme";
	public static String PROP_LIBELLEORGANISME = "LibelleOrganisme";
	public static String PROP_CRGPTCORG = "Crgptcorg";
	
	
	
	public BaseOrganisme(Long codeOrganisme) {
		super();
		this.codeOrganisme = codeOrganisme;
		initialize();
	}
	public BaseOrganisme() {
		super();initialize ();
		// TODO Auto-generated constructor stub
	}
	protected void initialize () {}
	
	private Long codeOrganisme;
	private String libelleOrganisme;
	private String crgptcorg;



	public Long getCodeOrganisme() {
		return codeOrganisme;
	}
	public void setCodeOrganisme(Long codeOrganisme) {
		this.codeOrganisme = codeOrganisme;
	}
	public String getLibelleOrganisme() {
		return libelleOrganisme;
	}
	public void setLibelleOrganisme(String libelleOrganisme) {
		this.libelleOrganisme = libelleOrganisme;
	}
	public String getCrgptcorg() {
		return crgptcorg;
	}
	public void setCrgptcorg(String crgptcorg) {
		this.crgptcorg = crgptcorg;
	}
	
/**End feature/wsTools*/	
}
