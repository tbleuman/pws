package ma.iam.wiam.ws.neto.model.base;

import java.io.Serializable;
import java.util.Date;

import ma.iam.wiam.neto.bean.CreateContractOrder;
import ma.iam.wiam.ws.neto.model.StatusTemplate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**<!--  @todo feature/Template -->**/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace="http://webservice.iam.ma/wiamws")
public class BaseTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int hashCode = Integer.MIN_VALUE;
	
	protected java.lang.Long templateID;
	protected StatusTemplate statusTemplateID;
	protected java.lang.String inputData;
	protected java.lang.String categorie;
	protected java.lang.String rateplan;
	protected java.util.Date statusUpdateDate;
	protected java.lang.String description;
	protected java.lang.String shdes;
	/**@todo : feature /Template_CreateContractOrder**/
	@XmlElement (name = "createContractOrder")
	protected CreateContractOrder createContractOrder;
			
	public CreateContractOrder getCreateContractOrder() {
		return createContractOrder;
	}

	public BaseTemplate(Long templateID, StatusTemplate statusTemplateID, String inputData, String categorie,
			String rateplan, Date statusUpdateDate, String description, String shdes,
			CreateContractOrder createContractOrder) {
		super();
		this.templateID = templateID;
		this.statusTemplateID = statusTemplateID;
		this.inputData = inputData;
		this.categorie = categorie;
		this.rateplan = rateplan;
		this.statusUpdateDate = statusUpdateDate;
		this.description = description;
		this.shdes = shdes;
		this.createContractOrder = createContractOrder;
	}

	public void setCreateContractOrder(CreateContractOrder createContractOrder) {
		this.createContractOrder = createContractOrder;
	}
	
	public java.lang.Long getTemplateID() {
		return templateID;
	}
	public void setTemplateID(java.lang.Long templateID) {
		this.templateID = templateID;
		this.hashCode = Integer.MIN_VALUE;
	}
	public StatusTemplate getStatusTemplateID() {
		return statusTemplateID;
	}
	public void setStatusTemplateID(StatusTemplate statusTemplateID) {
		this.statusTemplateID = statusTemplateID;
	}
	public java.lang.String getInputData() {
		return inputData;
	}
	public void setInputData(java.lang.String inputData) {
		this.inputData = inputData;
	}
	public java.lang.String getCategorie() {
		return categorie;
	}
	public void setCategorie(java.lang.String categorie) {
		this.categorie = categorie;
	}
	public java.lang.String getRateplan() {
		return rateplan;
	}
	public void setRateplan(java.lang.String rateplan) {
		this.rateplan = rateplan;
	}
	public java.util.Date getStatusUpdateDate() {
		return statusUpdateDate;
	}
	public void setStatusUpdateDate(java.util.Date statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}
	public java.lang.String getDescription() {
		return description;
	}
	public void setDescription(java.lang.String description) {
		this.description = description;
	}
	public java.lang.String getShdes() {
		return shdes;
	}
	public void setShdes(java.lang.String shdes) {
		this.shdes = shdes;
	}
	
	protected void initialize () {}
	
	public BaseTemplate() {
		super();
		initialize ();
	}
	
	public BaseTemplate(Long templateID, StatusTemplate statusTemplateID, String inputData, String categorie,
			String rateplan, Date statusUpdateDate, String description, String shdes) {
		super();
		this.templateID = templateID;
		this.statusTemplateID = statusTemplateID;
		this.inputData = inputData;
		this.categorie = categorie;
		this.rateplan = rateplan;
		this.statusUpdateDate = statusUpdateDate;
		this.description = description;
		this.shdes = shdes;
		initialize ();
	}
	
	public BaseTemplate(Long templateID) {
		super();
		this.templateID = templateID;
	}
	
	
	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof ma.iam.wiam.ws.neto.model.Template)) return false;
		else {
			ma.iam.wiam.ws.neto.model.Template templ = (ma.iam.wiam.ws.neto.model.Template) obj;
			return (this.getTemplateID() == templ.getTemplateID());
		}
	}
	
	
	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getTemplateID())
				return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getTemplateID().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}
	
	
	
	
}
