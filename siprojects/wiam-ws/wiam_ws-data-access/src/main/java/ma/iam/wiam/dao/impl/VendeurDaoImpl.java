package ma.iam.wiam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.VendeurDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Vendeur;
/**Begin feature/wsTools*/
@Repository
public class VendeurDaoImpl extends HibernateGenericDaoImpl<Vendeur, String> implements VendeurDao{

	/**Begin feature_Changement_Req_ACICGC*/
	@Override
	public List<Vendeur> getListAcicgcByAgenceId(String agenceCode) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("agenceCode", agenceCode);
			list = super.getPersistenceManager().getNamedQuery("getListAcicgcByAgenceId", params);
			return (List<Vendeur>)(CollectionUtils.isNotEmpty(list) ? list : null);
		}catch(Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}
	/**End feature_Changement_Req_ACICGC*/
/**End feature/wsTools*/
}
