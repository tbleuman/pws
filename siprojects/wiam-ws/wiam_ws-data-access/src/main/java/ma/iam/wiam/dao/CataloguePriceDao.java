package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.CataloguePrice;

/**Begin feature/wsTools*/
public interface CataloguePriceDao extends HibernateGenericDao<Object, String>{

	List<CataloguePrice> getListeEquipementByOperationND(String rateplan)throws TechnicalException;
	
	List<CataloguePrice> getListEquipementNAByTypeComm(String rateplanCode,String typeCommercial)throws TechnicalException;
	
	Boolean VerifierEquipementCompatibleND(String rateplan,String ANetoId) throws TechnicalException;
	
/**End feature/wsTools*/	
}
