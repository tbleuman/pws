package ma.iam.wiam.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.dao.ClientDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Client;
import ma.iam.wiam.ws.neto.model.Parameter;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NamedQuery;

@Repository
@Transactional
public class ClientDaoImpl extends HibernateGenericDaoImpl<Object, Long> implements ClientDao {

	public Integer getCountContractDouteuxByNcli(String ncli, String[] status) throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("ncli", ncli);
			params.put("statusDouteux", status);
			list = super.getPersistenceManager().getNamedQuery(Constants.REQ_CLIENT_DOUTEUX_BY_NCLI, params);
			return (Integer) (CollectionUtils.isNotEmpty(list) ? list.get(0) : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Client> findClientByNdOrLogin(String loginNd) throws TechnicalException {
		try{
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("lineNumber", loginNd);
			list = super.getPersistenceManager().getNamedQuery(Constants.REQ_CLIENT_BY_ND_OR_LOGIN, params);
		return (List<Client>) (CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	public List<Client> findClientByClientNum(String numClient) throws TechnicalException {
		try{
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("numClient", numClient);
			list = super.getPersistenceManager().getNamedQuery(Constants.REQ_CLIENT_BY_NUM_CLIENT,params);
			return (List<Client>) (CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	public List<Client> findClientByIdent(String ident) throws TechnicalException {
		try{
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("identifiant", ident);
			params.put("paramCode", Parameter.IDENTIFIANT_CONTACT);
			list= super.getPersistenceManager().getNamedQuery(Constants.REQ_CLIENT_BY_IDENT,params);
		return (List<Client>) (CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	public List<Client> findClientBySocialReason(String reason) throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("socialReason", reason);
			list = super.getPersistenceManager().getNamedQuery(Constants.REQ_CLIENT_BY_SOCIAL_REASON,params);
		return (List<Client>) (CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	@Override
	public List<Client> findClientByRequest(String request) throws TechnicalException {
		try{
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("request_id", request);
			list= super.getPersistenceManager().getNamedQuery(Constants.REQ_CLIENT_BY_REQUEST,params);
			return (List<Client>) (CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}
}
