package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PDansVoie;
import ma.iam.wiam.ws.neto.model.PDansVoiePK;
/**Begin feature/wsTools*/
public interface DansVoieDao extends HibernateGenericDao<PDansVoie, PDansVoiePK>{

	List<PDansVoie> getNumeroDansVoieByVoie(String codeCommune,String codeQuartier,String CodeVoie)throws TechnicalException;
/**End feature/wsTools*/	
}
