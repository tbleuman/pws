package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PVoie;
import ma.iam.wiam.ws.neto.model.PVoiePK;

public interface VoieDao extends HibernateGenericDao<PVoie, PVoiePK> {

	List<PVoie> listVoieByCommuneAndQuartier(String codeCommune, String codeQuartier) throws TechnicalException;

	PVoie getVoie(String codeCommune, String codeQuartier, String voie) throws TechnicalException;
	
	
}