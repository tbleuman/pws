
package ma.iam.wiam.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.mutualisation.fwk.dao.PersistenceManager;
import ma.iam.mutualisation.fwk.dao.impl.PersistenceManagerImpl;
import ma.iam.wiam.WIAM;
import ma.iam.wiam.dao.AppConfigSousCategorieDao;
import ma.iam.wiam.ws.neto.model.AppConfigSousCategorie;
import ma.iam.wiam.exceptions.TechnicalException;

// feature/24: Repository Sous Categorie
@Repository
@Transactional
public class AppConfSousCategorieDaoImpl extends HibernateGenericDaoImpl<AppConfigSousCategorie, Long> implements AppConfigSousCategorieDao {

	public AppConfSousCategorieDaoImpl() {
		super(AppConfigSousCategorie.class);
	}
	
	
	public String[] getSousCategorie(String code)  {
			 
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("id_list", code);
			list = super.getPersistenceManager().getNamedQuery("getSousCategory", params);
			String[] sousCategories = new String[list.size()];
			for (int i = 0; i < list.size(); i++) {
				sousCategories[i] = (String) list.get(i);

			}
			return sousCategories;
		} catch (Exception e) {
			return null;
		}
			   
		
		
	}


	/**feature/Changement_Categorie_Client*/
	@Override
	public String getOrganizationTypeIdByCode(String code) {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("code", code);
			list = super.getPersistenceManager().getNamedQuery("getCategoryByLibelle", params);
			String id = (String)list.get(0);
			return id;
		}catch(Exception e) {
			return null;
		}
		
	}
	
}
