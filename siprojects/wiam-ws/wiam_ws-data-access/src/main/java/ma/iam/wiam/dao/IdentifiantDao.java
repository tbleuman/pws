package ma.iam.wiam.dao;

import ma.iam.wiam.exceptions.TechnicalException;

/**Begin feature/wsTools*/
public interface IdentifiantDao extends HibernateGenericDao<Object, String>{

	Boolean VerifierNonExistanceIdentifiant(String login,String serviceId)throws TechnicalException;
	
	Boolean VerifierNonExistanceLoginSoftSwitch(String login)throws TechnicalException;
/**End feature/wsTools*/	
}
