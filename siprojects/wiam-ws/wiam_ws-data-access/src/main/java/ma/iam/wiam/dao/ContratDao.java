package ma.iam.wiam.dao;

import java.util.Date;
import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Contrat;
/**Begin : feature/Recherche_Contrat_Client */
public interface ContratDao extends HibernateGenericDao<Contrat, Long>{
	
	List<Contrat> getContratByClientNumber(String numcli) throws TechnicalException;
	
	Contrat getContratByNdNumCli(String numCli,String nd)throws TechnicalException;
	
	String getOrganizationTypeByNumCli(String numCli)throws TechnicalException;
	/**Begin feature/wsTools*/
	Date getDateFinEngagementContrat(Long contratId)throws TechnicalException;
	/**End feature/wsTools*/
/**End : feature/Recherche_Contrat_Client */
}
