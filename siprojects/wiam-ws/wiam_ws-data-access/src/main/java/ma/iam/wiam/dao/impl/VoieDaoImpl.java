package ma.iam.wiam.dao.impl;

import java.util.List;

import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.VoieDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PVoie;
import ma.iam.wiam.ws.neto.model.PVoiePK;;

@Repository
public class VoieDaoImpl extends HibernateGenericDaoImpl<PVoie, PVoiePK> implements VoieDao {

	public VoieDaoImpl() {
		super(PVoie.class);

	}


	@SuppressWarnings("unchecked")
	public List<PVoie> listVoieByCommuneAndQuartier(String codeCommune, String codeQuartier) throws TechnicalException {
		try {
			Criteria criteria = getPersistenceManager().createCriteria(PVoie.class);
			criteria.add(Restrictions.eq(PVoie.PROP_ID + "." + PVoiePK.PROP_CCOM, codeCommune));
			criteria.add(Restrictions.eq(PVoie.PROP_ID + "." + PVoiePK.PROP_CQUARTIER, codeQuartier));
			List<PVoie> list = criteria.list();
			return list;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	public PVoie getVoie(String codeCommune, String codeQuartier, String codeVoie) throws TechnicalException {
		try {
			Criteria criteria = getPersistenceManager().createCriteria(PVoie.class);
			criteria.add(Restrictions.eq(PVoie.PROP_ID + "." + PVoiePK.PROP_CCOM, codeCommune))
				.add(Restrictions.eq(PVoie.PROP_ID + "." + PVoiePK.PROP_CQUARTIER, codeQuartier))
				.add(Restrictions.eq(PVoie.PROP_ID + "." + PVoiePK.PROP_CVOIE, codeVoie));
			List<PVoie> pVoies = criteria.list();
			return CollectionUtils.isNotEmpty(pVoies) ? pVoies.get(0) : null;
		}catch(Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	
}
