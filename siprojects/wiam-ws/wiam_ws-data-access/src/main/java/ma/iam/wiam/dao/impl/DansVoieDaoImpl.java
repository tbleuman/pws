package ma.iam.wiam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.DansVoieDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.PDansVoie;
import ma.iam.wiam.ws.neto.model.PDansVoiePK;
import ma.iam.wiam.ws.neto.model.PVoie;
/**Begin feature/wsTools*/
@Repository
public class DansVoieDaoImpl extends HibernateGenericDaoImpl<PDansVoie, PDansVoiePK> implements DansVoieDao{

	public DansVoieDaoImpl() {
		super(PDansVoie.class);
	}

	@SuppressWarnings("unchecked")
	public List<PDansVoie> getNumeroDansVoieByVoie(String codeCommune, String codeQuartier, String CodeVoie)
			throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			Criteria criteria = getPersistenceManager().createCriteria(PDansVoie.class);
			criteria.add(Restrictions.eq(PVoie.PROP_ID + "." + PDansVoiePK.PROP_CCOM, codeCommune));
			criteria.add(Restrictions.eq(PVoie.PROP_ID + "." + PDansVoiePK.PROP_CQUARTIER,codeQuartier));
			criteria.add(Restrictions.eq(PVoie.PROP_ID + "." + PDansVoiePK.PROP_CVOIE,CodeVoie));
			List<PDansVoie> list = criteria.list();
			return list;
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}
/**End feature/wsTools*/	
}
