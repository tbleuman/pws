package ma.iam.wiam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.OrganismeDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Organisme;
/**Begin feature/wsTools*/
@Repository
public class OrganismeDaoImpl extends HibernateGenericDaoImpl<Organisme, Long> implements OrganismeDao{

	public OrganismeDaoImpl() {
		super(Organisme.class);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Organisme> getListOrganisme() throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("CRGPTCORG", "");
			list = super.getPersistenceManager().getNamedQuery("getListOrganisme", params);
			return (List<Organisme>) (CollectionUtils.isNotEmpty(list) ? list : null);
		}catch(Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}

	
/**End feature/wsTools*/	
}
