package ma.iam.wiam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.dao.IdentifiantDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Contrat;

/**Begin feature/wsTools*/
@Repository
public class IdentifiantDaoImpl extends HibernateGenericDaoImpl<Object, String> implements IdentifiantDao{

	
	
	private final static int TYPE_SVC_ADSL = 1;
	private final static int TYPE_SVC_HEB = 7;
	private final static int TYPE_SVC_CDMA = 6;
	private final static int TYPE_SVC_SGBD = 14;
	private final static Integer SVCID_HEBMUTIALISE = new Integer(10907);
	public final static Integer SVCID_CDMA = new Integer(10950);
	public final static Integer SVCID_ADSL = new Integer(10901);
	public final static String SVCID_SGBD = "10928";
	private final static Integer STATUS_DESACTIVE = new Integer(2);
	@Override
	public Boolean VerifierNonExistanceIdentifiant(String login,String serviceId) throws TechnicalException {
		// TODO Auto-generated method stub
		
		try {
			List<Integer> tabS=null;
			List<Object> identifiants=null;
			if(checkBlackList(login,serviceId)) {
				return true;
			}else{
				/*List<Integer> tabS = getServicesSameType(serviceId);*/
				if(getTypeService(serviceId) == TYPE_SVC_SGBD) {
					tabS =getServicesSameType(String.valueOf(SVCID_HEBMUTIALISE));
				}else if(getTypeService(serviceId) == TYPE_SVC_HEB) {
					tabS =getServicesSameType(SVCID_SGBD);
				}else if(getTypeService(serviceId) == TYPE_SVC_ADSL) {
					tabS =getServicesSameType(String.valueOf(SVCID_CDMA));
				}else if(getTypeService(serviceId) == TYPE_SVC_CDMA) {
					tabS =getServicesSameType(String.valueOf(SVCID_ADSL));
				}else if(getTypeService(serviceId) == 4) {
					return null;
				}else {
					tabS = getServicesSameType(serviceId);
				}
				
				for(int i : tabS) {
					try {
						if(getTypeService(String.valueOf(i)) == TYPE_SVC_SGBD) {
							identifiants = getIdentifiantByLoginAndServiceForSGBD(login,String.valueOf(i));
						}
						else if(getTypeService(String.valueOf(i)) == TYPE_SVC_HEB) {
							identifiants = getIdentifiantByLoginAndServiceForSGBD(login.substring(0, login.indexOf("@")),String.valueOf(i));
						}
						else {
							identifiants = getIdentifiantByLoginAndService(login,String.valueOf(i));
						}
						
						if(identifiants == null) {
							return false;
						}else {
							
							for(Object ident : identifiants) {
									int status = (int)ident;
									if( status == STATUS_DESACTIVE ) {
										return false;
									}else {
										return true;
									}
								}
							}
								
					}catch(Throwable e) {
						throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,"Erreur au nivean de l'identification ");
					}
				}
				return false;	
			}
			
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS,"Erreur au nivean du chargement de la list des services");
		}
			
	}

	
	
	private List<Object > getIdentifiantByLoginAndService(String login, String serviceId) throws TechnicalException{

		try {
			List<Object > list = null;
			Map<String, Object> params = new HashMap<>();
			if(getTypeService(serviceId) == 4) {
				params.put("login", login+"%");
				params.put("serviceId", serviceId);
				list = super.getPersistenceManager().getNamedQuery("getIdentifiantByLoginIP", params);
			}else {
				params.put("login", login);
				params.put("serviceId", serviceId);
				list = super.getPersistenceManager().getNamedQuery("getIdentifiantByLogin", params);
			}
			
			if(list == null) {
				return null;
			}else {
				return list;
			}
			//return (List<Object[]>)(CollectionUtils.isNotEmpty(list)) ? list : null;
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

	}
	
	
	private List<Object> getIdentifiantByLoginAndServiceForSGBD(String login,String serviceId) throws TechnicalException{
		
		try {
			List<Object > list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("login", login+"%");
			params.put("serviceId", serviceId);
			list = super.getPersistenceManager().getNamedQuery("getIdentifiantByLoginForSGBD", params);
			if(list == null) {
				return null;
			}else {
				return list;
			}
		}catch(Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
		
	}
	
	
	
	
	
	
	/**
	 * retourne la list des serviceId qui on le même type de service que le serviceId passé en paramétre
	 * */
	@SuppressWarnings("unchecked")
	private List<Integer> getServicesSameType(String serviceId) throws TechnicalException {
		try {
			List<Integer> list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("serviceId", serviceId);
			list = super.getPersistenceManager().getNamedQuery("getServicesSameType", params);
			return (CollectionUtils.isNotEmpty(list)) ? list : null;
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}
	
	
	/**
	 * retourne le service_type_id du service 
	 * */
	private Integer getTypeService(String serviceId) throws TechnicalException{
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("serviceId", serviceId);
			list = super.getPersistenceManager().getNamedQuery("getTypeService", params);
			return (Integer) (CollectionUtils.isNotEmpty(list) ? list.get(0) : null);
		}catch(Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}
	/**Vérifie si l'identifiant est BlackLister
	 * */
	private Boolean checkBlackList(String login,String serviceId) throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("identifiant", login);
			params.put("serviceTypeId", String.valueOf(getTypeService(serviceId)));
			list = super.getPersistenceManager().getNamedQuery("checkIdentInBlackList", params);
			if(list.isEmpty())
				return false;
			else
				return true;
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}

		
	}


	@Override
	public Boolean VerifierNonExistanceLoginSoftSwitch(String login) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("login", login);
			list = super.getPersistenceManager().getNamedQuery("checkLoginSoftSwitch", params);
			if(list.isEmpty())
				return false;
			else
				return true;
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}
	
	
	
	
	
	
	
	
	
/**End feature/wsTools*/	
}
