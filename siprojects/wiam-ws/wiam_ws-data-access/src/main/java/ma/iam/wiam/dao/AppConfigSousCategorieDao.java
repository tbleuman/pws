package ma.iam.wiam.dao;
import java.util.List;
import ma.iam.wiam.ws.neto.model.AppConfigSousCategorie;

//feature/24:création de l'inteerface DAO
public interface AppConfigSousCategorieDao extends HibernateGenericDao<AppConfigSousCategorie, Long>{

	String[] getSousCategorie(String code);
	/**feature/Changement_Categorie_Client */
	String getOrganizationTypeIdByCode(String code);
}
