package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.PQuartier;
import ma.iam.wiam.ws.neto.model.PQuartierPK;

public interface QuartierDao extends HibernateGenericDao<PQuartier, PQuartierPK> {

	List<PQuartier> listQuartierByCodeCommune(String codeCommune) throws TechnicalException;

	PQuartier getQuartierByCodeCommuneAndCodeQuartier(String codeCommune, String codeQratier) throws TechnicalException;

}