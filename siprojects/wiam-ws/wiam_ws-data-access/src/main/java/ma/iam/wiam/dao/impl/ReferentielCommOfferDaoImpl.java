package ma.iam.wiam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.UniqueConstraint;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ma.iam.wiam.dao.ReferentielCommOfferDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.CommofferInfoValue;
/**Begin feature/wsTools*/
@Repository
@Transactional
public class ReferentielCommOfferDaoImpl extends HibernateGenericDaoImpl<CommofferInfoValue, String> implements ReferentielCommOfferDao{

	@Override
	public List<CommofferInfoValue> getListCommOfferValueByCode(String code) throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("id_list", code);
			list = super.getPersistenceManager().getNamedQuery("getCommofferValueListById", params);
			return (List<CommofferInfoValue>)(CollectionUtils.isNotEmpty(list) ? list : null);
		}catch(Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}
/**End feature/wsTools*/
}
