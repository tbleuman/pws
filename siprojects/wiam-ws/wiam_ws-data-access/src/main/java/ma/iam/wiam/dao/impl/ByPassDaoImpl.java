package ma.iam.wiam.dao.impl;

import ma.iam.wiam.dao.ByPassDao;
import ma.iam.wiam.netoapi.dao.impl.BaseNetoDAOImpl;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ByPassDaoImpl extends BaseNetoDAOImpl implements ByPassDao {

	@Autowired
	private JDBCPing pingFixeNeto;

	public boolean modeByPassBscsNeto() {
		return pingFixeNeto.check();
	}

}
