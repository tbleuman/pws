package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.CommofferInfoValue;
/**Begin feature/wsTools*/
public interface ReferentielCommOfferDao extends HibernateGenericDao<CommofferInfoValue, String> {

	List<CommofferInfoValue> getListCommOfferValueByCode(String code)  throws TechnicalException;
	
/**End feature/wsTools*/	
}
