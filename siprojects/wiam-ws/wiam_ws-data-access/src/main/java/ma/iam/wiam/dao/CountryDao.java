package ma.iam.wiam.dao;

import java.util.List;

import com.netonomy.blm.interfaces.util.CountryIF;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Country;

public interface CountryDao extends HibernateGenericDao<Country, Long> {

	Country getCountryById(Long id) throws TechnicalException;
	/**Begin feature/wsTools**/
	List<CountryIF> getAllCountrys()throws TechnicalException;
	
	/**End feature/wsTools**/
}
