package ma.iam.wiam.dao;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Ccu;

public interface CcuDao extends HibernateGenericDao<Ccu, String> {

}
