package ma.iam.wiam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.CataloguePriceDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.CataloguePrice;
/**Begin feature/wsTools*/
@Repository
public class CataloguePriceDaoImpl extends HibernateGenericDaoImpl<Object, String> implements CataloguePriceDao{

	@Override
	public List<CataloguePrice> getListeEquipementByOperationND(String rateplan) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("rateplanCode", rateplan);
			list = super.getPersistenceManager().getNamedQuery("getListEquipementGestionFidelite",params);
			return (List<CataloguePrice>)(CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}


	@Override
	public List<CataloguePrice> getListEquipementNAByTypeComm(String rateplanCode, String typeCommercial)
			throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("rateplanCode", rateplanCode);
			params.put("typeCommercial", typeCommercial);
			list = super.getPersistenceManager().getNamedQuery("getListEquipementNAByTypeComm",params);
			return (List<CataloguePrice>)(CollectionUtils.isNotEmpty(list) ? list : null);
		}catch (Throwable e){
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}


	@Override
	public Boolean VerifierEquipementCompatibleND(String rateplan, String ANetoId) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("rateplan", rateplan);
			params.put("ANetoId", ANetoId);
			list = super.getPersistenceManager().getNamedQuery("getEquipementCompatible",params);
			if(list.isEmpty()) {
				return false;
			}
			else {
				return true;
			}
			
	}catch (Throwable e){
		throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
	}
		
	}
	
	
	
	
	
	
	
	/**End feature/wsTools*/	
}
