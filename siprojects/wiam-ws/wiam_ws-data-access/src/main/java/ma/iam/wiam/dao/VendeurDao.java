package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Vendeur;
/**Begin feature/wsTools*/
public interface VendeurDao extends HibernateGenericDao<Vendeur, String>{
	/**Begin feature_Changement_Req_ACICGC*/
	List<Vendeur> getListAcicgcByAgenceId (String agenceCode)throws TechnicalException;
	/**End feature_Changement_Req_ACICGC*/
/**End feature/wsTools*/
}
