package ma.iam.wiam.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.ContactDao;
import ma.iam.wiam.ws.neto.model.Contact;

@Repository
public class ContactDaoImpl extends HibernateGenericDaoImpl<Contact, Long> implements ContactDao {

	public ContactDaoImpl() {
		super(Contact.class);
		// TODO Auto-generated constructor stub
	}

}
