package ma.iam.wiam.constants;

public class Constants {

	public static final String REQ_CLIENT_DOUTEUX_BY_NCLI = "clientDouteux.by.ncli";
	public static final String REQ_CLIENT_BY_ND_OR_LOGIN = "getClient.by.nd";
	public static final String REQ_CLIENT_BY_NUM_CLIENT = "getClient.by.numClient";
	public static final String REQ_CLIENT_BY_IDENT = "getClient.by.ident";
	public static final String REQ_CLIENT_BY_REQUEST = "getClient.by.request";
	public static final String REQ_CLIENT_BY_SOCIAL_REASON = "getClient.by.socialReason";

	public static final String SEGMENTS1 = "SEGMENT1";

	public static final String MT_AGENCIES_IN_REGIE = "MT_AGENCIES_IN_REGIE";

	public static final String PROSPECTION_INFOS = "PROSPECTION_INFO";

	public static final String BANKS = "BANKS";

	public static final String COMMUNES = "COMMUNES";

	public static final String PERSON_CODES = "PERSON_CODES";

	public static final String MARITAL_STATUS = "MARITAL_STATUS";

	public static final String GP_IDENTITY_TYPES = "GP_IDENTITY_TYPES";

	public static final String PRO_IDENTITY_TYPES = "PRO_IDENTITY_TYPES";

	public static final String ENT_IDENTITY_TYPES = "ENT_IDENTITY_TYPES";

	public static final String PROFESSIONS = "PROFESSIONS";
	
	public static final String ORG_SEGMENT2 = "ORG_SEGMENT2";

	public static final String ORG_SEGMENT = "ORG_SEGMENT";
	
	public static final String OFF = "off";
	
	public static final String ON = "on";

	public static final String ORG_LIB_PROSPECT = "ORG_LIB_PROSPECT";
	
	public static final String ORG_QUALITE_PAYEUR = "ORG_QUALITE_PAYEUR";

	public static final String PAYER_QUALITIES = "PAYER_QUALITIES";
	
	public static final String BA_CYCLE_FACTURATION = "BA_CYCLE_FACTURATION";

	public static final String BILLING_CYCLES = "BILLING_CYCLES";

	public static final String BA_TERME_PAIEMENT = "BA_TERME_PAIEMENT";

	public static final String PAYMENT_TERMS = "PAYMENT_METHODS";
	
	public static final String TITLES = "TITLES";
	
	public static final String BA_SUPPORT_FACTURE = "BA_SUPPORT_FACTURE";
	
	public static final String BILLING_SUPPORTS = "BILLING_SUPPORTS";
	
	public static final String SMART_FACT_ORGANIZATIONS = "SMART_FACT_ORGANIZATIONS";
	
	public static final String BA_LANGUE_FACTURE = "BA_LANGUE_FACTURE";
	
	public static final String BILLING_LANGUAGES = "BILLING_LANGUAGES";
	
	public static final String ALL_COUNTRIES = "ALL_COUNTRIES";
	
	public static final String GENDERS = "GENDERS";
	
	
	//feature/24: Constante de la sous_category dans commofferinfolist
	public static final String SOUS_CATEGORY_EREV = "SOUS_CATEGORY_EREV";

	public static final long NETOCMDREQUEST_ORIGIN_ID = 1;
	public static final long NETOCMDREQUEST_ACTION_CREATE_CUSTOMER = 1;
	public static final long NETOCMDREQUEST_ACTION_CREATE_CONTRACT = 2;
	public static final long NETOCMDREQUEST_STATUS_BEING_PROCESSED_99 = 99;
	public static final long NETOCMDREQUEST_STATUS_OK_1 = 1;
	public static final long NETOCMDREQUEST_STATUS_TO_BE_PROCESSED_2 = 2;
	public static final long NETOCMDREQUEST_STATUS_TEMPORARY_STOPPED_3 = 3;
	public static final long NETOCMDREQUEST_STATUS_CANCELLED_77 = 77;
	public static final long NETOCMDREQUEST_STATUS_KO_0 = 0;


	/**Begin feature/wsTools**/
	public static final String SUPPORT_FACTURATION = "SUPPORT_FACTURATION";
	public static final String COUNTRY ="COUNTRY";
	public static final String TYPE_DISTRIBUTION = "TYPE_DISTRIBUTION";
	public static final String WINBACK = "WINBACK";
	public static final String ORGANISME ="ORGANISME";
	public static final String ADRESS_ANNUAIRE_EREV = "ADRESS_ANNUAIRE_EREV";
	public static final String ACICGC = "ACICGC";
	/**End feature/wsTools**/

}
