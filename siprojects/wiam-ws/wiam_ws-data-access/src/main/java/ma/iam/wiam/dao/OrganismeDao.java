package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Organisme;
/**Begin feature/wsTools*/
public interface OrganismeDao extends HibernateGenericDao<Organisme	, Long>{
	
	List<Organisme> getListOrganisme()throws  TechnicalException;

/**End feature/wsTools*/	
}
