package ma.iam.wiam.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ma.iam.wiam.dao.TitleDao;
import ma.iam.wiam.ws.neto.model.Title;

@Repository
public class TitleDaoImpl extends HibernateGenericDaoImpl<Title, Long> implements TitleDao {

	public TitleDaoImpl() {
		super(Title.class);
	}

}
