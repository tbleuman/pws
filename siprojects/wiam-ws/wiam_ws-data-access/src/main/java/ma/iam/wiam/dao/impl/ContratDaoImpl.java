package ma.iam.wiam.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ma.iam.wiam.constants.Constants;
import ma.iam.wiam.dao.ContratDao;
import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.enums.ExceptionCodeTypeEnum;
import ma.iam.wiam.ws.neto.model.Contrat;

/**Begin : feature/Recherche_Contrat_Client */
@Repository
@Transactional
public class ContratDaoImpl extends HibernateGenericDaoImpl<Contrat, Long> implements ContratDao{

	public ContratDaoImpl() {
		super(Contrat.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Contrat> getContratByClientNumber(String numcli) throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("numclient", numcli);
			list = super.getPersistenceManager().getNamedQuery("getContractCliByNumCli", params);
			return (List<Contrat>) (CollectionUtils.isNotEmpty(list) ? list : null);
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}
	
	@Override
	public Contrat getContratByNdNumCli(String numCli, String nd) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("numclient", numCli);
			params.put("Nd_Login", nd);
			list = super.getPersistenceManager().getNamedQuery("getContratByNdNumCli", params);
			return (Contrat)list.get(0);
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}
	
	
	@Override
	public String getOrganizationTypeByNumCli(String numCli) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("numclient", numCli);
			list = super.getPersistenceManager().getNamedQuery("getOrganizationTypeByNumCli", params);
			String catg = (String)list.get(0);
			return catg;
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
	}

	
	
/**End : feature/Recherche_Contrat_Client */

	/** Begin feature/wsTools **/
	@Override
	public Date getDateFinEngagementContrat(Long contratId) throws TechnicalException {
		// TODO Auto-generated method stub
		try {
			Long contratLegacyId =getContratLegacyId(contratId);
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("CO_ID", contratLegacyId);
			list = super.getPersistenceManager().getNamedQuery("getDateFinEngagement", params);
			return (Date) (CollectionUtils.isNotEmpty(list) ? list.get(0) : null);
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}
	
	private Long getContratLegacyId (Long contratId)throws TechnicalException {
		try {
			List list = null;
			Map<String, Object> params = new HashMap<>();
			params.put("contratId", contratId);
			list = super.getPersistenceManager().getNamedQuery("getContratLegacyId", params);
			return (Long) (CollectionUtils.isNotEmpty(list) ? list.get(0) : null);
		} catch (Throwable e) {
			throw new TechnicalException(ExceptionCodeTypeEnum.SYSTEM_ERROR_WS);
		}
		
	}
	
	
	/** End feature/wsTools **/	
}
