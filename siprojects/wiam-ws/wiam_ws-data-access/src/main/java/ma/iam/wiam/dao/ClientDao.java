package ma.iam.wiam.dao;

import java.util.List;

import ma.iam.wiam.exceptions.TechnicalException;
import ma.iam.wiam.ws.neto.model.Client;

public interface ClientDao extends HibernateGenericDao<Object, Long> {

	Integer getCountContractDouteuxByNcli(String ncli, String[] status) throws TechnicalException;

	List<Client> findClientByNdOrLogin(String loginNd) throws TechnicalException;

	List<Client> findClientByClientNum(String ncli) throws TechnicalException;

	List<Client> findClientByIdent(String ident) throws TechnicalException;

	List<Client> findClientByRequest(String request) throws TechnicalException;

	List<Client> findClientBySocialReason(String reason) throws TechnicalException;

}

