package ma.iam.wiam.neto.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class DateAdapter extends XmlAdapter<String, XMLGregorianCalendar> {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public XMLGregorianCalendar unmarshal(String v) throws Exception {
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(v);
    }
    @Override
    public String marshal(XMLGregorianCalendar v) throws Exception {
        synchronized (dateFormat) {
            //return dateFormat.format(v);
            return specialFormatForXmlGregorianCalander(v);
        }
    }
    private String specialFormatForXmlGregorianCalander(XMLGregorianCalendar calander){
        GregorianCalendar gCalender = calander.toGregorianCalendar();
        java.util.Date date = gCalender.getTime();
        dateFormat.setCalendar(gCalender);
        return dateFormat.format(date);
    }
}
