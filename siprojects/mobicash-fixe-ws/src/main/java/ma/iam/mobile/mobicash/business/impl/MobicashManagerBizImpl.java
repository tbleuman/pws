package ma.iam.mobile.mobicash.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.iam.mobile.mobicash.bean.CustomerPosition;
import ma.iam.mobile.mobicash.business.MobicashManagerBiz;
import ma.iam.mobile.mobicash.dao.MobicashDao;
import ma.iam.mobile.mobicash.exceptions.FunctionnalException;
import ma.iam.mobile.mobicash.exceptions.SyntaxiqueException;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

@Component(value = "mobicashManagerBiz")
public class MobicashManagerBizImpl implements MobicashManagerBiz {
    @Autowired
    private Properties config;
    @Autowired
    MobicashDao mobicashDao;

	@Override
	public CustomerPosition getOpenInvoiceListInternet(String mobicashPrefixFixe, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.getOpenInvoiceListInternet(mobicashPrefixFixe, uniqueId, pin);
	}

	@Override
	public CustomerPosition getOpenInvoiceListFixe(String mobicashPrefixFixe, String uniqueId, String pin)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.getOpenInvoiceListFixe(mobicashPrefixFixe, uniqueId, pin);
	}

	@Override
	public String payInvoiceListInternet(String mobicashPrefixFixe, int customerId, String uniqueId, String pin,
			String authNum, ArrayList invoiceList)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.payInvoiceListInternet(mobicashPrefixFixe, customerId, uniqueId, pin, authNum, invoiceList);
	}

	@Override
	public String payInvoiceListFixe(String mobicashPrefixFixe, int customerId, String uniqueId, String pin,
			String authNum, ArrayList invoiceList)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.payInvoiceListFixe(mobicashPrefixFixe, customerId, uniqueId, pin, authNum, invoiceList);
	}

	@Override
	public String reconcileInternet(String mobicashPrefixFixe, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.reconcileInternet(mobicashPrefixFixe, date, operationCount, totalPaidAmount);
	}

	@Override
	public String reconcileFixe(String mobicashPrefixFixe, Date date, int operationCount, float totalPaidAmount)
			throws FunctionnalException, SyntaxiqueException, TechnicalException {
		// TODO Auto-generated method stub
		return mobicashDao.reconcileFixe(mobicashPrefixFixe, date, operationCount, totalPaidAmount);
	}
}
