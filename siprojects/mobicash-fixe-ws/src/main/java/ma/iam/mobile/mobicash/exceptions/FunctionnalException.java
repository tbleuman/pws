/*
 * @author AtoS
 */
package ma.iam.mobile.mobicash.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.ws.WebFault;

import ma.iam.mobile.mobicash.constants.ExceptionCodeTypeEnum;

/**
 * The Class FunctionnalException.
 */
@WebFault(name = "FunctionnalException", faultBean = "ma.iam.identification.exceptions.FaultBean")
public class FunctionnalException extends Exception {

    /** La constante serialVersionUID. */
    private static final long serialVersionUID = -1127071073519514459L;

    /** The fault bean. */
    private final FaultBean faultBean;

    /**
     * Instantiates a new  exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param cause
     *            the cause
     */
    public FunctionnalException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final Throwable cause) {
        super(cause);
        final StringBuilder result = new StringBuilder();
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        result.append(sw.toString());
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode());
        this.faultBean.setText(result.toString());
    }

    /**
     * Instantiates a new  exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public FunctionnalException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final String message, final Throwable cause) {
        super(cause);
        final StringBuilder result = new StringBuilder();
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        result.append(sw.toString());
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode(), message);
        this.faultBean.setText(result.toString());
    }

    /**
     * Instantiates a new exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     */
    public FunctionnalException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum) {
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode());
    }

    /**
     * Instantiates a new  exception.
     * 
     * @param exceptionCodeTypeEnum
     *            the exception code type enum
     * @param message
     *            the message
     */
    public FunctionnalException(final ExceptionCodeTypeEnum exceptionCodeTypeEnum, final String message) {
        this.faultBean = new FaultBean(exceptionCodeTypeEnum.getErrorContext(), exceptionCodeTypeEnum.getErrorCode(), message);
    }

    /**
     * Gets the fault info.
     * 
     * @return the fault info
     */
    public FaultBean getFaultInfo() {
        return faultBean;
    }

}
