package ma.iam.mobile.mobicash.helper;

import java.util.Properties;

import org.jasypt.commons.CommonUtils;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

// TODO: Auto-generated Javadoc
/**
 * The Class EncryptablePropertyPlaceholderConfigurer.
 */
public final class EncryptablePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    /** The properties. */
    private Properties properties;

    /*
     * Only one of these instances will be initialized, the other one will be
     * null.
     */
    /** The string encryptor. */
    private final StringEncryptor stringEncryptor;

    /** The text encryptor. */
    private final TextEncryptor textEncryptor;

    /**
     * <p>
     * Creates an <tt>EncryptablePropertyPlaceholderConfigurer</tt> instance
     * which will use the passed {@link StringEncryptor} object to decrypt
     * encrypted values.
     * </p>
     * 
     * @param stringEncryptor
     *            the {@link StringEncryptor} to be used do decrypt values. It
     *            can not be null.
     */
    public EncryptablePropertyPlaceholderConfigurer(final StringEncryptor stringEncryptor) {
        super();
        CommonUtils.validateNotNull(stringEncryptor, "Encryptor cannot be null");
        this.stringEncryptor = stringEncryptor;
        this.textEncryptor = null;
    }

    /**
     * <p>
     * Creates an <tt>EncryptablePropertyPlaceholderConfigurer</tt> instance
     * which will use the passed {@link TextEncryptor} object to decrypt
     * encrypted values.
     * </p>
     * 
     * @param textEncryptor
     *            the {@link TextEncryptor} to be used do decrypt values. It can
     *            not be null.
     */
    public EncryptablePropertyPlaceholderConfigurer(final TextEncryptor textEncryptor) {
        super();
        CommonUtils.validateNotNull(textEncryptor, "Encryptor cannot be null");
        this.stringEncryptor = null;
        this.textEncryptor = textEncryptor;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.beans.factory.config.PropertyResourceConfigurer#
     * convertPropertyValue(java.lang.String)
     */
    
    protected String convertPropertyValue(final String originalValue) {
        if (!PropertyValueEncryptionUtils.isEncryptedValue(originalValue)) {
            return originalValue;
        }
        if (this.stringEncryptor != null) {
            return PropertyValueEncryptionUtils.decrypt(originalValue, this.stringEncryptor);

        }
        return PropertyValueEncryptionUtils.decrypt(originalValue, this.textEncryptor);
    }

    /*
     * (non-Javadoc)
     * 
     * @since 1.8
     * 
     * @see
     * org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
     * #resolveSystemProperty(java.lang.String)
     */
    
    protected String resolveSystemProperty(final String key) {
        return convertPropertyValue(super.resolveSystemProperty(key));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
     * #processProperties
     * (org.springframework.beans.factory.config.ConfigurableListableBeanFactory
     * , java.util.Properties)
     */
    
    protected void processProperties(final ConfigurableListableBeanFactory beanFactoryToProcess, final Properties props) {
        super.processProperties(beanFactoryToProcess, props);
        properties = props;

    }

    /**
     * Gets the property.
     * 
     * @param name
     *            the name
     * @return the property
     */
    public String getProperty(final String name) {
        return properties.getProperty(name);
    }

}
