package ma.iam.mobile.customer.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ma.iam.mobile.customer.bean.TicketRequest;
import ma.iam.mobile.customer.bean.TicketResponse;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

public interface TicketTraceDao extends BaseDAOMobile {
	
	public List<TicketResponse> updateTicketStatus( List<TicketRequest> request) 
			throws FunctionnalException, TechnicalException;
	
}
