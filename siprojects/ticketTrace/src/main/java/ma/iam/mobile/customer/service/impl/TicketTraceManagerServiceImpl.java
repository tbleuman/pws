package ma.iam.mobile.customer.service.impl;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;
import ma.iam.mobile.customer.bean.TicketRequest;
import ma.iam.mobile.customer.bean.TicketResponse;
import ma.iam.mobile.customer.business.ByPassManagerBiz;
import ma.iam.mobile.customer.business.TicketTraceManagerBiz;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.service.TicketTraceManagerService;
import net.atos.ma.integration.jee.bypass.JDBCPing;

import java.util.HashMap;
import java.util.List;

import javax.jws.WebParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component(value = "ticketTraceManagerService")
public class TicketTraceManagerServiceImpl extends SpringBeanAutowiringSupport implements TicketTraceManagerService {

	/** The customer infos manager biz. */
	@Autowired
	private ByPassManagerBiz byPassManagerBiz;

	@Autowired
	private TicketTraceManagerBiz ticketTraceManagerBiz;

	@Autowired
	private JDBCPing pingMobileBscs;

	public Integer modeByPass() {
		return byPassManagerBiz.modeByPass();
	}
	


	@Override
	public  List<TicketResponse> updateTicketStatus( List<TicketRequest> request) 
			throws FunctionnalException, TechnicalException {

		return ticketTraceManagerBiz.updateTicketStatus(request);
	}

	
}
