/*
 * @author AtoS
 */
package ma.iam.mobile.customer.constants;

/**
 * The Enum ExceptionCodeTypeEnum.
 */
public enum ExceptionCodeTypeEnum {
	

	SYSTEM_ERROR_WS("ERR_CI_00_000", "Erreur systéme au niveau du webservice"),

	PROBLEM_UI_INVALIDE_CUST_CODE("ERR_CI_UI_001", "Le numéro client est vide ou nulle"),

	PROBLEM_UI_CUSTOMER_NOT_FOUND("ERR_CI_UI_002", "Client inexistant"),

	PROBLEM_BL_INVALIDE_CUST_CODE("ERR_CI_BL_001", "Le numéro client est vide ou nulle"),

	PROBLEM_BL_CUSTOMER_NOT_FOUND("ERR_CI_BL_002", "Client inexistant"),

	PROBLEM_CI_INVALIDE_CUST_CODE("ERR_CI_CI_001", "Le numéro client est vide ou nulle"),

	PROBLEM_CI_CUSTOMER_NOT_FOUND("ERR_CI_CI_002", "Client inexistant"),

	PROBLEM_ID_TYPE_NUMERIQUE("CSX0001", "ID_TYPE doit être numérique"),

	PROBLEM_PASSPORTNO_ALPHANUMERIQUE("CSX0002", "PASSPORTNO doit être alphanumérique"),

	PROBLEM_MSISDN_NUMEROTATION("CSX0003", "MSISDN doit respecter le plan de numérotation natianal mobile"),

	PROBLEM_CUSTCODE_STRUCTURE("CSX0004", "CUSTCODE doit respecter à la structure connue des custcodes sur BSCS"),

	PROBLEM_SIM_ALPHANUMERIQUE("CSX0005", "Carte SIM doit être numérique"),

	PROBLEM_NOM_PRENOM_ALPHA("CSX0006", "FIRST_NAME et LAST_NAME  doit être alpha et sans caractères spéciaux"),

	PROBLEM_CCU_ALPHANUMERIQUE("CSX0007", "CCU doit être alphanumérique"),

	PROBLEM_NON_FLAT_CONTRAT("CF0001", "Ce client ne peut pas avoir des contrat"),

	PROBLEM_NON_TROUVE("CF0002", "Aucun client ne correspond aux critères de recheche"),

	PROBLEM_ID_TYPE_INEXISTANT("CF0003", "Le ID_TYPE doit être exister sur la table  IDENTITY_TYPE"),

	PROBLEM_NON_TROUVE2("CF0004", "Aucun client ne correspond aux critères de recheche"),

	PROBLEM_RECHEARCH_INVALID("CF0005", "Les paramètres d'entrée sont incomplets"),
	
	QUERY_SYNTAX_ERROR_WS("ERR_CI_00_002", "QUERY_ERROR Erreur de format de la requête utilisée"),

	PROBLEM_OHSTATUS("CX0008", "OHSATUS doit être alphabétique sur deux caractères")

	;

	/** The error code. */
	private final String errorCode;

	/** The error context. */
	private final String errorContext;

	/**
	 * Instantiates a new exception code type enum.
	 * 
	 * @param errorCode
	 *            the error code
	 * @param errorContext
	 *            the error context
	 */
	ExceptionCodeTypeEnum(final String errorCode, final String errorContext) {
		this.errorCode = errorCode;
		this.errorContext = errorContext;
	}

	/**
	 * Gets the error code.
	 * 
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error name.
	 * 
	 * @return the error name
	 */
	public String getErrorContext() {
		return errorContext;
	}

}
