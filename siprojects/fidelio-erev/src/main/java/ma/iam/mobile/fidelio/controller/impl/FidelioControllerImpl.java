package ma.iam.mobile.fidelio.controller.impl;

import org.springframework.stereotype.Component;

import ma.iam.mobile.fidelio.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.fidelio.controller.FidelioController;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.util.StringUtil;
import ma.iam.mobile.fidelio.util.WSValidator;

@Component(value = "fidelioController")
public class FidelioControllerImpl implements FidelioController {


	public void verifyND(String nd) throws FunctionnalException {

		if (StringUtil.isEmpty(nd)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_INVALIDE_NUM_APPEL,
					" Le numéro d'appel est vide ou nulle");
		}

		if (!WSValidator.validateMobileNumber(nd)) {
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_INVALIDE_NUM_APPEL,
					" Le numéro d'appel est invalid");
		}

	}

}
