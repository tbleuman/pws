/*
 * @author AtoS
 */
package ma.iam.mobile.fidelio.dao.impl;

import javax.sql.DataSource;

import ma.iam.mobile.fidelio.dao.BaseDAOFidelio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * The Class BaseDAOMobile.
 */
@Repository
public class BaseDAOFidelioImpl implements BaseDAOFidelio {

	/** The named parameter jdbc template. */
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private DataSource dataSource;

	/**
	 * Sets the mobile bscs data source.
	 * 
	 * @param mobileBSCSDataSource
	 *            the new mobile bscs data source
	 */
	@Autowired
	@Qualifier("fidelioDataSource")
	public void setMobileBscsDataSource(final DataSource fidelioDataSource) {

		this.setNamedParameterJdbcTemplate(new NamedParameterJdbcTemplate(
				fidelioDataSource));

		this.dataSource = fidelioDataSource;
	}

	/**
	 * Sets the named parameter jdbc template.
	 * 
	 * @param namedParameterJdbcTemplate
	 *            the new named parameter jdbc template
	 */
	public void setNamedParameterJdbcTemplate(
			final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * Gets the named parameter jdbc template.
	 * 
	 * @return the named parameter jdbc template-
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
