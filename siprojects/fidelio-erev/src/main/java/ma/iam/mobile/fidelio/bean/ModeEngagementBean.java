package ma.iam.mobile.fidelio.bean;


public class ModeEngagementBean  {

	
	/** The id. */
	private String id;

	/** The code. */
	private String code;

	/** The libelle. */
	private String libelle;

	/** The duree en mois. */
	private String dureeEnMois;

	/** The en service. */
	private String enService;
	// Mode Engagement BSCS
	/** The engagement bscs id. */
	private String engagementBscsId;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the libelle.
	 * 
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * Sets the libelle.
	 * 
	 * @param libelle
	 *            the new libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * Gets the duree en mois.
	 * 
	 * @return the duree en mois
	 */
	public String getDureeEnMois() {
		return dureeEnMois;
	}

	/**
	 * Sets the duree en mois.
	 * 
	 * @param dureeEnMois
	 *            the new duree en mois
	 */
	public void setDureeEnMois(String dureeEnMois) {
		this.dureeEnMois = dureeEnMois;
	}

	/**
	 * Gets the engagement bscs id.
	 * 
	 * @return the engagement bscs id
	 */
	public String getEngagementBscsId() {
		return engagementBscsId;
	}

	/**
	 * Sets the engagement bscs id.
	 * 
	 * @param engagementBscsId
	 *            the new engagement bscs id
	 */
	public void setEngagementBscsId(String engagementBscsId) {
		this.engagementBscsId = engagementBscsId;
	}

	/**
	 * Gets the en service.
	 * 
	 * @return the en service
	 */
	public String getEnService() {
		return enService;
	}

	/**
	 * Sets the en service.
	 * 
	 * @param enService
	 *            the new en service
	 */
	public void setEnService(String enService) {
		this.enService = enService;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
