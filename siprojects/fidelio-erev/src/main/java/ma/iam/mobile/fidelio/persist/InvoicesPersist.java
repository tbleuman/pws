package ma.iam.mobile.fidelio.persist;

import java.sql.Date;

public class InvoicesPersist {
	private String invoiceRef;
	private String dateFact;
	private Long customerId;
	private Double amount;
	private String type;
	private String ohxact;
	private Date OHENTDATE;

	public String getInvoiceRef() {
		return invoiceRef;
	}

	public void setInvoiceRef(String invoiceRef) {
		this.invoiceRef = invoiceRef;
	}

	public String getDateFact() {
		return dateFact;
	}

	public void setDateFact(String dateFact) {
		this.dateFact = dateFact;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOhxact() {
		return ohxact;
	}

	public void setOhxact(String ohxact) {
		this.ohxact = ohxact;
	}

	public Date getOHENTDATE() {
		return OHENTDATE;
	}

	public void setOHENTDATE(Date oHENTDATE) {
		OHENTDATE = oHENTDATE;
	}

}
