package ma.iam.mobile.fidelio.dao;


import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ma.iam.mobile.fidelio.bean.EngagementContratBean;
import ma.iam.mobile.fidelio.bean.TarifBSCSBean;
import ma.iam.mobile.fidelio.exceptions.FunctionnalException;
import ma.iam.mobile.fidelio.persist.ContractCustomer;
import ma.iam.mobile.fidelio.persist.ContractPersist;
import ma.iam.mobile.fidelio.persist.InvoicesPersist;

public interface MobileBSCSDao extends BaseDAOMobile {

	
	ContractPersist getContractByDnNum(String nd);

	List<ContractPersist> getCustomersWithSameIdent(ContractPersist contract);

	List<InvoicesPersist> getInvoices(List<Long> customerIds);

	ContractPersist getIdentifiantByCustomerId(String customerId);

	String getCodeFid(String customerId);

	List<ContractCustomer> getCustomersWithContract(String customerId);

	String getPartner(String partnerId);

	Map<String, String> createPayment(String transactionId, Date transactionDate, String transactionType,
			String shdesPartner, String transactionRemarq, String ohxAct, String type);

	List<InvoicesPersist> getInvoicesByRefInvoice(List<String> invoices);

	TarifBSCSBean getPrixPackXMoisSema(String tmcode, String articleCode, String dureeEnMois);

	Float getRemisePackPrivilege(String codeClient, String numAppel, Float prixPack, Float seuilPrix,
			Float remisePrivilege);

	List<String> getAllNumAppels(String customerId);

	String getCustCodeById(String customerId);

	int affecterLivraison(String codeClient, Long coId, String numAppel, String codePoste, Integer engagementId,
			Date dateAffectation, String user, String imei, Long agenceId, Integer commandId, boolean affectComplement,
			Double montantComplement, String occComplementCode, boolean affectFraisReengagemnt,
			Double montantFraisReengagemnt, String occFraisReengagemntCode, boolean affectFraisLivraison) throws FunctionnalException;

	int removeIMEI(Long pCoId, String pIMEI) throws FunctionnalException;

	String addIMEI(Long coId, String pND, String pArticleDes, String codePoste, String engagement, String user,
			String pOldIMEI, String pAgence, String pNomClient, String pNewIMEI) throws FunctionnalException;

	 EngagementContratBean getEngagementEnCours(Long coId);
}
