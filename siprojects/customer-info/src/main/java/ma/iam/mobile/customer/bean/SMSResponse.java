package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class SMSResponse implements Serializable {

	private long request;
	private long smscode;

	public long getRequest() {
		return request;
	}

	public void setRequest(long request) {
		this.request = request;
	}

	public long getSmscode() {
		return smscode;
	}

	public void setSmscode(long smscode) {
		this.smscode = smscode;
	}
}
