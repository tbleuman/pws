package ma.iam.mobile.customer.bean;

import javax.jws.WebParam;
import java.io.Serializable;

public class AllContractsRequest implements Serializable {
	
	private String iso;
	private String name;
	private String custcode;
	private String customerId;
	private String nd;
	private String coId;
	private boolean hierarchique;
	private int produit;
	/**
	 * @return the iso
	 */
	public String getIso() {
		return iso;
	}
	/**
	 * @param iso the iso to set
	 */
	public void setIso(String iso) {
		this.iso = iso;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the custcode
	 */
	public String getCustcode() {
		return custcode;
	}
	/**
	 * @param custcode the custcode to set
	 */
	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the nd
	 */
	public String getNd() {
		return nd;
	}
	/**
	 * @param nd the nd to set
	 */
	public void setNd(String nd) {
		this.nd = nd;
	}
	/**
	 * @return the coId
	 */
	public String getCoId() {
		return coId;
	}
	/**
	 * @param coId the coId to set
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}
	/**
	 * @return the hierarchique
	 */
	public boolean isHierarchique() {
		return hierarchique;
	}
	/**
	 * @param hierarchique the hierarchique to set
	 */
	public void setHierarchique(boolean hierarchique) {
		this.hierarchique = hierarchique;
	}
	/**
	 * @return the produit
	 */
	public int getProduit() {
		return produit;
	}
	/**
	 * @param produit the produit to set
	 */
	public void setProduit(int produit) {
		this.produit = produit;
	}
	
	
}
