package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class ParameterValues implements Serializable {

	private String seqNo;
	private String label;
	private String code;
	private String valueString;

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {	this.seqNo = seqNo;	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ParameterValues() {
		super();
	}

	public String getValueString() {		return valueString;	}

	public void setValueString(String valueString) {		this.valueString = valueString;	}

	public ParameterValues(String seqNo, String label, String code) {
		super();
		this.seqNo = seqNo;
		this.label = label;
		this.code = code;
	}

}
