package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebParam;

public class ValideSIMRequest implements Serializable {

	private String sim; 
	//private List<String> bo;
	
	
	/**
	 * @return the sim
	 */
	public String getSim() {
		return sim;
	}
	/**
	 * @param sim the sim to set
	 */
	public void setSim(String sim) {
		this.sim = sim;
	}
	/**
	 * @return the bo
	 */
//	public List<String> getBo() {
//		return bo;
//	}
//	/**
//	 * @param bo the bo to set
//	 */
//	public void setBo(List<String> bo) {
//		this.bo = bo;
//	}
}
