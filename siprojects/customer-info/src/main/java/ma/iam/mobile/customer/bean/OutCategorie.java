package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class OutCategorie implements Serializable {

	private String categorieID;
	private String categorieDes;
	private List<OutPlanTarifaire> listePlanTarifaire;
	/**
	 * @return the categorieID
	 */
	public String getCategorieID() {
		return categorieID;
	}
	/**
	 * @param categorieID the categorieID to set
	 */
	public void setCategorieID(String categorieID) {
		this.categorieID = categorieID;
	}
	/**
	 * @return the categorieDes
	 */
	public String getCategorieDes() {
		return categorieDes;
	}
	/**
	 * @param categorieDes the categorieDes to set
	 */
	public void setCategorieDes(String categorieDes) {
		this.categorieDes = categorieDes;
	}
	/**
	 * @return the listePlanTarifaire
	 */
	public List<OutPlanTarifaire> getListePlanTarifaire() {
		return listePlanTarifaire;
	}
	/**
	 * @param listePlanTarifaire the listePlanTarifaire to set
	 */
	public void setListePlanTarifaire(List<OutPlanTarifaire> listePlanTarifaire) {
		this.listePlanTarifaire = listePlanTarifaire;
	}
	

}
