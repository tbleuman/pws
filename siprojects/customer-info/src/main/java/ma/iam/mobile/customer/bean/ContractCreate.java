package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class ContractCreate implements Serializable {

	 private String storage_medium_number;
	 private String rate_plan_code;
	 private String network_code;
	 private String market_code;
	 private String submarket_code;
	 private String dealer_id;
	 private String remiseEnService;
	 private List<Service> services;
	/**
	 * @return the remiseEnService
	 */
	public String getRemiseEnService() {
		return remiseEnService;
	}
	/**
	 * @param remiseEnService the remiseEnService to set
	 */
	public void setRemiseEnService(String remiseEnService) {
		this.remiseEnService = remiseEnService;
	}
	/**
	 * @return the storage_medium_number
	 */
	public String getStorage_medium_number() {
		return storage_medium_number;
	}
	/**
	 * @param storage_medium_number the storage_medium_number to set
	 */
	public void setStorage_medium_number(String storage_medium_number) {
		this.storage_medium_number = storage_medium_number;
	}
	/**
	 * @return the rate_plan_code
	 */
	public String getRate_plan_code() {
		return rate_plan_code;
	}
	/**
	 * @param rate_plan_code the rate_plan_code to set
	 */
	public void setRate_plan_code(String rate_plan_code) {
		this.rate_plan_code = rate_plan_code;
	}
	/**
	 * @return the network_code
	 */
	public String getNetwork_code() {
		return network_code;
	}
	/**
	 * @param network_code the network_code to set
	 */
	public void setNetwork_code(String network_code) {
		this.network_code = network_code;
	}
	/**
	 * @return the market_code
	 */
	public String getMarket_code() {
		return market_code;
	}
	/**
	 * @param market_code the market_code to set
	 */
	public void setMarket_code(String market_code) {
		this.market_code = market_code;
	}
	/**
	 * @return the submarket_code
	 */
	public String getSubmarket_code() {
		return submarket_code;
	}
	/**
	 * @param submarket_code the submarket_code to set
	 */
	public void setSubmarket_code(String submarket_code) {
		this.submarket_code = submarket_code;
	}
	/**
	 * @return the dealer_id
	 */
	public String getDealer_id() {
		return dealer_id;
	}
	/**
	 * @param dealer_id the dealer_id to set
	 */
	public void setDealer_id(String dealer_id) {
		this.dealer_id = dealer_id;
	}
	/**
	 * @return the services
	 */
	public List<Service> getServices() {
		return services;
	}
	/**
	 * @param services the services to set
	 */
	public void setServices(List<Service> services) {
		this.services = services;
	}
	 
	 
}
