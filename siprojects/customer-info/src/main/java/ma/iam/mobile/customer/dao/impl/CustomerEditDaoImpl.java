/*
 * @author AtoS
 */

package ma.iam.mobile.customer.dao.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.bean.cmsint.OccType;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.dao.CustomerEditDao;
import ma.iam.mobile.customer.dao.CustomerInfoDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.WsException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mobile.customer.util.SQLQueryBuilder;
import ma.iam.mobile.customer.util.StringUtil;


import ma.iam.mobile.customer.util.XMLUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@Repository
public class CustomerEditDaoImpl extends BaseDAOMobileImpl implements CustomerEditDao {
	private static final Logger LOG = LoggerFactory.getLogger(CustomerEditDaoImpl.class);
	private static final String OCC_HEADER="<occ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../occ.xsd\">";
	private static  JAXBContext JAXB_CONTEXT = null;

	static {
		try {
			JAXB_CONTEXT = JAXBContext.newInstance(OccType.class);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public static final String ORIGIN_PREFIX = "EREV_";

	private static final String GET_CLIENT_BY_CUSTCODE = CustomerEditDaoImpl.class.getName()
			+ ".GET_CLIENT_BY_CUSTCODE";

	private static final String GET_CMS_INT_SEQ = CustomerEditDaoImpl.class.getName() + ".GET_CMS_INT_SEQ";

	private static final String INSERT_CMD_CMS = CustomerEditDaoImpl.class.getName() + ".INSERT_CMD_CMS";

	private static final String GET_CMS_INTB_SEQ = CustomerEditDaoImpl.class.getName() + ".GET_CMS_INTB_SEQ";

	private static final String INSERT_CMD_CMSB = CustomerEditDaoImpl.class.getName() + ".INSERT_CMD_CMSB";

	private static final String GET_CO_ID_BY_DN_NUM = CustomerEditDaoImpl.class.getName() + ".GET_CO_ID_BY_DN_NUM";

	private static final String GET_CU_ID_BY_CO_ID = CustomerEditDaoImpl.class.getName() + ".GET_CU_ID_BY_CO_ID";

	private static final String INSERT_PRE_ORDER_POP = CustomerEditDaoImpl.class.getName() + ".INSERT_PRE_ORDER_POP";
	
	
	private static final String GET_PRECOMMANDE_POP_SEQ = CustomerEditDaoImpl.class.getName() + ".GET_PRECOMMANDE_POP_SEQ";

	private static final String UPDATE_DIRECTORY_NUMBER_TO_SYSDATE = CustomerEditDaoImpl.class.getName() + ".UPDATE_DIRECTORY_NUMBER_TO_SYSDATE";

	private static final String UPDATE_HLCODE = CustomerEditDaoImpl.class.getName() + ".UPDATE_HLCODE";

	private static final String GET_HLCODE_DN_BY_SIM = CustomerEditDaoImpl.class.getName() + ".GET_HLCODE_DN_BY_SIM";
	private static final String GET_PRGCODE_BY_ND = CustomerEditDaoImpl.class.getName() + ".GET_PRGCODE_BY_ND";

	public Integer getCoIDFromDnNum(String nd) {

		LOG.debug("--> getCoIDFromDnNum");
		Integer result = null;
		try {

			final String sql = CustomSQLUtil.get(GET_CO_ID_BY_DN_NUM);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("nd", nd);

			result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

		return result;
	}
	
	public Integer getCuIDFromCoID(String coID) {

		LOG.debug("--> getCuIDFromCoID");
		Integer result = null;
		try {

			final String sql = CustomSQLUtil.get(GET_CU_ID_BY_CO_ID);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("coID", coID);

			result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

		return result;
	}

	public Long insertCMSCreateCustomer(Customer customer, String userName) throws FunctionnalException {
		String vXMLData;

		LOG.debug("--> Start insertCMS");
		String xsd="xsd/create.customer.xsd";
		String originSuffix = null;
		String actionId="1";
		if (!StringUtil.isEmpty(customer.getCustomerID())){
			xsd="xsd/create.contract.xsd";
			originSuffix="CREATE_CONTR";
			actionId="2";
			vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
						"<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../create.contract.xsd\">"+
						"<id type=\"cs_id\">"+customer.getCustomerID()+"</id>";
		}
		else{
			if (customer.getContracts()!= null && customer.getContracts().size()>0){
				throw new FunctionnalException(ExceptionCodeTypeEnum.CREATE_CUST_CONTR);
			}
			xsd="xsd/create.customer.xsd";
			originSuffix="CREATE_CUST";
			actionId="1";
			vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"+
					"<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../create.customer.xsd\">\n"+
				    "<is_business_partner>"+customer.getIs_business_partner()+"</is_business_partner>\n"+
				    "<occ_rate_plan_code>"+customer.getOcc_rate_plan_code()+"</occ_rate_plan_code>\n"+
				    "<price_group_code>"+customer.getCategorie()+"</price_group_code>\n"+
					"<family_id>"+customer.getFamily_id()+"</family_id>\n"+
					"<cost_center>"+customer.getCost_center()+"</cost_center>\n"+
				    "<billing_cycle>"+customer.getBilling_cycle()+"</billing_cycle>\n"+
				    "<dealer_id>"+customer.getDealer_id()+"</dealer_id>\n"+
				    "<area_id>"+customer.getSegment()+"</area_id>";
			List<Address> addresses = customer.getAddresses();
			if(addresses!=null && addresses.size()>0){
				for (int i = 0; i<addresses.size();i++){
					vXMLData = vXMLData + 
							 "<address>\n";
					List<Role> roles = addresses.get(i).getRoles();
					if(roles!=null && roles.size()>0){
						vXMLData = vXMLData + 
							        "<roles>\n";
						for (int j = 0; j<roles.size();j++){
							vXMLData = vXMLData + 
								            "<role>"+roles.get(j).getRole()+"</role>\n";
						}
						vXMLData = vXMLData + "</roles>\n";
						
					}
					vXMLData = vXMLData +
							        "<custtype>"+addresses.get(i).getCusttype()+"</custtype>\n"+
							        "<first_name>"+addresses.get(i).getPrenom()+"</first_name>\n"+
							        "<last_name>"+addresses.get(i).getNom()+"</last_name>\n"+
							        "<title>"+addresses.get(i).getTitre()+"</title>\n"+
							        "<email>"+addresses.get(i).getEmail()+"</email>\n"+
							        "<cctn>"+addresses.get(i).getTelephone1Part1()+"</cctn>\n"+
							        "<cctn_area>"+addresses.get(i).getTelephone1Part2()+"</cctn_area>\n"+
							        "<cctn2>"+addresses.get(i).getTelephone2Part1()+"</cctn2>\n"+
							        "<cctn2_area>"+addresses.get(i).getTelephone2Part2()+"</cctn2_area>\n"+
							        "<fax>"+addresses.get(i).getFax()+"</fax>\n"+
							        "<street>"+addresses.get(i).getRue()+"</street>\n"+
							        "<house_no>"+addresses.get(i).getStreetNumber()+"</house_no>\n"+
							        "<zip_code>"+addresses.get(i).getCodePostal()+"</zip_code>\n"+
							        "<city>"+(addresses.get(i).getCity()!=null?addresses.get(i).getCity().getNom():"")+"</city>\n"+
							        "<state>"+addresses.get(i).getQuartier()+"</state>\n"+
							        "<country_id>"+(addresses.get(i).getCountry()!=null?addresses.get(i).getCountry().getName():"")+"</country_id>\n"+
							        "<language>"+addresses.get(i).getLangue()+"</language>\n"+
							        "<birth_date>"+addresses.get(i).getDateNaissance()+"</birth_date>\n"+
							        "<address_note_1>"+addresses.get(i).getComplementAdresse()+"</address_note_1>\n"+
							        "<address_note_2>"+addresses.get(i).getContactLegal()+"</address_note_2>\n"+
							        "<address_note_3>"+addresses.get(i).getCIN()+"</address_note_3>\n"+
							        "<nationality>"+addresses.get(i).getNationalite()+"</nationality>\n"+
							        "<id_type>"+addresses.get(i).getTypeIdentifiant()+"</id_type>\n"+
							        "<passportno>"+addresses.get(i).getIdentifiant()+"</passportno>\n"+
							        "<drive_licence_no>"+addresses.get(i).getCcu()+"</drive_licence_no>\n"+
							    "</address>";
				}
			}

				    
				   
			vXMLData = vXMLData + "<payment_arrangement>\n";
			if(customer.getPaymentArrangement()!=null){
				vXMLData = vXMLData + "<payment_method_id>"+customer.getPaymentArrangement().getPayment_method_id()+"</payment_method_id>\n"+
					        "<account_no>"+customer.getPaymentArrangement().getAccount_no()+"</account_no>\n"+
					        "<account_owner>"+customer.getPaymentArrangement().getAccount_owner()+"</account_owner>\n"+
					        "<bank_name>"+customer.getPaymentArrangement().getBank_name()+"</bank_name>\n"+
					        "<bank_zip>"+customer.getPaymentArrangement().getBank_zip()+"</bank_zip>\n"+
					        "<bank_city>"+customer.getPaymentArrangement().getBank_city()+"</bank_city>\n"+
					        "<swift>"+customer.getPaymentArrangement().getSwift()+"</swift>\n"+
							(GRCStringUtil.isNullOrEmpty(customer.getPaymentArrangement().getOrderNumber())?"":"<orderNumber>"+customer.getPaymentArrangement().getOrderNumber()+"</orderNumber>\n");
			}
				       
			vXMLData = vXMLData + "</payment_arrangement>";

		}
		
		
		List<ContractCreate> contracts = customer.getContracts();
		if(contracts!=null && contracts.size()>0){
			List nds = new ArrayList(contracts.size());
			for (int i = 0; i<contracts.size();i++){
				vXMLData = vXMLData + 
						"<contract>\n"+
				        "<device>\n"+
				            "<storage_medium_number>"+contracts.get(i).getStorage_medium_number()+"</storage_medium_number>\n"+
				        "</device>\n"+
				        "<rate_plan_code>"+contracts.get(i).getRate_plan_code()+"</rate_plan_code>\n"+
				        "<network_code>"+contracts.get(i).getNetwork_code()+"</network_code>\n"+
				        "<market_code>"+contracts.get(i).getMarket_code()+"</market_code>\n"+
				        "<submarket_code>"+contracts.get(i).getSubmarket_code()+"</submarket_code>\n"+
				        "<dealer_id>"+contracts.get(i).getDealer_id()+"</dealer_id>";
				List<Service> services = contracts.get(i).getServices();
				for (int j = 0; j<services.size();j++){
					vXMLData = vXMLData + 
							"<service>\n"+
				            "<sncode>"+services.get(j).getSncode()+"</sncode>\n"+
				            "<service_package_code>"+services.get(j).getService_package_code()+"</service_package_code>";
					
					if(services.get(j).getDirectory_number()!=null){
						nds.add(services.get(j).getDirectory_number());
						final String updateDIRSQL=CustomSQLUtil.get(UPDATE_DIRECTORY_NUMBER_TO_SYSDATE);
						Map<String, String> updateDIRSQLparameters = new HashMap();
						updateDIRSQLparameters.put("nd",services.get(j).getDirectory_number());
						super.getNamedParameterJdbcTemplate().update(updateDIRSQL,updateDIRSQLparameters);
						vXMLData = vXMLData +
								"<directory_number create_if_unexisting=\"false\" main=\"true\" npcode=\"1\">"+services.get(j).getDirectory_number()+"</directory_number>";
					}
					else
						if(services.get(j).getParameters()!=null){
							List<Parameter> parameters = services.get(j).getParameters();
							for (int k = 0; k<parameters.size();k++){
								vXMLData = vXMLData + 
										"<parameter>\n"+
						                "<id>"+parameters.get(k).getId()+"</id>\n"+
						                "<no>"+parameters.get(k).getNo()+"</no>\n"+
						                "<value>"+parameters.get(k).getValue()+"</value>\n"+
						                "<des>"+parameters.get(k).getDes()+"</des>\n"+
						            "</parameter>";
							}
						}
					vXMLData = vXMLData + 
				        "</service>";
				}
			}
			vXMLData = vXMLData + 
				      "</contract>\n";
		}
		vXMLData = vXMLData + 
			"</customer>";

		try {

			final String sql = CustomSQLUtil.get(GET_CMS_INT_SEQ);
			Map<String, String> namedParameters = new HashMap();
			vXMLData=vXMLData.replaceAll("null","");
			try {
				XMLUtil.validate(vXMLData,xsd);
			} catch (Exception e) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID,e.getMessage());
			}
			long seqCMS = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);

			if (seqCMS > 0) {

				namedParameters.put("seqCMS", String.valueOf(seqCMS));
				namedParameters.put("vXMLData", vXMLData);
				namedParameters.put("actionId", actionId);
				namedParameters.put("userName", userName);
				namedParameters.put("coID", null);
				namedParameters.put("parentID", null);
				namedParameters.put("addInfo", null);
				namedParameters.put("origin", ORIGIN_PREFIX+originSuffix);
				namedParameters.put("customerId", customer.getCustomerID());

				final String sql2 = CustomSQLUtil.get(INSERT_CMD_CMS);

				super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
				
				

				if(contracts!=null && contracts.size()>0){
					List nds = new ArrayList(contracts.size());
					for (int i = 0; i<contracts.size();i++){
						if (!StringUtil.isEmpty(contracts.get(i).getRemiseEnService())){
							long seqCMSTemp = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);
							if (seqCMSTemp > 0) {
								xsd="xsd/create.tickler.xsd";
								vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
										"<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../create.tickler.xsd\">\n"+
										"<id type=\"cs_id\">"+customer.getCustomerID()+"</id>";
								
								vXMLData = vXMLData + "<contract type=\"msisdn\">"+nds.get(i)+"</contract>\n";
								vXMLData = vXMLData + "<tickler>\n";
								vXMLData = vXMLData + "<tickler_code>SYSTEM</tickler_code>\n";
								vXMLData = vXMLData + "<tickler_status>NOTE</tickler_status>\n";
								vXMLData = vXMLData + "<tickler_desc>Remise en service: "+ contracts.get(i).getRemiseEnService() +"</tickler_desc>\n";
								vXMLData = vXMLData + "<tickler_shdes>remis en service</tickler_shdes>\n";
								vXMLData = vXMLData + "<tickler_priority>5</tickler_priority>\n";
								vXMLData = vXMLData + "</tickler>\n</customer>";


								try {
									XMLUtil.validate(vXMLData,xsd);
								} catch (Exception e) {
									throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID);
								}
								namedParameters.put("seqCMS", String.valueOf(seqCMSTemp));
								namedParameters.put("vXMLData", vXMLData.replaceAll("null",""));
								namedParameters.put("actionId", "20");
								namedParameters.put("userName", userName);
								namedParameters.put("coID", null);
								namedParameters.put("parentID", String.valueOf(seqCMS));
								namedParameters.put("addInfo", null);

								super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
							}
						}
					}
				}
				
				
				return seqCMS;
			} else
				LOG.error("Erreur d'insertion de la commande CMS");

			LOG.debug("--> End insertCMS");
		} catch (EmptyResultDataAccessException e) {
			LOG.error("Erreur d'insertion de la commande CMS");
		}
		return -1l;
	}
	
	public Long insertIdentifieClient(CustomerUpdate customer, String userName) throws FunctionnalException {
		String vXMLData;
		final String  originSuffix ="IDENTIFY_CUSTOMER";
		String cuID = getCuIDFromCoID(customer.getCoID()).toString();
		String xsd="xsd/modify.customer.xsd";
		LOG.debug("--> Start insertCMS");
		vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
			"<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../modify.customer.xsd\">\n"+
			"<id type=\"cs_id\">"+cuID+"</id>\n"+
			"<address>\n"+
			"<id>"+customer.getId()+"</id>\n";
			if (!StringUtil.isEmpty(customer.getPrenom())){
				vXMLData = vXMLData + "<first_name>"+customer.getPrenom()+"</first_name>\n";
			}	
			if (!StringUtil.isEmpty(customer.getNom())){
				vXMLData = vXMLData + "<last_name>"+customer.getNom()+"</last_name>\n";
			}
			if (!StringUtil.isEmpty(customer.getTitre())){
				vXMLData = vXMLData + "<title>"+customer.getTitre()+"</title>\n";
			}
			if (!StringUtil.isEmpty(customer.getSex())){
				vXMLData = vXMLData + "<sex>"+customer.getSex()+"</sex>\n";
			}
			if (!StringUtil.isEmpty(customer.getRue())){
				vXMLData = vXMLData + "<street>"+customer.getRue()+"</street>\n";
			}
			if (!StringUtil.isEmpty(customer.getZipCode())){
				vXMLData = vXMLData + "<zip_code>"+customer.getZipCode()+"</zip_code>\n";
			}
			if (!StringUtil.isEmpty(customer.getVille())){
				vXMLData = vXMLData + "<city>"+customer.getVille()+"</city>\n";
			}
			if (!StringUtil.isEmpty(customer.getQuartier())){
				vXMLData = vXMLData + "<state>"+customer.getQuartier()+"</state>\n";
			}
			if (!StringUtil.isEmpty(customer.getPays())){
				vXMLData = vXMLData + "<country_id>"+customer.getPays()+"</country_id>\n";
			}
			if (!StringUtil.isEmpty(customer.getAddrComp())){
				vXMLData = vXMLData + "<address_note_1>"+customer.getAddrComp()+"</address_note_1>\n";
			}	
			if (!StringUtil.isEmpty(customer.getNationality())){
				vXMLData = vXMLData + "<nationality>"+customer.getNationality()+"</nationality>\n";
			}	
			if (!StringUtil.isEmpty(customer.getTypeIdentif())){
				vXMLData = vXMLData + "<id_type>"+customer.getTypeIdentif()+"</id_type>\n";
			}	
			if (!StringUtil.isEmpty(customer.getIdentif())){
				vXMLData = vXMLData + "<passportno>"+customer.getIdentif()+"</passportno>\n";
			}			
			
			vXMLData = vXMLData + "</address>\n"+
			"</customer>";

		try {

			final String sql = CustomSQLUtil.get(GET_CMS_INT_SEQ);
			Map<String, String> namedParameters = new HashMap();
			try {
				XMLUtil.validate(vXMLData,xsd);
			} catch (Exception e) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID);
			}
			long seqCMS = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);

			if (seqCMS > 0) {

				namedParameters.put("seqCMS", String.valueOf(seqCMS));
				namedParameters.put("vXMLData", vXMLData.replaceAll("null",""));
				namedParameters.put("actionId", "18");
				namedParameters.put("userName", userName);
				namedParameters.put("coID", customer.getCoID());
				namedParameters.put("parentID", null);
				namedParameters.put("origin", ORIGIN_PREFIX+originSuffix);
				namedParameters.put("customerId", cuID);
				namedParameters.put("addInfo", "co_id="+customer.getCoID()+",ticket="+customer.getTicketNo());

				final String sql2 = CustomSQLUtil.get(INSERT_CMD_CMS);

				super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
				return seqCMS;
			} else
				LOG.error("Erreur d'insertion de la commande CMS");

			LOG.debug("--> End insertCMS");
		} catch (EmptyResultDataAccessException e) {
			LOG.error("Erreur d'insertion de la commande CMS");
		}
		return 0l;
	}

	@Override
	public Long changementSIM(SIMChange sim, String username) throws FunctionnalException {
		String vXMLData;
		final String originSuffix="CHANGESIM";
		String xsd="xsd/del.service.list.xsd";
		LOG.debug("--> Start changementSIM");

		/*final List<Map<String, Object>> 	list = new SQLQueryBuilder(null,GET_HLCODE_DN_BY_SIM,getNamedParameterJdbcTemplate())
				.addNamedParameter("oldsim",sim.getOldSim())
				.addNamedParameter("newsim",sim.getNewSim())
				.executeAsSelectUsingMap();
		String dn_id = StringUtil.getValueAsStringOrNull(list.get(0).get("DN_ID"));
		String hlcode =StringUtil.getValueAsStringOrNull( list.get(0).get("HLCODE"));
		if(! StringUtil.isNumeric(dn_id) || ! StringUtil.isNumeric(hlcode) )
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID,"DN_ID ou HLCODE de la sim invalides");
		SQLQueryBuilder.getInstance(UPDATE_HLCODE,getNamedParameterJdbcTemplate())
				.addNamedParameter("hlcode",hlcode)
				.addNamedParameter("dn_id",dn_id).executeAsUpdate();
				*/
		vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
			"<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../resource.replace.xsd\">\n"+
			"<contract>\n"+
			"<id type=\"co_id\">"+sim.getCoID()+"</id>\n"+
			"<RES_TYPE>"+sim.getType()+"</RES_TYPE>\n"+
			"<OLD_RES_NUM>"+sim.getOldSim()+"</OLD_RES_NUM>\n"+
			"<NEW_RES_NUM>"+sim.getNewSim()+"</NEW_RES_NUM>\n"+
			"<REASON>"+sim.getReason()+"</REASON>\n"+
			"<RETENTION>"+sim.getRetention()+"</RETENTION>\n"+
			"</contract>\n"+
			"</customer>";
		

		try {

			final String sql = CustomSQLUtil.get(GET_CMS_INT_SEQ);
			Map<String, String> namedParameters = new HashMap();
			/*
			try {r
				XMLUtil.validate(vXMLData,xsd);
			} catch (Exception e) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_rCOMMAND_INVALID);
			}*/
			long seqCMS = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);
			Integer cuIdInt =getCuIDFromCoID(sim.getCoID());
			String cuId=null;
			cuId=cuIdInt == null ? null :String.valueOf(cuIdInt);
			if (seqCMS > 0) {

				namedParameters.put("seqCMS", String.valueOf(seqCMS));
				namedParameters.put("vXMLData", vXMLData.replaceAll("null",""));
				namedParameters.put("actionId", "52");
				namedParameters.put("userName", username);
				namedParameters.put("coID", sim.getCoID());
				namedParameters.put("parentID", null);
				namedParameters.put("addInfo", null);
				namedParameters.put("origin", ORIGIN_PREFIX+originSuffix);
				namedParameters.put("customerId", cuId);

				final String sql2 = CustomSQLUtil.get(INSERT_CMD_CMS);

				super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
				String ticklerDesc ="Changement SIM: current SIM : " + sim.getOldSim() + " new SIM : " + sim.getNewSim() + " login  : " + username +  " id de motif dans erev : " + sim.getReason();
				long seqCMSTemp = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);
				if (seqCMSTemp > 0) {
					xsd="xsd/create.tickler.xsd";
					vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
							"<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../create.tickler.xsd\">\n"+
							"<id type=\"cs_id\">"+cuId+"</id>";

					vXMLData = vXMLData + "<contract type=\"co_id\">"+sim.getCoID()+"</contract>\n";
					vXMLData = vXMLData + "<tickler>\n";
					vXMLData = vXMLData + "<tickler_code>SYSTEM</tickler_code>\n";
					vXMLData = vXMLData + "<tickler_status>NOTE</tickler_status>\n";
					vXMLData = vXMLData + "<tickler_desc>"+ ticklerDesc +"</tickler_desc>\n";
					vXMLData = vXMLData + "<tickler_shdes>EREV_CHG_SIM_ORDER</tickler_shdes>\n";
					vXMLData = vXMLData + "<tickler_priority>5</tickler_priority>\n";
					vXMLData = vXMLData + "</tickler>\n</customer>";


					try {
						XMLUtil.validate(vXMLData,xsd);
					} catch (Exception e) {
						throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID);
					}
					namedParameters.put("seqCMS", String.valueOf(seqCMSTemp));
					namedParameters.put("vXMLData", vXMLData.replaceAll("null",""));
					namedParameters.put("actionId", "20");
					namedParameters.put("userName", username);
					namedParameters.put("coID", sim.getCoID());
					namedParameters.put("parentID", String.valueOf(seqCMS));
					namedParameters.put("addInfo", null);

					super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
				}

				return seqCMS;
			} else
				LOG.error("Erreur d'insertion de la commande CMS");

			LOG.debug("--> End changementSIM");
		} catch (EmptyResultDataAccessException e) {
			LOG.error("Erreur d'insertion de la commande CMS");
		}
		return 0l;
	}

	@Override
	public SMSResponse genererSMS(String nd, String userName, String coID) throws FunctionnalException {
		String vXMLData;
		final String originSuffix ="VERIFSMS";
		SMSResponse smsResponse = new SMSResponse();
		smsResponse.setSmscode(01);
		Random rnd = new Random();
		int n = 10000 + rnd.nextInt(90000);
		String code = (new Integer(n)).toString();
		String xsd="xsd/customer_types.xsd";
		vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
				"<send_sms xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../customer_types.xsd\">\n"+
				    "<DIR_NUM>"+nd+"</DIR_NUM>\n"+
				    "<SMS_TEXT>"+code+" est le code de validation pour l'opération demandée</SMS_TEXT>\n"+
				    "<MODULE>MOB</MODULE>\n"+
					"</send_sms>";
		

		try {

			final String sql = CustomSQLUtil.get(GET_CMS_INT_SEQ);
			Map<String, String> namedParameters = new HashMap();
			/*try {
				XMLUtil.validate(vXMLData,xsd);
			} catch (Exception e) {
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID);
			}*/
			long seqCMS = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);

			if (seqCMS > 0) {
				String coId = getCoIDFromDnNum(nd).toString();

				namedParameters.put("seqCMS", String.valueOf(seqCMS));
				namedParameters.put("vXMLData", vXMLData.replaceAll("null",""));
				namedParameters.put("actionId", "28");
				namedParameters.put("userName", userName);
				namedParameters.put("coID", coId);
				namedParameters.put("parentID", null);
				namedParameters.put("addInfo", "co_id="+coId+",code="+code);
				namedParameters.put("origin", ORIGIN_PREFIX+originSuffix);
				namedParameters.put("customerId", null);
				final String sql2 = CustomSQLUtil.get(INSERT_CMD_CMS);

				super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
				smsResponse.setRequest(seqCMS);
				smsResponse.setSmscode(Long.parseLong(code));
				return smsResponse;
			} else
				LOG.error("Erreur d'insertion de la commande CMS");

			LOG.debug("--> End insertCMS");
		} catch (EmptyResultDataAccessException e) {
			LOG.error("Erreur d'insertion de la commande CMS");
		}
		return smsResponse;
	}

	@Override
	public String savePreOrderPoP(PreOrderPoPRequest request) throws FunctionnalException {

		LOG.debug("--> Start savePreOrderPoP");
	
		

		try {
			// finding customer by ND
			String sql = CustomSQLUtil.get(GET_PRGCODE_BY_ND);

			Map<String, String> namedParameters = new HashMap();
			namedParameters.put("nd", request.getNd());

			String prgcode = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);

			if(GRCStringUtil.isNullOrEmpty(prgcode))
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2,"Le ND ne correspont à aucun client");
			if ("14".equals(prgcode) && request.getCategorie()==null)
				throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_POP_PRGCODE);
			sql = CustomSQLUtil.get(GET_PRECOMMANDE_POP_SEQ);
			namedParameters = new HashMap();

			long seqId = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);

			if (seqId > 0) {

				namedParameters.put("seqId", String.valueOf(seqId));
				namedParameters.put("planTarifaireId", String.valueOf(request.getPlanTarifaireId()));
				namedParameters.put("forfait", request.getForfait());
				namedParameters.put("nd", request.getNd());
				namedParameters.put("sim", request.getSIM());
				namedParameters.put("newsim", request.getNouveauSIM());
				namedParameters.put("simreason", request.getMotifChngSIM());
				namedParameters.put("username", request.getUserName());
				namedParameters.put("categorie", StringUtil.getValueAsStringOrNull(request.getCategorie()));

				 

				final String sql2 = CustomSQLUtil.get(INSERT_PRE_ORDER_POP);

				super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
				return String.valueOf(seqId);
			} else
				LOG.error("Erreur d'insertion de la commande CMS");

			LOG.debug("--> End changementSIM");
		} catch (EmptyResultDataAccessException e) {
			LOG.error("Erreur d'insertion de la commande CMS");
		}
		return "-1";
	}

	@Override
	public Long addOcc(OCCRequest occRequest) throws FunctionnalException {
		try {
			LOG.debug("--> Start addOcc");
			String originSuffix = "OCC";
			OccType occType = occRequest.getOccType();
			Marshaller jaxbMarshaller = JAXB_CONTEXT.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(occType, sw);
			String vXMLData = sw.toString();
			vXMLData=vXMLData.replaceAll("<occ>", OCC_HEADER);

			final String sql = CustomSQLUtil.get(GET_CMS_INT_SEQ);
			Map<String, String> namedParameters = new HashMap();

			long seqCMS = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);
			String addInfo = "";
			String co_id=null;
			if (occType.getId()!=null)
				addInfo=occType.getId().getType()+"=" + occType.getId().getValue()+";";
			if (occType.getCoId()!=null) {
				addInfo += occType.getCoId().getType() + "=" + occType.getCoId().getValue() + ";";
				if ("co_id".equals(occType.getCoId().getType()))
					co_id= occType.getCoId().getValue();
			}

			if (seqCMS > 0) {
				namedParameters.put("seqCMS", String.valueOf(seqCMS));
				namedParameters.put("vXMLData", vXMLData.replaceAll("null", ""));
				namedParameters.put("actionId", "11");
				namedParameters.put("userName", occRequest.getUserName());
				namedParameters.put("coID", co_id);
				namedParameters.put("parentID", occRequest.getParentID());
				namedParameters.put("addInfo", addInfo);
				namedParameters.put("origin", ORIGIN_PREFIX + originSuffix);
				namedParameters.put("customerId",  occType.getId().getValue().toString());
				final String sql2 = CustomSQLUtil.get(INSERT_CMD_CMS);

				super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);
			}
			LOG.debug("--> End addOcc");
			return seqCMS;
		}catch (JAXBException jaxbException){
			LOG.error("Erreur d'insertion de la commande CMS");
			throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_COMMAND_INVALID);
		}

	}

}
