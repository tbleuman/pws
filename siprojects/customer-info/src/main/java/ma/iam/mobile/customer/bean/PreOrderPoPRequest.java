package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.Date;

public class PreOrderPoPRequest implements Serializable {

	private String nd;
	private String SIM;
	private String nouveauSIM;
	private String motifChngSIM;
	private Long categorie;
	private Long planTarifaireId;
	private String forfait;
	private String userName;
	private String ticket;


	public String getNd() {
		return nd;
	}

	public void setNd(String nd) {
		this.nd = nd;
	}

	public String getSIM() {
		return SIM;
	}

	public void setSIM(String sIM) {
		SIM = sIM;
	}

	public String getMotifChngSIM() {
		return motifChngSIM;
	}

	public Long getCategorie() {
		return categorie;
	}

	public void setCategorie(Long categorie) {
		this.categorie = categorie;
	}

	public void setMotifChngSIM(String motifChngSIM) {
		this.motifChngSIM = motifChngSIM;
	}

	public String getNouveauSIM() {
		return nouveauSIM;
	}

	public void setNouveauSIM(String nouveauSIM) {
		this.nouveauSIM = nouveauSIM;
	}


	public Long getPlanTarifaireId() {
		return planTarifaireId;
	}

	public void setPlanTarifaireId(Long planTarifaireId) {
		this.planTarifaireId = planTarifaireId;
	}

	public String getForfait() {
		return forfait;
	}

	public void setForfait(String forfait) {
		this.forfait = forfait;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
