package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Service implements Serializable {

	 private String sncode;
     private String service_package_code;
     private String directory_number;     
     private List<Parameter> parameters;
	/**
	 * @return the sncode
	 */
	public String getSncode() {
		return sncode;
	}
	/**
	 * @return the directory_number
	 */
	public String getDirectory_number() {
		return directory_number;
	}
	/**
	 * @param directory_number the directory_number to set
	 */
	public void setDirectory_number(String directory_number) {
		this.directory_number = directory_number;
	}
	/**
	 * @param sncode the sncode to set
	 */
	public void setSncode(String sncode) {
		this.sncode = sncode;
	}
	/**
	 * @return the service_package_code
	 */
	public String getService_package_code() {
		return service_package_code;
	}
	/**
	 * @param service_package_code the service_package_code to set
	 */
	public void setService_package_code(String service_package_code) {
		this.service_package_code = service_package_code;
	}
	/**
	 * @return the parameters
	 */
	public List<Parameter> getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}


}
