package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class Language implements Serializable {

	private String id;
	private String label;
	private String code;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Language() {
		super();
	}

}
