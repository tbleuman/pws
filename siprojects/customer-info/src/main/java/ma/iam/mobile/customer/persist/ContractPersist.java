package ma.iam.mobile.customer.persist;

public class ContractPersist {

	private String dnNum;
	private String coId;
	private String status;
	private String customerIdentifiant;
	private String typeIdent;
	private String codeFid;
	private String prgCode;
	private String idCustomer;
	private String idCustomerParent;
	private String ratePlan;

	public String getDnNum() {
		return dnNum;
	}

	public void setDnNum(String dnNum) {
		this.dnNum = dnNum;
	}

	public String getCoId() {
		return coId;
	}

	public void setCoId(String coId) {
		this.coId = coId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerIdentifiant() {
		return customerIdentifiant;
	}

	public void setCustomerIdentifiant(String customerIdentifiant) {
		this.customerIdentifiant = customerIdentifiant;
	}

	public String getTypeIdent() {
		return typeIdent;
	}

	public void setTypeIdent(String typeIdent) {
		this.typeIdent = typeIdent;
	}

	public String getCodeFid() {
		return codeFid;
	}

	public void setCodeFid(String codeFid) {
		this.codeFid = codeFid;
	}

	public String getPrgCode() {
		return prgCode;
	}

	public void setPrgCode(String prgCode) {
		this.prgCode = prgCode;
	}

	public String getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getIdCustomerParent() {
		return idCustomerParent;
	}

	public String getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

	public void setIdCustomerParent(String idCustomerParent) {
		this.idCustomerParent = idCustomerParent;
	}

}
