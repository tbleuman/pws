package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Operation implements Serializable {
	public Operation(String code, String des, String eligibility, String comment) {
		super();
		this.code = code;
		this.des = des;
		this.eligibility = eligibility;
		this.comment = comment;
	}
	/**
	 * @return the eligibility
	 */
	public String getEligibility() {
		return eligibility;
	}
	/**
	 * @param eligibility the eligibility to set
	 */
	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	private String code;
	private String des;
	
	private String eligibility;
	private String comment;
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the des
	 */
	public String getDes() {
		return des;
	}
	/**
	 * @param des the des to set
	 */
	public void setDes(String des) {
		this.des = des;
	}

	
}
