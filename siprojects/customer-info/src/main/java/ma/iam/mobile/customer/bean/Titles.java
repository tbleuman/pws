package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.List;

public class Titles implements Serializable {
	
	

	private List <Title> title;

	/**
	 * @return the title
	 */
	public List<Title> getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(List<Title> title) {
		this.title = title;
	}

}