package ma.iam.mobile.customer.interceptor;

import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.util.StringUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.opensaml.ws.WSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class InTraceInterceptorImpl extends AbstractPhaseInterceptor implements TraceInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(InTraceInterceptorImpl.class);
	private BaseDAO baseDAO;

	private String ticket = "ticket";

	private String userName = "userName";

	private String pwsTraceQuery = "select request from PWS_TRACES t , pws_requests r where r.call_id (+) = t.call_id and t.REQUEST_EXTERNAL_REFERENCE = :request_external_reference";

	public InTraceInterceptorImpl() {
		super(Phase.PRE_INVOKE);
	}

	public void handleMessage(Message message) throws Fault {
		MessageContentsList inObjects = MessageContentsList.getContentsList(message);
		if (inObjects != null && !inObjects.isEmpty()){
			for (Iterator<Object> it = inObjects.iterator(); it.hasNext() ;){
				Object ob = it.next();
				if( ob !=null) {
                    try {
                        String ticketId = BeanUtils.getProperty(ob, ticket);
                        String userNameId = BeanUtils.getProperty(ob, userName);
                        if (ticketId != null && !"".equals(ticketId.trim()) && ticketId.matches("[0-9]+")) {
                            Map<String, String> namedParameters = new HashMap<String, String>();
                            namedParameters.put("request_external_reference", ticketId);
                            List<Map<String, Object>> listt = baseDAO.getNamedParameterJdbcTemplate().queryForList(pwsTraceQuery, namedParameters);
                            LOG.debug("******************* " + ob.getClass().getName() + " ++++++++  " + listt);
                            message.getExchange().put("ma.iam.ticket", ticketId + "");
                            message.getExchange().put("ma.iam.userName", userNameId + "");
                            System.out.println(message.getExchange().get("ma.iam.ticket"));
                            if (listt !=null && listt.size() > 0) {
                                String req = StringUtil.getValueAsStringOrNull(listt.get(0).get("REQUEST"));
                                throw new Fault(new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_TICKET, "Ticket déjà traité : " + ticketId + (req != null ? ", correspondant au request : "+req : "")));
                            }


                        }
                    } catch (NoSuchMethodException e) {
                        LOG.debug("NoSuchMethodException : "+e.getMessage());
                    } catch (IllegalAccessException e) {
                        LOG.debug("IllegalAccessException : "+e.getMessage());
                    } catch (InvocationTargetException e) {
                        LOG.debug("InvocationTargetException : "+e.getMessage());
                    }
                }

			}
		}
	}


    public BaseDAO getBaseDAO() {
        return baseDAO;
    }

    public void setBaseDAO(BaseDAO baseDAO) {
        this.baseDAO = baseDAO;
    }

    public String getPwsTraceQuery() {
        return pwsTraceQuery;
    }

    public void setPwsTraceQuery(String pwsTraceQuery) {
        this.pwsTraceQuery = pwsTraceQuery;
    }

    public String getTicket() {        return ticket;    }

    public void setTicket(String ticket) {        this.ticket = ticket;    }
}
