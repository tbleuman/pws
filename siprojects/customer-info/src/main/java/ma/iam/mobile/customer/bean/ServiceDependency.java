package ma.iam.mobile.customer.bean;

import java.io.Serializable;
import java.util.Vector;

public class ServiceDependency implements Serializable {
    private String rateplane;
    private String service;
    private Vector<String>  dependeningServices;
    private Vector<String>  icompatibleServices;

    public String getRateplane() {
        return rateplane;
    }

    public void setRateplane(String rateplane) {
        this.rateplane = rateplane;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Vector<String> getDependeningServices() {
        return dependeningServices;
    }

    public void setDependeningServices( Vector<String> dependeningServices) {
        this.dependeningServices = dependeningServices;
    }

    public  Vector<String> getIcompatibleServices() {
        return icompatibleServices;
    }

    public void setIcompatibleServices( Vector<String> icompatibleServices) {
        this.icompatibleServices = icompatibleServices;
    }
}
