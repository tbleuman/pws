package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class ContractRequest implements Serializable {
	
	
	private String MSISDN;
	private String CUSTCODE;
	private String SM_SERIALNUM;
	



	/**
	 * @return the mSISDN
	 */
	public String getMSISDN() {
		return MSISDN;
	}


	/**
	 * @param mSISDN the mSISDN to set
	 */
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}


	/**
	 * @return the cUSTCODE
	 */
	public String getCUSTCODE() {
		return CUSTCODE;
	}


	/**
	 * @param cUSTCODE the cUSTCODE to set
	 */
	public void setCUSTCODE(String cUSTCODE) {
		CUSTCODE = cUSTCODE;
	}


	/**
	 * @return the sM_SERIALNUM
	 */
	public String getSM_SERIALNUM() {
		return SM_SERIALNUM;
	}


	/**
	 * @param sM_SERIALNUM the sM_SERIALNUM to set
	 */
	public void setSM_SERIALNUM(String sM_SERIALNUM) {
		SM_SERIALNUM = sM_SERIALNUM;
	}



	public ContractRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

}
