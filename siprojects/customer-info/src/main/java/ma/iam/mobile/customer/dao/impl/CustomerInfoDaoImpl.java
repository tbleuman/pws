/*
 * @author AtoS
 */

package ma.iam.mobile.customer.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import ma.iam.mobile.customer.bean.*;
import ma.iam.mobile.customer.util.SQLQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import fr.capgemini.iam.grc.core.utils.CustomBeanPropertyRowMapper;
import fr.capgemini.iam.grc.core.utils.CustomSQLUtil;
import fr.capgemini.iam.grc.core.utils.GRCStringUtil;
import ma.iam.mobile.customer.constants.ExceptionCodeTypeEnum;
import ma.iam.mobile.customer.dao.CustomerInfoDao;
import ma.iam.mobile.customer.exceptions.FunctionnalException;
import ma.iam.mobile.customer.exceptions.SyntaxiqueException;
import ma.iam.mobile.customer.persist.ContractPersist;
import ma.iam.mobile.customer.util.StringUtil;
import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

@Repository
public class CustomerInfoDaoImpl extends BaseDAOMobileImpl implements CustomerInfoDao {
    private static final Logger LOG = LoggerFactory.getLogger(CustomerInfoDaoImpl.class);

    private static final String GET_INFO_CUSTOMER = CustomerInfoDaoImpl.class
            .getName() + ".GET_INFO_CUSTOMER";

    private static final String LISTE_SERVICE_PRM_STANDARD = CustomerInfoDaoImpl.class
            .getName() + ".LISTE_SERVICE_PRM_STANDARD";

    private static final String LISTE_PRM_VAL = CustomerInfoDaoImpl.class
            .getName() + ".LISTE_PRM_VAL";
    private static final String LISTE_RATEPLAN_SERVICE = CustomerInfoDaoImpl.class
            .getName() + ".LISTE_RATEPLAN_SERVICE";
    private static final String GET_BOOLEAN_PRM = CustomerInfoDaoImpl.class
            .getName() + ".GET_BOOLEAN_PRM";
    private static final String GET_HLCODE_BY_SIM = CustomerInfoDaoImpl.class
            .getName() + ".GET_HLCODE_BY_SIM";

    private static final String LISTE_ENGAGEMENT = CustomerInfoDaoImpl.class.getName() + ".LISTE_ENGAGEMENT";

    private static final String LISTE_ENGAGEMENT_FORFAIT = CustomerInfoDaoImpl.class.getName() + ".LISTE_ENGAGEMENT_FORFAIT";

    private static final String LISTE_ENGAGEMENT_FORFAIT_TMCODE_ONLY = CustomerInfoDaoImpl.class.getName() + ".LISTE_ENGAGEMENT_FORFAIT_TMCODE_ONLY";

    private static final String GET_CUSTOMER_BY_CUSTCODE = "select customer_id from customer_all where ";

    private static final String GET_CUSTOMERS = CustomerInfoDaoImpl.class
            .getName() + ".GET_CUSTOMERS";

    private static final String GET_INFO_CONTRACT = CustomerInfoDaoImpl.class
            .getName() + ".GET_INFO_CONTRACT";

    private static final String VERIFY_RC_EXISTENCE = CustomerInfoDaoImpl.class.getName()
            + ".VERIFY_RC_EXISTENCE";

    private static final String GET_CONTRACT_BY_DN_NUM = CustomerInfoDaoImpl.class.getName()
            + ".GET_CONTRACT_BY_DN_NUM";


    private static final String GET_ELIGIBILITY = CustomerInfoDaoImpl.class.getName()
            + ".GET_ELIGIBILITY";

    private static final String GET_CAT_TMCODES_SERVICES = CustomerInfoDaoImpl.class.getName()
            + ".GET_CAT_TMCODES_SERVICES";

    private static final String GET_CAT_SERVICES = CustomerInfoDaoImpl.class.getName()
            + ".GET_CAT_SERVICES";

    private static final String GET_CAT_TMCODES = CustomerInfoDaoImpl.class.getName()
            + ".GET_CAT_TMCODES";

    private static final String GET_SEG_CATEGORY = CustomerInfoDaoImpl.class.getName()
            + ".GET_SEG_CATEGORY";

    private static final String SIM_LIST_RAISONS_MODIF = CustomerInfoDaoImpl.class.getName()
            + ".SIM_LIST_RAISONS_MODIF";

    private static final String GET_CURR_ENG_MOI = CustomerInfoDaoImpl.class.getName()
            + ".GET_CURR_ENG_MOI";

    private static final String GET_CURR_ENG = CustomerInfoDaoImpl.class.getName()
            + ".GET_CURR_ENG";

    private static final String GET_CURR_SIM = CustomerInfoDaoImpl.class.getName()
            + ".GET_CURR_SIM";

    private static final String LIST_RAISONS_MODIF = CustomerInfoDaoImpl.class.getName()
            + ".LIST_RAISONS_MODIF";

    private static final String GET_SERVICE_INTERNET_BY_DN_NUM = CustomerInfoDaoImpl.class.getName()
            + ".GET_SERVICE_INTERNET_BY_DN_NUM";

    private static final String LIST_RAISON_CLIENT = CustomerInfoDaoImpl.class.getName()
            + ".LIST_RAISON_CLIENT";

    private static final String VERIFY_SMS_CODE = CustomerInfoDaoImpl.class.getName()
            + ".VERIFY_SMS_CODE";

    private static final String GET_TM_FORFAITS = CustomerInfoDaoImpl.class.getName()
            + ".GET_TM_FORFAITS";

    private static final String SIM_VALIDATION = CustomerInfoDaoImpl.class.getName()
            + ".SIM_VALIDATION";

    private static final String VERIFY_PACK_TEST = CustomerInfoDaoImpl.class.getName()
            + ".VERIFY_PACK_TEST";

    private static final String VERIFY_PACK_1 = CustomerInfoDaoImpl.class.getName()
            + ".VERIFY_PACK_1";

    private static final String VERIFY_PACK_2 = CustomerInfoDaoImpl.class.getName()
            + ".VERIFY_PACK_2";

    private static final String GET_CMS_INT_SEQ = CustomerInfoDaoImpl.class.getName() + ".GET_CMS_INT_SEQ";

    private static final String INSERT_CMD_CMS = CustomerInfoDaoImpl.class.getName() + ".INSERT_CMD_CMS";

    private static final String GET_CLIENT_BY_CUSTCODE = CustomerInfoDaoImpl.class.getName()
            + ".GET_CLIENT_BY_CUSTCODE";

    private static final String GET_CONTRACTS_BY_CUSTCODE = CustomerInfoDaoImpl.class.getName()
            + ".GET_CONTRACTS_BY_CUSTCODE";

    private static final String LIST_UNPAID_INVOICES = CustomerInfoDaoImpl.class.getName() + ".LIST_UNPAID_INVOICES";

    private static final String LIST_SEG_CATEGORY = CustomerInfoDaoImpl.class.getName() + ".LIST_SEG_CATEGORY";

    private static final String GET_CLIENT_BLACKLIST_BY_CUSTCODE = CustomerInfoDaoImpl.class.getName()
            + ".GET_CLIENT_BLACKLIST_BY_CUSTCODE";

    private static final String LIST_IDENTIFIER_TYPE = CustomerInfoDaoImpl.class.getName() + ".LIST_IDENTIFIER_TYPE";

    private static final String LIST_LANGUAGE = CustomerInfoDaoImpl.class.getName() + ".LIST_LANGUAGE";

    private static final String LIST_CITY = CustomerInfoDaoImpl.class.getName() + ".LIST_CITY";

    private static final String LIST_TITLE = CustomerInfoDaoImpl.class.getName() + ".LIST_TITLE";

    private static final String LIST_COUNTRY = CustomerInfoDaoImpl.class.getName() + ".LIST_COUNTRY";

    private static final String LIST_SEGMENT = CustomerInfoDaoImpl.class.getName() + ".LIST_SEGMENT";

    private static final String LIST_PAYMENT = CustomerInfoDaoImpl.class.getName() + ".LIST_PAYMENT";

    private static final String LIST_DN_TYPE = CustomerInfoDaoImpl.class.getName() + ".LIST_DN_TYPE";

    private static final String GET_INFO_CLIENT_BY_CUSTCODE = CustomerInfoDaoImpl.class.getName()
            + ".GET_INFO_CLIENT_BY_CUSTCODE";

    private static final String GET_INFO_CLIENT_BY_CUSTCODE_NO_ND = CustomerInfoDaoImpl.class.getName()
            + ".GET_INFO_CLIENT_BY_CUSTCODE_NO_ND";

    private static final String LIST_DIRECTORY_NUMBER = CustomerInfoDaoImpl.class.getName() + ".LIST_DIRECTORY_NUMBER";

    private static final String UPDATE_LIST_DIRECTORY_NUMBER = CustomerInfoDaoImpl.class.getName() + ".UPDATE_LIST_DIRECTORY_NUMBER";

    private static final String UPDATE_RESERVED_DIRECTORY_NUMBER = CustomerInfoDaoImpl.class.getName() + ".UPDATE_RESERVED_DIRECTORY_NUMBER";

    private static final String SELECT_RESERVED_DIRECTORY_NUMBER = CustomerInfoDaoImpl.class.getName() + ".SELECT_RESERVED_DIRECTORY_NUMBER";

    private static final String GET_CLIENT_ND = CustomerInfoDaoImpl.class.getName() + ".GET_CLIENT_ND";

    private static final String GET_CLIENT_SM = CustomerInfoDaoImpl.class.getName() + ".GET_CLIENT_SM";

    private static final String GET_DN_TYPE = CustomerInfoDaoImpl.class.getName() + ".GET_DN_TYPE";

    private static final String GET_CO_ID_BY_DN_NUM = CustomerEditDaoImpl.class.getName() + ".GET_CO_ID_BY_DN_NUM";

    private static final String GET_CS_ID_BY_CO_ID = CustomerEditDaoImpl.class.getName() + ".GET_CS_ID_BY_CO_ID";

    private static final String LISTE_DEPENDENCIES = CustomerEditDaoImpl.class.getName() + ".LISTE_DEPENDENCIES";

    private static final String PARAMETERES = CustomerEditDaoImpl.class.getName() + ".PARAMETERES";
    private static final String PARAMETERESVALUES = CustomerEditDaoImpl.class.getName() + ".PARAMETERESVALUES";

   	public Integer getCoIDFromDnNum(String nd) {

        LOG.debug("--> getCoIDFromDnNum");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_CO_ID_BY_DN_NUM);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("nd", nd);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return result;
    }

    public Integer getCuIDFromCo(String co) {

        LOG.debug("--> getCuIDFromCo");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_CS_ID_BY_CO_ID);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("coId", co);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return result;
    }

    public List<ServiceDTO> listeRateplanServicesParameters(String tmcode, String foId)
            throws FunctionnalException, TechnicalException, SyntaxiqueException {
        LOG.debug("--> listeRateplanServicesParameters");
        final List<ServiceDTO> list;
        LOG.debug("Fetching Services with parameters .. ");
        list = new SQLQueryBuilder(ServiceDTO.class,LISTE_RATEPLAN_SERVICE,getNamedParameterJdbcTemplate())
                .addNamedParameter("tmcode",tmcode)
                .executeAsSelectUsingRowMapper();
        Map<String,ServiceDTO> servicesMapsHelper = new HashMap<String,ServiceDTO> ();
        for (ServiceDTO serviceDTO :list
             ) {
            servicesMapsHelper.put(serviceDTO.getId(),serviceDTO);
            serviceDTO.setListParametreService(new ArrayList<ParametreServiceDTO>());
        }
        List<ParametreServiceDTO> parameters =  new SQLQueryBuilder(ParametreServiceDTO.class,PARAMETERES,getNamedParameterJdbcTemplate())
                .addNamedParameter("tmcode",tmcode)
                .executeAsSelectUsingRowMapper();
        Map<String,ParametreServiceDTO> parametersMapsHelper = new HashMap<String, ParametreServiceDTO>();
        for (ParametreServiceDTO parametreServiceDTO : parameters
             ) {
            parametreServiceDTO.setListValeur(new ArrayList<String>());
            String key=parametreServiceDTO.getSncode()+"$"+parametreServiceDTO.getId();
            parametersMapsHelper.put(key,parametreServiceDTO);
            servicesMapsHelper.get(parametreServiceDTO.getSncode()).getListParametreService().add(parametreServiceDTO);
        }
        List<Map<String,Object>> paramsValues = SQLQueryBuilder.getInstance(PARAMETERESVALUES,getNamedParameterJdbcTemplate())
                .addNamedParameter("tmcode",tmcode)
                .executeAsSelectUsingMap();

        for (Map<String,Object> pvalue:paramsValues
             ) {
            String key=pvalue.get("CODE")+"$"+pvalue.get("PARAMETER_ID");
            ParametreServiceDTO parametreServiceDTO =parametersMapsHelper.get(key);
            if(parametreServiceDTO!=null) {
                if ("Chaîne".equals(parametreServiceDTO.getTypeValeur()) && "ListDB".equals(parametreServiceDTO.getParameterType())) {
                    parametreServiceDTO.getListValeur().add(pvalue.get("PRM_VALUE_STRING").toString() + "|" + pvalue.get("PRM_VALUE_DES").toString());
                } else {
                    parametreServiceDTO.getListValeur().add(pvalue.get("PRM_VALUE_SEQNO").toString() + "|" + pvalue.get("PRM_VALUE_DES").toString());
                }
            }
        }
   	    return list;
    }

    public List<ServiceDTO> listeRateplanServicesParameters2(String tmcode, String foId)
            throws FunctionnalException, TechnicalException, SyntaxiqueException {

        LOG.debug("--> listeRateplanServicesParameters");
        final List<ServiceDTO> list;
        try {

            String sql = CustomSQLUtil.get(LISTE_RATEPLAN_SERVICE);


            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("tmcode", tmcode);


            final CustomBeanPropertyRowMapper<ServiceDTO> custRowMapper = new CustomBeanPropertyRowMapper<ServiceDTO>(
                    ServiceDTO.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);

            if (list != null && list.size() > 0) {

                sql = CustomSQLUtil.get(LISTE_SERVICE_PRM_STANDARD);
                namedParameters = new HashMap();

                namedParameters.put("tmcode", tmcode);

                List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
                if (listt != null && listt.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        ParametreServiceDTO prm = new ParametreServiceDTO();
                        List<ParametreServiceDTO> prmList = new ArrayList<ParametreServiceDTO>();
                        List<String> vals = new ArrayList<String>();
                        String oldId = "0";
                        for (int j = 0; j < listt.size(); j++) {
                            Map<String, Object> tempp = listt.get(j);
                            if (list.get(i).getConfigurable() && (tempp.get("CODE") != null && tempp.get("CODE").toString().equals(list.get(i).getId()))) {
                                if (!oldId.equals(tempp.get("PARAMETER_ID").toString())) {
                                    if (!"0".equals(oldId)) {
                                        prm.setListValeur(vals);
                                        prmList.add(prm);
                                        vals = new ArrayList<String>();
                                    }
                                    prm = new ParametreServiceDTO();
                                    prm.setTypeValeur(tempp.get("DATA_TYPE").toString());
                                    prm.setCode(tempp.get("SHDES").toString());
                                    prm.setId(tempp.get("PARAMETER_ID").toString());
                                    prm.setLabel(tempp.get("PRM_DES").toString());
                                    prm.setObligatoire(true);
                                    prm.setNo(tempp.get("PRM_NO").toString());
                                    oldId = tempp.get("PARAMETER_ID").toString();
                                }
                                if (tempp.get("PRM_VALUE_DEF") != null) {
                                    if ("Chaîne".equals(tempp.get("DATA_TYPE")) && "ListDB".equals(tempp.get("PARAMETER_TYPE"))) {
                                        prm.setValeurParDefaut(tempp.get("PRM_VALUE_STRING").toString());
                                    } else {
                                        prm.setValeurParDefaut(tempp.get("PRM_VALUE_SEQNO").toString());
                                    }
                                }
                                if ("Chaîne".equals(tempp.get("DATA_TYPE")) && "ListDB".equals(tempp.get("PARAMETER_TYPE"))) {
                                    vals.add(tempp.get("PRM_VALUE_STRING").toString() + "|" + tempp.get("PRM_VALUE_DES").toString());
                                } else {
                                    vals.add(tempp.get("PRM_VALUE_SEQNO").toString() + "|" + tempp.get("PRM_VALUE_DES").toString());
                                }

                            }
                        }
                        if (vals != null && vals.size() > 0) {
                            prm.setListValeur(vals);
                            prmList.add(prm);
                            list.get(i).setListParametreService(prmList);
                        }


                    }

                }


                final List<String> tags = new ArrayList<String>(0);
                sql = CustomSQLUtil.get(LISTE_PRM_VAL);
                namedParameters = new HashMap();

                namedParameters.put("tmcode", tmcode);

                if (!GRCStringUtil.isNullOrEmpty(foId)) {
                    tags.add("FORFAIT");
                }

                try {
                    sql = GRCStringUtil.formatQuery(sql, tags);
                } catch (final Exception e) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
                }


                if (!GRCStringUtil.isNullOrEmpty(foId)) {
                    namedParameters.put("foId", foId);
                }

                listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
                if (listt != null && listt.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {


                        boolean toBeChecked = true;
                        List<ParametreServiceDTO> listPrm = list.get(i).getListParametreService();
                        if (listPrm != null) {
                            for (int j = 0; j < listPrm.size(); j++) {
                                if (listPrm.get(j).getId().equals(list.get(i).getId())
                                        && (listPrm.get(j).getListValeur() == null || listPrm.get(j).getListValeur().size() == 0)) {
                                    toBeChecked = false;
                                }
                            }
                        }


                        if (list.get(i).getConfigurable() && toBeChecked) {
                            ParametreServiceDTO prm = new ParametreServiceDTO();
                            List<ParametreServiceDTO> prmList = listPrm != null ? listPrm : new ArrayList<ParametreServiceDTO>();
                            List<String> vals = new ArrayList<String>();
                            String oldId = "0";
                            for (int j = 0; j < listt.size(); j++) {
                                Map<String, Object> tempp = listt.get(j);
                                if (list.get(i).getConfigurable() && (tempp.get("CODE") != null && tempp.get("CODE").toString().equals(list.get(i).getId()))) {
                                    if (!oldId.equals(tempp.get("PARAMETER_ID").toString())) {
                                        if (!"0".equals(oldId)) {
                                            prm.setListValeur(vals);
                                            prmList.add(prm);
                                            vals = new ArrayList<String>();
                                        }
                                        prm = new ParametreServiceDTO();
                                        prm.setTypeValeur(tempp.get("DATA_TYPE").toString());
                                        prm.setCode(tempp.get("SHDES").toString());
                                        prm.setId(tempp.get("PARAMETER_ID").toString());
                                        prm.setLabel(tempp.get("PRM_DES").toString());
                                        prm.setNo(tempp.get("PRM_VALUE_SEQNO")==null?null:tempp.get("PRM_VALUE_SEQNO").toString());
                                        prm.setObligatoire(true);
                                        oldId = tempp.get("PARAMETER_ID").toString();
                                    }
                                    if (tempp.get("PRM_VALUE_DEF") != null) {
                                        prm.setValeurParDefaut(tempp.get("PRM_VALUE_SEQNO").toString());
                                    }
                                    if(tempp.get("PRM_VALUE_SEQNO")!=null & tempp.get("PRM_VALUE_DES")!=null)
                                        vals.add(tempp.get("PRM_VALUE_SEQNO").toString() + "|" + tempp.get("PRM_VALUE_DES").toString());

                                }
                            }
                            if (vals != null && vals.size() > 0) {
                                prm.setListValeur(vals);
                                prmList.add(prm);
                                list.get(i).setListParametreService(prmList);
                            }
                        }

                    }

                }


                sql = CustomSQLUtil.get(GET_BOOLEAN_PRM);
                namedParameters = new HashMap();

                namedParameters.put("tmcode", tmcode);


                listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
                if (listt != null && listt.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getConfigurable()) {
                            List<ParametreServiceDTO> listPrm = list.get(i).getListParametreService();
                            ParametreServiceDTO prm = new ParametreServiceDTO();
                            List<ParametreServiceDTO> prmList = listPrm != null ? listPrm : new ArrayList<ParametreServiceDTO>();
                            List<String> vals = new ArrayList<String>();

                            for (int j = 0; j < listt.size(); j++) {
                                Map<String, Object> tempp = listt.get(j);
                                if (list.get(i).getConfigurable() && (tempp.get("CODE") != null && tempp.get("CODE").toString().equals(list.get(i).getId()))) {
                                    prm = new ParametreServiceDTO();
                                    vals = new ArrayList<String>();
                                    prm.setTypeValeur(tempp.get("DATA_TYPE").toString());
                                    prm.setCode(tempp.get("SHDES").toString());
                                    prm.setId(tempp.get("PARAMETER_ID").toString());
                                    prm.setLabel(tempp.get("PRM_DES").toString());
                                    prm.setObligatoire(true);
                                    prm.setValeurParDefaut("false");
                                    vals.add("true");
                                    vals.add("false");
                                    prm.setListValeur(vals);
                                    prmList.add(prm);
                                    list.get(i).setListParametreService(prmList);

                                }
                            }

                        }

                    }
                }


            }


            LOG.debug("<-- listeRateplanServicesParameters");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listeRateplanServicesParameters is  null ");
            return null;
        }


        return list;

    }

    public List<Operation> verifierEligibilite(String nd, String coId, String cuId) throws FunctionnalException {

        LOG.debug("--> verifierEligibilite");
        final List<Operation> list = new ArrayList<Operation>();
        try {

            String sql = CustomSQLUtil.get(GET_ELIGIBILITY);
            String co = coId;
            if (GRCStringUtil.isNullOrEmpty(nd) && GRCStringUtil.isNullOrEmpty(coId)) {
                co = "0";
            } else if (!GRCStringUtil.isNullOrEmpty(nd)) {
                Integer coIdTemp = getCoIDFromDnNum(nd);
                if (!GRCStringUtil.isNullOrEmpty(coId)) {
                    if (coIdTemp == null || !coIdTemp.toString().equals(coId)) {
                        throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2);
                    } else {
                        co = coIdTemp.toString();
                    }
                } else if (coIdTemp == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2);
                } else
                    co = coIdTemp.toString();


            }


            String cs = cuId;
            if (co != null && !co.equals("0")) {
                Integer csTemp = getCuIDFromCo(co);
                if (!GRCStringUtil.isNullOrEmpty(cuId)) {
                    if (csTemp == null || !csTemp.toString().equals(cuId)) {
                        throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2);
                    } else {
                        cs = csTemp.toString();
                    }
                } else if (csTemp == null) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.PROBLEM_NON_TROUVE2);
                } else
                    cs = csTemp.toString();
            }


            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("csID", cs);
            namedParameters.put("coID", co);

            List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);

            if (listt != null && listt.size() > 0) {
                Map<String, Object> tempp = listt.get(0);
                if (!GRCStringUtil.isNullOrEmpty(cuId)) {
                    if ((tempp.get("CSLEVEL") != null && tempp.get("CSLEVEL").toString().equals("40")) &&
                            tempp.get("STATUS").toString().equals("a") &&
                            tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("NA", "Nouvel abonnement", "1", ""));
                    } else if (tempp.get("CSLEVEL") == null || !tempp.get("CSLEVEL").toString().equals("40")) {
                        list.add(new Operation("NA", "Nouvel abonnement", "0", "Le niveau de client est erroné"));
                    } else if (!tempp.get("STATUS").toString().equals("a")) {
                        list.add(new Operation("NA", "Nouvel abonnement", "0", "Le statut de client est erroné"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("NA", "Nouvel abonnement", "0", "Le client n'est pas identifié"));
                    }
                    if ((tempp.get("CSLEVEL") != null && (tempp.get("CSLEVEL").toString().equals("10") || tempp.get("CSLEVEL").toString().equals("30"))
                    )) {
                        list.add(new Operation("NEW_CLIENT", "Nouveau Client", "1", ""));
                    } else {
                        list.add(new Operation("NEW_CLIENT", "Nouveau Client", "0", "Le niveau de client est erroné"));
                    }


                    if ((tempp.get("CSLEVEL") != null && tempp.get("ISIDENTIFIED").toString().equals("0") && tempp.get("CSLEVEL").toString().equals("40")) &&
                            tempp.get("STATUS").toString().equals("a")
                    ) {
                        list.add(new Operation("IDENTIFICATION_CLIENT", "Identification client", "1", ""));
                    } else if (tempp.get("CSLEVEL") == null || !tempp.get("CSLEVEL").toString().equals("40")) {
                        list.add(new Operation("IDENTIFICATION_CLIENT", "Identification client", "0", "Le niveau de client est erroné"));
                    } else if (!tempp.get("STATUS").toString().equals("a")) {
                        list.add(new Operation("IDENTIFICATION_CLIENT", "Identification client", "0", "Le statut de client est erroné"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("0")) {
                        list.add(new Operation("IDENTIFICATION_CLIENT", "Identification client", "0", "Le client est déjà identifié"));
                    }


                    if (tempp.get("ISIDENTIFIED").toString().equals("1") &&
                            tempp.get("STATUS").toString().equals("a")) {
                        list.add(new Operation("PORTABILITE_IN", "Portabilite IN", "1", ""));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("PORTABILITE_IN", "Portabilite IN", "0", "Le client n'est pas identifié"));
                    } else if (!tempp.get("STATUS").toString().equals("a")) {
                        list.add(new Operation("PORTABILITE_IN", "Portabilite IN", "0", "Le statut de client est erroné"));
                    }

                }


                if (!GRCStringUtil.isNullOrEmpty(coId)) {
                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("a") && tempp.get("PENDING").toString().equals("0")) &&
                            tempp.get("RECENTCHANGE").toString().equals("0") && tempp.get("ISIDENTIFIED").toString().equals("1")
                            && !tempp.get("PREPOST").toString().equals("0")) {
                        list.add(new Operation("MIGRATION_PRE_TO_POSTPAID", "Migration prepaid to postpaid", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("a")) {
                        list.add(new Operation("MIGRATION_PRE_TO_POSTPAID", "Migration prepaid to postpaid", "0", "Contrat n'est pas actif"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("MIGRATION_PRE_TO_POSTPAID", "Migration prepaid to postpaid", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("MIGRATION_PRE_TO_POSTPAID", "Migration prepaid to postpaid", "0", "Le client n'est pas identifié"));
                    } else if (!tempp.get("RECENTCHANGE").toString().equals("0")) {
                        list.add(new Operation("MIGRATION_PRE_TO_POSTPAID", "Migration prepaid to postpaid", "0", "Contrat a déjà éfféctué un changement aujourd'hui"));
                    } else if (tempp.get("PREPOST").toString().equals("0")) {
                        list.add(new Operation("MIGRATION_PRE_TO_POSTPAID", "Migration prepaid to postpaid", "0", "Contrat n'a pas de plan tarifaire prépayé"));
                    }


                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("a") && tempp.get("PENDING").toString().equals("0")) &&
                            tempp.get("RECENTCHANGE").toString().equals("0") && tempp.get("ISIDENTIFIED").toString().equals("1")
                            && !tempp.get("TMCHANGE").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_PT", "Changement de PT", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("a")) {
                        list.add(new Operation("CHANGEMENT_PT", "Changement de PT", "0", "Contrat n'est pas actif"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_PT", "Changement de PT", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("CHANGEMENT_PT", "Changement de PT", "0", "Le client n'est pas identifié"));
                    } else if (!tempp.get("RECENTCHANGE").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_PT", "Changement de PT", "0", "Contrat a déjà éfféctué un changement aujourd'hui"));
                    } else if (tempp.get("TMCHANGE").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_PT", "Changement de PT", "0", "Aucune migration n'est éligible pour ce contrat"));
                    }


                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("a") && tempp.get("PENDING").toString().equals("0")) &&
                            tempp.get("RECENTCHANGE").toString().equals("0") && tempp.get("ISIDENTIFIED").toString().equals("1")
                            && !tempp.get("TMCHANGE").toString().equals("0") && !tempp.get("FORFAIT").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("a")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "0", "Contrat n'est pas actif"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "0", "Le client n'est pas identifié"));
                    } else if (!tempp.get("RECENTCHANGE").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "0", "Contrat a déjà éfféctué un changement aujourd'hui"));
                    } else if (tempp.get("TMCHANGE").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "0", "Aucune migration n'est éligible pour ce contrat"));
                    } else if (tempp.get("FORFAIT").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_FORFAIT", "Changement de forfait", "0", "Le plan tarifaire de ce contrat n'a pas de forfaits"));
                    }


                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("a") && tempp.get("PENDING").toString().equals("0"))
                            && tempp.get("ISIDENTIFIED").toString().equals("1")
                            && !tempp.get("ENGAGE").toString().equals("0")
                            && !tempp.get("REENG").toString().equals("0")) {
                        list.add(new Operation("REENGAGEMENT", "Reengagement", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("a")) {
                        list.add(new Operation("REENGAGEMENT", "Reengagement", "0", "Contrat n'est pas actif"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("REENGAGEMENT", "Reengagement", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("REENGAGEMENT", "Reengagement", "0", "Le client n'est pas identifié"));
                    } else if (tempp.get("ENGAGE").toString().equals("0")) {
                        list.add(new Operation("REENGAGEMENT", "Reengagement", "0", "Le plan tarifaire de ce contrat n'est pas éligible à un engagement"));
                    } else if (tempp.get("REENG").toString().equals("0")) {
                        list.add(new Operation("REENGAGEMENT", "Reengagement", "0", "Le contrat n'a jamais eu d'engagement"));
                    }


                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("d") && tempp.get("PENDING").toString().equals("0"))
                            && tempp.get("ISIDENTIFIED").toString().equals("1") && tempp.get("REMISE").toString().equals("0")
                            && !tempp.get("REMISE_ELIG").toString().equals("0")) {
                        list.add(new Operation("REMISE_EN_SERVICE", "Remise en service", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("d")) {
                        list.add(new Operation("REMISE_EN_SERVICE", "Remise en service", "0", "Contrat n'est pas désactivé"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("REMISE_EN_SERVICE", "Remise en service", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("REMISE_EN_SERVICE", "Remise en service", "0", "Le client n'est pas identifié"));
                    } else if (!tempp.get("REMISE").toString().equals("0")) {
                        list.add(new Operation("REMISE_EN_SERVICE", "Remise en service", "0", "Contrat déjà fait remise en service"));
                    } else if (tempp.get("REMISE_ELIG").toString().equals("0")) {
                        list.add(new Operation("REMISE_EN_SERVICE", "Remise en service", "0", "Le plan tarifaire de ce contrat n'est pas éligible à une remise en service"));
                    }


                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("a") && tempp.get("PENDING").toString().equals("0"))
                            && tempp.get("ISIDENTIFIED").toString().equals("1")
                    ) {
                        list.add(new Operation("RESILIATION", "Resiliation", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("a")) {
                        list.add(new Operation("RESILIATION", "Resiliation", "0", "Contrat n'est pas actif"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("RESILIATION", "Resiliation", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("RESILIATION", "Resiliation", "0", "Le client n'est pas identifié"));
                    }


                    if ((tempp.get("CO_STATUS") != null && tempp.get("CO_STATUS").toString().equals("a") && tempp.get("PENDING").toString().equals("0")) &&
                            tempp.get("ISIDENTIFIED").toString().equals("1")
                    ) {
                        list.add(new Operation("CHANGEMENT_CARTE_SIM", "Changement de carte SIM", "1", ""));
                    } else if (tempp.get("CO_STATUS") == null || !tempp.get("CO_STATUS").toString().equals("a")) {
                        list.add(new Operation("CHANGEMENT_CARTE_SIM", "Changement de carte SIM", "0", "Contrat n'est pas actif"));
                    } else if (!tempp.get("PENDING").toString().equals("0")) {
                        list.add(new Operation("CHANGEMENT_CARTE_SIM", "Changement de carte SIM", "0", "Contrat a demande en attente"));
                    } else if (!tempp.get("ISIDENTIFIED").toString().equals("1")) {
                        list.add(new Operation("CHANGEMENT_CARTE_SIM", "Changement de carte SIM", "0", "Le client n'est pas identifié"));
                    }


                }


            }

            LOG.debug("<-- verifierEligibilite");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("verifierEligibilite is  null ");
            return null;
        }


        return list;

    }

    public DureeEngagementResponse listeDureeEngagement(String coId, String tmcode,String sncode, String gammeForfait, String identifiantForfait) throws FunctionnalException {

        LOG.debug("--> listeDureeEngagement");
        final List<ParameterValues> list;
        try {
            Map<String, String> namedParameters = new HashMap();
            String sql = null;

            //[coId and all null]  or [tmcode and all null] or (tmcode and sncode and gammeForfait and identifiantForfait)
            if (!GRCStringUtil.isNullOrEmpty(coId) && GRCStringUtil.isNullOrEmpty(tmcode)
                    && GRCStringUtil.isNullOrEmpty(sncode) && GRCStringUtil.isNullOrEmpty(gammeForfait)
                    && GRCStringUtil.isNullOrEmpty(identifiantForfait)) {
                sql = CustomSQLUtil.get(LISTE_ENGAGEMENT);
                namedParameters.put("coId", coId);
                namedParameters.put("tmcode", tmcode);
            } else if (!GRCStringUtil.isNullOrEmpty(tmcode) && GRCStringUtil.isNullOrEmpty(sncode)
                    && GRCStringUtil.isNullOrEmpty(gammeForfait) && GRCStringUtil.isNullOrEmpty(identifiantForfait)){
                sql = CustomSQLUtil.get(LISTE_ENGAGEMENT_FORFAIT_TMCODE_ONLY);
                namedParameters.put("tmcode", tmcode);
            }else if (!GRCStringUtil.isNullOrEmpty(tmcode) && !GRCStringUtil.isNullOrEmpty(sncode)
                    && !GRCStringUtil.isNullOrEmpty(gammeForfait) && !GRCStringUtil.isNullOrEmpty(identifiantForfait)) {
                sql = CustomSQLUtil.get(LISTE_ENGAGEMENT_FORFAIT);
                namedParameters.put("tmcode", tmcode);
                namedParameters.put("sncode", sncode);
                namedParameters.put("gammeForfait", gammeForfait);
                namedParameters.put("identifiantForfait", identifiantForfait);
            }
            if (sql ==null)
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS," doit être [coId and all null]  or [tmcode and all null] or (tmcode and sncode and gammeForfait and identifiantForfait)");
            final CustomBeanPropertyRowMapper<ParameterValues> custRowMapper = new CustomBeanPropertyRowMapper<ParameterValues>(
                    ParameterValues.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);

            LOG.debug("<-- listeDureeEngagement");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listeDureeEngagement is  null ");
            return null;
        }
        if(list!= null && !list.isEmpty()){
            DureeEngagementResponse dureeEngagementResponse = new DureeEngagementResponse();
            dureeEngagementResponse.setParameterId(new Long(2));
            dureeEngagementResponse.setParameterValuesList(list);
            return dureeEngagementResponse;
        }else
            return null;

    }


    public List<ParameterValues> listePack(String coId, String tmcode) throws FunctionnalException {

        LOG.debug("--> listePack");
        final List<ParameterValues> list;
        try {
            String res = null;
            String res2 = null;
            Map<String, String> namedParameters = new HashMap();

            if (!GRCStringUtil.isNullOrEmpty(coId)) {
                String sql = CustomSQLUtil.get(VERIFY_PACK_TEST + "_P1");
                namedParameters.put("coId", coId);
                List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
                res = listt.get(0).get("RES").toString();
            }
            if (!GRCStringUtil.isNullOrEmpty(tmcode)) {
                namedParameters.clear();
                String sql = CustomSQLUtil.get(VERIFY_PACK_TEST + "_P2");
                namedParameters.put("tmcode", tmcode);
                List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);
                res2 = listt.get(0).get("RES2").toString();
            }

            String sql2 = CustomSQLUtil.get(VERIFY_PACK_2);

            if ("1".equals(res)) {
                sql2 = CustomSQLUtil.get(VERIFY_PACK_1);
            }

            final CustomBeanPropertyRowMapper<ParameterValues> custRowMapper = new CustomBeanPropertyRowMapper<ParameterValues>(
                    ParameterValues.class);
            namedParameters.clear();
            list = super.getNamedParameterJdbcTemplate().query(sql2, namedParameters, custRowMapper);


            if ("1".equals(res2)) {
                ParameterValues temp = new ParameterValues("751", "Sans Terminal", "SansTerminal");
                list.add(temp);
            }


            LOG.debug("<-- listePack");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listePack is  null ");
            return null;
        }


        return list;

    }


    public List<SimValidate> valideSIM(String sim, List<String> bo) throws FunctionnalException {

        LOG.debug("--> valideSIM");
        final List<SimValidate> list;
        try {

            String sql = CustomSQLUtil.get(SIM_VALIDATION);
            final List<String> tags = new ArrayList<String>(0);

            String bos = ",";

            if (bo != null && bo.size() > 0) {
                for (int i = 0; i < bo.size(); i++) {
                    bos = bos.concat(bo.get(i)).concat(",");
                }
            }


            if (!bos.equals(",")) {
                tags.add("AGENCE");
            }
            String appendSql = "";

            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }


            Map<String, String> namedParameters = new HashMap();


            if (!StringUtil.isEmpty(sim)) {
                namedParameters.put("sim", sim);
            }
            if (!bos.equals(",")) {
                namedParameters.put("bo", bos);
            }


            final CustomBeanPropertyRowMapper<SimValidate> custRowMapper = new CustomBeanPropertyRowMapper<SimValidate>(
                    SimValidate.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- valideSIM");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("valideSIM is  null ");
            return null;
        }


        return list;

    }

    public ContractPersist getContractByDnNum(String nd) {

        LOG.debug("--> getContractByDnNum nd {}", nd);
        final List<ContractPersist> list;
        try {

            final String sql = CustomSQLUtil.get(GET_CONTRACT_BY_DN_NUM);

            final SqlParameterSource namedParameters = new MapSqlParameterSource("nd", nd);

            final CustomBeanPropertyRowMapper<ContractPersist> custRowMapper = new CustomBeanPropertyRowMapper<ContractPersist>(
                    ContractPersist.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getContractByDnNum");
        } catch (EmptyResultDataAccessException e) {

            LOG.error("getContractByDnNum is  null for nd = " + nd);
            return null;
        }
        return (list != null && !list.isEmpty()) ? list.get(0) : null;

    }

    public boolean verifierExistanceRC(String rc) {

        LOG.debug("--> verifierExistanceRC");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(VERIFY_RC_EXISTENCE);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("rc", rc);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.error("verifierExistanceRC is  null for rc = " + rc);
            return false;
        }

        return result != null ? result > 0 : false;
    }

    public boolean isServiceInternetActif(String coId) {

        LOG.debug("--> isServiceInternetActif");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_SERVICE_INTERNET_BY_DN_NUM);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("coId", coId);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.error("isServiceInternetActif is  null for co_Id = " + coId);
            return false;
        }

        return result != null ? result > 0 : false;
    }

    public boolean isCustomerExist(String custCode) {

        LOG.debug("--> isCustomerExist");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_CLIENT_BY_CUSTCODE);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("custCode", custCode);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("isCustomerExist is null for custCode = " + custCode);
            return false;
        }

        return result != null ? result > 0 : false;
    }


    private Double getFraisStand(String pack_moi) {
        Double result = new Double(0);
        int eng = Integer.parseInt(pack_moi.substring(0, 2));
        int months = Integer.parseInt(pack_moi.substring(3));
        if (months < eng) {
            if (eng == 24) {
                if ((24 - months) > 12)
                    return new Double(((24 - months) - 12) * 125 + 500);
                else
                    return new Double(500);
            } else {
                return new Double((12 - months) * 125);
            }
        }
        return new Double(0);
    }


    public ConfigAll getConfigAll() {

        LOG.debug("--> getConfigAll  ");
            ConfigAll res = new ConfigAll();
            try {

                Cities ss = new Cities();
                ss.setCity(getAllCities());
                res.setCities(ss);

                Countries ss2 = new Countries();
                ss2.setCountry(getAllCountries());
                res.setCountries(ss2);

                DnTypes ss3 = new DnTypes();
                ss3.setDnType(getAllDnType());
                res.setDnTypes(ss3);

                IdTypes ss4 = new IdTypes();
                ss4.setIdentifierType(getAllIdentifierType());
                res.setIdTypes(ss4);

                Languages ss5 = new Languages();
                ss5.setLanguage(getAllLanguages());
                res.setLanguages(ss5);

                PaymentTypes ss6 = new PaymentTypes();
                ss6.setPaymentType(getAllPaymentType());
                res.setPaymentType(ss6);

                Raisons ss7 = new Raisons();
                ss7.setRaison(listeRaisonsModification());
                res.setReasons(ss7);

                Segments ss8 = new Segments();
                ss8.setSegment(getAllSegments());
                res.setSegments(ss8);

                Titles ss9 = new Titles();
                ss9.setTitle(getAllTitles());
                res.setTitles(ss9);
                LOG.debug("<-- getConfigAll");
            } catch (EmptyResultDataAccessException e) {

                LOG.debug("getConfigAll is  null");
                return null;
            }
        return res;

    }

    public Sim getSIMActuel(String nd) {

        LOG.debug("--> getSIMActuel nd  ", nd);
        final List<Sim> list;
        try {

            final String sql = CustomSQLUtil.get(GET_CURR_SIM);

            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("nd", nd);

            final CustomBeanPropertyRowMapper<Sim> custRowMapper = new CustomBeanPropertyRowMapper<Sim>(
                    Sim.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getSIMActuel");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getSIMActuel is  null for nd = " + nd);
            return null;
        }
        return list.size() == 0 ? null : list.get(0);

    }


    public Eng engagementEnCours(String nd) {

        LOG.debug("--> engagementEnCours nd  ", nd);
        String result = null;
        try {

            final String sql2 = CustomSQLUtil.get(GET_CURR_ENG_MOI);

            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("nd", nd);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql2, namedParameters, String.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("engagementEnCours is  null for nd = " + nd);
            return null;
        }

        if (result != null && !result.equals("0")) {

            Double toPay = getFraisStand(result);


            final List<Eng> list;
            try {

                final String sql = CustomSQLUtil.get(GET_CURR_ENG);

                Map<String, String> namedParameters = new HashMap();

                namedParameters.put("nd", nd);

                final CustomBeanPropertyRowMapper<Eng> custRowMapper = new CustomBeanPropertyRowMapper<Eng>(
                        Eng.class);
                list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
                LOG.debug("<-- engagementEnCours");
            } catch (EmptyResultDataAccessException e) {

                LOG.debug("engagementEnCours is  null for nd = " + nd);
                return null;
            }
            if (list.size() > 0) {
                list.get(0).setPenaliteAPayer(toPay.toString());
                return list.get(0);
            } else
                return null;
        } else
            return null;


    }


    public boolean isCustomerND(CustomerRequest cust) {

        LOG.debug("--> isCustomerND");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_CLIENT_ND);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("dnNum", cust.getMSISDN());
            namedParameters.put("custCode", cust.getCUSTCODE());

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("isCustomerND is null for custCode = " + cust.getCUSTCODE());
            return false;
        }

        return result != null ? result > 0 : false;
    }


    public boolean isCustomerSim(CustomerRequest cust) {

        LOG.debug("--> isCustomerSim");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_CLIENT_SM);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("smNum", cust.getSM_SERIALNUM());
            namedParameters.put("custCode", cust.getCUSTCODE());

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("isCustomerSim is null for custCode = " + cust.getCUSTCODE());
            return false;
        }

        return result != null ? result > 0 : false;
    }


    public boolean isDnType(CustomerRequest cust) {

        LOG.debug("--> isDnType");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_DN_TYPE);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("dnType", cust.getID_TYPE());

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("isDnType is null for custCode = " + cust.getCUSTCODE());
            return false;
        }

        return result != null ? result > 0 : false;
    }

    public List<OutPlanTarifaire> listeCategoryPlanTarifaire(String prgcode) {

        LOG.debug("--> listePlanTarifaire category  ", prgcode);
        final List<OutPlanTarifaire> list = new ArrayList<OutPlanTarifaire>();
        try {

            final String sql2 = CustomSQLUtil.get(GET_CAT_TMCODES);

            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("prgcode", prgcode);


            List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql2, namedParameters);


            if (listt != null) {
                String rpcode = "0";
                OutPlanTarifaire rpSrv = null;


                for (int j = 0; j < listt.size(); j++) {
                    Map<String, Object> tempp = listt.get(j);
                    rpSrv = new OutPlanTarifaire();
                    rpcode = tempp.get("TMCODE").toString();
                    rpSrv.setTmcode(rpcode);
                    rpSrv.setDes(tempp.get("DES").toString());
                    rpSrv.setEligOperat(tempp.get("eligOperat").toString());
                    rpSrv.setVscode(tempp.get("vscode").toString());
                    list.add(rpSrv);
                }
            }

            LOG.debug("<-- listePlanTarifaire");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listePlanTarifaire is  null for prgcode = " + prgcode);
            return null;
        }
        return list;

    }

    public List<OutCategorie> listePlanTarifaire(String areaID) {

        LOG.debug("--> listePlanTarifaire segment  ", areaID);
        final List<OutCategorie> list;
        try {

            final String sql = CustomSQLUtil.get(GET_SEG_CATEGORY);

            final String sql2 = CustomSQLUtil.get(GET_CAT_TMCODES_SERVICES);

            final String sql3 = CustomSQLUtil.get(GET_CAT_SERVICES);

            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("areaID", areaID);

            CustomBeanPropertyRowMapper<OutCategorie> custRowMapper = new CustomBeanPropertyRowMapper<OutCategorie>(
                    OutCategorie.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);


            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    OutCategorie temp = list.get(i);
                    String cat = temp.getCategorieID();
                    List<OutPlanTarifaire> list2 = new ArrayList<OutPlanTarifaire>();
                    List<OutService> list3 = new ArrayList<OutService>();
                    List<OutParameter> listprm = new ArrayList<OutParameter>();

                    namedParameters = new HashMap();

                    namedParameters.put("prgcode", cat);


                    List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql2, namedParameters);


                    if (listt != null) {
                        String rpcode = "0";
                        OutPlanTarifaire rpSrv = null;

                        String sncode = "0";
                        OutService snPrm = null;
                        OutParameter prm = null;


                        for (int j = 0; j < listt.size(); j++) {
                            Map<String, Object> tempp = listt.get(j);
                            if (!tempp.get("TMCODE").toString().equals(rpcode)) {

                                if (rpSrv != null) {
                                    rpSrv.setServices(list3);
                                    list2.add(rpSrv);
                                }
                                rpSrv = new OutPlanTarifaire();
                                list3 = new ArrayList<OutService>();
                                rpcode = tempp.get("TMCODE").toString();
                                rpSrv.setTmcode(rpcode);
                                rpSrv.setDes(tempp.get("DES").toString());

                            }

                            if (!tempp.get("SNCODE").toString().equals(sncode)) {
                                if (snPrm != null) {
                                    snPrm.setSncode(sncode);
                                    snPrm.setService_package_code(listt.get(j - 1).get("SPCODE").toString());
                                    snPrm.setCore((String) listt.get(j - 1).get("CSIND"));
                                    snPrm.setParameters(listprm);
                                    list3.add(snPrm);
                                }
                                snPrm = new OutService();
                                listprm = new ArrayList<OutParameter>();
                                sncode = tempp.get("SNCODE").toString();
                            }
                            prm = new OutParameter();
                            if (tempp.get("PRMID") != null) {
                                prm.setId(tempp.get("PRMID").toString());
                                prm.setNo(tempp.get("PRMNO").toString());
                                listprm.add(prm);
                            }

                        }

                        if (snPrm != null) {
                            snPrm.setSncode(sncode);
                            snPrm.setService_package_code(listt.get(listt.size() - 1).get("SPCODE").toString());
                            snPrm.setCore((String) listt.get(listt.size() - 1).get("CSIND"));
                            snPrm.setParameters(listprm);
                            list3.add(snPrm);
                        }

                        rpSrv.setServices(list3);
                        list2.add(rpSrv);
                    }

                    list.get(i).setListePlanTarifaire(list2);

                }
            }


            LOG.debug("<-- listePlanTarifaire");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listePlanTarifaire is  null for segment = " + areaID);
            return null;
        }


        return list;

    }

    public List<Forfait> listeForfaits(String tmcode,String prgcode)  throws FunctionnalException  {

        LOG.debug("--> listeForfaits tmcode  ", tmcode);
        final List<Forfait> list;
        try {

            String sql = CustomSQLUtil.get(GET_TM_FORFAITS);

            final List<String> tags = new ArrayList<String>(0);

            if (!GRCStringUtil.isNullOrEmpty(prgcode)) {
                tags.add("PRGCODE");
            }
            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }
            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("tmcode", tmcode);
            namedParameters.put("prgcode", prgcode);

            final CustomBeanPropertyRowMapper<Forfait> custRowMapper = new CustomBeanPropertyRowMapper<Forfait>(
                    Forfait.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- listeForfaits");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listeForfaits is  null for tmcode = " + tmcode);
            return null;
        }
        return list;

    }

    public List<Invoice> getUnpaidInvoices(String custcode, String ohstatus) {

        LOG.debug("--> getUnpaidInvoices custCode  ", custcode);
        final List<Invoice> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_UNPAID_INVOICES);

            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("custCode", custcode);
            namedParameters.put("ohStatus", ohstatus);

            final CustomBeanPropertyRowMapper<Invoice> custRowMapper = new CustomBeanPropertyRowMapper<Invoice>(
                    Invoice.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getUnpaidInvoices");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getUnpaidInvoices is  null for custCode = " + custcode);
            return null;
        }
        return list;

    }

    public boolean verifierCodeGenere(VerifierCodeGenereRequest request) {

        LOG.debug("--> verifierCodeGenere");
        Integer result = null;
        try {

            final String sql = CustomSQLUtil.get(VERIFY_SMS_CODE);
            String coId = getCoIDFromDnNum(request.getNd()).toString();


            Map<String, String> namedParameters = new HashMap();//'co_id=79072061,code=45908'
            namedParameters.put("verify", "co_id=" + coId + ",code=" + request.getCode());
            namedParameters.put("coID", coId);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("verifierCodeGenere is null for nd = " + request.getNd());
            return false;
        }

        return result != null ? result > 0 : false;
    }


    public boolean isBlackList(String custCode,String cin) throws FunctionnalException {

        LOG.debug("--> isBlackList");
        Integer result = null;
        try {
            final List<String> tags = new ArrayList<String>(0);
            String sql = CustomSQLUtil.get(GET_CLIENT_BLACKLIST_BY_CUSTCODE);
            Map<String, String> namedParameters = new HashMap();



            if (!GRCStringUtil.isNullOrEmpty(custCode)) {
                tags.add("CUSTCODE");
                namedParameters.put("custCode", custCode);
            } if (!GRCStringUtil.isNullOrEmpty(cin)) {
                tags.add("CIN");
                namedParameters.put("cin", cin);
            }

            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }




            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Integer.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("isBlackList is null for custCode = " + custCode);
            return false;
        }

        return result != null ? result > 0 : false;
    }

    public List<Raison> getRaisonmodificationSIM() {

        LOG.debug("--> getRaisonmodificationSIM ");
        final List<Raison> list;
        try {

            final String sql = CustomSQLUtil.get(SIM_LIST_RAISONS_MODIF);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Raison> custRowMapper = new CustomBeanPropertyRowMapper<Raison>(
                    Raison.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getRaisonmodificationSIM");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getRaisonmodificationSIM is  null");
            return null;
        }
        return list;

    }

    public List<Raison> listeRaisonsModification() {

        LOG.debug("--> listeRaisonsModification ");
        final List<Raison> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_RAISONS_MODIF);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Raison> custRowMapper = new CustomBeanPropertyRowMapper<Raison>(
                    Raison.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- listeRaisonsModification");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("listeRaisonsModification is  null");
            return null;
        }
        return list;

    }


    public List<Raison> statutRaisonClient() {

        LOG.debug("--> statutRaisonClient ");
        final List<Raison> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_RAISON_CLIENT);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Raison> custRowMapper = new CustomBeanPropertyRowMapper<Raison>(
                    Raison.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- statutRaisonClient");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("statutRaisonClient is  null");
            return null;
        }
        return list;

    }

    public List<IdentifierType> getAllIdentifierType() {

        LOG.debug("--> getAllIdentifierType ");
        final List<IdentifierType> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_IDENTIFIER_TYPE);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<IdentifierType> custRowMapper = new CustomBeanPropertyRowMapper<IdentifierType>(
                    IdentifierType.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllIdentifierType");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllIdentifierType is  null");
            return null;
        }
        return list;

    }

    private String addMinutesToDate(String min) {

        int minutes = Integer.parseInt(min);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs
        Date beforeTime = new Date();
        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return df.format(afterAddingMins);
    }


    public String getHlcodeFromSim(String hlcode) {

        LOG.debug("--> isCustomerExist");
        String result = null;
        try {

            final String sql = CustomSQLUtil.get(GET_HLCODE_BY_SIM);

            Map<String, String> namedParameters = new HashMap();
            namedParameters.put("smnum", hlcode);

            result = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, String.class);
        } catch (EmptyResultDataAccessException e) {

            LOG.info("isCustomerExist is null for custCode = " + hlcode);
            return null;
        }

        return result;
    }

    public List<DirectoryNumber> rechercheNumeroAppel(DirectoryNumber dnSearch) throws FunctionnalException {
/**
 * // TODO: MBO: 18/11/2018
 *
 */
        LOG.debug("--> rechercheNumeroAppel");
        final List<DirectoryNumber> list;
        try {
            String date = addMinutesToDate(dnSearch.getBOOKING_TIME());
            String sql = CustomSQLUtil.get(UPDATE_LIST_DIRECTORY_NUMBER);

            String hlcode = null;


            if (!StringUtil.isEmpty(dnSearch.getSM_SERIALNUM())) {
                hlcode = getHlcodeFromSim(dnSearch.getSM_SERIALNUM());
            } else {
                hlcode = dnSearch.getHLCODE();
            }


            List<String> tags = getTagsFromSearchBean(dnSearch, hlcode);
            List<DirectoryNumber> listCustAcct;
            String appendSql = "";

            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }

            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("dnDate", date);
            if (!StringUtil.isEmpty(hlcode)) {
                namedParameters.put("hlcode", hlcode);
            }
            if (!StringUtil.isEmpty(dnSearch.getDN_STATUS())) {
                namedParameters.put("status", dnSearch.getDN_STATUS());
            }
            if (!StringUtil.isEmpty(dnSearch.getDN_NUM())) {
                namedParameters.put("nd", dnSearch.getDN_NUM());
            }
            if (!StringUtil.isEmpty(dnSearch.getDN_TYPE())) {
                namedParameters.put("type", dnSearch.getDN_TYPE());
            }


            int u = super.getNamedParameterJdbcTemplate().update(sql, namedParameters);


            sql = CustomSQLUtil.get(LIST_DIRECTORY_NUMBER);
            tags = getTagsFromSearchBean(dnSearch, hlcode);
            appendSql = "";

            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }


            final CustomBeanPropertyRowMapper<DirectoryNumber> custRowMapper = new CustomBeanPropertyRowMapper<DirectoryNumber>(
                    DirectoryNumber.class);
            namedParameters.put("status","r");
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- rechercheNumeroAppel");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("rechercheNumeroAppel is  null");
            return null;
        }

        return list;

    }

    public DirectoryNumber prolongerNumeroAppel(String nd, String minutesToExtend) throws FunctionnalException {

        LOG.debug("--> prolongerNumeroAppel");
        String date = addMinutesToDate(minutesToExtend);

        Map<String, String> namedParameters = new HashMap();
        namedParameters.put("dnDate", date);
        namedParameters.put("nd", nd);

        String sql = CustomSQLUtil.get(UPDATE_RESERVED_DIRECTORY_NUMBER);
        int rowsUpdatedCount = super.getNamedParameterJdbcTemplate().update(sql, namedParameters);

        sql = CustomSQLUtil.get(SELECT_RESERVED_DIRECTORY_NUMBER);
        final CustomBeanPropertyRowMapper<DirectoryNumber> custRowMapper = new CustomBeanPropertyRowMapper<DirectoryNumber>(
                DirectoryNumber.class);
        List<DirectoryNumber> list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
        LOG.debug("<-- prolongerNumeroAppel");

        if (list == null || list.isEmpty())
            throw new FunctionnalException(ExceptionCodeTypeEnum.EMPTY_RESULT);
        else
            return list.get(0);

    }

    public List<Language> getAllLanguages() {

        LOG.debug("--> getAllIdentifierType ");
        final List<Language> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_LANGUAGE);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Language> custRowMapper = new CustomBeanPropertyRowMapper<Language>(
                    Language.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllLanguages");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllLanguages is  null");
            return null;
        }
        return list;

    }

    public List<City> getAllCities() {

        LOG.debug("--> getAllCities ");
        final List<City> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_CITY);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<City> custRowMapper = new CustomBeanPropertyRowMapper<City>(
                    City.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllCities");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllCities is  null");
            return null;
        }
        return list;

    }

    public List<Country> getAllCountries() {

        LOG.debug("--> getAllCountries ");
        final List<Country> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_COUNTRY);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Country> custRowMapper = new CustomBeanPropertyRowMapper<Country>(
                    Country.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllCountries");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllCountries is  null");
            return null;
        }
        return list;

    }

    public List<Title> getAllTitles() {

        LOG.debug("--> getAllTitles ");
        final List<Title> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_TITLE);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Title> custRowMapper = new CustomBeanPropertyRowMapper<Title>(
                    Title.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllTitles");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllTitles is  null");
            return null;
        }
        return list;

    }

    public List<Segment> getAllSegments() {

        LOG.debug("--> getAllSegments ");
        final List<Segment> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_SEGMENT);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<Segment> custRowMapper = new CustomBeanPropertyRowMapper<Segment>(
                    Segment.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllSegments");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllSegments is  null");
            return null;
        }
        return list;

    }

    public List<DnType> getAllDnType() {

        LOG.debug("--> getAllDnType ");
        final List<DnType> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_DN_TYPE);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<DnType> custRowMapper = new CustomBeanPropertyRowMapper<DnType>(
                    DnType.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllDnType");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllDnType is  null");
            return null;
        }
        return list;

    }

    public List<PaymentType> getAllPaymentType() {

        LOG.debug("--> getAllPaymentType ");
        final List<PaymentType> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_PAYMENT);

            final SqlParameterSource namedParameters = null;

            final CustomBeanPropertyRowMapper<PaymentType> custRowMapper = new CustomBeanPropertyRowMapper<PaymentType>(
                    PaymentType.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllPaymentType");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllPaymentType is  null");
            return null;
        }
        return list;

    }

    public List<Category> getListCategoryBySegment(String segmentCode) {

        LOG.debug("--> getListCategoryBySegment segmentCode  ", segmentCode);
        final List<Category> list;
        try {

            final String sql = CustomSQLUtil.get(LIST_SEG_CATEGORY);

            final SqlParameterSource namedParameters = new MapSqlParameterSource("segmentCode", segmentCode);

            final CustomBeanPropertyRowMapper<Category> custRowMapper = new CustomBeanPropertyRowMapper<Category>(
                    Category.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getListCategoryBySegment");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getListCategoryBySegment is  null for segmentCode = " + segmentCode);
            return null;
        }
        return list;

    }

    protected List<String> getTagsFromSearchBean(ContractRequest cust) {

        final List<String> tags = new ArrayList<String>(0);

        if (!GRCStringUtil.isNullOrEmpty(cust.getMSISDN())) {
            tags.add("ND");
        }
        if (!GRCStringUtil
                .isNullOrEmpty(cust.getSM_SERIALNUM())) {
            tags.add("SIM");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getCUSTCODE())) {
            tags.add("CODE");
        }
        return tags;
    }


    protected List<String> getTagsFromSearchBean2(CustomerRequest cust) {

        final List<String> tags = new ArrayList<String>(0);
        if (!GRCStringUtil.isNullOrEmpty(cust.getCUSTCODE())) {
            tags.add("CODE");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getCCU())) {
            tags.add("CCU");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getPASSPORTNO())) {
            tags.add("PASS");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getID_TYPE())) {
            tags.add("TYPE");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getLASTNAME())) {
            tags.add("NOM");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getFIRST_NAME())) {
            tags.add("PRENOM");
        }
        return tags;
    }


    protected List<String> getTagsFromSearchBean(CustomerRequest cust) {

        final List<String> tags = new ArrayList<String>(0);

        if (!GRCStringUtil.isNullOrEmpty(cust.getMSISDN())) {
            tags.add("ND");
        }
        if (!GRCStringUtil
                .isNullOrEmpty(cust.getSM_SERIALNUM())) {
            tags.add("SIM");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getCUSTCODE())) {
            tags.add("CODE");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getCCU())) {
            tags.add("CCU");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getPASSPORTNO())) {
            tags.add("PASS");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getID_TYPE())) {
            tags.add("TYPE");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getLASTNAME())) {
            tags.add("NOM");
        }
        if (!GRCStringUtil.isNullOrEmpty(cust.getFIRST_NAME())) {
            tags.add("PRENOM");
        }
        return tags;
    }


    protected List<String> getTagsFromSearchBean(DirectoryNumber dn, String hlcode) {

        final List<String> tags = new ArrayList<String>(0);

        if (hlcode != null) {
            tags.add("HLCODE");
        }
        if (!GRCStringUtil
                .isNullOrEmpty(dn.getDN_NUM())) {
            tags.add("DN_NUM");
        }
        if (!GRCStringUtil.isNullOrEmpty(dn.getDN_TYPE())) {
            tags.add("DN_TYPE");
        }
        if (!GRCStringUtil.isNullOrEmpty(dn.getDN_STATUS())) {
            tags.add("DN_STATUS");
        }
        return tags;
    }

    public List<Contract> getAllContracts(ContractRequest cust) throws FunctionnalException {

        LOG.debug("--> getAllContracts custCode  ", cust.getCUSTCODE());
        final List<Contract> list;
        try {

            String sql = CustomSQLUtil.get(GET_CONTRACTS_BY_CUSTCODE);
            final List<String> tags = getTagsFromSearchBean(cust);
            final List<Contract> listCustAcct;
            String appendSql = "";

            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }


            Map<String, String> namedParameters = new HashMap();

            namedParameters.put("custCode", cust.getCUSTCODE());
            if (!StringUtil.isEmpty(cust.getSM_SERIALNUM())) {
                namedParameters.put("sim", cust.getSM_SERIALNUM());
            }
            if (!StringUtil.isEmpty(cust.getMSISDN())) {
                namedParameters.put("dnNUm", cust.getMSISDN());
            }


            final CustomBeanPropertyRowMapper<Contract> custRowMapper = new CustomBeanPropertyRowMapper<Contract>(
                    Contract.class);
            list = super.getNamedParameterJdbcTemplate().query(sql, namedParameters, custRowMapper);
            LOG.debug("<-- getAllContracts");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getAllContracts is  null for custCode = " + cust.getCUSTCODE());
            return null;
        }


        return list;

    }


    public List<CustomerResponse> getCustomerInfo(CustomerRequest cust) throws FunctionnalException {

        LOG.debug("--> getCustomerInfo custCode  ", cust.getCUSTCODE());
        List<CustomerResponse> list = new ArrayList<CustomerResponse>();
        try {

            String sql = CustomSQLUtil.get(GET_INFO_CLIENT_BY_CUSTCODE);
            List<String> tags = getTagsFromSearchBean(cust);

            try {
                sql = GRCStringUtil.formatQuery(sql, tags);
            } catch (final Exception e) {
                throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
            }


            Map<String, String> namedParameters = new HashMap();


            if (!StringUtil.isEmpty(cust.getSM_SERIALNUM())) {
                namedParameters.put("sim", cust.getSM_SERIALNUM());
            }
            if (!StringUtil.isEmpty(cust.getMSISDN())) {
                namedParameters.put("dnNUm", cust.getMSISDN());
            }
            if (!StringUtil.isEmpty(cust.getCCU())) {
                namedParameters.put("ccu", cust.getCCU());
            }
            if (!StringUtil.isEmpty(cust.getCUSTCODE())) {
                namedParameters.put("custCode", cust.getCUSTCODE());
            }
            if (!StringUtil.isEmpty(cust.getPASSPORTNO())) {
                namedParameters.put("passport", cust.getPASSPORTNO());
            }
            if (!StringUtil.isEmpty(cust.getID_TYPE())) {
                namedParameters.put("segmentCode", cust.getID_TYPE());
            }
            if (!StringUtil.isEmpty(cust.getFIRST_NAME())) {
                namedParameters.put("prenom", cust.getFIRST_NAME());
            }
            if (!StringUtil.isEmpty(cust.getLASTNAME())) {
                namedParameters.put("nom", cust.getLASTNAME());
            }


            List<Map<String, Object>> listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);

            CustomerResponse custRes = new CustomerResponse();
            if (listt != null && listt.size() > 0) {
                Map<String, Object> tempp = null;

                List<Address> addresses = new ArrayList<Address>();
                String custid = null;
                for (int j = 0; j < listt.size(); j++) {
                    tempp = listt.get(j);
                    if (custid != null) {
                        if (!tempp.get("CUSTOMER_ID").toString().equals(custid)) {
                            list.add(custRes);
                            custRes = new CustomerResponse();
                            custid = tempp.get("CUSTOMER_ID").toString();

                        }
                    } else {
                        custid = tempp.get("CUSTOMER_ID").toString();
                    }
                    custRes.setBillingAccountId(tempp.get("BILLING_ACCOUNT_ID") != null ? tempp.get("BILLING_ACCOUNT_ID").toString() : null);
                    custRes.setCcseq(tempp.get("CCSEQ") != null ? tempp.get("CCSEQ").toString() : null);
                    custRes.setCcsex(tempp.get("CCSEX") != null ? tempp.get("CCSEX").toString() : null);
                    Address add = new Address();
                    tempp = listt.get(j);
                    add.setTitre(tempp.get("CCTITLE") != null ? tempp.get("CCTITLE").toString() : null);
                    add.setEmail(tempp.get("EMAIL") != null ? tempp.get("EMAIL").toString() : null);
                    add.setNom(tempp.get("NOM") != null ? tempp.get("NOM").toString() : null);
                    add.setPrenom(tempp.get("PRENOM") != null ? tempp.get("PRENOM").toString() : null);


                    Country country = new Country();
                    country.setIso(tempp.get("ISO") != null ? tempp.get("ISO").toString() : null);
                    country.setName(tempp.get("PAYS") != null ? tempp.get("PAYS").toString() : null);
                    add.setCountry(country);


                    City city = new City();
                    city.setNom(tempp.get("VILLE") != null ? tempp.get("VILLE").toString() : null);
                    city.setZip(tempp.get("CCZIP") != null ? tempp.get("CCZIP").toString() : null);
                    add.setCity(city);

                    add.setRue(tempp.get("ADRESSE") != null ? tempp.get("ADRESSE").toString() : null);


                    add.setTypeIdentifiant(tempp.get("TYPEIDENTIFIANT") != null ? tempp.get("TYPEIDENTIFIANT").toString() : null);
                    add.setIdentifiant(tempp.get("IDENTIFIANT") != null ? tempp.get("IDENTIFIANT").toString() : null);
                    List<Role> roles = new ArrayList<Role>();
                    if (tempp.get("CCUSER") != null) {
                        Role role = new Role();
                        role.setRole("U");
                        roles.add(role);
                    }
                    if (tempp.get("CCBILL") != null) {
                        Role role = new Role();
                        role.setRole("B");
                        roles.add(role);

                        custRes.setDnStatus(tempp.get("DN_STATUS") != null ? tempp.get("DN_STATUS").toString() : null);

                        custRes.setIsIdentified((tempp.get("PRENOM") == null || tempp.get("PRENOM").toString().toLowerCase().startsWith("anonyme")
                                || tempp.get("NOM") == null || tempp.get("NOM").toString().toLowerCase().startsWith("anonyme")
                                || tempp.get("IDENTIFIANT") == null || tempp.get("IDENTIFIANT").equals("")) ? "0" : "1");

                        custRes.setDnStatus(tempp.get("DN_STATUS") != null ? tempp.get("DN_STATUS").toString() : null);


                        custRes.setIsPaymentResponsable(tempp.get("PAYMNTRESP") != null ? tempp.get("PAYMNTRESP").toString() : null);
                        custRes.setCustomerID(tempp.get("CUSTOMER_ID") != null ? tempp.get("CUSTOMER_ID").toString() : null);
                        custRes.setCcu(tempp.get("CCU") != null ? tempp.get("CCU").toString() : null);
                        custRes.setCustCode(tempp.get("CUSTCODE") != null ? tempp.get("CUSTCODE").toString() : null);

                        custRes.setIsBalckListed(tempp.get("BLACK") != null ? tempp.get("BLACK").toString() : null);

                        ContactLegal contactLegal = new ContactLegal();
                        contactLegal.setCinContactLegal(tempp.get("CINCONTACTLEGAL") != null ? tempp.get("CINCONTACTLEGAL").toString() : null);
                        contactLegal.setNomContactLegal(tempp.get("NOMCONTACTLEGAL") != null ? tempp.get("NOMCONTACTLEGAL").toString() : null);
                        contactLegal.setTellContactLegal(tempp.get("TELLCONTACTLEGAL") != null ? tempp.get("TELLCONTACTLEGAL").toString() : null);
                        custRes.setContactLegal(contactLegal);


                        Category category = new Category();
                        category.setLabel(tempp.get("CATEGORIECLIENT") != null ? tempp.get("CATEGORIECLIENT").toString() : null);
                        category.setId(tempp.get("PRGCODE") != null ? tempp.get("PRGCODE").toString() : null);
                        category.setCode(tempp.get("PRGCODE") != null ? tempp.get("PRGCODE").toString() : null);
                        category.setSegmentCode(tempp.get("segmentCode") != null ? tempp.get("segmentCode").toString() : null);
                        category.setSegmentLabel(tempp.get("segmentLabel") != null ? tempp.get("segmentLabel").toString() : null);
                        custRes.setCategory(category);


                        Agency agency = new Agency();
                        agency.setName(tempp.get("AGENCECLIENT") != null ? tempp.get("AGENCECLIENT").toString() : null);
                        agency.setId(tempp.get("COSTID") != null ? tempp.get("COSTID").toString() : null);
                        custRes.setAgency(agency);

                        custRes.setFlagNomPrenom(
                                (((custRes.getNom() == null && cust.getLASTNAME() == null) || (custRes.getNom() != null && custRes.getNom().toString().equals(cust.getLASTNAME())))
                                        && ((custRes.getPrenom() == null && cust.getFIRST_NAME() == null) || (custRes.getPrenom() != null && custRes.getPrenom().toString().equals(cust.getFIRST_NAME())))));


                    }
                    if (tempp.get("CCCONTRACT") != null) {
                        Role role = new Role();
                        role.setRole("C");
                        roles.add(role);
                    }
                    if (tempp.get("CCSHIP") != null) {
                        Role role = new Role();
                        role.setRole("S");
                        roles.add(role);
                    }
                    if (tempp.get("CCMAGAZINE") != null) {
                        Role role = new Role();
                        role.setRole("P");
                        roles.add(role);
                    }
                    if (tempp.get("CCDIRECTORY") != null) {
                        Role role = new Role();
                        role.setRole("D");
                        roles.add(role);
                    }
                    if (tempp.get("CCBILL_PREVIOUS") != null) {
                        Role role = new Role();
                        role.setRole("R");
                        roles.add(role);
                    }
                    if (tempp.get("CCBILLTEMP") != null) {
                        Role role = new Role();
                        role.setRole("E");
                        roles.add(role);
                    }

                    add.setRoles(roles);
                    addresses.add(add);
                }

                custRes.setAddresses(addresses);
                list.add(custRes);
            } else if (StringUtil.isEmpty(cust.getSM_SERIALNUM()) && StringUtil.isEmpty(cust.getMSISDN())) {
                list = new ArrayList<CustomerResponse>();
                sql = CustomSQLUtil.get(GET_INFO_CLIENT_BY_CUSTCODE_NO_ND);
                tags = getTagsFromSearchBean(cust);

                try {
                    sql = GRCStringUtil.formatQuery(sql, tags);
                } catch (final Exception e) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
                }


                namedParameters = new HashMap();


                if (!StringUtil.isEmpty(cust.getCCU())) {
                    namedParameters.put("ccu", cust.getCCU());
                }
                if (!StringUtil.isEmpty(cust.getCUSTCODE())) {
                    namedParameters.put("custCode", cust.getCUSTCODE());
                }
                if (!StringUtil.isEmpty(cust.getPASSPORTNO())) {
                    namedParameters.put("passport", cust.getPASSPORTNO());
                }
                if (!StringUtil.isEmpty(cust.getID_TYPE())) {
                    namedParameters.put("segmentCode", cust.getID_TYPE());
                }
                if (!StringUtil.isEmpty(cust.getFIRST_NAME())) {
                    namedParameters.put("prenom", cust.getFIRST_NAME());
                }
                if (!StringUtil.isEmpty(cust.getLASTNAME())) {
                    namedParameters.put("nom", cust.getLASTNAME());
                }


                listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);

                custRes = new CustomerResponse();
                if (listt != null && listt.size() > 0) {
                    Map<String, Object> tempp = null;

                    List<Address> addresses = new ArrayList<Address>();
                    String custid = null;
                    for (int j = 0; j < listt.size(); j++) {
                        tempp = listt.get(j);
                        if (custid != null) {
                            if (!tempp.get("CUSTOMER_ID").toString().equals(custid)) {
                                list.add(custRes);
                                custRes = new CustomerResponse();
                                custid = tempp.get("CUSTOMER_ID").toString();

                            }
                        } else {
                            custid = tempp.get("CUSTOMER_ID").toString();
                        }
                        custRes.setBillingAccountId(tempp.get("BILLING_ACCOUNT_ID") != null ? tempp.get("BILLING_ACCOUNT_ID").toString() : null);
                        custRes.setCcseq(tempp.get("CCSEQ") != null ? tempp.get("CCSEQ").toString() : null);
                        custRes.setCcsex(tempp.get("CCSEX") != null ? tempp.get("CCSEX").toString() : null);
                        Address add = new Address();
                        tempp = listt.get(j);
                        add.setTitre(tempp.get("CCTITLE") != null ? tempp.get("CCTITLE").toString() : null);
                        add.setEmail(tempp.get("EMAIL") != null ? tempp.get("EMAIL").toString() : null);
                        add.setNom(tempp.get("NOM") != null ? tempp.get("NOM").toString() : null);
                        add.setPrenom(tempp.get("PRENOM") != null ? tempp.get("PRENOM").toString() : null);


                        Country country = new Country();
                        country.setIso(tempp.get("ISO") != null ? tempp.get("ISO").toString() : null);
                        country.setName(tempp.get("PAYS") != null ? tempp.get("PAYS").toString() : null);
                        add.setCountry(country);


                        City city = new City();
                        city.setNom(tempp.get("VILLE") != null ? tempp.get("VILLE").toString() : null);
                        city.setZip(tempp.get("CCZIP") != null ? tempp.get("CCZIP").toString() : null);
                        add.setCity(city);

                        add.setRue(tempp.get("ADRESSE") != null ? tempp.get("ADRESSE").toString() : null);


                        add.setTypeIdentifiant(tempp.get("TYPEIDENTIFIANT") != null ? tempp.get("TYPEIDENTIFIANT").toString() : null);
                        add.setIdentifiant(tempp.get("IDENTIFIANT") != null ? tempp.get("IDENTIFIANT").toString() : null);
                        List<Role> roles = new ArrayList<Role>();
                        if (tempp.get("CCUSER") != null) {
                            Role role = new Role();
                            role.setRole("U");
                            roles.add(role);
                        }
                        if (tempp.get("CCBILL") != null) {
                            Role role = new Role();
                            role.setRole("B");
                            roles.add(role);

                            custRes.setDnStatus(tempp.get("DN_STATUS") != null ? tempp.get("DN_STATUS").toString() : null);

                            custRes.setIsIdentified((tempp.get("PRENOM") == null || tempp.get("PRENOM").toString().toLowerCase().startsWith("anonyme")
                                    || tempp.get("NOM") == null || tempp.get("NOM").toString().toLowerCase().startsWith("anonyme")
                                    || tempp.get("IDENTIFIANT") == null || tempp.get("IDENTIFIANT").equals("")) ? "0" : "1");

                            custRes.setDnStatus(tempp.get("DN_STATUS") != null ? tempp.get("DN_STATUS").toString() : null);


                            custRes.setIsPaymentResponsable(tempp.get("PAYMNTRESP") != null ? tempp.get("PAYMNTRESP").toString() : null);
                            custRes.setCustomerID(tempp.get("CUSTOMER_ID") != null ? tempp.get("CUSTOMER_ID").toString() : null);
                            custRes.setCcu(tempp.get("CCU") != null ? tempp.get("CCU").toString() : null);
                            custRes.setCustCode(tempp.get("CUSTCODE") != null ? tempp.get("CUSTCODE").toString() : null);


                            custRes.setIsBalckListed(tempp.get("BLACK") != null ? tempp.get("BLACK").toString() : null);

                            ContactLegal contactLegal = new ContactLegal();
                            contactLegal.setCinContactLegal(tempp.get("CINCONTACTLEGAL") != null ? tempp.get("CINCONTACTLEGAL").toString() : null);
                            contactLegal.setNomContactLegal(tempp.get("NOMCONTACTLEGAL") != null ? tempp.get("NOMCONTACTLEGAL").toString() : null);
                            contactLegal.setTellContactLegal(tempp.get("TELLCONTACTLEGAL") != null ? tempp.get("TELLCONTACTLEGAL").toString() : null);
                            custRes.setContactLegal(contactLegal);


                            Category category = new Category();
                            category.setLabel(tempp.get("CATEGORIECLIENT") != null ? tempp.get("CATEGORIECLIENT").toString() : null);
                            category.setId(tempp.get("PRGCODE") != null ? tempp.get("PRGCODE").toString() : null);
                            category.setCode(tempp.get("PRGCODE") != null ? tempp.get("PRGCODE").toString() : null);
                            category.setSegmentCode(tempp.get("segmentCode") != null ? tempp.get("segmentCode").toString() : null);
                            category.setSegmentLabel(tempp.get("segmentLabel") != null ? tempp.get("segmentLabel").toString() : null);
                            custRes.setCategory(category);


                            Agency agency = new Agency();
                            agency.setName(tempp.get("AGENCECLIENT") != null ? tempp.get("AGENCECLIENT").toString() : null);
                            agency.setId(tempp.get("COSTID") != null ? tempp.get("COSTID").toString() : null);
                            custRes.setAgency(agency);

                            custRes.setFlagNomPrenom(
                                    (((custRes.getNom() == null && cust.getLASTNAME() == null) || (custRes.getNom() != null && custRes.getNom().toString().equals(cust.getLASTNAME())))
                                            && ((custRes.getPrenom() == null && cust.getFIRST_NAME() == null) || (custRes.getPrenom() != null && custRes.getPrenom().toString().equals(cust.getFIRST_NAME())))));


                        }
                        if (tempp.get("CCCONTRACT") != null) {
                            Role role = new Role();
                            role.setRole("C");
                            roles.add(role);
                        }
                        if (tempp.get("CCSHIP") != null) {
                            Role role = new Role();
                            role.setRole("S");
                            roles.add(role);
                        }
                        if (tempp.get("CCMAGAZINE") != null) {
                            Role role = new Role();
                            role.setRole("P");
                            roles.add(role);
                        }
                        if (tempp.get("CCDIRECTORY") != null) {
                            Role role = new Role();
                            role.setRole("D");
                            roles.add(role);
                        }
                        if (tempp.get("CCBILL_PREVIOUS") != null) {
                            Role role = new Role();
                            role.setRole("R");
                            roles.add(role);
                        }
                        if (tempp.get("CCBILLTEMP") != null) {
                            Role role = new Role();
                            role.setRole("E");
                            roles.add(role);
                        }
                        add.setRoles(roles);
                        addresses.add(add);
                    }
                    custRes.setAddresses(addresses);
                    list.add(custRes);


                } else if (listt == null || listt.size() == 0) {
                    return list;
                }
            } else if ((!StringUtil.isEmpty(cust.getCCU()) || !StringUtil.isEmpty(cust.getPASSPORTNO())
                    || !StringUtil.isEmpty(cust.getID_TYPE()) || !StringUtil.isEmpty(cust.getFIRST_NAME())
                    || !StringUtil.isEmpty(cust.getLASTNAME())) &&
                    !StringUtil.isEmpty(cust.getMSISDN())) {

                list = new ArrayList<CustomerResponse>();
                sql = CustomSQLUtil.get(GET_INFO_CLIENT_BY_CUSTCODE);
                tags = new ArrayList<String>(0);
                tags.add("ND");

                try {
                    sql = GRCStringUtil.formatQuery(sql, tags);
                } catch (final Exception e) {
                    throw new FunctionnalException(ExceptionCodeTypeEnum.QUERY_SYNTAX_ERROR_WS);
                }


                namedParameters = new HashMap();


                namedParameters.put("dnNUm", cust.getMSISDN());


                listt = super.getNamedParameterJdbcTemplate().queryForList(sql, namedParameters);

                custRes = new CustomerResponse();
                if (listt != null && listt.size() > 0) {
                    Map<String, Object> tempp = null;

                    List<Address> addresses = new ArrayList<Address>();
                    String custid = null;
                    for (int j = 0; j < listt.size(); j++) {
                        tempp = listt.get(j);
                        if (custid != null) {
                            if (!tempp.get("CUSTOMER_ID").toString().equals(custid)) {
                                if (custRes.getIsIdentified().equals("0"))
                                    list.add(custRes);
                                custRes = new CustomerResponse();
                                custid = tempp.get("CUSTOMER_ID").toString();

                            }
                        } else {
                            custid = tempp.get("CUSTOMER_ID").toString();
                        }
                        custRes.setBillingAccountId(tempp.get("BILLING_ACCOUNT_ID") != null ? tempp.get("BILLING_ACCOUNT_ID").toString() : null);
                        custRes.setCcseq(tempp.get("CCSEQ") != null ? tempp.get("CCSEQ").toString() : null);
                        custRes.setCcsex(tempp.get("CCSEX") != null ? tempp.get("CCSEX").toString() : null);
                        Address add = new Address();
                        tempp = listt.get(j);
                        add.setTitre(tempp.get("CCTITLE") != null ? tempp.get("CCTITLE").toString() : null);
                        add.setEmail(tempp.get("EMAIL") != null ? tempp.get("EMAIL").toString() : null);
                        add.setNom(tempp.get("NOM") != null ? tempp.get("NOM").toString() : null);
                        add.setPrenom(tempp.get("PRENOM") != null ? tempp.get("PRENOM").toString() : null);


                        Country country = new Country();
                        country.setIso(tempp.get("ISO") != null ? tempp.get("ISO").toString() : null);
                        country.setName(tempp.get("PAYS") != null ? tempp.get("PAYS").toString() : null);
                        add.setCountry(country);


                        City city = new City();
                        city.setNom(tempp.get("VILLE") != null ? tempp.get("VILLE").toString() : null);
                        city.setZip(tempp.get("CCZIP") != null ? tempp.get("CCZIP").toString() : null);
                        add.setCity(city);

                        add.setRue(tempp.get("ADRESSE") != null ? tempp.get("ADRESSE").toString() : null);


                        add.setTypeIdentifiant(tempp.get("TYPEIDENTIFIANT") != null ? tempp.get("TYPEIDENTIFIANT").toString() : null);
                        add.setIdentifiant(tempp.get("IDENTIFIANT") != null ? tempp.get("IDENTIFIANT").toString() : null);
                        List<Role> roles = new ArrayList<Role>();
                        if (tempp.get("CCUSER") != null) {
                            Role role = new Role();
                            role.setRole("U");
                            roles.add(role);
                        }
                        if (tempp.get("CCBILL") != null) {
                            Role role = new Role();
                            role.setRole("B");
                            roles.add(role);

                            custRes.setDnStatus(tempp.get("DN_STATUS") != null ? tempp.get("DN_STATUS").toString() : null);

                            custRes.setIsIdentified((tempp.get("PRENOM") == null || tempp.get("PRENOM").toString().toLowerCase().startsWith("anonyme")
                                    || tempp.get("NOM") == null || tempp.get("NOM").toString().toLowerCase().startsWith("anonyme")
                                    || tempp.get("IDENTIFIANT") == null || tempp.get("IDENTIFIANT").equals("")) ? "0" : "1");

                            custRes.setDnStatus(tempp.get("DN_STATUS") != null ? tempp.get("DN_STATUS").toString() : null);


                            custRes.setIsPaymentResponsable(tempp.get("PAYMNTRESP") != null ? tempp.get("PAYMNTRESP").toString() : null);
                            custRes.setCustomerID(tempp.get("CUSTOMER_ID") != null ? tempp.get("CUSTOMER_ID").toString() : null);
                            custRes.setCcu(tempp.get("CCU") != null ? tempp.get("CCU").toString() : null);
                            custRes.setCustCode(tempp.get("CUSTCODE") != null ? tempp.get("CUSTCODE").toString() : null);
                            custRes.setIsBalckListed(tempp.get("BLACK") != null ? tempp.get("BLACK").toString() : null);

                            ContactLegal contactLegal = new ContactLegal();
                            contactLegal.setCinContactLegal(tempp.get("CINCONTACTLEGAL") != null ? tempp.get("CINCONTACTLEGAL").toString() : null);
                            contactLegal.setNomContactLegal(tempp.get("NOMCONTACTLEGAL") != null ? tempp.get("NOMCONTACTLEGAL").toString() : null);
                            contactLegal.setTellContactLegal(tempp.get("TELLCONTACTLEGAL") != null ? tempp.get("TELLCONTACTLEGAL").toString() : null);
                            custRes.setContactLegal(contactLegal);


                            Category category = new Category();
                            category.setLabel(tempp.get("CATEGORIECLIENT") != null ? tempp.get("CATEGORIECLIENT").toString() : null);
                            category.setId(tempp.get("PRGCODE") != null ? tempp.get("PRGCODE").toString() : null);
                            category.setCode(tempp.get("PRGCODE") != null ? tempp.get("PRGCODE").toString() : null);
                            category.setSegmentCode(tempp.get("segmentCode") != null ? tempp.get("segmentCode").toString() : null);
                            category.setSegmentLabel(tempp.get("segmentLabel") != null ? tempp.get("segmentLabel").toString() : null);
                            custRes.setCategory(category);


                            Agency agency = new Agency();
                            agency.setName(tempp.get("AGENCECLIENT") != null ? tempp.get("AGENCECLIENT").toString() : null);
                            agency.setId(tempp.get("COSTID") != null ? tempp.get("COSTID").toString() : null);
                            custRes.setAgency(agency);

                            custRes.setFlagNomPrenom(
                                    (((custRes.getNom() == null && cust.getLASTNAME() == null) || (custRes.getNom() != null && custRes.getNom().toString().equals(cust.getLASTNAME())))
                                            && ((custRes.getPrenom() == null && cust.getFIRST_NAME() == null) || (custRes.getPrenom() != null && custRes.getPrenom().toString().equals(cust.getFIRST_NAME())))));


                        }
                        if (tempp.get("CCCONTRACT") != null) {
                            Role role = new Role();
                            role.setRole("C");
                            roles.add(role);
                        }
                        if (tempp.get("CCSHIP") != null) {
                            Role role = new Role();
                            role.setRole("S");
                            roles.add(role);
                        }
                        if (tempp.get("CCMAGAZINE") != null) {
                            Role role = new Role();
                            role.setRole("P");
                            roles.add(role);
                        }
                        if (tempp.get("CCDIRECTORY") != null) {
                            Role role = new Role();
                            role.setRole("D");
                            roles.add(role);
                        }
                        if (tempp.get("CCBILL_PREVIOUS") != null) {
                            Role role = new Role();
                            role.setRole("R");
                            roles.add(role);
                        }
                        if (tempp.get("CCBILLTEMP") != null) {
                            Role role = new Role();
                            role.setRole("E");
                            roles.add(role);
                        }
                        add.setRoles(roles);
                        addresses.add(add);
                    }
                    custRes.setAddresses(addresses);

                    if (custRes.getIsIdentified().equals("0"))
                        list.add(custRes);

                } else if (listt == null || listt.size() == 0) {
                    return list;
                }
            }
            if (listt == null || listt.size() == 0) {
                return list;
            }


            LOG.debug("<-- getCustomerInfo");
        } catch (EmptyResultDataAccessException e) {

            LOG.debug("getCustomerInfo is  null for custCode = " + cust.getCUSTCODE());
            return null;
        }


        return list;

    }

    public void insertCMS(String coID, String custID, String ND, String cmdData2) {
        String vXMLData;
        String originSuffix="VERIFSMS";
        LOG.debug("--> Start insertCMS");
        vXMLData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<customer xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../change.contract.status.xsd\">\n"
                + "<contract>\n" + "<id type=\"msisdn\">" + ND + "</id>\n" + "<cmd>" + cmdData2 + "</cmd>\n"
                + "</contract>\n" + "</customer>";

        try {

            final String sql = CustomSQLUtil.get(GET_CMS_INT_SEQ);
            Map<String, String> namedParameters = new HashMap();

            long seqCMS = super.getNamedParameterJdbcTemplate().queryForObject(sql, namedParameters, Long.class);

            if (seqCMS > 0) {

                namedParameters.put("seqCMS", String.valueOf(seqCMS));
                namedParameters.put("custID", custID);
                namedParameters.put("vXMLData", vXMLData);
                namedParameters.put("coID", coID);
                namedParameters.put("origin", CustomerEditDaoImpl.ORIGIN_PREFIX+originSuffix);
                namedParameters.put("customerId", custID);
                final String sql2 = CustomSQLUtil.get(INSERT_CMD_CMS);

                super.getNamedParameterJdbcTemplate().update(sql2, namedParameters);

            } else
                LOG.error("Erreur d'insertion de la commande CMS");

            LOG.debug("--> End insertCMS");
        } catch (EmptyResultDataAccessException e) {
            LOG.error("Erreur d'insertion de la commande CMS");
        }
    }


    public List<CustFindContractWS> findContract(String custcode,
                                                 String customerId, String nd, String coId) {
        List<CustFindContractWS> listContracts = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("--> findContract: ");
        }
        final StringBuilder sqlBuffer = new StringBuilder(
                CustomSQLUtil.get(GET_INFO_CONTRACT));

        if (!GRCStringUtil.isNullOrEmpty(custcode)) {
            sqlBuffer.append(" and CU.CUSTCODE = '" + custcode + "'");
        }
        if (!GRCStringUtil.isNullOrEmpty(coId)) {
            sqlBuffer.append(" and ca.CO_ID= " + coId);
        }

        if (!GRCStringUtil.isNullOrEmpty(customerId)) {
            sqlBuffer.append(" and ca.CUSTOMER_ID=" + customerId);
        }

        if (!GRCStringUtil.isNullOrEmpty(nd)) {
            sqlBuffer.append(" and dn.DN_NUM= '" + nd + "'");
        }

        final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
                CustFindContractWS.class);
        listContracts = super.getNamedParameterJdbcTemplate()
                .getJdbcOperations().query(sqlBuffer.toString(), rowMapper);

        if (LOG.isDebugEnabled())
            LOG.debug("<-- findContract: " + listContracts);

        return listContracts;
    }

    public List<CustFindContractWS> findChildContract(
            List<CustFindContractWS> list) {
        List<CustFindContractWS> listContracts = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("--> findContract: ");
        }
        final StringBuilder sqlBuffer = new StringBuilder(
                CustomSQLUtil.get(GET_INFO_CONTRACT));

        if (list != null && !list.isEmpty()) {

            sqlBuffer.append(" and cu.CUSTOMER_ID_HIGH in (");
            for (int i = 0; i < list.size(); i++) {
                sqlBuffer.append(list.get(i).getCustomerId());
                if (i != list.size() - 1) {
                    sqlBuffer.append(" , ");
                }
            }
            sqlBuffer.append(" ) ");
            final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
                    CustFindContractWS.class);
            listContracts = super.getNamedParameterJdbcTemplate()
                    .getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
        }

        if (LOG.isDebugEnabled())
            LOG.debug("<-- findContract: " + listContracts);

        return listContracts;
    }

    public List<Map<String, Object>> findChildCustomer(List<String> list) {
        List<Map<String, Object>> listContracts = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("--> findContract: ");
        }
        final StringBuilder sqlBuffer = new StringBuilder(
                CustomSQLUtil.get(GET_CUSTOMERS));

        if (list != null && !list.isEmpty()) {

            sqlBuffer.append("  CUSTOMER_ID_HIGH in (");
            for (int i = 0; i < list.size(); i++) {
                sqlBuffer.append(list.get(i));
                if (i != list.size() - 1) {
                    sqlBuffer.append(" , ");
                }
            }
            sqlBuffer.append(" ) ");
            listContracts = super.getNamedParameterJdbcTemplate()
                    .getJdbcOperations().queryForList(sqlBuffer.toString());
        }

        if (LOG.isDebugEnabled())
            LOG.debug("<-- findChildCustomer: " + listContracts);

        return listContracts;
    }

    public List<CustFindContractWS> findContract(List<String> listCustomer,
                                                 String nd, String coId) {
        List<CustFindContractWS> listContracts = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("--> findContract: ");
        }
        final StringBuilder sqlBuffer = new StringBuilder(
                CustomSQLUtil.get(GET_INFO_CONTRACT));

        if (!GRCStringUtil.isNullOrEmpty(coId)) {
            sqlBuffer.append(" and ca.CO_ID= " + coId);
        }

        if (!GRCStringUtil.isNullOrEmpty(nd)) {
            sqlBuffer.append(" and dn.DN_NUM= '" + nd + "'");
        }

        if (listCustomer != null && !listCustomer.isEmpty()) {

            sqlBuffer.append(" and cu.CUSTOMER_ID in (");
            for (int i = 0; i < listCustomer.size(); i++) {
                sqlBuffer.append(listCustomer.get(i));
                if (i != listCustomer.size() - 1) {
                    sqlBuffer.append(" , ");
                }
            }
            sqlBuffer.append(" ) ");

            final CustomBeanPropertyRowMapper<CustFindContractWS> rowMapper = new CustomBeanPropertyRowMapper<CustFindContractWS>(
                    CustFindContractWS.class);
            listContracts = super.getNamedParameterJdbcTemplate()
                    .getJdbcOperations().query(sqlBuffer.toString(), rowMapper);
        }

        if (LOG.isDebugEnabled())
            LOG.debug("<-- findContract: " + listContracts);

        return listContracts;
    }

    public String getCustomerByCustCode(String custcode) {
        if (LOG.isDebugEnabled())
            LOG.debug("--> getCustomerByCustCode: custCode=" + custcode);

        final StringBuilder sqlBuffer = new StringBuilder(
                GET_CUSTOMER_BY_CUSTCODE);
        sqlBuffer.append(" custcode='" + custcode + "'");
        Object result = null;
        try {
            result = super.getNamedParameterJdbcTemplate().getJdbcOperations()
                    .queryForObject(sqlBuffer.toString(), Object.class);
        } catch (EmptyResultDataAccessException e) {
            LOG.error("<-- getCustomerByCustCode" + e.getMessage());
        }
        LOG.debug("<-- getCustomerByCustCode");
        return String.valueOf(result);
    }

    public List<CustSearchWS> searchCustomers(SearchCustomerWS searchCustomerWS) {

        List<CustSearchWS> listCustomers = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("--> searchCustomers: " + searchCustomerWS.toString());
        }
        final StringBuilder sqlBuffer = new StringBuilder(
                CustomSQLUtil.get(GET_INFO_CUSTOMER));

        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCo_id())) {
            sqlBuffer.append(" and ca.CO_ID = " + searchCustomerWS.getCo_id());
        }
        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNd())) {
            sqlBuffer.append(" and dn.DN_NUM= '" + searchCustomerWS.getNd()
                    + "'");
        }

        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getNom())) {
            sqlBuffer.append(" and upper( cca.CCLNAME ) ='" + searchCustomerWS.getNom().toUpperCase()
                    + "'");
        }

        if (!GRCStringUtil
                .isNullOrEmpty(searchCustomerWS.getNumPieceIdentite())) {
            sqlBuffer.append(" and upper (cca.PASSPORTNO )= '"
                    + searchCustomerWS.getNumPieceIdentite().toUpperCase() + "'");
        }
        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getPrenom())) {
            sqlBuffer.append(" and upper (cca.CCFNAME) ='"
                    + searchCustomerWS.getPrenom().toUpperCase() + "'");
        }
        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getVille())) {
            sqlBuffer.append(" and cca.CCCITY='" + searchCustomerWS.getVille()
                    + "'");
        }if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getIdType())) {
            sqlBuffer.append(" and cca.ID_TYPE=" + searchCustomerWS.getIdType());
        }
        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCodeCategorie())) {
            sqlBuffer.append(" and pga.PRGCODE= '"
                    + searchCustomerWS.getCodeCategorie() + "'");
        }

        if (!GRCStringUtil.isNullOrEmpty(searchCustomerWS.getCustCode())) {
            sqlBuffer.append(" and cust.CUSTCODE= '"
                    + searchCustomerWS.getCustCode() + "'");
        }

        sqlBuffer.append(" and rownum <= 200");
        final CustomBeanPropertyRowMapper<CustSearchWS> rowMapper = new CustomBeanPropertyRowMapper<CustSearchWS>(
                CustSearchWS.class);
        listCustomers = super.getNamedParameterJdbcTemplate()
                .getJdbcOperations().query(sqlBuffer.toString(), rowMapper);

        if (LOG.isDebugEnabled())
            LOG.debug("<-- searchCustomers: " + listCustomers);

        return listCustomers;

	}

	@Override
	public List<Map<String, Object>> loadDependecies() {
		String sql  = CustomSQLUtil.get(LISTE_DEPENDENCIES);
		List<Map<String, Object>> list=  super.getNamedParameterJdbcTemplate().getJdbcOperations().queryForList(sql);
		return list;
	}


}
