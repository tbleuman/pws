package ma.iam.mobile.customer.bean;

import java.io.Serializable;

public class DirectoryNumber implements Serializable {

	private String DN_NUM;
	private String DN_ID;
	private String DN_TYPE;
	private String SM_SERIALNUM;
	private String HLCODE;
	private String DN_STATUS;
	private String BOOKING_TIME;
	private String BOOKING_EXPIRATION_DATE;
	
	/**
	 * @return the bOOKING_EXPIRATION_DATE
	 */
	public String getBOOKING_EXPIRATION_DATE() {
		return BOOKING_EXPIRATION_DATE;
	}
	/**
	 * @param bOOKING_EXPIRATION_DATE the bOOKING_EXPIRATION_DATE to set
	 */
	public void setBOOKING_EXPIRATION_DATE(String bOOKING_EXPIRATION_DATE) {
		BOOKING_EXPIRATION_DATE = bOOKING_EXPIRATION_DATE;
	}
	/**
	 * @return the dN_STATUS
	 */
	public String getDN_STATUS() {
		return DN_STATUS;
	}
	/**
	 * @param dN_STATUS the dN_STATUS to set
	 */
	public void setDN_STATUS(String dN_STATUS) {
		DN_STATUS = dN_STATUS;
	}
	/**
	 * @return the dN_NUM
	 */
	public String getDN_NUM() {
		return DN_NUM;
	}
	/**
	 * @param dN_NUM the dN_NUM to set
	 */
	public void setDN_NUM(String dN_NUM) {
		DN_NUM = dN_NUM;
	}
	/**
	 * @return the dN_TYPE
	 */
	public String getDN_TYPE() {
		return DN_TYPE;
	}
	/**
	 * @param dN_TYPE the dN_TYPE to set
	 */
	public void setDN_TYPE(String dN_TYPE) {
		DN_TYPE = dN_TYPE;
	}
	/**
	 * @return the sM_SERIALNUM
	 */
	public String getSM_SERIALNUM() {
		return SM_SERIALNUM;
	}
	/**
	 * @param sM_SERIALNUM the sM_SERIALNUM to set
	 */
	public void setSM_SERIALNUM(String sM_SERIALNUM) {
		SM_SERIALNUM = sM_SERIALNUM;
	}
	/**
	 * @return the hLCODE
	 */
	public String getHLCODE() {
		return HLCODE;
	}
	/**
	 * @param hLCODE the hLCODE to set
	 */
	public void setHLCODE(String hLCODE) {
		HLCODE = hLCODE;
	}
	/**
	 * @return the bOOKING_TIME
	 */
	public String getBOOKING_TIME() {
		return BOOKING_TIME;
	}
	/**
	 * @param bOOKING_TIME the bOOKING_TIME to set
	 */
	public void setBOOKING_TIME(String bOOKING_TIME) {
		BOOKING_TIME = bOOKING_TIME;
	}
	public String getDN_ID() {
		return DN_ID;
	}
	public void setDN_ID(String dN_ID) {
		DN_ID = dN_ID;
	}
	
	
}
