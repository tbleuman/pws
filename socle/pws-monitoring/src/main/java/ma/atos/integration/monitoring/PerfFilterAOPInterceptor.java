package ma.atos.integration.monitoring;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class PerfFilterAOPInterceptor implements MethodInterceptor {
	Logger logger = LogManager.getLogger(PerfFilterAOPInterceptor.class
			.getName());

	private static ConcurrentHashMap<String, MethodStats> methodStats = new ConcurrentHashMap<String, MethodStats>();
	private long statLogFrequency = 1L;
	private long methodWarningThreshold = 1000L;
	private boolean enabled = false;

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public long getStatLogFrequency() {
		return this.statLogFrequency;
	}

	public void setStatLogFrequency(long statLogFrequency) {
		this.statLogFrequency = statLogFrequency;
	}

	public long getMethodWarningThreshold() {
		return this.methodWarningThreshold;
	}

	public void setMethodWarningThreshold(long methodWarningThreshold) {
		this.methodWarningThreshold = methodWarningThreshold;
	}

	public Object invoke(MethodInvocation method) throws Throwable {
		long end;
		Object[] args;
		HttpServletRequest httpServletRequest;
		long start = System.nanoTime();
		try {
			return method.proceed();
		} finally {
			end = System.nanoTime();
			if (this.enabled) {
				args = method.getArguments();
				if ((args.length > 0)
						&& (args[0] instanceof HttpServletRequest)) {
					httpServletRequest = (HttpServletRequest) args[0];
					updateStats(httpServletRequest.getPathInfo(), end - start);
				} else {
					updateStats(method.getMethod().getName(), end - start);
				}
			}
		}
	}

	public String getMethodStats() {
		String ret = "";
		for (String methodName : methodStats.keySet()) {
			MethodStats stats = (MethodStats) methodStats.get(methodName);
			long avgTime = stats.totalTime / stats.count;
			long runningAvg = (stats.totalTime - stats.lastTotalTime)
					/ this.statLogFrequency;
			ret = ret + "method: " + methodName + "(), cnt = " + stats.count
					+ ", lastTime = " + stats.elapsedTime + ", avgTime = "
					+ avgTime + ", runningAvg = " + runningAvg + ", maxTime = "
					+ stats.maxTime + "\n";
		}
		return ret;
	}

	private void updateStats(String methodName, long elapsedTime) {
		MethodStats stats = (MethodStats) methodStats.get(methodName);
		if (stats == null) {
			stats = new MethodStats(methodName);
			methodStats.put(methodName, stats);
		}
		stats.count += 1L;
		stats.elapsedTime = elapsedTime;
		stats.totalTime += elapsedTime;
		if (elapsedTime > stats.maxTime) {
			stats.maxTime = elapsedTime;
		}

		if (elapsedTime > this.methodWarningThreshold) {
			this.logger.warn("method warning: for methodWarningThreshold= "
					+ this.methodWarningThreshold + " is " + methodName
					+ "(), cnt = " + stats.count + ", lastTime = "
					+ elapsedTime + ", maxTime = " + stats.maxTime);

		}

		if (stats.count % this.statLogFrequency == 0L) {
			long avgTime = stats.totalTime / stats.count;
			long runningAvg = (stats.totalTime - stats.lastTotalTime)
					/ this.statLogFrequency;
			this.logger.debug(methodName + ": cnt = " + stats.count
					+ ", lastTime = " + elapsedTime + ", avgTime = " + avgTime
					+ ", runningAvg = " + runningAvg + ", maxTime = "
					+ stats.maxTime);

		}
	}

	class MethodStats {
		public String methodName;
		public long count;
		public long totalTime;
		public long lastTotalTime;
		public long maxTime;
		public long elapsedTime;

		public MethodStats(String paramString) {
			this.methodName = paramString;
		}
	}
}
