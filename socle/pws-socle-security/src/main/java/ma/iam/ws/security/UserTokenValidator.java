package ma.iam.ws.security;

import ma.iam.mutualisation.fwk.common.exception.FunctionalException;
import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.mutualisation.fwk.web.context.ContextManager;
import ma.iam.services.authentication.AuthenticationManager;
import ma.iam.services.authentication.Constants;

import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.common.ext.WSSecurityException.ErrorCode;
import org.apache.wss4j.dom.handler.RequestData;
import org.apache.wss4j.dom.message.token.UsernameToken;
import org.apache.wss4j.dom.validate.Credential;
import org.apache.wss4j.dom.validate.Validator;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * This class represent .
 * <br/><br/>
 * Created on Sprint #
 * 
 * @author mei
 *
 */
public class UserTokenValidator implements Validator {
	
	/** Instance du LOGGER */
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * (non javadoc)
	 * See @see org.apache.ws.security.validate.Validator#validate(org.apache.ws.security.validate.Credential, org.apache.ws.security.handler.RequestData).
	 * @param credential
	 * @param requestData
	 * @return
	 * @throws WSSecurityException
	 */
	public Credential validate(Credential credential, RequestData requestData)
			throws WSSecurityException {
		
		AuthenticationManager authenticationManager = (AuthenticationManager) ContextManager.getInstance().getBean(Constants.BEAN_AUTHENTICATION_MANAGER);;
		UserCache userCache = (UserCache) ContextManager.getInstance().getBean(Constants.BEAN_USER_CACHE);
		UsernameToken userToken =  credential.getUsernametoken();

		String password = userToken.getPassword();
		String username = userToken.getName();
		UserDetails user = userCache.getUserFromCache(username);
		
		boolean authenticationStatus = false;
		if (user != null && user.getPassword() != null) {
			if (password.equals(user.getPassword())) {
				authenticationStatus = true;
			}
		} 
		if (!authenticationStatus) {
			try {
				user = authenticationManager.authenticate(username, password);
				userCache.putUserInCache(user);
				
			} catch (FunctionalException e) {
				LOGGER.error(getClass(), "validate", e.getMessage());
				userCache.removeUserFromCache(username);
				throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION ,e.getMessage());
			} catch (Exception e) {
				LOGGER.error(getClass(), "validate", e.getMessage(), e);
				userCache.removeUserFromCache(username);
				throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION ,e.getMessage());
			}
		}
		return credential;
	}
}
