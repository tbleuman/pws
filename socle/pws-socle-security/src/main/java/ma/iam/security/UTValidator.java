package ma.iam.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;
import ma.iam.security.interceptor.AuthorizationInterceptor;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.common.ext.WSSecurityException.ErrorCode;
import org.apache.wss4j.dom.handler.RequestData;
import org.apache.wss4j.dom.message.token.UsernameToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class UTValidator extends
		org.apache.wss4j.dom.validate.UsernameTokenValidator {

	private static final Logger LOGGER = LogManager.getLogger(UTValidator.class
			.getName());

	public static final ShaPasswordEncoder ENCODER = new ShaPasswordEncoder();

	private Properties roles;

	private Properties logins;

	public void setLogins(Properties logins) {
		this.logins = logins;
	}

	private AuthorizationInterceptor authorizationInterceptor;

	public void setRoles(Properties roles) {
		this.roles = roles;
	}

	@Override
	protected void verifyPlaintextPassword(UsernameToken usernameToken,
			RequestData data) throws WSSecurityException {
		String login = usernameToken.getName();
		String password = usernameToken.getPassword();

		SoapMessage msg = (SoapMessage) data.getMsgContext();
		HttpServletRequest request = (HttpServletRequest) msg
				.get(AbstractHTTPDestination.HTTP_REQUEST);

		LOGGER.info("Module d'audit d'accees: tentative de " + login + " @ip :"
				+ request.getRemoteAddr());

		if (logins == null) {
			LOGGER.error("Module d'audit d'accees: failure de " + login
					+ " @ip :" + request.getRemoteAddr() + " Logins Not Loaded");
			// throw new WSSecurityException("Logins Not Loaded");
			throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION,
					"Logins Not Loaded");
		}
		if (roles == null) {
			LOGGER.error("Module d'audit d'accees: failure de " + login
					+ " @ip :" + request.getRemoteAddr() + " Roles Not Loaded");
			// throw new WSSecurityException("Roles Not Loaded");
			throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION,
					"Roles Not Loaded");
		}
		if (logins.getProperty(login) == null) {
			LOGGER.error("Module d'audit d'accees: failure de " + login
					+ " @ip :" + request.getRemoteAddr()
					+ " Username Not Found");
			throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION,
					"Username Not Found");
		}
		String storedPassword = logins.getProperty(login);
		String digestedPassword = ENCODER.encodePassword(password, null);

		if (roles.getProperty(login) == null) {
			LOGGER.error("Module d'audit d'accees: failure de " + login
					+ " @ip :" + request.getRemoteAddr() + " Role Not Found");

			throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION,
					"Role Not Found");
		}
		if (!digestedPassword.equals(storedPassword)) {
			LOGGER.error("Module d'audit d'accees: failure de " + login
					+ " @ip :" + request.getRemoteAddr()
					+ " Password not Equal");
			throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION,
					"Password not Equal");
		}

		Map<String, String> usersRoles = new HashMap<String, String>();
		usersRoles.put(login, roles.getProperty(login));
		authorizationInterceptor.setUserRolesMap(usersRoles);

		LOGGER.info("Module d'audit d'accees: success de " + login + " @ip :"
				+ request.getRemoteAddr());
	}

	@Override
	protected void verifyDigestPassword(UsernameToken usernameToken,
			RequestData data) throws WSSecurityException {
		super.verifyDigestPassword(usernameToken, data);
		String password = usernameToken.getPassword();
		if (!password.equals("abcdef"))
			throw new WSSecurityException(ErrorCode.FAILED_AUTHENTICATION);

	}

	public AuthorizationInterceptor getAuthorizationInterceptor() {
		return authorizationInterceptor;
	}

	public void setAuthorizationInterceptor(
			AuthorizationInterceptor authorizationInterceptor) {
		this.authorizationInterceptor = authorizationInterceptor;
	}

}
