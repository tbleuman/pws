package ma.iam.mutualisation.fwk.common.exception;

import ma.iam.mutualisation.fwk.common.Constants;
import ma.iam.mutualisation.fwk.common.util.MessageUtils;


/**
 * The Class FunctionalException.
 * 
 * @author Atos Origin
 * @version 1.0.0
 */
public class FunctionalException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7981637821409421377L;
	
	private int type = Constants.ERROR_CODE;
	
	private String messageCode;
	
	
	/**
	 * 
	 * @param type
	 * @param messageKey
	 * @param params
	 */
	public FunctionalException(int type, String messageKey, Object[] params) {
		super(MessageUtils.getMessage(messageKey, params));
		this.type = type;
	}

	/**
	 * 
	 * @param type
	 * @param messageKey
	 */
	public FunctionalException(int type, String messageKey) {
		super(MessageUtils.getMessage(messageKey));
		this.type = type;
	}

	/**
	 * Instantiates a new functional exception.
	 *
	 * @param messageKey the message key
	 * @param params the params
	 */
	public FunctionalException(String messageKey, Object[] params) {
		super(MessageUtils.getMessage(messageKey, params));
		this.messageCode = messageKey;
	}
	
	/**
	 * Instantiates a new functional exception.
	 * @param messageKey
	 *            the message key
	 */
	public FunctionalException(String messageKey) {
		super(MessageUtils.getMessage(messageKey));
		this.messageCode = messageKey;
	}
	
	/**
	 * Instantiates a new functional exception.
	 *
	 * @param messageCode
	 * @param messageKey the message key
	 * @param params the params
	 */
	public FunctionalException(String messageCode, String messageKey, Object[] params) {
		super(MessageUtils.getMessage(messageKey, params));
		this.messageCode = messageCode;
	}
	
	/**
	 * Instantiates a new functional exception.
	 * @param messageCode
	 * @param messageKey
	 *            the message key
	 */
	public FunctionalException(String messageCode, String messageKey) {
		super(MessageUtils.getMessage(messageKey));
		this.messageCode = messageCode;
	}	

	/**
	 * The getter method for the field type.
	 * @return the type.
	 */
	public int getCode() {
		return type;
	}

	/**
	 * The getter method for the field messageKey.
	 * @return the errorNum.
	 */
	public String getMessageCode() {
		return messageCode;
	}

}
