package ma.iam.mutualisation.fwk.web.context;


import javax.servlet.ServletContextEvent;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.springframework.web.context.ContextLoader;

public class ContextLoaderListener extends org.springframework.web.context.ContextLoaderListener {

	/**
	 * Méthode surchargée pour intialiser GEContextManager See @see
	 * org.springframework
	 * .web.context.ContextLoaderListener#contextInitialized(javax
	 * .servlet.ServletContextEvent).
	 * 
	 * @param event
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		try {
			ContextManager.init(ContextLoader.getCurrentWebApplicationContext());
		} catch (TechnicalException e) {

			throw new IllegalStateException(e.getMessage(), e);
		}
	}

}
