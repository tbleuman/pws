package ma.iam.mutualisation.fwk.security.wss4j;

import java.util.Map;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;

/**
 * Performs WS-Security inbound actions.
 * 
 * @author <a href="mailto:tsztelak@gmail.com">Tomasz Sztelak</a>
 */
public class WSS4JInInterceptor extends org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor {
	public WSS4JInInterceptor(Map<String, Object> properties) {
		super(properties);
		System.out.println("WSS4JInInterceptor extends org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor");
	}
	
	@Override
	public void handleMessage(SoapMessage arg0) throws Fault {
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		
	}

}