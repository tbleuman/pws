package ma.iam.mutualisation.fwk.web.context;

import ma.iam.mutualisation.fwk.common.exception.TechnicalException;

import org.springframework.context.ApplicationContext;

public class ContextManager {

	private static ContextManager instance = null;
	private ApplicationContext context;

	private ContextManager(ApplicationContext pApplicationContext) {
		this.context = pApplicationContext;
	}

	public static ContextManager getInstance() {
		if (instance == null) {
			throw new IllegalStateException("Context non initialisé !");
		}
		return instance;
	}

	public Object getBean(String beanName) {
		return this.context.getBean(beanName);
	}

	public static synchronized void init(ApplicationContext applicationContext) throws TechnicalException {
		if (instance == null) {
			instance = new ContextManager(applicationContext);
		} else {
			throw new TechnicalException("context.deja.initialise");
		}

	}
}
