package ma.iam.mutualisation.fwk.cms.service;

import java.util.HashMap;
import java.util.Map;

import com.alcatel.ccl.client.CMSException;



/**
 * The Interface Connecteur.
 * 
 * @author Atos Origin
 * @version 1.0
 */
public interface ISoiService {

	@SuppressWarnings("unchecked")
	void login(String login, String pwd) throws CMSException;

	Map executeCommand(String command, Map inputParameters)
			throws CMSException;

}
