package ma.iam.mutualisation.fwk.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

import ma.iam.mutualisation.fwk.common.logging.LogManager;
import ma.iam.mutualisation.fwk.common.logging.Logger;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class PropertiesUtil extends PropertyPlaceholderConfigurer {

	private static final Logger logger = LogManager.getLogger(PropertiesUtil.class.getName());
	public Map propertiesMap;

	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactory, Properties props)
			throws BeansException {

		super.processProperties(beanFactory, props);

		propertiesMap = new HashMap<String, String>();
		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			propertiesMap.put(
					keyStr,
					parseStringValue(props.getProperty(keyStr), props,
							new HashSet()));
		}
		try {
			logger.info("org.omg.CORBA.ORBInitialHost  : "
					+ getProperty("org.omg.CORBA.ORBInitialHost"));
			logger.info("org.omg.CORBA.ORBInitialPort  : "
					+ getProperty("org.omg.CORBA.ORBInitialPort"));
			logger.info("org.omg.CORBA.ORBInitRef  : "
					+ getProperty("org.omg.CORBA.ORBInitRef"));
			logger.info("org.omg.CORBA.ORBInitRef.NameService  : "
					+ getProperty("org.omg.CORBA.ORBInitRef.NameService"));
			logger.info("soi.cil.name  : " + getProperty("soi.cil.name"));
			logger.info("soi.cil.version  : " + getProperty("soi.cil.version"));
			logger.info("soi.security.name  : "
					+ getProperty("soi.security.name"));
			logger.info("soi.security.version  : "
					+ getProperty("soi.security.version"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Erreur Parsing Orb.properties");
		}

	}

	public Map getPropertiesMap() {
		return propertiesMap;
	}

	public void setPropertiesMap(Map propertiesMap) {
		this.propertiesMap = propertiesMap;
	}

	public String getProperty(String name) {

		return (String) propertiesMap.get(name);
	}
}