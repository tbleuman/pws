package ma.iam.mutualisation.fwk.webservice.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebResult;

public interface WSModeByPasse {

	@WebMethod(operationName = "byPass")
    @WebResult(name = "statut")
	public String modeByPass();
}
